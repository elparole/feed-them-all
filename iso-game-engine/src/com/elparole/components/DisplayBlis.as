package com.elparole.components
{
import com.elparole.feedThemAll.test.IsoAnimItem;

import flash.display.BitmapData;
import flash.geom.Point;

public class DisplayBlis
	{
		private var _bitmap : BitmapData = null;
		public var _renderItem:IsoAnimItem;
		public var skinId:String;
		public var animId:String;
		public var dirId:String;
		public var offset:Point;
		public var type:int = 0;
		public var textureSize:Point = new Point(128,128);
		public var id:int;
		public static var globId:int = 0;

		private var _selected:Boolean;

	/**
	 * todo - refactor to floor3d and item3d
	 * @param skinId
	 * @param animId
	 * @param dirId
	 * @param bitmap
	 * @param offset
	 */

		public function DisplayBlis(skinId:String, animId:String, dirId:String, bitmap:BitmapData,offset:Point)
		{
			this.id = DisplayBlis.globId;
			DisplayBlis.globId++;
			this.skinId = skinId;
			this.animId = animId;
			this.dirId = dirId;
			this._bitmap = bitmap;
			if(offset)
				this.offset = offset.clone();
			else
				this.offset = new Point();

//			if((displayObject as Object).core)
//				(displayObject as Object).core.visible = false;
		}

	public function get bitmap():BitmapData {
		return _bitmap;
	}

	public function set bitmap(value:BitmapData):void {
		_bitmap = value;
	}

	public function setBitmap(bitmap:BitmapData):void{
		_bitmap = bitmap;
		_renderItem.bd = bitmap;
		_renderItem.rect.width = bitmap.width;
		_renderItem.rect.height = bitmap.height;
	}

	public function get renderItem():IsoAnimItem {
		return _renderItem;
	}

	public function set renderItem(renderItem:IsoAnimItem):void {
		_renderItem = renderItem;
	}

	public function setOffset(offset:Point):void {
		this.offset = offset;
		renderItem.offsetPt.x = -offset.x;
		renderItem.offsetPt.y = -offset.y;
	}

	public function get selected():Boolean {
		return _selected;
	}

	public function set selected(value:Boolean):void {
		_selected = value;
		if(renderItem)
			renderItem.selected = value;
	}
}
}
