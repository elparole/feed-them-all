/**
 * User: Elparole
 * Date: 25.03.13
 * Time: 15:14
 */
package com.elparole.feedThemAll.controller
{
import com.elparole.feedThemAll.components.PathComponent;
import com.elparole.feedThemAll.model.vo.PathPoint;

public class PathManager
{
	public var px:Number;
	public var pz:Number;
	public var dir:String;

	public function PathManager() {
	}

	public function solvePosition(pathComp:PathComponent):void {
		var li:int = pathComp.currentIndex;
		while (pathComp.timeElapsed * pathComp.velocity >
				pathComp.pathPts[Math.min(pathComp.currentIndex + 1, pathComp.pathPts.length - 1)].distanceFromStart &&
				(pathComp.currentIndex<pathComp.pathPts.length - 1))
			pathComp.currentIndex = Math.min(pathComp.currentIndex + 1, pathComp.pathPts.length - 1);
		
//		if(pathComp.pathPts[li].dirId == "UL" && pathComp.pathPts[pathComp.currentIndex].dirId=="DL")
//			trace('check');
//		trace('te*vel',pathComp.timeElapsed * pathComp.velocity);
//		trace('distanceFromStart',pathComp.pathPts[Math.min(pathComp.currentIndex, pathComp.pathPts.length - 1)].distanceFromStart);
//		trace('pathComp.timeElapsed',pathComp.timeElapsed);
//		trace('pathComp.currentIndex',pathComp.currentIndex);

		var currentPt:PathPoint = (pathComp.pathPts[pathComp.currentIndex] as PathPoint);
//		trace('start point',pathComp.startPt.x, pathComp.startPt.y );
//		trace('pt x y',currentPt.pos.x,currentPt.pos.y);
		px = currentPt.pos.x;
		pz = currentPt.pos.y;
		var extraDist:Number;
		extraDist = pathComp.timeElapsed * pathComp.velocity - currentPt.distanceFromStart;

//		trace('extraDist',extraDist);

		switch (currentPt.dirId) {
			case 'UL':
				px += extraDist;
				break;
			case 'UR':
				pz += extraDist;
				break;
			case 'DL':
				pz -= extraDist;
				break;
			case 'DR':
				px -= extraDist;
				break;
		}
		dir = currentPt.dirId;
//		try {
////			if (currentPt.dirId == 'UL' && pathComp.pathPts[pathComp.currentIndex + 1].pos.x <= currentPt.pos.x)
////				trace('wrong dir !!!');
////			if (currentPt.dirId == 'UR' && pathComp.pathPts[pathComp.currentIndex + 1].pos.y <= currentPt.pos.y)
////				trace('wrong dir !!!');
////			if (currentPt.dirId == 'DL' && pathComp.pathPts[pathComp.currentIndex + 1].pos.y >= currentPt.pos.y)
////				trace('wrong dir !!!');
//			if (currentPt.dirId == 'DR' && pathComp.currentIndex <3 )
//				trace('wrong dir !!!',
//						pathComp.currentIndex,
//						pathComp.pathPts.length,
//						px,pz,
//						pathComp.pathPts[0].distanceFromStart,
//						pathComp.pathPts[0].dirId,
//						pathComp.pathPts[0].pos.x,pathComp.pathPts[0].pos.y,
//						pathComp.pathPts[1].distanceFromStart,
//						pathComp.pathPts[1].dirId,
//						pathComp.pathPts[1].pos.x,pathComp.pathPts[1].pos.y,
//						pathComp.pathPts[2].distanceFromStart,
//						pathComp.pathPts[2].dirId,
//						pathComp.pathPts[2].pos.x,pathComp.pathPts[2].pos.y,
//						pathComp.pathPts[3].dirId,
//						pathComp.pathPts[3].distanceFromStart,
//						pathComp.pathPts[3].pos.x,pathComp.pathPts[3].pos.y);
//		} catch (e:Error) { }
		
//		trace('solvePosition', px, pz);
	}
}
}
