/**
 * User: Elparole
 * Date: 24.03.13
 * Time: 15:57
 */
package com.elparole.feedThemAll.model
{
public interface ISolvable
{

	function solve(ex:int, ez:int, sx:int, sz:int):Array;
}
}
