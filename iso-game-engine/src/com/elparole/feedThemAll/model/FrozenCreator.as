/**
 * User: Elparole
 * Date: 22.05.13
 * Time: 11:36
 */
package com.elparole.feedThemAll.model
{
import com.elparole.feedThemAll.view.AnimsIDs;

import flash.geom.ColorTransform;

public class FrozenCreator
{
	public function FrozenCreator() {
	}

	public function createFrozenFrames(config:Object):void {
		createFrozenMonster(config, AnimsIDs.MONSTER1);
		createFrozenMonster(config, AnimsIDs.MONSTER2);
		createFrozenMonster(config, AnimsIDs.MONSTER3);
	}

	private function createFrozenMonster(config:Object, monsterStr:String):void {
		createFrozenDir(config,
				monsterStr + '_' + AnimsIDs.IDLE + AnimsIDs.FROZEN + '_' + AnimsIDs.DL,
				monsterStr + '_' + AnimsIDs.IDLE + '_' + AnimsIDs.DL);
		createFrozenDir(config,
				monsterStr + '_' + AnimsIDs.IDLE + AnimsIDs.FROZEN + '_' + AnimsIDs.DR,
				monsterStr + '_' + AnimsIDs.IDLE + '_' + AnimsIDs.DR);
		createFrozenDir(config,
				monsterStr + '_' + AnimsIDs.IDLE + AnimsIDs.FROZEN + '_' + AnimsIDs.UL,
				monsterStr + '_' + AnimsIDs.IDLE + '_' + AnimsIDs.UL);
		createFrozenDir(config,
				monsterStr + '_' + AnimsIDs.IDLE + AnimsIDs.FROZEN + '_' + AnimsIDs.UR,
				monsterStr + '_' + AnimsIDs.IDLE + '_' + AnimsIDs.UR);
	}

	private function createFrozenDir(config:Object, strFrozen:String, strBasic:String):void {
		config[strFrozen] ||= {frames:[], totalFrames:1};
		var fitem:ItemVO = new ItemVO(strFrozen, strFrozen, config[strBasic].frames[0].bitmap.clone());
		
		var ct:ColorTransform = new ColorTransform();
//		ct.blueOffset = 205;
//		ct.redMultiplier = 1.5;
//		ct.greenMultiplier = 1.5;
//		ct.blueMultiplier = 1.5;


		fitem.bitmap.fillRect(fitem.bitmap.rect,0);
		
		fitem.bitmap.draw(config[strBasic].frames[0].bitmap, null,ct);

		var ct:ColorTransform = new ColorTransform();
		ct.color = 0xff9df1ff;
		ct.alphaMultiplier = 0.7;
		
		fitem.bitmap.draw(config[strBasic].frames[0].bitmap,null,ct);

		fitem.id = config[strBasic].frames[0].id;
		fitem.animId = config[strBasic].frames[0].id + AnimsIDs.FROZEN;
		fitem.dirId = config[strBasic].frames[0].dirId;
		fitem.offset = config[strBasic].frames[0].offset.clone();
		fitem.frameNum = 0;
		fitem.totalFrames = 1;

		config[strFrozen].frames[0] = fitem;
		config[strFrozen].item = fitem;
		config[strFrozen].bitmap = fitem.bitmap;
		config[strFrozen].totalFrames = 1;
	}
}
}
