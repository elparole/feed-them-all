/**
 * User: Elparole
 * Date: 04.03.13
 * Time: 15:42
 */
package com.elparole.feedThemAll.model
{
import flash.geom.Matrix;
import flash.geom.Point;

import mx.collections.ArrayCollection;

public class FilesListConfigParser implements IConfigParser
{

	public var filesList:ArrayCollection;

	private var frozenCreator:FrozenCreator = new FrozenCreator();

	public function FilesListConfigParser() {

	}

	public function parseNamesToConfig(config:Object):Object{

		var mat:Matrix = new Matrix();
		for each (var item:ItemVO in filesList) {
			var configArr2:Array = item.name.split('_');
			item.id = configArr2[0];
			item.animId = configArr2[1];
			item.dirId = configArr2[2];
			item.offset = new Point(int(configArr2[3]),int(configArr2[4]));
			item.frameNum = int(configArr2[5]);

			var animId:String =configArr2[0]+'_'+configArr2[1]+'_'+configArr2[2];//+'_'+configArr2[3]+'_'+configArr2[4];
			config[animId] ||={frames:[], totalFrames:0};
			config[animId].frames[item.frameNum-1]=item;
			config[animId].totalFrames++;
			config[animId].item = item;
			config[animId].bitmap = item.bitmap;

//			mat.tx = 64-item.offset.x;
//			mat.ty = 64-item.offset.y;
//			config[animId].bitmap.draw(item.bitmap, mat);
//			item.bitmap = config[animId].bitmap;

//			0xff000000+0xffffff*Math.random());
//			config[animId].bitmap.fillRect(new Rectangle(32,32,64,64),0xffff0000+Math.random()*256*256);
//			config[animId].bitmap = item.bitmap = new BitmapData(128,128,true, 0xffff0000);
		}

		frozenCreator.createFrozenFrames(config);

		return config;
	}



	public function parseConfig(config:Object):Object{
//		var configsObj:AssetsDataEmbedded = new AssetsDataEmbedded();
//		config = configsObj.config;
//		sheetB = (new sheetC()).bitmapData;

//		var rect:Rectangle = new Rectangle(0,0,100,100);
//		var mat:Matrix = new Matrix();
//		for each(var item:ItemVO in filesList) {
//			var str:String = item.getIdAnimDir();
//			var i:int = item.frameNum;
//			config[str] ||={frames:[]};
//			if(config[str].frames[i].sheetItemW>128 && config[str].frames[i].sheetItemW<=512){
//				config[str].frames[i].bitmap = new BitmapData(
//						1024,
//						512,true,0 );
////				mat.tx = -config[str].frames[i].sheetItemX+config[str].frames[i].bitmap.width/2-config[str].frames[i].offset.x;
////				mat.ty = -config[str].frames[i].sheetItemY+config[str].frames[i].bitmap.height/2-config[str].frames[i].offset.y;
//				rect.x = config[str].frames[i].bitmap.width/2-config[str].frames[i].offset.x;
//				rect.y = config[str].frames[i].bitmap.height/2-config[str].frames[i].offset.y;
//				rect.width = config[str].frames[i].sheetItemW;//+config[str].frames[i].offset.x;
//				rect.height = config[str].frames[i].sheetItemH;//+config[str].frames[i].offset.y;
//			}else if(config[str].frames[i].sheetItemW>512){
//				config[str].frames[i].bitmap = new BitmapData(
//						2048,
//						1024,true,0 );
//				mat.tx = -config[str].frames[i].sheetItemX;
//				mat.ty = -config[str].frames[i].sheetItemY;
//				rect.width = config[str].frames[i].sheetItemW;
//				rect.height = config[str].frames[i].sheetItemH;
//			}else{
//				config[str].frames[i].bitmap = new BitmapData(
//						128,
//						128,true,0 );
//				mat.tx = -config[str].frames[i].sheetItemX+64-config[str].frames[i].offset.x;
//				mat.ty = -config[str].frames[i].sheetItemY+64-config[str].frames[i].offset.y;
//				rect.x = 64-config[str].frames[i].offset.x;
//				rect.y = 64-config[str].frames[i].offset.y;
//				rect.width = config[str].frames[i].sheetItemW;//+config[str].frames[i].offset.x;
//				rect.height = config[str].frames[i].sheetItemH;//+config[str].frames[i].offset.y;
//			}
//			//				config[str].frames[i].bitmap.draw(sheetB, mat,null,null,rect);
//
//		}
		return config;
	}
}
}
