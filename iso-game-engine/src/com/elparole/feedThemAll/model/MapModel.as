/**
 * User: Elparole
 * Date: 11.03.13
 * Time: 16:29
 */
package com.elparole.feedThemAll.model
{
import com.blackmoondev.geom.IntPoint;
import com.blackmoondev.pathfind.astar.AStar;
import com.blackmoondev.pathfind.map.Tilemap;
import com.elparole.levelEditor.model.vo.LevelConfig;

import flash.geom.Rectangle;
import flash.utils.getTimer;

public class MapModel implements ISolvable
{

	private var tilemap:Tilemap = new Tilemap();
	private var astar:AStar;
	private var sp:IntPoint = new IntPoint();
	private var ep:IntPoint = new IntPoint();
	private var _bounds:Rectangle;

	public function MapModel() {
//		tilemap.setWalkable(1,3,false);
//		tilemap.setWalkable(1,4,false);
//		tilemap.setWalkable(1,5,false);
//		tilemap.setWalkable(1,7,false);
//		tilemap.setWalkable(1,8,false);
//
//		tilemap.setWalkable(12,12,false);
//		tilemap.setWalkable(11,12,false);
//		tilemap.setWalkable(12,11,false);
//		tilemap.setWalkable(11,11,false);
//		tilemap.setWalkable(16,12,false);
//		tilemap.setWalkable(15,12,false);
//		tilemap.setWalkable(16,11,false);
//		tilemap.setWalkable(15,11,false);

//		tilemap.setWalkable(0,10,false);
//		tilemap.setWalkable(1,10,false);
//		tilemap.setWalkable(2,10,false);
//		tilemap.setWalkable(3,10,false);
//		tilemap.setWalkable(4,10,false);
//		tilemap.setWalkable(5,10,false);
//
//		tilemap.setWalkable(3,12,false);
//		tilemap.setWalkable(4,12,false);
//		tilemap.setWalkable(5,12,false);
//		tilemap.setWalkable(6,12,false);
//		tilemap.setWalkable(7,12,false);
//		tilemap.setWalkable(8,12,false);
//		tilemap.setWalkable(9,12,false);
//		tilemap.setWalkable(10,12,false);
//
//		tilemap.setWalkable(0,1,false);
//		tilemap.setWalkable(1,1,false);
//		tilemap.setWalkable(2,1,false);
//		tilemap.setWalkable(3,1,false);
//		tilemap.setWalkable(4,1,false);
//		tilemap.setWalkable(5,1,false);
//
//		tilemap.setWalkable(3,3,false);
//		tilemap.setWalkable(4,3,false);
//		tilemap.setWalkable(5,3,false);
//		tilemap.setWalkable(6,3,false);
//		tilemap.setWalkable(7,3,false);
//		tilemap.setWalkable(8,3,false);
//		tilemap.setWalkable(9,3,false);
//		tilemap.setWalkable(10,3,false);
		astar = new AStar(tilemap);
	}

	public function solve(ex:int, ez:int, sx:int, sz:int):Array {
		sp.x = sx;
		sp.y = sz;
		ep.x = ex;
		ep.y = ez;

		var arr:Array = astar.solve(sp, ep);
		arr.push()
		return astar.solve(sp, ep);
	}

//	public function solveToGlobalPos(sx:int,sz:int,ex:int,ez:int):Array{
//		var arr:Array = solve(sx,sz, ex, ez);
//		for each (var tile:Object in arr) {
//			tile.x
//		}
//	}
	public function parseObstacleMap(lc:LevelConfig,err:Object):LevelConfig {
//		tilemap = new Tilemap(lc.size.x, lc.size.y);
		var t:int = getTimer();
		tilemap.clearTilemap();
//		tilemap.resize(new IntPoint(Math.max(30,lc.size.x), Math.max(lc.size.y,30)));
		tilemap.resize(new IntPoint(lc.size.y, lc.size.x));

		for (var key:String in lc.obstacleMap) {
			var ix:int = key.split('_')[0];
			var iz:int = key.split('_')[1];
//			trace('setWalkable',ix,iz,lc.obstacleMap[ix+'_'+iz]);
			tilemap.setWalkable(ix,iz, lc.obstacleMap[ix+'_'+iz]);
		}
		trace('parse obstacle map',getTimer()-t);
		return lc;
	}

	public function get bounds():Rectangle{
		_bounds ||= new Rectangle();
		_bounds.width = tilemap.getWidth();
		_bounds.height = tilemap.getHeight();
		return _bounds;
	}

	public function isWalkable(px:Number, py:Number):Boolean {
		return tilemap.isWalkable(px,py);
	}

	public function setWalkable(gx:int, gz:int, walkable:Boolean):void {
		tilemap.setWalkable(gx, gz,walkable);
	}
}
}
