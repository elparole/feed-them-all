/**
 * User: Elparole
 * Date: 31.07.12
 * Time: 13:56
 */
package com.elparole.feedThemAll.model
{
import ash.core.Node;
import ash.core.NodeList;

import com.blackmoondev.geom.IntPoint;
import com.elparole.cartesianToPolarCoords;
import com.elparole.feedThemAll.AppConsts;
import com.elparole.feedThemAll.components.Anim;
import com.elparole.feedThemAll.components.PathComponent;
import com.elparole.feedThemAll.model.vo.PathPoint;
import com.elparole.randomIntInRange;
import com.elparole.randomValFromArr;

import flash.display.BitmapData;
import flash.display.Sprite;
import flash.geom.Point;
import flash.utils.Dictionary;

import org.casalib.math.geom.Point3d;

import utils.type.getClassByName;

public class GameUtils
{
	private var explosions:Array = [];
	private var enemiesHitFields:Dictionary = new Dictionary();
	private var enemiesSkins:Dictionary = new Dictionary();
	private var skins:Dictionary = new Dictionary();
	private var bulletHitBDs = [];
	private var coinsToRemove:Array;
	public var config:Object = {};

	public var sheetB:BitmapData;
	public var parser:IConfigParser;

	public function GameUtils() {
//		parseConfigs(new AssetsDataEmbedded());
	}

	private function cacheClass(klazz:Class):void {
		skins[klazz] = [];
		for (var i:int = 0; i < 150; i++) {
			skins[klazz][i] = new klazz();
			(skins[klazz][i] as Sprite).cacheAsBitmap = true;
		}
	}

	public function distanceSquared(p2x,p2y,p1x,p1y):Number{
		return (p2x - p1x) * (p2x - p1x) + (p2y - p1y) * (p2y - p1y);
	}

	public static function distanceSquaredStat(p2x,p2y,p1x,p1y):Number{
		return (p2x - p1x) * (p2x - p1x) + (p2y - p1y) * (p2y - p1y);
	}

	public static function distancePt(p2:Point, p1:Point):Number{
		return Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
	}

//	public static function toPolar():void {
//
//	}
	
	public function distanceSquaredPt(p2:Point, p1:Point):Number{
		return (p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y);
	}

	public function distanceSquaredPt3D(p2:*, p1:*):Number{
		return (p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y) + (p2.z - p1.z) * (p2.z - p1.z);
	}

	public function getSkin(skinClazz:Class):Sprite {
		skins[skinClazz].unshift(skins[skinClazz].pop());
		return skins[skinClazz][0];
	}

	public function getEnemySkin(type:Class):Sprite {
		enemiesSkins[type].unshift(enemiesSkins[type].pop());
//		enemiesSkins[type][0].gotoAndStop(1);
		return enemiesSkins[type][0];
	}

	public function getBulletSkin(bulletId:int):Sprite {
		return getSkin(getClassByName('Bullet'+bulletId + 'Skin'));
	}

	public function getBulletHitBD(bulletId:int):BitmapData {
		return bulletHitBDs[bulletId];
	}

	public function getRandomNode(controlNodes:NodeList):Node {
		var node:Node;
		var num:int = 0;
		for (node = controlNodes.head; node; node = node.next) {
			num++;
		}
		var rid:int = randomIntInRange(0,num-1);
		for (node = controlNodes.head; node; node = node.next) {
			rid--;
			if(rid <=-1)
				return node;
		}
		return null;
	}

	public function getSkinByID(id:String, animId:String, dirId:String, frame:int = 0):BitmapData {
		return config[id+'_'+animId+'_'+dirId].frames[frame].bitmap;
	}

	public function getSkinOffset(id:String, animId:String, dirId:String, frame:int = 0):Point {
		return config[id+'_'+animId+'_'+dirId].frames[frame].offset as Point;
	}

	public function getAnim(id:String, animId:String, dirId:String):Anim {
		return new Anim(0,
						config[id+'_'+animId+'_'+dirId].totalFrames,
						id+'_'+animId+'_'+dirId);
	}

	public function getTotalFrames(id:String, animId:String, dirId:String):int{
		return config[id+'_'+animId+'_'+dirId].totalFrames;
	}

	public function cloneBD(bd:BitmapData):BitmapData {
		var cbd:BitmapData = new BitmapData(bd.width,bd.height,bd.transparent,0);
		cbd.draw(bd);
		return cbd;
	}

	public function getPathComponent(px:int,pz:int, dx:int, dz:int, mapModel:ISolvable):PathComponent {
//		trace('new creaturer', badCreature.id);
		trace('start pos',
				px,
				pz,
				Math.round(px / AppConsts.xGrid),
				Math.round(pz / AppConsts.zGrid));
		trace('dest pos',
				dx,
				dz,
				Math.round(dx / AppConsts.xGrid),
				Math.round(dz / AppConsts.zGrid));

		var distFromStart:Number;
		var ppt:PathPoint;
		var needHelperPoint:Boolean = false;

		var pc:PathComponent = new PathComponent();
		pc.currentIndex = 0;
		pc.startPt = new Point(Math.round(px),
				Math.round(pz));
		pc.pathPts = mapModel.solve(
				Math.round(px / AppConsts.xGrid),
				Math.round(pz / AppConsts.zGrid),
				Math.round(dx / AppConsts.xGrid),
				Math.round(dz / AppConsts.zGrid));

		var disabledPts:Array = [];
		var dpl:int = pc.pathPts.length/8;
		for (var i:int = 0; i < dpl; i++) {
			
			if(Math.random()<(0.25+Math.min(0.5, 0.01*pc.pathPts.length)) && (mapModel is MapModel)){
				var scrobbledPt:IntPoint = randomValFromArr(pc.pathPts);
				disabledPts.push(scrobbledPt);
				(mapModel as MapModel).setWalkable(scrobbledPt.x,scrobbledPt.y, false);
			}
		}

		pc.pathPts = mapModel.solve(
				Math.round(px / AppConsts.xGrid),
				Math.round(pz / AppConsts.zGrid),
				Math.round(dx / AppConsts.xGrid),
				Math.round(dz / AppConsts.zGrid));		

		for (i = 0; i < disabledPts.length; i++) {
			(mapModel as MapModel).setWalkable(disabledPts[i].x,disabledPts[i].y, true);
		}

		disabledPts.length = 0;
		
		if(!pc.pathPts || pc.pathPts.length==0)
			pc.pathPts = mapModel.solve(
					Math.round(px / AppConsts.xGrid),
					Math.round(pz / AppConsts.zGrid),
					Math.round(dx / AppConsts.xGrid),
					Math.round(dz / AppConsts.zGrid));

		pc.pathPts.shift();

		if(pc.pathPts[0] &&
			(pc.pathPts[0].x*AppConsts.xGrid != pc.startPt.x &&
			pc.pathPts[0].y*AppConsts.xGrid != pc.startPt.y )){
			needHelperPoint = true;
		}

		var firstToChange:String = 'both';
		var firstDir:String = '';
		if (pc.pathPts && pc.pathPts.length > 1) {

			if(needHelperPoint){
				var hp:Point = new Point();
				if(Math.abs(pc.pathPts[0].x*AppConsts.xGrid - pc.startPt.x)<
						Math.abs(pc.pathPts[0].y*AppConsts.xGrid - pc.startPt.y)){
					firstToChange = 'y';
					hp.x = pc.pathPts[0].x;
					hp.y = pc.startPt.y/AppConsts.xGrid;
				}else{
					firstToChange = 'x';
					hp.y = pc.pathPts[0].y;
					hp.x = pc.startPt.x/AppConsts.xGrid;
				}

				pc.pathPts.unshift(hp);
//				pc.pathPts.unshift(new Point());


//				if(pc.pathPts.length>1)
//					switch (true) {
//						case (firstToChange != "y") && (pc.pathPts[0].x - pc.startPt.x > 0):
//							firstDir = 'UL';
//							break;
//						case (firstToChange != "y") && (pc.pathPts[0].x - pc.startPt.x < 0):
//							firstDir = 'DR';
//							break;
//						case (firstToChange != "x") && (pc.pathPts[0].y - pc.startPt.y > 0):
//							firstDir = 'UR';
//							break;
//						case (firstToChange != "x") && (pc.pathPts[0].y - pc.startPt.y < 0):
//							firstDir = 'DL';
//							break;
//					}
			}
//			else
				distFromStart = Math.abs(pc.pathPts[0].x*AppConsts.xGrid  - pc.startPt.x) +
					Math.abs(pc.pathPts[0].y*AppConsts.xGrid  - pc.startPt.y);
			var distToNext:Number = 0;

			for (var i:int = 0; i < pc.pathPts.length; i++) {

				if (i < pc.pathPts.length - 1) {
					distToNext = Math.abs(pc.pathPts[i + 1].x - pc.pathPts[i].x) +
							Math.abs(pc.pathPts[i + 1].y - pc.pathPts[i].y);
				} else
					distToNext = 0;

				var ppt:PathPoint = new PathPoint(pc.pathPts[i], 'DR', distFromStart, distToNext);

//				if(firstDir.length>0 && needHelperPoint){
//					ppt.dirId = firstDir;
//				}else
				if (i < pc.pathPts.length - 1) {
					switch (true) {
						case pc.pathPts[i + 1].x - pc.pathPts[i].x > 0:
							ppt.dirId = 'UL';
							break;
						case pc.pathPts[i + 1].x - pc.pathPts[i].x < 0:
							ppt.dirId = 'DR';
							break;
						case pc.pathPts[i + 1].y - pc.pathPts[i].y > 0:
							ppt.dirId = 'UR';
							break;
						case pc.pathPts[i + 1].y - pc.pathPts[i].y < 0:
							ppt.dirId = 'DL';
							break;
					}
				} else 
					ppt.dirId = pc.pathPts[i - 1].dirId;

				if (i < pc.pathPts.length - 1) {
					distFromStart += (Math.abs(pc.pathPts[i + 1].x - pc.pathPts[i].x)  +
							Math.abs(pc.pathPts[i + 1].y - pc.pathPts[i].y))*AppConsts.xGrid;
				}
				pc.pathPts[i] = ppt;
			}
			ppt = new PathPoint(new IntPoint(pc.startPt.x/AppConsts.xGrid, pc.startPt.y/AppConsts.xGrid),'DR',0,0);
			ppt.pos.x =pc.startPt.x;
			ppt.pos.y =pc.startPt.y;
			switch (true) {
				case pc.pathPts[0].pos.x - ppt.pos.x > 0:
					ppt.dirId = 'UL';
					break;
				case pc.pathPts[0].pos.x - ppt.pos.x < 0:
					ppt.dirId = 'DR';
					break;
				case pc.pathPts[0].pos.y - ppt.pos.y > 0:
					ppt.dirId = 'UR';
					break;
				case pc.pathPts[0].pos.y - ppt.pos.y < 0:
					ppt.dirId = 'DL';
					break;
			}
			pc.pathPts.unshift(ppt);
//			ppt.distanceFromStart = Math.abs(pc.pathPts[0].pos.x - pc.startPt.x) +
//					Math.abs(pc.pathPts[0].pos.y - pc.startPt.y);

//			pc.pathPts[1].distanceFromStart +=ppt.distanceFromStart;

		}else{
			dx = Math.round(dx / AppConsts.xGrid)*AppConsts.xGrid;
			dz = Math.round(dz / AppConsts.zGrid)*AppConsts.zGrid;
			distFromStart = Math.abs(dx - pc.startPt.x) +
							Math.abs(dz - pc.startPt.y);
//					Math.abs(Math.round(dz / AppConsts.zGrid) * AppConsts.zGrid - pc.startPt.y);
			ppt = new PathPoint(new IntPoint(
					Math.round(dx / AppConsts.xGrid),
					Math.round(dz / AppConsts.zGrid)),
					'DR', distFromStart, 0);

			switch (true) {
				case dx - pc.startPt.x > 0:
					ppt.dirId = 'UL';
					break;
				case dx - pc.startPt.x < 0:
					ppt.dirId = 'DR';
					break;
				case dz - pc.startPt.y > 0:
					ppt.dirId = 'UR';
					break;
				case dz - pc.startPt.y < 0:
					ppt.dirId = 'DL';
					break;
			}

			pc.pathPts=[ppt];
		}

//		if (pc.pathPts.length == 1)
//			pc.pathPts = null;
		pc.velocity = 100;
		return pc;
	}


	public function dist(pt1:*,pt2:*):Number{
		return Math.pow((pt2.x-pt1.x)*(pt2.x-pt1.x)+(pt2.y-pt1.y)*(pt2.y-pt1.y)+(pt2.z-pt1.z)*(pt2.z-pt1.z),0.33);
	}

	public function followPlayer(position:Point3d, targetPosition:Point3d, time:Number, speed:Number):void {
//		motion = node.motion;
		var rx:Number = position.x;
		var rz:Number = position.z;
		var px:Number = targetPosition.x;
		var pz:Number = targetPosition.z;
//
//		if(controlNode){
////				trace('passed reference', controlNode.position.position, offsetY);
//			px = controlNode.position.position.x;
//			py = controlNode.position.position.y+offsetY;
//		}else{
////				trace('iterate reference', (node as CoinCollisionNode).coin.destPlayer.gPlayerPosition.position, offsetY);
//			if((node as CoinCollisionNode).coin.destPlayer &&
//					(distancePt((node as CoinCollisionNode).position.position,(node as CoinCollisionNode).coin.destPlayer.gPlayerPosition.position)<170)){
//				px = (node as CoinCollisionNode).coin.destPlayer.gPlayerPosition.position.x;
//				py = (node as CoinCollisionNode).coin.destPlayer.gPlayerPosition.position.y+offsetY;
//			}else{
//				px =  rx;
//				py = ry+1;
//			}
//		}
//
		var angle:Number = cartesianToPolarCoords(px - rx, pz - rz)[1];// + Math.random() * burst - burst/2;
		position.x += /*randomIntInRange(0,100);*/Math.cos(angle) * speed*time;
		position.z += /*randomIntInRange(0,100);*/Math.sin(angle) * speed*time;
	}
}
}
