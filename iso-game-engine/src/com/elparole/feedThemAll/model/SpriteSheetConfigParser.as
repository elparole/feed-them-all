/**
 * User: Elparole
 * Date: 04.03.13
 * Time: 15:42
 */
package com.elparole.feedThemAll.model
{
import flash.display.BitmapData;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;

public class SpriteSheetConfigParser implements IConfigParser
{
	public var sheetB:BitmapData;

	private var frozenCreator:FrozenCreator = new FrozenCreator();

	[Embed(source="../../../../../../main-app/src/assets/sheet.png")]
	public var sheetC:Class;

	public function SpriteSheetConfigParser() {

	}

	public function parseConfig(config:Object):Object{
		var configsObj:AssetsDataEmbedded = new AssetsDataEmbedded();
//		copyAllProperties(configsObj.config, config);
		config = configsObj.config;
		sheetB = (new sheetC()).bitmapData;

		var rect:Rectangle = new Rectangle(0,0,100,100);
		var mat:Matrix = new Matrix();
		for (var str:String in config) {
			for (var i:int = 0; i < config[str].frames.length; i++) {

				config[str].frames[i] ||=new Point();

				config[str].frames[i].bitmap = new BitmapData(
						config[str].frames[i].sheetItemW,
						config[str].frames[i].sheetItemH,true,0/*0x55ff000000*/ );

				mat.tx = -config[str].frames[i].sheetItemX;
				mat.ty = -config[str].frames[i].sheetItemY;
				rect.x = 0;
				rect.y = 0;
				rect.width = config[str].frames[i].sheetItemW;//+config[str].frames[i].offset.x;
				rect.height = config[str].frames[i].sheetItemH;//+config[str].frames[i].offset.y;


				config[str].frames[i].bitmap.draw(sheetB, mat,null,null,rect);
			}
		}

		frozenCreator.createFrozenFrames(config);

		return config;
	}


}
}
