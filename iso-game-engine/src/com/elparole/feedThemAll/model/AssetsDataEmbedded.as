package com.elparole.feedThemAll.model{

import flash.geom.Point;
import flash.utils.ByteArray;

[Embed(source="../../../../../../main-app/src/assets/sheet.txt", mimeType="application/octet-stream")]
public class AssetsDataEmbedded extends ByteArray
{

	public var config:Object={};
	public var dataArr:Array;

	public function AssetsDataEmbedded() {

		parse();
	}

	public function parse():void{

		
		var dataStr:String = readUTFBytes(length);
		dataStr = dataStr.replace('\r','');
		dataArr = dataStr.split('\n');
		dataArr.pop();
		for each (var configId:String in dataArr) {
			var configArr1:Array = configId.split(' ');
			configArr1.splice(1,1);
			var configArr2:Array = configArr1[0].split('_');
			var obj:Object = {
				id:configArr2[0], 
				animId:configArr2[1], 
				dirId:configArr2[2],
				offset:new Point(int(configArr2[3]),int(configArr2[4])),
				frameNum:int(configArr2[5]),
				sheetItemX:int(configArr1[1]),
				sheetItemY:int(configArr1[2]),
				sheetItemW:int(configArr1[3]),
				sheetItemH:int(configArr1[4])
			};
			var animId:String =configArr2[0]+'_'+configArr2[1]+'_'+configArr2[2];//+'_'+configArr2[3]+'_'+configArr2[4];
			config[animId] ||={frames:[], totalFrames:0};
			config[animId].frames[obj.frameNum-1]=obj;
			config[animId].totalFrames++;
		}
//		config = JSON.decode(readUTFBytes(length));
	}
}
}