/**
 * User: Elparole
 * Date: 04.03.13
 * Time: 16:57
 */
package com.elparole.feedThemAll.model
{
import flash.display.BitmapData;
import flash.geom.Point;

public class ItemVO
{
	public var path:String;
	public var name:String;
	public var bitmap:BitmapData;
	public var offset:Point;
	public var width:int;
	public var height:int;
	public var frameNum:int;
	public var id:String;
	public var dirId:String;
	public var animId:String;
	public var totalFrames:int = 0;


	public function ItemVO(name:String,path:String, bitmap:BitmapData) {
		this.name = name;
		this.path = path;
		this.bitmap = bitmap;
//		var nameArr:Array = name.split('_');
//		this.offset = new Point(nameArr[3],nameArr[4]);
//		this.frameNum = nameArr[5];
//		this.animName = nameArr[1];
//		this.id = nameArr[0];
//		this.dir = nameArr[2];
	}

	public function getIdAnimDir():String {
		var nameArr:Array = name.split('_');
		return nameArr[0]+'_'+nameArr[1]+'_'+nameArr[2];
	}

	public function get listId():String{
//		return id+'_'+dirId+'_'+totalFrames;
		return id+'_'+dirId;
	}

	public function toString():String {
		return listId;
	}
}
}
