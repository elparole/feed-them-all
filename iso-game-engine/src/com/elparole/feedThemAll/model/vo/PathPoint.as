/**
 * User: Elparole
 * Date: 20.03.13
 * Time: 13:10
 */
package com.elparole.feedThemAll.model.vo
{
import com.blackmoondev.geom.IntPoint;
import com.elparole.feedThemAll.AppConsts;

public class PathPoint
{
	public var pos:IntPoint;
	public var dirId:String;
	public var distanceFromStart:Number;
	public var distanceToNext:Number;


	public function PathPoint(pos:*, dirId:String, distanceFromStart:Number, distanceToNext:Number) {
		this.pos = new IntPoint(pos.x*AppConsts.xGrid,pos.y*AppConsts.zGrid);
		this.dirId = dirId;
		this.distanceFromStart = distanceFromStart;
		this.distanceToNext = distanceToNext;
	}


	public function toString():String {
		return 'pos: '+pos.x+':'+pos.y+', dirId:'+dirId+', distanceFromStart:'+distanceFromStart+', distanceToNext:'+distanceToNext+'\n';
	}
}
}
