/**
 * User: Elparole
 * Date: 04.03.13
 * Time: 15:44
 */
package com.elparole.feedThemAll.model
{
public interface IConfigParser
{
	function parseConfig(config:Object):Object;

}
}
