/**
 * User: Elparole
 * Date: 27.07.12
 * Time: 01:00
 */
package com.elparole.feedThemAll.components
{
import flash.geom.Point;

public class PathComponent
{
	public var pathPts:Array;
	public var startPt:Point;
	public var timeElapsed:Number = 0;
	public var timeToWait:Number = 0;
	public var velocity:Number = 0;
	public var currentIndex:int = 0;

	public function PathComponent() {

	}


}
}
