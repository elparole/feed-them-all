/**
 * User: Elparole
 * Date: 27.07.12
 * Time: 01:00
 */
package com.elparole.feedThemAll.components
{
public class Anim
{
	public var frameNum:Number = 0;
	public var totalFrames:int;
	public var animationID:String;
	public var animSpeed:Number = 30;
	public var animEnded:Boolean = false;
	public var loopsCounter:int = 0;
//	public var animSpeed:Number = 5;

	public function Anim(frameNum:int, totalFrames:int, animationID:String) {
		this.frameNum = frameNum;
		this.totalFrames = totalFrames;
		this.animationID = animationID;
	}
}
}
