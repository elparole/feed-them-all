/**
 * User: Elparole
 * Date: 26.01.13
 * Time: 15:23
 */
package com.elparole.feedThemAll.view
{
import flash.utils.Dictionary;

public class AnimsIDs
{
	public static const MONSTER:String = 'monster';
	public static const MONSTER1:String = 'monster1';
	public static const MONSTER2:String = 'monster2';
	public static const MONSTER3:String = 'monster3';
	public static const FLOOR1:String = 'floor1';
	public static const FLOOR2:String = 'floor2';
	public static const FLOOR3:String = 'floor3';
	public static const SHEEP:String = 'sheep';
	public static const CATAPULTWHEELS:String = 'catapultWheels';
	public static const CATAPULTSPOON:String = 'catapultSpoon';
	public static const WALK:String = 'walk';
	public static const IDLE:String = 'idle';

	public static var DL:String ='DL';
	public static var UL:String ='UL';
	public static var UR:String ='UR';
	public static var DR:String ='DR';
	public static const SHOOT:String = 'shoot';
	public static const HEART:String = 'heart';
	public static const HEART1:String = 'heart1';
	public static const HEART2:String = 'heart2';
	public static const HEART3:String = 'heart3';
	public static var EAT:String = 'eat';
	public static const TARGETPOINTER:String = 'targetPointer';
	public static const ARROW_STRENGTH:String = 'arrowStrength';
	public static const PUDDLE:String = 'bloodPuddle';
	public static const TILE_GREEN:String = 'tileGreen';
	public static const TILE_RED:String = 'tileRed';
	public static const TERRAIN_PLAIN:String = 'terrainPlain';

	public static const FENCE:String = 'plotek';
	public static const FENCE_CORNER:String = 'plotekCorner';
	public static const FENCE_RUINED:String = 'plotekRozwalony';
	public static const FENCE_CORNER_RUINED:String = 'plotekCornerRozwalony';

	public static const MEAT1:String = "meat1";
	public static const MEAT2:String = "meat2";
	public static const FROZEN_MEAT:String = "frozenMeat";
	public static const SAUSAGE:String = "sausageBombMeat";
	public static const SHEEP_BOMB_MEAT:String = "sheepBombMeat";

	public static const BILL:String = 'cash';
	public static const MONSTER_CAVE:String = 'monsterCave';
	public static const FROZEN:String = 'Frozen';
	public static var rockObstacles:Array = [
		'rock1',
		'rock2',
		'rock3',
		'rock4'
			]
	public static var obstacleIds:Array = [
			'tree1',
			'tree2',
			'tree3',
			'bush',
			'rock1',
			'rock2',
			'rock3',
			'rock4'
	];

	public static var selectSounds:Dictionary;
	public static var hitGroundSounds:Dictionary;



	public static function isFenceId(skinID:String):Boolean {
		return skinID==AnimsIDs.FENCE||
				skinID==AnimsIDs.FENCE_CORNER||
				skinID==AnimsIDs.FENCE_RUINED||
				skinID==AnimsIDs.FENCE_CORNER_RUINED

	}

	initStatic();

	public static function initStatic():void {
		selectSounds = new Dictionary();
		selectSounds[HEART1] = RegularMeat_SelectSnd;
		selectSounds[MEAT2] = BetterMeat_SelectSnd;
		selectSounds[FROZEN_MEAT] = FrozenMeat_SelectSnd;
		selectSounds[SAUSAGE] = TNTSausage_SelectSnd;
		selectSounds[SHEEP_BOMB_MEAT] = SheepMeat_SelectSnd;

		hitGroundSounds = new Dictionary();
		hitGroundSounds[HEART1] = RegularMeat_HittingGroundSnd;
		hitGroundSounds[MEAT2] = BetterMeat_HittingGroundSnd
		hitGroundSounds[FROZEN_MEAT] = FrozenMeat_HittingGroundSnd
		hitGroundSounds[SAUSAGE] = TNTSausage_HittingGroundSnd;
		hitGroundSounds[SHEEP_BOMB_MEAT] = SheepMeat_HittingGroudSnd;
	}

	public function AnimsIDs() {

	}

	public static function isRuined(skinID:String):Boolean {
		return skinID==AnimsIDs.FENCE_RUINED||
				skinID==AnimsIDs.FENCE_CORNER_RUINED
	}

	public static function isMonsterCave(skinId:String):Boolean {
		return skinId==AnimsIDs.MONSTER_CAVE;
	}

	public static function isBadCreature(skinId:String):Boolean {
		return skinId==AnimsIDs.MONSTER ||
				skinId==AnimsIDs.MONSTER1 ||
				 skinId==AnimsIDs.MONSTER2 ||
				  skinId==AnimsIDs.MONSTER3;
	}

	public static function isMeat(skinId:String):Boolean {
		return skinId==AnimsIDs.HEART ||
				skinId==AnimsIDs.HEART1 ||
				skinId==AnimsIDs.MEAT1 ||
				skinId==AnimsIDs.MEAT2 ||
				skinId==AnimsIDs.FROZEN_MEAT ||
				skinId==AnimsIDs.SAUSAGE ||
				skinId==AnimsIDs.SHEEP_BOMB_MEAT

	}
}
}
