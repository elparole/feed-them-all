/**
 * User: Elparole
 * Date: 19.03.13
 * Time: 11:39
 */
package com.elparole.feedThemAll.view
{
import com.cenizal.blis.BlisSystem;
import com.elparole.components.DisplayBlis;
import com.elparole.feedThemAll.AppConsts;
import com.elparole.feedThemAll.test.IsoAnimItem;

import flash.display.BitmapData;
import flash.display.Sprite;
import flash.geom.Point;
import flash.geom.Rectangle;

import org.casalib.math.geom.Point3d;

public class BlisContainer extends Sprite
{
	private var _system:BlisSystem;
	private var _lastTimer:int = 0;
	private var items:Array = [];
	private var _count:int;
	private var speeds:Array;
	private var hearts:Array = [];
	private var itemId:int = 0;
	private var w:int;
	private var h:int;
	private var useMouseCanvas:Boolean;
	private var gridMarks:Array = [];

	public function BlisContainer(w:int = 640, h:int = 480) {
		this.w = w;
		this.h = h;
	}

	public function init(useMouseCanvas:Boolean=false, showGrid:Boolean = false):void {
//		this.mouseEnabled = this.mouseChildren = false;
//		removeEventListener( Event.ADDED_TO_STAGE, init );

		// Set up Blis.
		this.useMouseCanvas= useMouseCanvas;
		_system = new BlisSystem( w, h, w, h, useMouseCanvas );
//			_system.addLayer( "bg" );
//			_system.addLayer( "shadows" );
		_system.addLayer( "background" );
		useMouseCanvas && _system.addLayer( "obstacle_map" );
		!useMouseCanvas && _system.addLayer( "cursorLayer" );
		showGrid && _system.addLayer("grid");
		_system.addLayer( "items" );


//		var spriteSheet:BitmapData = ( new Resources.BuildingImage() as Bitmap ).bitmapData;

//		var floor:Bitmap = new Bitmap(gameUtils.getSkinByID(AnimsIDs.FLOOR1,AnimsIDs.IDLE,AnimsIDs.DR));
//		addChild(floor);
//		floor.x = -400;
//		floor.y = -100;

		addChild( _system );

		var dim:int = 12;//32/2;
		var dir:int = 1;
//		while(_count<10){
//			addMonster(randomIntInRange(-400,400),randomIntInRange(-400,400));
//			_count++;
//		}

		trace('objects',_count);
		speeds = [];
		for ( var i:int = 0; i < items.length; i++) {
			var len:Number = Math.floor(Math.random()*5+5)
			switch(items[i].dirId){
				case 'UL':
					speeds[i]=new Point(-len, 0);
					break;
				case 'UR':
					speeds[i]=new Point(0, -len);
					break;
				case 'DL':
					speeds[i]=new Point(0, len);
					break;
				case 'DR':
					speeds[i]=new Point(len, 0);
					break;

			}
//			speeds[i]=5;//Math.floor(Math.random()*40+5);

		}

		// Set up our initial frame.
		_system.moveCameraTo( 0, 0 );
		_system.draw();

//		if(showGrid){
//			addGridMark(0,0)
//		}

		// Add stats and label.
//		addChild( new Stats() );
	}

	public function renderGrid(w:int,h:int):void{

		for each (var isoAnimItem:IsoAnimItem in gridMarks) {
			isoAnimItem.destroy();
		}
		gridMarks.length= 0;

		for (var ix:int = 0; ix < h; ix++) {
			for (var iy:int = 0; iy < w; iy++) {
				if(ix==0 || ix ==h-1|| iy ==0||iy ==w-1) {
					addGridMark(ix, iy);					
				}
			}
		}
		update(0.33);
		update(0.33);
	}

	private function addGridMark(ix:int, iy:int):void {
		var colorId:String = _system.makeNewObjectId().toString();
		var isoItem:IsoAnimItem = new IsoAnimItem(String(itemId), colorId, 6, 6);
		isoItem.bd = new BitmapData(6, 6, true, 0xffff0000);
		isoItem.rect = new Rectangle(0, 0, 6, 6);
		isoItem.pt = new Point(0, 0);
		isoItem.offsetPt = new Point(-3, -3);
		isoItem.isoX = -ix*AppConsts.xGrid;
		isoItem.isoY = -iy*AppConsts.xGrid;
		isoItem.mouseEnabled = false;
		_system.addObject(isoItem, "grid");
		gridMarks.push(isoItem);
	}

	private function update(deltaTime:Number):void {
//		var currentTime:int = getTimer();
//		var deltaTime:Number = ( currentTime - _lastTimer ) * 0.001;
//		_lastTimer = currentTime;
		var mx:int = this.mouseX - w * .5;
		var my:int = this.mouseY - h * .5;
		_system.setMouseCull( mx, my );
		_system.onTick( deltaTime );
	}

	public function add3DBitmapWithSize(display:DisplayBlis, y:Number):void {
		var isoItem:IsoAnimItem;
		var frame:int = 0;
		itemId++;
		var colorId:String = _system.makeNewObjectId().toString();
//				var bd:BitmapData = new BitmapData(128,128,true,0xff000000+Math.random()*0xffffff);
//		var bd:BitmapData = new BitmapData(128, 128, true, 0);
		isoItem = new IsoAnimItem(String(itemId), colorId,display.bitmap.width,display.bitmap.height);
		isoItem.bd = display.bitmap;
		isoItem.rect = new Rectangle(0, 0, display.bitmap.width, display.bitmap.height);
		isoItem.pt = new Point(0, 0);
		isoItem.offsetPt.x = -display.offset.x;
		isoItem.offsetPt.y = -display.offset.y;
		isoItem.skinId = display.skinId;
		display.renderItem = isoItem;
//		isoItem.offsetPt = bitmapCont.
//				isoItem.isoX = i;// * 31 * 2;
//		isoItem.isoY = j;// * 31 * 2;
		items.push(isoItem);
		if(isoItem.skinId=='tileRed' || isoItem.skinId=='tileGreen')
			_system.addObject(isoItem, "obstacle_map");
		else
			_system.addObject(isoItem, y==-1? "background":(y==-0.5? "cursorLayer":"items"));

	}
	
	public function moveCameraTo(isoX:Number,isoY:Number):void {
		_system.moveCameraTo(isoY, isoX);
	}

	public function remove3DBitmap(renderItem:*):void {
		renderItem.destroy();
	}

	public function add3DBitmap(display:DisplayBlis, y:Number):void {
		var isoItem:IsoAnimItem;
		var frame:int = 0;
		itemId++;
		var colorId:String = _system.makeNewObjectId().toString();
//		var bd:BitmapData = new BitmapData(128,128,true,0xff000000+Math.random()*0xffffff);
//		var bd:BitmapData = new BitmapData(128, 128, true, 0);
		isoItem = new IsoAnimItem(String(itemId), colorId,display.bitmap.width,display.bitmap.height);
		isoItem.bd = display.bitmap;
		isoItem.rect = new Rectangle(0, 0, display.bitmap.width, display.bitmap.height);
		isoItem.pt = new Point(0, 0);
		isoItem.offsetPt.x = -display.offset.x;
		isoItem.offsetPt.y = -display.offset.y;
		isoItem.skinId = display.skinId;
		display.renderItem = isoItem;
//		isoItem.offsetPt = bitmapCont.
//				isoItem.isoX = i;// * 31 * 2;
//		isoItem.isoY = j;// * 31 * 2;
		items.push(isoItem);
		if(isoItem.skinId=='tileRed' || isoItem.skinId=='tileGreen')
			_system.addObject(isoItem, "obstacle_map");
		else
			_system.addObject(isoItem, y==-1? "background":(y==-0.5? "cursorLayer":"items"));
//		_system.addObject(isoItem, "items");
	}

	public function movePlaneToPoint(renderItem:IsoAnimItem, point3d:Point3d):void {
//		trace('movePlaneToPoint',renderItem,point3d.x, point3d.y,point3d.z)
		if(renderItem && point3d){
			renderItem.isoX = -point3d.x;
			renderItem.isoY = -point3d.z;
//			if(point3d.y ==600)
				renderItem.offsetY = point3d.y;
		}
	}

	public function render(time:Number):void {
		update(time);
	}

	public function getScreenCoords(disp:DisplayBlis):Point {
		(disp.renderItem as IsoAnimItem).calcScreenPos(_system.cameraX,_system.cameraY);
		return new Point((disp.renderItem as IsoAnimItem).pt.x,(disp.renderItem as IsoAnimItem).pt.y);
	}

	public function getScreenTo3DCoords(mouseX:Number, mouseY:Number, y:int):Point {
		return _system.screenToIso(mouseX,mouseY);
	}

	public function getCameraPos():Point {
		return new Point(_system.cameraX,_system.cameraY);
	}

	public function getCameraPosIso():Point {
		return getScreenTo3DCoords(_system.cameraX+w/2,_system.cameraY+h/2,0);
	}

	public function getObjectUnderMouse():IsoAnimItem {
		return _system.getObjectAt( mouseX, mouseY ) as IsoAnimItem;
	}
}
}

