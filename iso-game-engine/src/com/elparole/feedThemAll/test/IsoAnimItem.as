/**
 * User: Elparole
 * Date: 18.03.13
 * Time: 23:35
 */
package com.elparole.feedThemAll.test
{
import com.cenizal.blis.display.BlisDisplayObject;

import flash.display.BitmapData;
import flash.filters.GlowFilter;
import flash.geom.Point;
import flash.geom.Rectangle;

import org.casalib.math.geom.Point3d;

public class IsoAnimItem extends BlisDisplayObject
{

	public var skinId:String;
	public var animId:String;
	public var dirId:String;
	public var bd:BitmapData;
	public var rect:Rectangle;
	public var pt:Point;
	public var offsetPt:Point = new Point();
	public var offsetY:Number = 0;
	public var frame:Number = 0;
	public var totalFrames:int = 0;
	public var motion:Point3d;
	public var selected:Boolean = false;
	private var _overBD:BitmapData
	public var visible:Boolean = true;
	public var mouseEnabled:Boolean = true;

	public function IsoAnimItem(id:String, colorId:String,w:int = 0,h:int = 0) {
		_width = w;
		_height = h;
		super(id, colorId);
	}


	override public function draw(canvas:BitmapData, mouseCanvas:BitmapData, cameraX:Number, cameraY:Number,
	                              mouseCullX:int, mouseCullY:int, mouseCullX2:int, mouseCullY2:int):void {
		if(!visible)
			return;

		pt.x = x+cameraX+offsetPt.x;
		pt.y = y+cameraY+offsetPt.y+offsetY;

		canvas.copyPixels( selected? overBD:bd, rect, pt, null, null, true );
		if ( mouseEnabled && (x > mouseCullX )) {
			if ( x < mouseCullX2 ) {
				if ( y > mouseCullY ) {
					if ( y < mouseCullY2 ) {
						mouseCanvas.threshold( bd, rect, pt, ">", 0x00000000, color, 0xFFFFFF, false );
					}
				}
			}
		}
	}

	public function calcScreenPos(cameraX:Number, cameraY:Number):void{
		pt.x = x+cameraX+offsetPt.x;
		pt.y = y+cameraY+offsetPt.y+offsetY;
	}
	public function get overBD():BitmapData {
		if(!_overBD)
			drawSelectedBD();
		return _overBD;
	}

	public function drawSelectedBD():void {
		_overBD = new BitmapData( bd.width, bd.height );
		_overBD.applyFilter( bd, bd.rect, new Point(), new GlowFilter( 0xFF0000, 1, 10, 10, 40, 1, false, false ) );

	}

//	override public function draw( canvas:BitmapData, mouseCanvas:BitmapData, cameraX:Number, cameraY:Number,
//	                               mouseCullX:int, mouseCullY:int, mouseCullX2:int, mouseCullY2:int ):void {
//		var rect:Rectangle = _spriteSheet.getFrameRect();
//		var point:Point = _spriteSheet.getFramePoint( x, y, cameraX, cameraY );
//		if ( _alpha > 0 ) {
//			if ( isOver ) {
//				if ( _alpha < 1 ) {
//					canvas.copyPixels( _overBitmapData, rect, point, _alphaData, _alphaPoint, true );
//				} else {
//					canvas.copyPixels( _overBitmapData, rect, point, null, null, true );
//				}
//			} else {
//				if ( _alpha < 1 ) {
//					canvas.copyPixels( _bitmapData, rect, point, _alphaData, _alphaPoint, true );
//				} else {
//					canvas.copyPixels( _bitmapData, rect, point, null, null, true );
//				}
//			}
//			if ( x > mouseCullX ) {
//				if ( x < mouseCullX2 ) {
//					if ( y > mouseCullY ) {
//						if ( y < mouseCullY2 ) {
//							mouseCanvas.threshold( _bitmapData, rect, point, ">", 0x00000000, color, 0xFFFFFF, false );
//						}
//					}
//				}
//			}
//		}
//	}
}
}
