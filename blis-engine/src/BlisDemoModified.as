package
{
	import com.cenizal.blis.BlisSystem;
	import com.cenizal.blis.display.IBlisDisplayObject;
	import com.cenizal.blisDemo.Background;
	import com.cenizal.blisDemo.Buildings;
	import com.cenizal.blisDemo.IGameController;
	import com.cenizal.blisDemo.IIsoBuilding;
	import com.cenizal.blisDemo.Resources;
	import com.cenizal.blisDemo.basic.GameController;
import com.cenizal.blisDemo.basic.IsoBuilding;
import com.cenizal.ui.Label;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.utils.getTimer;
	
	import net.hires.debug.Stats;

	[SWF(width=800, height=600, frameRate=60)]
	public class BlisDemoModified extends Sprite
	{
		// Isometric system.
		private var _system:BlisSystem;
		
		// Dragging.
		private var _prevX:int = 0;
		private var _prevY:int = 0;
		
		// Animation.
		private var _lastTimer:int = 0;
		
		// Interaction.
		private var _isDraggingMode:Boolean = false;
		private var _overObject:IIsoBuilding;
		private var _draggingObject:IIsoBuilding;
		private var _isMouseDown:Boolean = false;
		
		// UI.
		private var _label:Label;
		
		// Objects.
		private var _buildings:Buildings;
		private var _gameController:IGameController;
		private var speeds:Array;
		private var movingBuildings:Array;
		
		public function BlisDemoModified() {
			super();
			_buildings = new Buildings();
			addEventListener( Event.ADDED_TO_STAGE, onAdded );
		}
		
		private function onAdded( e:Event ):void {
			this.mouseEnabled = this.mouseChildren = false;
			removeEventListener( Event.ADDED_TO_STAGE, onAdded );
			
			// Set our stage.
			stage.frameRate = 60;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			// Set up Blis.
			_system = new BlisSystem( stage.stageWidth, stage.stageHeight, 2002, 1402 );
			addChild( _system );
//			_system.addLayer( "bg" );
//			_system.addLayer( "shadows" );
			_system.addLayer( "buildings" );
			
			// Add a boring gray background.
//			var bg:Background = new Background( 2000, 1400 );
//			bg.isoX = bg.isoY = 0;
//			_system.addObject( bg, "bg" );
			
			// Set up our bitmap data for the blocks.
			var spriteSheet:BitmapData = ( new Resources.BuildingImage() as Bitmap ).bitmapData;
			var overSpriteSheet:BitmapData = new BitmapData( spriteSheet.width, spriteSheet.height );
			overSpriteSheet.applyFilter( spriteSheet, spriteSheet.rect, new Point(), new GlowFilter( 0xFF0000, 1, 10, 10, 40, 1, false, false ) );
			
			_gameController = new GameController( _system, spriteSheet, overSpriteSheet, _buildings );
			_gameController.initialize();
			
			// Set up our initial frame.
			_system.moveCameraTo( 0, 0 );
			_system.draw();
			
			// Add stats and label.
			addChild( new Stats() );
//			_label = new Label( this, 400, 100, "Currently showing " + _gameController.count.toString() + " blocks."
//				+ " Click on an empty area to rearrange the blocks."
//				+ " Click a block to select it."
//				+ " Click and drag to move the map."
//				+ " Mouse wheel zooms."
//				+ " Hit tab to see color coding of the game objects." );
//			_label.x = 10;
//			_label.y = stage.stageHeight - _label.height - 10;
			
			// Event handlers.
			stage.addEventListener( Event.ENTER_FRAME, onTick );
//			stage.addEventListener( Event.RESIZE, onResize );
			speeds = [];
			for (var i:int = 0; i < _buildings.count; i++) {
				speeds[i]=Math.floor(Math.random()*40+5);

			}
			this.mouseEnabled = this.mouseChildren = false;
//			stage.addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
//			stage.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
//			stage.addEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
//			stage.addEventListener( MouseEvent.MOUSE_WHEEL, onMouseWheel );
//			stage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
//			stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
		}
		
		private function onMouseDown( e:MouseEvent ):void {
			_isMouseDown = true;
			_prevX = mouseX;
			_prevY = mouseY;
		}

		var camPosX:Number = 0;
		
		private function onTick( e:Event = null ):void {
			var currentTime:int = getTimer();
			var deltaTime:Number = ( currentTime - _lastTimer ) * 0.001;
			_lastTimer = currentTime;
			_system.onTick( deltaTime );

			_system.moveCameraTo(camPosX,0);
			camPosX+=50;
			if(camPosX>1000)
				camPosX = -400;

			if(Math.floor(currentTime/1000)%1==0){
				var len:int
				if(!movingBuildings){
					movingBuildings = [];
					len = _buildings.count;
					for ( var j:int = 0; j < len; j++ ) {
						movingBuildings.push(_buildings.getAt( j ).view);
					}
			}
				len = _buildings.count;
				for ( var i:int = 0; i < len; i++ ) {
					var destix:int = movingBuildings[i].isoX+speeds[i]*deltaTime*10;
//					if((_buildings.getAt( i ).view as IsoBuilding).alpha ==0 &&
//							(_buildings.getAt( i ).view as IsoBuilding).isoX<-900)
//						(_buildings.getAt( i ).view as IsoBuilding).alpha = 1;

					if(destix>500){
						destix = -500;
//						trace((_buildings.getAt( i ).view as IsoBuilding).alpha);// = 0;
//						(_buildings.getAt( i ).view as IsoBuilding).alpha = 0;// = 0;
					}
//					_buildings.getAt( i ).view.setTarget( destix,_buildings.getAt( i ).view.isoY);
//					_buildings.getAt( i ).view.setTarget( destix,_buildings.getAt( i ).view.isoY);


					movingBuildings[i].isoX = destix;

//					_buildings.getAt( i ).view.setNewTarget( 1200/2 );
				}
			}
		}
		
//		private function onResize( e:Event ):void {
//			_system.setCameraSize( stage.stageWidth, stage.stageHeight );
//			_label.y = stage.stageHeight - _label.height - 10;
//		}

	}
}
