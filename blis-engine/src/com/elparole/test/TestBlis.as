/**
 * User: Elparole
 * Date: 18.03.13
 * Time: 18:40
 */
package com.elparole.test
{
import com.bit101.components.CheckBox;
import com.bit101.components.NumericStepper;
import com.cenizal.blis.BlisSystem;
import com.cenizal.blis.display.BlisDisplayObject;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.SpriteSheetConfigParser;
import com.elparole.feedThemAll.test.IsoAnimItem;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.randomIntInRange;
import com.elparole.randomValFromArr;

import flash.display.Bitmap;

import flash.display.BitmapData;
import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.globalization.NationalDigitsType;
import flash.utils.getTimer;

import net.hires.debug.Stats;

import org.casalib.math.geom.Point3d;

[SWF(width=800, height=600, frameRate=30)]
public class TestBlis extends Sprite

{
	private var _system:BlisSystem;
	private var gameUtils:GameUtils = new GameUtils();
	private var _lastTimer:int = 0;
	private var items:Array = [];
	private var _count:int;
	private var speeds:Array;
	private var hearts:Array = [];
	private var stepper:NumericStepper;
	private var selHearts:CheckBox;

	public function TestBlis() {
		addEventListener(Event.ADDED_TO_STAGE, onAdded);
	}

	private function onAdded(event:Event):void {
		this.mouseEnabled = this.mouseChildren = false;
		removeEventListener( Event.ADDED_TO_STAGE, onAdded );

		gameUtils.parser = new SpriteSheetConfigParser();
		gameUtils.config = gameUtils.parser.parseConfig(gameUtils.config);

		// Set our stage.
		stage.frameRate = 30;
		stage.scaleMode = StageScaleMode.NO_SCALE;
		stage.align = StageAlign.TOP_LEFT;
		
		stepper = new NumericStepper(stage, 200,0, onStep);
		stepper.minimum = 100;
		stepper.step = 100;
//		selHearts = new CheckBox(stage, 300,0,'hearts');
//		selHearts.selected = false;

		// Set up Blis.
		_system = new BlisSystem( stage.stageWidth, stage.stageHeight, 2002, 1402 );
//			_system.addLayer( "bg" );
//			_system.addLayer( "shadows" );
		_system.addLayer( "items" );
		
//		var spriteSheet:BitmapData = ( new Resources.BuildingImage() as Bitmap ).bitmapData;

		var floor:Bitmap = new Bitmap(gameUtils.getSkinByID(AnimsIDs.FLOOR1,AnimsIDs.IDLE,AnimsIDs.DR));
		addChild(floor);
		floor.x = -400;
		floor.y = -100;
		addChild( _system );

		var dim:int = 12;//32/2;
		var dir:int = 1;
		while(_count<100){
			addMonster(randomIntInRange(-400,400),randomIntInRange(-400,400));
			_count++;
		}

		trace('objects',_count);
		speeds = [];
		for ( var i:int = 0; i < items.length; i++) {
			var len:Number = Math.floor(Math.random()*5+5)
			switch(items[i].dirId){
				case 'UL':
					speeds[i]=new Point(-len, 0);
					break;
				case 'UR':
					speeds[i]=new Point(0, -len);
					break;
				case 'DL':
					speeds[i]=new Point(0, len);
					break;
				case 'DR':
					speeds[i]=new Point(len, 0);
					break;
					 
			}
//			speeds[i]=5;//Math.floor(Math.random()*40+5);

		}

		// Set up our initial frame.
		_system.moveCameraTo( 0, 0 );
		_system.draw();

		// Add stats and label.
		addChild( new Stats() );

		stage.addEventListener(Event.ENTER_FRAME, update)
	}

	private function addMonster(i:Number, j:Number):void {
		var isoItem:IsoAnimItem;
		var frame:int = 0;
		var colorId:String = _system.makeNewObjectId().toString();
//				var bd:BitmapData = new BitmapData(128,128,true,0xff000000+Math.random()*0xffffff);
		var bd:BitmapData = new BitmapData(128, 128, true, 0);
		bd.draw(gameUtils.getSkinByID(AnimsIDs.MONSTER, AnimsIDs.WALK, AnimsIDs.DR));
		isoItem = new IsoAnimItem(String(i), colorId);
		isoItem.skinId = AnimsIDs.MONSTER;
		isoItem.animId = AnimsIDs.WALK;
		isoItem.dirId = randomValFromArr(['UR', 'UL', 'DR', 'DL']);
//				isoItem.totalFrames = gameUtils.getAnim(AnimsIDs.MONSTER,AnimsIDs.WALK,AnimsIDs.DR).totalFrames;
//				isoItem.totalFrames = gameUtils.getAnim(AnimsIDs.MONSTER,AnimsIDs.WALK,AnimsIDs.DR).totalFrames;
		isoItem.totalFrames = gameUtils.getAnim(AnimsIDs.MONSTER, AnimsIDs.WALK, isoItem.dirId).totalFrames;
		isoItem.frame = randomIntInRange(0, isoItem.totalFrames);
		isoItem.bd = bd;
		isoItem.rect = new Rectangle(0, 0, 128, 128);
		isoItem.pt = new Point(0, 0);
		isoItem.offsetPt = gameUtils.getSkinOffset(AnimsIDs.MONSTER, AnimsIDs.WALK, AnimsIDs.DR)
		isoItem.isoX = i;// * 31 * 2;
		isoItem.isoY = j;// * 31 * 2;
		items.push(isoItem);
		_system.addObject(isoItem, "items");
	}

	private function onStep(...args):void {
		while(_count<stepper.value){
			addMonster(randomIntInRange(-400,400),randomIntInRange(-400,400));
			_count++;
		}

		while(_count>stepper.value){
			items.pop().destroy();
			_count--;
		}

		for ( var i:int = 0; i < items.length; i++) {
			var len:Number = Math.floor(Math.random()*5+5)
			switch(items[i].dirId){
				case 'UL':
					speeds[i]=new Point(-len, 0);
					break;
				case 'UR':
					speeds[i]=new Point(0, -len);
					break;
				case 'DL':
					speeds[i]=new Point(0, len);
					break;
				case 'DR':
					speeds[i]=new Point(len, 0);
					break;

			}
//			speeds[i]=5;//Math.floor(Math.random()*40+5);

		}
	}

	private function update(event:Event):void {
		var currentTime:int = getTimer();
		var deltaTime:Number = ( currentTime - _lastTimer ) * 0.001;
		_lastTimer = currentTime;
		_system.onTick( deltaTime );
		for ( var i:int = 0; i < items.length; i++ ) {
			items[i].frame = (items[i].frame+speeds[i].length*deltaTime*4)%items[i].totalFrames;
			items[i].bd = gameUtils.getSkinByID(AnimsIDs.MONSTER,AnimsIDs.WALK,items[i].dirId,Math.floor(items[i].frame));
			items[i].offsetPt = gameUtils.getSkinOffset(AnimsIDs.MONSTER,AnimsIDs.WALK,items[i].dirId,Math.floor(items[i].frame));
		}
		
		for ( var i:int = 0; i < hearts.length; i++ ) {
			hearts[i].bd = gameUtils.getSkinByID(hearts[i].skinId,hearts[i].animId,hearts[i].dirId,Math.floor(hearts[i].frame));
			hearts[i].offsetPt = gameUtils.getSkinOffset(hearts[i].skinId,hearts[i].animId,hearts[i].dirId,Math.floor(hearts[i].frame));
		}

//		if(selHearts.selected && Math.floor(currentTime/50)%20==0){
//			trace('exp');
//			addExplosion(randomIntInRange(-400,400),randomIntInRange(-400,400));
//		}
		
		if(Math.floor(currentTime/100)%1==0){
//			trace('speeds[i]*deltaTime*0.01',speeds[0]*deltaTime*10);
			for ( i = 0; i < hearts.length; i++ ) {
				if(hearts[i].offsetY<0){
					var destix:Number = hearts[i].isoX+hearts[i].motion.x*deltaTime*5;
					var destiz:Number = hearts[i].isoY+hearts[i].motion.z*deltaTime*5;
					var destiy:Number = hearts[i].offsetY+hearts[i].motion.y*deltaTime*5;


					hearts[i].isoX = destix;
					hearts[i].isoY = destiz;
					hearts[i].offsetY = destiy;
					hearts[i].motion.y+=120*2*deltaTime;
				}else{
					(hearts[i] as BlisDisplayObject).destroy();
					hearts.splice(i, 1);
					i--;
				}

//					_items.getAt( i ).view.setNewTarget( 1200/2 );
			}
			
			for ( i = 0; i < items.length; i++ ) {
				var destix:Number = items[i].isoX+speeds[i].x*deltaTime*10;
				var destiy:Number = items[i].isoY+speeds[i].y*deltaTime*10;

				if(destix>500){
					destix = -600;
				}else if(destix<-600){
					destix = 500;
				}
				if(destiy>500){
					destiy = -600;
				}else if(destiy<-600){
					destiy = 500;
				}

				items[i].isoX = destix;
				items[i].isoY = destiy;

//					_items.getAt( i ).view.setNewTarget( 1200/2 );
			}
		}
	}

	private function addExplosion(x:int, y:int):void {
		var isoItem:IsoAnimItem;
		for ( var i:Number = 0; i <4; i++ ) {
				var frame:int = 0;
				var colorId:String = _system.makeNewObjectId().toString();
//				var bd:BitmapData = new BitmapData(128,128,true,0xff000000+Math.random()*0xffffff);
				var bd:BitmapData = new BitmapData(128,128,true,0);
				bd.draw(gameUtils.getSkinByID(AnimsIDs.MONSTER,AnimsIDs.WALK,AnimsIDs.DR));
				isoItem = new IsoAnimItem(String(i), colorId);
				isoItem.skinId = AnimsIDs.HEART;
				isoItem.animId = AnimsIDs.IDLE;
				isoItem.dirId = 'DR';
//				isoItem.totalFrames = gameUtils.getAnim(AnimsIDs.MONSTER,AnimsIDs.WALK,AnimsIDs.DR).totalFrames;
//				isoItem.totalFrames = gameUtils.getAnim(AnimsIDs.MONSTER,AnimsIDs.WALK,AnimsIDs.DR).totalFrames;
				isoItem.totalFrames = 1;//gameUtils.getAnim(AnimsIDs.HEART,AnimsIDs.IDLE,isoItem.dirId).totalFrames;
				isoItem.frame = randomIntInRange(0,isoItem.totalFrames);
				isoItem.bd = bd;
				isoItem.rect = new Rectangle(0,0,128,128);
				isoItem.pt = new Point(0,0);
				isoItem.offsetPt = gameUtils.getSkinOffset(AnimsIDs.MONSTER,AnimsIDs.WALK,AnimsIDs.DR)
				isoItem.isoX = x;
				isoItem.isoY = y;
				isoItem.offsetY = -0.1;
				isoItem.motion = new Point3d(randomIntInRange(-45,45),-120,randomIntInRange(-45,45));
				hearts.push(isoItem);
				_system.addObject( isoItem, "items" );
		}
	}
}
}
