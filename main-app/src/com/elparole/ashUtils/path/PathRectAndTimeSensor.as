/**
 * User: Elparole
 * Date: 06.09.12
 * Time: 13:57
 */
package com.elparole.ashUtils.path
{

import flash.geom.Rectangle;

public class PathRectAndTimeSensor implements IEnemyPathSensor
{

	private var rect:Rectangle;
	private var time:int

	public function PathRectAndTimeSensor(rect:Rectangle, time:int) {
		this.rect= rect;
		this.time = time;
	}

	public function check(epn:EnemyVelPathNode, time:int):Boolean {
		if(epn.path.time <= 0 || !rect.containsPoint(epn.position.position)){
			epn.path.time = this.time;
			return true;
		}

		return false;
	}
}
}
