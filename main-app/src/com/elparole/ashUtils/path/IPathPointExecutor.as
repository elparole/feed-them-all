/**
 * User: Elparole
 * Date: 06.09.12
 * Time: 12:43
 */
package com.elparole.ashUtils.path
{
public interface IPathPointExecutor
{
	function execute(enemyPathNode:EnemyVelPathNode, time:int):Boolean;
}
}
