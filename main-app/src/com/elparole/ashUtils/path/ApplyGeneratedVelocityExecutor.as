/**
 * User: Elparole
 * Date: 06.09.12
 * Time: 12:48
 */
package com.elparole.ashUtils.path
{
import com.elparole.feedThemAll.model.GameUtils;

import flash.geom.Point;

public class ApplyGeneratedVelocityExecutor implements IPathPointExecutor
{

	private var speed:Number;
	private var node:EnemyVelPathNode;


	public function execute(enemyPathNode:EnemyVelPathNode, time:int):Boolean {
		enemyPathNode.entity.add(new PlanePositionPicker(this));
		enemyPathNode.path.getNextPt();
		return true;
	}

	public function executeToPos(pos:Point):void {
		var posa:Point = node.position.position;
		var dist:Number =  GameUtils.distancePt(pos, posa);
		
		node.motion.velocity.x = 0;//speed * (pos.x - posa.x)/dist;
		node.motion.velocity.y = 0;//speed * (pos.y - posa.y)/dist;
	}

	public function ApplyGeneratedVelocityExecutor(speed:Number) {
		this.speed = speed;
	}
}
}
