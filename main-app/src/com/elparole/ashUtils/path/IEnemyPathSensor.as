/**
 * User: Elparole
 * Date: 06.09.12
 * Time: 12:43
 */
package com.elparole.ashUtils.path
{
public interface IEnemyPathSensor
{
	function check(epn:EnemyVelPathNode, time:int):Boolean;
}
}
