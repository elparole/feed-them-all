/**
 * User: Elparole
 * Date: 06.09.12
 * Time: 13:07
 */
package com.elparole.ashUtils.path
{
public class EnemyPathTimeSensor implements IEnemyPathSensor
{

	private var time:int


	public function EnemyPathTimeSensor(time:int) {
		this.time = time;
	}

	public function check(epn:EnemyVelPathNode, time:int):Boolean {
		if(epn.path.time <= 0){
			epn.path.time = this.time;
			return true;
		}

		return false;
	}
}
}
