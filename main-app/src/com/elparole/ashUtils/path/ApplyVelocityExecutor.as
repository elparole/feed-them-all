/**
 * User: Elparole
 * Date: 06.09.12
 * Time: 12:48
 */
package com.elparole.ashUtils.path
{
public class ApplyVelocityExecutor implements IPathPointExecutor
{

	public var vx:Number;
	public var vy:Number;


	public function execute(enemyPathNode:EnemyVelPathNode, time:int):Boolean {
		enemyPathNode.motion.velocity.x = vx;
		enemyPathNode.motion.velocity.y = vy;
		enemyPathNode.path.getNextPt();
		return true;
	}

	public function ApplyVelocityExecutor(vx:Number, vy:Number) {
		this.vx = vx;
		this.vy = vy;
	}
}
}
