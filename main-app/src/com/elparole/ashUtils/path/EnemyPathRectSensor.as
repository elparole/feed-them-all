/**
 * User: Elparole
 * Date: 06.09.12
 * Time: 13:57
 */
package com.elparole.ashUtils.path
{

import flash.geom.Rectangle;

public class EnemyPathRectSensor implements IEnemyPathSensor
{
	public var rect:Rectangle;

	public function EnemyPathRectSensor(rect:Rectangle) {
		this.rect= rect;
	}

	public function check(epn:EnemyVelPathNode, time:int):Boolean {
		return rect.containsPoint(epn.position.position);
	}
}
}
