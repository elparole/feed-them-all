package com.elparole.levelEditor.service
{

import com.elparole.feedThemAll.model.LevelEmbedd;
import com.elparole.feedThemAll.model.vo.ChildVO;
import com.elparole.levelEditor.model.vo.GameItemConfig;
import com.elparole.levelEditor.model.vo.LevelConfig;

import flash.utils.setTimeout;

import org.casalib.math.geom.Point3d;
import org.robotlegs.oil.async.Promise;

public class LevelsEmbeddedDataService implements ILevelService
	{
//		[Inject]
//		public var logger:ILogger;

		private var prom:Promise;
		private var _currentLevelName:String;

		private var levelsObj:LevelEmbedd = new LevelEmbedd();
		private var configName:String;

		public function LevelsEmbeddedDataService() {
			levelsObj.parse();
		}

		public function loadConfig(name:String, path:String):Promise {
			prom = new Promise();
			configName = name;
			setTimeout(onLoadConfig,1);
			return prom;
		}

		public function loadConfigByNR(nr:int):Promise {
			prom = new Promise();
			configName = String(nr);
			setTimeout(onLoadConfig,1);
			return prom;
		}
	
		private function onLoadConfig():void {

			var lc:LevelConfig = LevelConfig.fromObj(levelsObj.config.levels[int(configName)]);
			lc.index = int(configName);
			var itemOnPosVos:Array = [];

			for each (var itemObj:Object in lc.floorItems) {
//			ensureItemVOExists(itemObj.skinId+'_'+itemObj.dirId, itemObj);
				itemOnPosVos.push(new GameItemConfig(
						itemObj.skinId,
						itemObj.animId,
						itemObj.dirId,
						new Point3d(itemObj.posX,itemObj.posY,itemObj.posZ)));
			}
			lc.floorItems = itemOnPosVos;

			itemOnPosVos = [];
			for each (itemObj in lc.items) {
				itemOnPosVos.push(new GameItemConfig(
						itemObj.skinId,
						itemObj.animId,
						itemObj.dirId,
						new Point3d(itemObj.posX,itemObj.posY,itemObj.posZ)));
			}
			lc.items = itemOnPosVos;

			itemOnPosVos = [];
			for each (itemObj in lc.childrenList) {
				itemOnPosVos.push(new ChildVO(
						itemObj.skinId,
						itemObj.startTime,
						itemObj.startX,
						itemObj.startY));
			}
			itemOnPosVos.sortOn('startTime',Array.NUMERIC);
			lc.childrenList = itemOnPosVos;

			prom.handleResult(lc);
		}

		public function get currentLevelName():String {
			return _currentLevelName;
		}

		public function loadLevelsList(path:String):Promise {
			prom = new Promise();
			return prom;
		}


		public function hasLevelNR(nr:int):Boolean {
			return levelsObj.config.levels.length>nr && (levelsObj.config.levels[nr] != null);
		}
}
}
