/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 23:40
 */
package com.elparole.feedThemAll.model
{
import com.elparole.feedThemAll.PublishMode;

import flash.utils.Dictionary;

import org.osflash.signals.Signal;

public class PlayerConfig
{
	public var weaponHeatRate:Number = 11;//22;
	public var temp:Number = 0;
	public var gunEnabled:Boolean = true;

	public var meat1Skill:int = 0;
	public var meat2Skill:int = 0;
	public var meat3Skill:int = 0;
	public var meat4Skill:int = 0;
	public var meat5Skill:int = 0;
	public var meat6Skill:int = 0;

	public const meat1SkillMax:int = 3;
	public const meat2SkillMax:int = 3;
	public const meat3SkillMax:int = 3;
	public const meat4SkillMax:int = 3;
	public const meat5SkillMax:int = 3;
	public const meat6SkillMax:int = 1;

	public var coins:int = 1000;
	public const skillBuyed:Signal = new Signal();
	public var healthSkill:int;
	public var id:int

	public function PlayerConfig(id:int = 1) {
		this.id = id;
		coins = PublishMode.devMode? 1000:0;
	}
	
	public function clone():PlayerConfig{
		var cl:PlayerConfig = new PlayerConfig(id);
		copyDataTo(cl);
		return cl;
	}

	private function copyDataTo(cl:PlayerConfig):void {
		cl.weaponHeatRate = weaponHeatRate;
		cl.temp = temp;
		cl.gunEnabled = gunEnabled;

		cl.meat1Skill = meat1Skill;
		cl.meat2Skill = meat2Skill;
		cl.meat3Skill = meat3Skill;
		cl.meat4Skill = meat4Skill;
		cl.meat5Skill = meat5Skill;
		cl.meat6Skill = meat6Skill;
		cl.coins = coins;
		cl.healthSkill = healthSkill;
		cl.id = id;
	}

	public function buy(s:String, prizes:Dictionary, target:int = -1):void {
		var currentVal:int = this[s+'Skill'];
		var targetVal:int = (target>0? target:currentVal+1);
		if(coins>=prizes[s+targetVal]){
			coins -=prizes[s+targetVal];
			this[s+'Skill'] = targetVal;
		}
		skillBuyed.dispatch();
	}

	[PreDestroy]
	public function preDestroy():void{
		skillBuyed && skillBuyed.removeAll();
	}

	public function getWeaponsSkills():Array {
		return [meat1Skill,meat2Skill,meat3Skill,meat4Skill,meat5Skill,meat6Skill];
	}
}
}
