/**
 * User: Elparole
 * Date: 16.04.13
 * Time: 13:47
 */
package com.elparole.feedThemAll.model.vo
{
public class MeatWeaponVO
{
	public var id:String = "meat1";
	public var reloadDuration:Number = 15;
	public var stayInGameDuration:Number = 2000;
	public var yummies:int = 1;
	public var percLoaded:Number = 0;
	public var enabled:Boolean = false;
	public var exploding:Boolean = false;
	public var range:Number = 10;


	public function MeatWeaponVO(id:String, reloadSpeed:Number, explode:Boolean = false) {
		this.id = id;
		this.reloadDuration = reloadSpeed;
		this.exploding = explode;
	}
}
}
