/**
 * User: Elparole
 * Date: 07.02.13
 * Time: 14:33
 */
package com.elparole.feedThemAll.model
{
import ash.core.Entity;

import com.blackmoondev.ashes.script.components.ScriptsHolderComponent;
import com.blackmoondev.ashes.script.impl.ExecuteOnGuardScript;
import com.blackmoondev.ashes.script.impl.ExecuteScript;
import com.blackmoondev.ashes.script.vo.IScript;
import com.blackmoondev.ashes.script.vo.ScriptList;
import com.blackmoondev.geom.IntPoint;
import com.elparole.components.DisplayBlis;
import com.elparole.components.Motion3d;
import com.elparole.components.Position;
import com.elparole.feedThemAll.AppConsts;
import com.elparole.feedThemAll.components.AddEntity;
import com.elparole.feedThemAll.components.Anim;
import com.elparole.feedThemAll.components.BadCreature;
import com.elparole.feedThemAll.components.CollisionSphere;
import com.elparole.feedThemAll.components.Destinations;
import com.elparole.feedThemAll.components.Direction;
import com.elparole.feedThemAll.components.GridPosition;
import com.elparole.feedThemAll.components.MarkedToRemove;
import com.elparole.feedThemAll.components.PathComponent;
import com.elparole.feedThemAll.components.RedrawToBackground;
import com.elparole.feedThemAll.components.Scalable;
import com.elparole.feedThemAll.controller.BadCreatureStatesDefCreator;
import com.elparole.feedThemAll.model.ids.StatesIds;
import com.elparole.feedThemAll.test.PathActionConfig;
import com.elparole.feedThemAll.test.TestPathGenerator;
import com.elparole.feedThemAll.view.AnimsIDs;

import flash.geom.Point;
import flash.geom.Rectangle;

public class ScriptGenerator
{
	[Inject]
	public var gameUtils:GameUtils;

	[Inject]
	public var gameState:GameState;

	[Inject]
	public var creaturesCreator:BadCreatureStatesDefCreator;

	[Inject]
	public var mapModel:MapModel;

	public function ScriptGenerator() {
	}

	public function generateScriptListFromPathArray(path:Array):ScriptList {
		var sl:ScriptList = new ScriptList();
		if(path.length==0)
			return sl;

		sl.addScript(new ExecuteScript(new PathAction(0,path[0].destDir,path[0].velocityLength), true));

//		var lastDest:Point = new Point(path[0].destPosX, path[0].destPosY );

//		path.splice(0,1);
		var next:Point = new Point(path[0].destPosX,path[0].destPosY);

		for (var i:int = 1; i < path.length; i++) {
			var pathElement:PathActionConfig = path[i];

			next.x = path[i-1].destPosX;
			next.y= path[i-1].destPosY;

			sl.addScript(new ExecuteOnGuardScript(
					new PositionInRectGuard(i,
							new Rectangle(
									next.x-5,
									next.y-5,
									10,
									10)),
					new PathAction(i,pathElement.destDir,pathElement.velocityLength),
					true));

		}
		return sl;
	}

	public function stopAndPlayEat():IScript {
		var es:ExecuteScript = new ExecuteScript(stopAndPlayEatCommand,true);
		return es;
	}

	private function stopAndPlayEatCommand(entity:Entity,time:Number):* {
		trace('stopAndPlayEatCommand');
		var fpc:PathComponent = entity.get(PathComponent);
		if(fpc){
			trace('fail');
			entity.remove(PathComponent);
		}
		var anim:Anim = entity.get(Anim);
		var disp:DisplayBlis = entity.get(DisplayBlis);
		var creature:BadCreature = entity.get(BadCreature);
		var direction:Direction = entity.get(Direction);
		var puddle:AddEntity = new AddEntity(creaturesCreator.getPuddle(entity));
		switch(disp.dirId){
			case 'UR':
				puddle.entity.add(new Direction(0));
				break;
			case 'UL':
				puddle.entity.add(new Direction(1));
				break;
			case 'DL':
				puddle.entity.add(new Direction(2));
				break;
			case 'DR':
				puddle.entity.add(new Direction(3));
				break;

		}
		entity.add(puddle);
//		disp.dirId = 'DR';
		anim.animationID = AnimsIDs.EAT;
		anim.animEnded = false;
		anim.loopsCounter = 0;
		anim.frameNum = 0;
		anim.totalFrames = 14;//gameUtils.getTotalFrames(disp.skinId,
//				disp.animId, disp.dirId);
		disp.animId = AnimsIDs.EAT;
		disp.bitmap = gameUtils.getSkinByID(disp.skinId, disp.animId, disp.dirId, 0);
		disp.setOffset(gameUtils.getSkinOffset(disp.skinId, disp.animId, disp.dirId, 0));
		var motion:Motion3d = entity.get(Motion3d);
		motion.velocity.x = 0;
		motion.velocity.y = 0;
		motion.velocity.z = 0;
		if(creature.meatImEatingNow == AnimsIDs.SHEEP_BOMB_MEAT){
			creature.eaten+=30;
		}else if(creature.meatImEatingNow == AnimsIDs.MEAT2){
			creature.eaten+=2;
		}else
			creature.eaten++;
	}

	public function getGrowPuddleScript():IScript {
		var es:ExecuteScript = new ExecuteScript(growPuddle,false);
		return es;
	}

	public function getGoHomeScript():IScript {
		var es:ExecuteScript = new ExecuteScript(goHomePathComp,false);
		return es;
	}

	public function goHomePathComp(entity:Entity, time:Number):void {

		var bc:BadCreature = entity.get(BadCreature);
		if(entity.get(PathComponent)||bc.sc.fsm.currentStateId != StatesIds.GO_HOME)
			return;

		var dest:Destinations = entity.get(Destinations);
		
		createProperPath(entity, new Point(
				dest.startPos.x * AppConsts.xGrid,
				dest.startPos.y * AppConsts.zGrid));
	}

	private function createProperPath(entity:Entity, destPoint:Point):void {
		var pc:PathComponent =
				gameUtils.getPathComponent(
						entity.get(Position).position.x,
						entity.get(Position).position.z,
						destPoint.x,
						destPoint.y,
						mapModel);
//		pc.pathPts.shift();
		entity.add(pc);

		var anim:Anim = entity.get(Anim);
		var disp:DisplayBlis = entity.get(DisplayBlis);
		anim.animationID = AnimsIDs.WALK;
		anim.animEnded = false;
		anim.frameNum = 0;
		disp.animId = AnimsIDs.WALK;
		anim.totalFrames = gameUtils.getTotalFrames(disp.skinId,
				disp.animId, disp.dirId);
		disp.bitmap = gameUtils.getSkinByID(disp.skinId, disp.animId, disp.dirId, 0);
		disp.offset = gameUtils.getSkinOffset(disp.skinId, disp.animId, disp.dirId, 0);

		if ((entity.get(BadCreature) as BadCreature).meatImEatingNow == AnimsIDs.FROZEN_MEAT) {
			entity.add(new GridPosition(
					Math.round(entity.get(Position).position.x / AppConsts.xGrid),
					Math.round(entity.get(Position).position.z / AppConsts.zGrid)));

			//		var pathComp:PathComponent = entity.get(PathComponent);

			var cs:CollisionSphere = entity.get(CollisionSphere);
			cs.radius = 20;

			pc.timeToWait = 4;
			pc.timeElapsed = 0;
			disp.animId = AnimsIDs.IDLE + AnimsIDs.FROZEN;
			anim.animationID = AnimsIDs.IDLE + AnimsIDs.FROZEN;
			anim.totalFrames = gameUtils.getTotalFrames(disp.skinId,
					disp.animId, disp.dirId);
			disp.bitmap = gameUtils.getSkinByID(disp.skinId, disp.animId, disp.dirId, 0);
			disp.offset = gameUtils.getSkinOffset(disp.skinId, disp.animId, disp.dirId, 0);
//			setIdleAnimation(entity,time);
		}

//		var shc:ScriptsHolderComponent = entity.get(ScriptsHolderComponent);
//		entity.remove(ScriptsHolderComponent);
	}

	public function goHome(entity:Entity, time:Number):void {
//		trace('go home script');
		var position:Position = entity.get(Position);
		var destination:Destinations = entity.get(Destinations);
//		var anim:Anim = entity.get(Anim);
//		var disp:Display = entity.get(Display);
//		anim.animationID = AnimsIDs.WALK;
//		anim.animEnded = false;
//		anim.frameNum = 0;
//		anim.totalFrames = gameUtils.getTotalFrames(disp.skinId,
//				disp.animId, disp.dirId);
//		disp.animId = AnimsIDs.WALK;
//		disp.bitmap = gameUtils.getSkinByID(disp.skinId, disp.animId, disp.dirId, 0);
//		disp.offset = gameUtils.getSkinOffset(disp.skinId, disp.animId, disp.dirId, 0);
//		var motion:Motion3d = entity.get(Motion3d);
//		motion.velocity.x = 0;
//		motion.velocity.y = 0;
//		motion.velocity.z = 4;

//		position.position.x = Math.floor(position.position.x/AppConsts.xGrid)*AppConsts.xGrid;
//		position.position.z = Math.floor(position.position.z/AppConsts.zGrid)*AppConsts.zGrid;

		var shc:ScriptsHolderComponent =  entity.get(ScriptsHolderComponent);
		position =  entity.get(Position);
		var cx:int = position.position.x;
		var cz:int = position.position.z;
		var loopCount:int = 0;
		var motion:Motion3d = entity.get(Motion3d);
		var dir:Direction = entity.get(Direction);

		switch(dir.dir){
			case 0:
				motion.velocity.x = 0;
				motion.velocity.y = 0;
				motion.velocity.z = 4;
				break;
			case 1:
				motion.velocity.x = 4;
				motion.velocity.y = 0;
				motion.velocity.z = 0;
				break;
			case 2:
				motion.velocity.x = 0;
				motion.velocity.y = 0;
				motion.velocity.z = -4;
				break;
			case 3:
				motion.velocity.x = -4;
				motion.velocity.y = 0;
				motion.velocity.z = 0;
				break;
		}

		while(	((Math.abs(Math.round(cx/AppConsts.xGrid)*AppConsts.xGrid-cx)>8) ||
						(Math.abs(Math.round(cz/AppConsts.zGrid)*AppConsts.zGrid-cz)>8))&&
						((Math.abs((Math.round(cx/AppConsts.xGrid)+1)*AppConsts.xGrid-cx)>8) ||
								(Math.abs((Math.round(cz/AppConsts.zGrid)+1)*AppConsts.zGrid-cz)>8)) &&
						((Math.abs((Math.round(cx/AppConsts.xGrid)-1)*AppConsts.xGrid-cx)>8) ||
								(Math.abs((Math.round(cz/AppConsts.zGrid)-1)*AppConsts.zGrid-cz)>8)) &&
						cx>0 &&
						cz>0 &&
						(loopCount<1000)){
			cx+=motion.velocity.x;
			cz+=motion.velocity.z;
			loopCount++;
		}

		trace('cx',Math.round(cx/AppConsts.xGrid),
				'cz',Math.round(cz/AppConsts.zGrid),
				'px',Math.round(position.position.x/AppConsts.xGrid),
				'pz',Math.round(position.position.z/AppConsts.zGrid) );

//		while(((Math.round(cx/AppConsts.xGrid)) == Math.round(position.position.x/AppConsts.xGrid))&&
//			  ((Math.round(cz/AppConsts.zGrid)) == Math.round(position.position.z/AppConsts.zGrid))){
//			position.position.x -=motion.velocity.x;
//			position.position.z -=motion.velocity.z;
//		}
//		motion.velocity.z = 0;
//		motion.velocity.x = 0;

		if(loopCount==1000){
			trace('loopCunt',loopCount);
			throw(new Error('infinit loop when changing sheep'));
		}

		var initArr:Array = [];
		if(((Math.round(cx/AppConsts.xGrid)) == Math.round(position.position.x/AppConsts.xGrid))&&
			  ((Math.round(cz/AppConsts.zGrid)) == Math.round(position.position.z/AppConsts.zGrid))){
			position.position.x = Math.round(position.position.x/AppConsts.xGrid)*AppConsts.xGrid;
			position.position.z = Math.round(position.position.z/AppConsts.zGrid)*AppConsts.zGrid;
		}else{
			initArr.push(new IntPoint(Math.round(position.position.x/AppConsts.xGrid),
					Math.round(position.position.z/AppConsts.zGrid)));
		}
//		entity.remove(ScriptsHolderComponent);
		shc.removeAll();
		shc.addConcurentScriptList(
						generateScriptListFromPathArray(
				TestPathGenerator.generateDRPathFromIntPtGrid(
						initArr.concat(
						mapModel.solve(
								Math.round(cx/AppConsts.xGrid),
								Math.round(cz/AppConsts.zGrid),
//								1,1
								destination.startPos.x,
								destination.startPos.y

						)))));
	}

	public function growPuddle(entity:Entity, time:Number):void {
//		trace('grow puddle',(entity.get(Scalable) as Scalable).scaleX);
		(entity.get(Scalable) as Scalable).scaleX = (entity.get(Scalable) as Scalable).scaleY =
				Math.min(1,(entity.get(Scalable) as Scalable).scaleX+time*0.6);
		if((entity.get(Scalable) as Scalable).scaleX >=1 && !entity.has(MarkedToRemove)){
			entity.add(new RedrawToBackground());
			entity.add(new MarkedToRemove());
//			trace('ent has', entity.has(MarkedToRemove));
		}
	}

	public function getSetIdleAnimation():IScript {
		var es:ExecuteScript = new ExecuteScript(setIdleAnimation,true);
		return es;		
	}
	
	public function setIdleAnimation(entity:Entity,time:Number):void{
		var disp:DisplayBlis = entity.get(DisplayBlis);
		var anim:Anim = entity.get(Anim);
		disp.animId = AnimsIDs.IDLE;
		anim.totalFrames = gameUtils.getTotalFrames(disp.skinId,disp.animId,disp.dirId);
	}
	
	public function getSetWalkAnimation():IScript {
		var es:ExecuteScript = new ExecuteScript(setWalkAnimation,true);
		return es;		
	}
	
	public function setWalkAnimation(entity:Entity,time:Number):void{
//		if(entity.has(BadCreature)){
//			var bc:BadCreature = entity.get(BadCreature) as BadCreature;
//			if(bc.meatImEatingNow == AnimsIDs.FROZEN_MEAT && gameState.currentWeapon.exploding){
//
//			}
//		}
		var disp:DisplayBlis = entity.get(DisplayBlis);
		var anim:Anim = entity.get(Anim);
		disp.animId = AnimsIDs.WALK;
		anim.totalFrames = gameUtils.getTotalFrames(disp.skinId,disp.animId,disp.dirId);
	}

	public function goForSheep(entity:Entity, time:Number):void {

		var bc:BadCreature = entity.get(BadCreature);
//		if(entity.get(PathComponent)||bc.sc.fsm.currentStateId != StatesIds.GO_HOME)
//			return;

		var dest:Destinations = entity.get(Destinations);

//		gameState.createPathToSheep()

		createProperPath(entity, new Point(
				dest.sheepDest.x * AppConsts.xGrid,
				dest.sheepDest.y * AppConsts.zGrid));

		setWalkAnimation(entity,time);
	}
	
	public function goForSheepScript():IScript {

		var es:ExecuteScript = new ExecuteScript(goForSheep,true);
		return es;
	}
}
}
