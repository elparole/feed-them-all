/**
 * User: Elparole
 * Date: 21.05.13
 * Time: 13:28
 */
package com.elparole.feedThemAll.model.ids
{
public class SoundIds
{
	public static const sheepSnds:Array = [
			sheep_05Snd,
			sheep_06Snd,
			sheep_07Snd,
	];
	public static const hittingFenceSnds:Array = [
			MonsterHittingFence_04Snd,
			MonsterHittingFence_05Snd,
			MonsterHittingFence_06Snd
	];
	public static const eatingSheepSnds:Array = [
			monster_eating_sheep_03Snd,
			monster_eating_sheep_04Snd
	];
	public static const eatingMeatSnds:Array = [
			monster_eating_meat_02Snd,
			monster_eating_meat_03Snd,
			monster_eating_meat_04Snd,
			monster_eating_meat_05Snd
	];
	public static const monsterGrumbles:Array = [
			MonsterGrumble_03Snd,
			MonsterGrumble_04Snd,
			MonsterGrumble_05Snd,
			MonsterGrumble_06Snd,
			MonsterGrumble_07Snd

	];

	public function SoundIds() {
	}
}
}
