/**
 * User: Elparole
 * Date: 06.02.13
 * Time: 17:08
 */
package com.elparole.feedThemAll.model.ids
{
public class ProcessIds
{
	public static const DRAW_SHEEP:String = 'drawSheep';
	public static const EAT_SHEEP:String = 'eatSheep';
	public static const GO_HOME:String = 'goHome';
	public static const EAT_MEAT:String = 'eatMeat';
	public static const KILL:String = 'kill';
	public static const EXPLODE:String = 'explode';

	public function ProcessIds() {
	}
}
}
