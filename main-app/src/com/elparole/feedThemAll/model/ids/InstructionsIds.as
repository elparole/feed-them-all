/**
 * User: Elparole
 * Date: 06.02.13
 * Time: 17:08
 */
package com.elparole.feedThemAll.model.ids
{
public class InstructionsIds
{
	public static const MONSTER_IS_COMING:int = 1;
	public static const DROP_WHEN_RECHARGES:int = 2;
	public static const CLICK_AND_DRAG:int = 3;
	public static const MORE_MEAT_TYPES:int = 4;
	public static const MONSTER_TWO_PIECES:int = 5;
	public static const MONSTER_THREE_PIECES:int = 6;
	public static const GROWLING:int = 7;
	public static const SHEEP_KEBAB:int = 8;
	public static const SNARLER:int = 9;
	public static const GREAT_GNARLED:int = 10;
	public static const SMOKING_KILLS:int = 11;


	public function InstructionsIds() {

	}
}
}
