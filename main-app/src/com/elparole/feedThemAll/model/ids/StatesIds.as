/**
 * User: Elparole
 * Date: 06.02.13
 * Time: 17:08
 */
package com.elparole.feedThemAll.model.ids
{
public class StatesIds
{
	public static const GO_FOR_SHEEP:String = 'goForSheep';
	public static const GO_HOME:String = 'goHome';
	public static const EAT:String = 'eat';
	public static const KILL:String = 'kill';
	public static const IDLE:String = 'idle';



	public function StatesIds() {
	}
}
}
