/**
 * User: Elparole
 * Date: 04.03.13
 * Time: 15:42
 */
package com.elparole.feedThemAll.model
{
import flash.display.BitmapData;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;

public class SpriteSheetConfigParserPPV implements IConfigParser
{
	public var sheetB:BitmapData;

	[Embed(source="../../../../assets/sheet.png")]
	public var sheetC:Class;

	public function SpriteSheetConfigParserPPV() {

	}

	public function parseConfig(config:Object):Object{
		var configsObj:AssetsDataEmbedded = new AssetsDataEmbedded();
//		copyAllProperties(configsObj.config, config);
		config = configsObj.config;
		sheetB = (new sheetC()).bitmapData;

		var rect:Rectangle = new Rectangle(0,0,100,100);
		var mat:Matrix = new Matrix();
		for (var str:String in config) {
			for (var i:int = 0; i < config[str].frames.length; i++) {

				config[str].frames[i] ||=new Point();

				if(config[str].frames[i].sheetItemW>128 && config[str].frames[i].sheetItemW<=512){
					config[str].frames[i].bitmap = new BitmapData(
							1024,
							512,true,0 );
					mat.tx = -config[str].frames[i].sheetItemX+config[str].frames[i].bitmap.width/2-config[str].frames[i].offset.x;
					mat.ty = -config[str].frames[i].sheetItemY+config[str].frames[i].bitmap.height/2-config[str].frames[i].offset.y;
					rect.x = config[str].frames[i].bitmap.width/2-config[str].frames[i].offset.x;
					rect.y = config[str].frames[i].bitmap.height/2-config[str].frames[i].offset.y;
					rect.width = config[str].frames[i].sheetItemW;//+config[str].frames[i].offset.x;
					rect.height = config[str].frames[i].sheetItemH;//+config[str].frames[i].offset.y;
				}else if(config[str].frames[i].sheetItemW>512){
					config[str].frames[i].bitmap = new BitmapData(
							2048,
							1024,true,0 );
					mat.tx = -config[str].frames[i].sheetItemX;
					mat.ty = -config[str].frames[i].sheetItemY;
					rect.width = config[str].frames[i].sheetItemW;
					rect.height = config[str].frames[i].sheetItemH;
				}else{
					config[str].frames[i].bitmap = new BitmapData(
							128,
							128,true,0 );
					mat.tx = -config[str].frames[i].sheetItemX+64-config[str].frames[i].offset.x;
					mat.ty = -config[str].frames[i].sheetItemY+64-config[str].frames[i].offset.y;
					rect.x = 64-config[str].frames[i].offset.x;
					rect.y = 64-config[str].frames[i].offset.y;
					rect.width = config[str].frames[i].sheetItemW;//+config[str].frames[i].offset.x;
					rect.height = config[str].frames[i].sheetItemH;//+config[str].frames[i].offset.y;
				}

				config[str].frames[i].bitmap.draw(sheetB, mat,null,null,rect);
			}
		}
		return config;
	}
}
}
