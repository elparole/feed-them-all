/**
 * User: Elparole
 * Date: 14.03.13
 * Time: 14:59
 */
package com.elparole.feedThemAll.model
{
import com.blackmoondev.geom.IntPoint;
import com.blackmoondev.iso.isoModel.math.IsoMathUtils;
import com.elparole.feedThemAll.AppConsts;
import com.elparole.feedThemAll.PublishMode;
import com.elparole.feedThemAll.model.vo.ChildVO;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.levelEditor.model.vo.GameItemConfig;
import com.elparole.levelEditor.model.vo.LevelConfig;
import com.elparole.randomValFromArr;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.text.TextField;
import flash.utils.Dictionary;
import flash.utils.getTimer;

import org.casalib.math.geom.Point3d;

import utils.number.clamp;

public class LevelModel
{
	[Inject]
	public var gameUtils:GameUtils;

	public var levelConfig:LevelConfig;
	public var currentChildIndex:int = 0;
	private const marginX:int = 10;
	private var _destPt:Point;
	public var levelsProgress:Array = [];
	public static const LEVEL_DISABLED_ID:int = -1;
	public static const LEVEL_ENABLED_ID:int = 0;
	public var monstersEnabled:Boolean = true;

	public function LevelModel() {
		levelsProgress[0] = LEVEL_ENABLED_ID;
		for (var i:int = 1; i < 20; i++) {
			levelsProgress[i] = PublishMode.devMode? 3:LEVEL_DISABLED_ID;
		}
	}

	public function parseConfig(lc:LevelConfig,err:Object):LevelConfig {

		var t:int = getTimer();
		lc = parseFloorItems(lc);
		this.levelConfig = lc;
		var sourcesNum:int = 0;
		for each (var gic:GameItemConfig in lc.items) {
			if(AnimsIDs.isMonsterCave(gic.skinId)){
				sourcesNum++;
			}
		}
		if(sourcesNum==0)
			this.levelConfig.childrenList.length = 0;
		trace('parse config', getTimer()-t);
		return lc;
	}

	private function parseFloorItems(lc:LevelConfig):LevelConfig {
		trace('lc',lc);
//		tile = (new sheetC()).bitmapData;

//		lc.floorItems.length  =  Math.min(lc.floorItems.length,10);
//		lc.floorItems = lc.floorItems.concat(gameItems);
		fillMargins(lc);
		lc.backupFloorItems = lc.floorItems;
//		lc.floorItems = rewriteFloors(lc.floorItems);
		lc.floorItems = rewriteFloorsBasic(lc.floorItems, lc.size);

//		for (var key:String in largeTiles) {
//			ipt.x = key.split('_')[1];
//			ipt.y = key.split('_')[0];
//			pt = IsoMathUtils.mapGridCoordToStage(50*9,ipt);
//
//			addChild(largeTiles[key]);
//			largeTiles[key].x = 450*5-pt.x;
//			largeTiles[key].y = 295*8-pt.y*1.3;
//		}
//
//		this.scaleX = this.scaleY = 0.2;
		return lc;
	}

	public function rewriteFloorsBasic(floorItems:Array, size:Point):Array {
//		var bd:BitmapData =new BitmapData(450*2,295*2+200,true,0);
//		var bd:BitmapData =new BitmapData(450*2,295*2+200,true,0x44ee0000);
//		var gic:GameItemConfig = floorItems[0];
//		floorItems = [];
//		var spr:Sprite = new Sprite();
//		var mat:Matrix = new Matrix();
//		mat.tx = -gameUtils.getSkinOffset(gic.skinId,gic.animId, gic.dirId).x;
//		mat.ty = -gameUtils.getSkinOffset(gic.skinId,gic.animId, gic.dirId).y;
//		spr.graphics.beginBitmapFill(gameUtils.getSkinByID(gic.skinId,gic.animId, gic.dirId), mat)
//		spr.graphics.drawRect(0,0,bd.width,bd.height);
//		spr.graphics.endFill();
//		bd.draw(spr);
//
//		for (var ix:int = 0; ix < size.x/9/50; ix++) {
//			for (var iy:int = 0; iy < size.y/9/50; iy++) {
//				var gc:GameItemConfig = new GameItemConfig('','','',
//						new Point3d(ix*70*9+9*50+8*50-10,-1,iy*70*9+8*50-10))
//				gc.bd = bd.clone();
//				floorItems.push(gc);
//			}
//		}
//
		var mat:Matrix = new Matrix();
		var ipt:IntPoint = new IntPoint();
		var iptc:IntPoint = new IntPoint();
		var pt:Point;
		var ptb:Point;
		var offset:Point;
		var largeTiles:Dictionary = new Dictionary();

		floorItems.sortOn('sortId',Array.DESCENDING|Array.NUMERIC);

		var debTF:TextField = new TextField();
		debTF.defaultTextFormat.size = 10;
		var n1:Number = 0;
		var n2:Number = 0;
		var ns:Dictionary = new Dictionary();


		var sampleBD:BitmapData = null;
		var gameItemConfig:GameItemConfig = new GameItemConfig(
				AnimsIDs.TERRAIN_PLAIN,
				AnimsIDs.IDLE,AnimsIDs.DR, new Point3d(0,-1,0)
		)

		for (var bx:int =size.x+marginX; bx >= -marginX; bx--) {
			for (var by:int = size.y+marginX; by>=-marginX; by--) {
				for (var iptx:int = -1; iptx <= (size.x/9)+1; iptx++) {
					for (var ipty:int = -1; ipty<=(size.y/9)+1; ipty++) {

							ipt.y = Math.floor(by);
							ipt.x = Math.floor(bx);
							pt = IsoMathUtils.mapGridCoordToStage(50,ipt);
							ipt.y = Math.floor(ipty);
							ipt.x = Math.floor(iptx);
							var yvalue:int = gameItemConfig.pos.y;

							iptc.x = iptx;//iptx>0? iptx%1: -(-iptx%2);
							iptc.y = ipty;//ipty>0? ipty%1: -(-ipty%2);
							ptb = IsoMathUtils.mapGridCoordToStage(70,iptc);
							iptc.y = ipty;
							iptc.x = iptx;

							offset = gameUtils.getSkinOffset(gameItemConfig.skinId,gameItemConfig.animId, gameItemConfig.dirId);
							ptb.x *= 9;
							ptb.y *= 9;
							mat.tx =450+pt.x-ptb.x-offset.x;
							mat.ty =614-(pt.y)+ptb.y-offset.y;
							largeTiles[iptc.x+'_'+iptc.y] ||= new Bitmap(new BitmapData(450*2,295*2+200,true,0));
							sampleBD = largeTiles[iptc.x+'_'+iptc.y].bitmapData;
							//						debTF.text = String(iptc.x+'_'+iptc.y+'\n'+ptb.x+'_'+ptb.y);
	//						if((ptb.x ==-1260) && (ptb.y == 630)){
	//							trace('floor1',ptb.x, ptb.y,iptx, ipty,mat.tx, mat.ty);
	//							n1++;
	//						}
	//						if((ptb.x ==-1890) && (ptb.y == 945)){
	//							trace('floor2',ptb.x, ptb.y,iptx, ipty,mat.tx, mat.ty);
	//							n2++;
	//						}
	//
	//						if(!ns[ptb.x+'_'+ptb.y])
	//							ns[ptb.x+'_'+ptb.y] = 0;
	//
	//						ns[ptb.x+'_'+ptb.y]++;


							var bd:BitmapData = gameUtils.getSkinByID(gameItemConfig.skinId,gameItemConfig.animId, gameItemConfig.dirId);
			//							gameUtils.getSkinOffset(gameItemConfig.skinId,gameItemConfig.animId, gameItemConfig.dirId));

							largeTiles[iptc.x+'_'+iptc.y].bitmapData.draw(bd,mat);
			//					mat.tx+=30;
			//					mat.ty+=30;
//							largeTiles[iptc.x+'_'+iptc.y].bitmapData.draw(debTF,mat);
						}
					}

//				trace('ptb',iptc.x, iptc.y, ptb.x,ptb.y);
//				}
			}
		}

//		trace('1260',n1);
//		trace('1890',n2);
//		for (var id:String in ns) {
//			trace(id,ns[id]);
//		}

		var gameItems:Array = [];
		for (var key:String in largeTiles) {
			ipt.x = key.split('_')[1];
			ipt.y = key.split('_')[0];
			var gic:GameItemConfig = new GameItemConfig('','','',
					new Point3d(ipt.x*70*9+9*50+8*50-10,yvalue,ipt.y*70*9+8*50-10));
//					new Point3d(ipt.y*74.3*12+50*12,yvalue,ipt.x*74.3*12+50*12));
//			var gic:GameItemConfig = new GameItemConfig('','','',new Point3d(900,yvalue,900));
			gic.bd = largeTiles[key].bitmapData;


//			if(ipt.y<1)
			gameItems.push(gic);
		}
		return gameItems;

	}

	public function rewriteFloors(floorItems:Array):Array {
		var mat:Matrix = new Matrix();
		var ipt:IntPoint = new IntPoint();
		var iptc:IntPoint = new IntPoint();
		var pt:Point;
		var ptb:Point;
		var offset:Point;
		var posDict:Dictionary = new Dictionary();
		var largeTiles:Dictionary = new Dictionary();

		floorItems.sortOn('sortId',Array.DESCENDING|Array.NUMERIC);

		var debTF:TextField = new TextField();
		debTF.defaultTextFormat.size = 10;
		var n1:Number = 0;
		var n2:Number = 0;
		var ns:Dictionary = new Dictionary();

		for each (var gameItemConfig:GameItemConfig in floorItems) {

			posDict[gameItemConfig.pos.x+'_'+gameItemConfig.pos.z] = gameItemConfig;
			ipt.y = Math.floor(gameItemConfig.pos.x/50);
			ipt.x = Math.floor(gameItemConfig.pos.z/50);
			pt = IsoMathUtils.mapGridCoordToStage(50,ipt);
			ipt.y = Math.floor(gameItemConfig.pos.x/50/9);
			ipt.x = Math.floor(gameItemConfig.pos.z/50/9);
			var yvalue:int = gameItemConfig.pos.y;

			for (var iptx:int = ipt.x; iptx <= ipt.x; iptx++) {
				for (var ipty:int = ipt.y; ipty <= ipt.y; ipty++) {
					iptc.x = iptx;
					iptc.y = ipty;
					ptb = IsoMathUtils.mapGridCoordToStage(70,iptc);

					offset = gameUtils.getSkinOffset(gameItemConfig.skinId,gameItemConfig.animId, gameItemConfig.dirId);
					ptb.x *= 9;
					ptb.y *= 9;//11.7;
					mat.tx =pt.x+490-89-ptb.x-offset.x+49;
					mat.ty =295*2-(pt.y)+ptb.y-offset.y+24;
//					largeTiles[iptc.x+'_'+iptc.y] ||= new Bitmap(new BitmapData(450*2,295*2+200,true,0x44ff0000));
					largeTiles[iptc.x+'_'+iptc.y] ||= new Bitmap(new BitmapData(450*2,295*2+200,true,0));

					debTF.text = String(iptc.x+'_'+iptc.y+'\n'+ptb.x+'_'+ptb.y);
					if((ptb.x ==-1260) && (ptb.y == 630)){
						trace('floor1',ptb.x, ptb.y,iptx, ipty,mat.tx, mat.ty);
						n1++;
					}
					if((ptb.x ==-1890) && (ptb.y == 945)){
						trace('floor2',ptb.x, ptb.y,iptx, ipty,mat.tx, mat.ty);
						n2++;
					}

					if(!ns[ptb.x+'_'+ptb.y])
						ns[ptb.x+'_'+ptb.y] = 0;

					ns[ptb.x+'_'+ptb.y]++;


					var bd:BitmapData = gameUtils.getSkinByID(gameItemConfig.skinId,gameItemConfig.animId, gameItemConfig.dirId);
//							gameUtils.getSkinOffset(gameItemConfig.skinId,gameItemConfig.animId, gameItemConfig.dirId));

					largeTiles[iptc.x+'_'+iptc.y].bitmapData.draw(bd,mat);
//					mat.tx+=30;
//					mat.ty+=30;
					largeTiles[iptc.x+'_'+iptc.y].bitmapData.draw(debTF,mat);
				}
			}
		}
		
		trace('1260',n1);
		trace('1890',n2);
		for (var id:String in ns) {
			trace(id,ns[id]);
		}

		var gameItems:Array = [];
		for (var key:String in largeTiles) {
			ipt.x = key.split('_')[1];
			ipt.y = key.split('_')[0];
			var gic:GameItemConfig = new GameItemConfig('','','',
					new Point3d(ipt.x*70*9+9*50+8*50-10,yvalue,ipt.y*70*9+8*50-10));
//					new Point3d(ipt.y*74.3*12+50*12,yvalue,ipt.x*74.3*12+50*12));
//			var gic:GameItemConfig = new GameItemConfig('','','',new Point3d(900,yvalue,900));
			gic.bd = largeTiles[key].bitmapData;


//			if(ipt.y<1)
			gameItems.push(gic);
		}
		return gameItems;
	}

	public function getNextMonster():ChildVO {
		return (levelConfig.childrenList[currentChildIndex] as ChildVO);
	}

	public function monsterAvailable():Boolean {
//		levelConfig.childrenList.length = 1;
		return (currentChildIndex < levelConfig.childrenList.length);
	}

	public function fillMargins(lc:LevelConfig):void {
		var itemsDict:Dictionary = new Dictionary();
		for each (var gameItemConfig:GameItemConfig in lc.floorItems) {
			itemsDict[Math.floor(gameItemConfig.pos.x/AppConsts.xGrid)+'_'+Math.floor(gameItemConfig.pos.z/AppConsts.xGrid)]
					= gameItemConfig;
		}

		for (var iy:int = -marginX; iy < lc.size.y+marginX; iy++) {
			for (var ix:int = -marginX; ix < lc.size.x+marginX; ix++) {
				if(!itemsDict[iy+'_'+ix]){
//					itemsDict[iy+'_'+ix] = new GameItemConfig(
//							AnimsIDs.TERRAIN_PLAIN,
//							AnimsIDs.IDLE,
//							AnimsIDs.DR,
//							new Point3d(iy*AppConsts.xGrid,-1,ix*AppConsts.xGrid));
//					lc.floorItems.push(itemsDict[iy+'_'+ix]);

					if(iy ==-1 && ix == clamp(ix, -1,lc.size.x)||
							ix ==-1 && iy == clamp(iy, -1,lc.size.y)||
							iy ==lc.size.y && ix == clamp(ix, -1,lc.size.x)||
							ix ==lc.size.x && iy == clamp(iy, -1,lc.size.y))
						lc.items.push(getRandomObstacle(iy,ix,AnimsIDs.rockObstacles));

					else if(Math.random()<0.03)
						lc.items.push(getRandomObstacle(iy,ix,AnimsIDs.obstacleIds));
				}
			}
		}
	}

	private function getRandomObstacle(ix:int,iy:int, obstacles:Array):GameItemConfig {

		return new GameItemConfig(
				randomValFromArr(obstacles),
				AnimsIDs.IDLE,
				AnimsIDs.DR,
				new Point3d(ix*AppConsts.xGrid,0,iy*AppConsts.xGrid));
	}

	public function setLevelsProgress(starsToShow:int):void {
		levelsProgress[levelConfig.index] = Math.max(levelsProgress[levelConfig.index],starsToShow);
		if((levelConfig.index<19) && (levelsProgress[levelConfig.index+1] == LevelModel.LEVEL_DISABLED_ID))
			levelsProgress[levelConfig.index+1] = LevelModel.LEVEL_ENABLED_ID;
	}

	public function get destPt():Point {
		return _destPt;
	}

	public function set destPt(value:Point):void {
		_destPt = value;
	}
}
}
