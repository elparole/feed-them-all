/**
 * User: Elparole
 * Date: 26.01.13
 * Time: 12:06
 */
package com.elparole.feedThemAll.model
{
import ash.core.Engine;
import ash.core.Entity;

import com.elparole.components.DisplayBlis;
import com.elparole.components.Motion3d;
import com.elparole.components.Position;
import com.elparole.feedThemAll.AppConsts;
import com.elparole.feedThemAll.components.Anim;
import com.elparole.feedThemAll.components.ArrowStrength;
import com.elparole.feedThemAll.components.BadCreature;
import com.elparole.feedThemAll.components.Bill;
import com.elparole.feedThemAll.components.CameraComp;
import com.elparole.feedThemAll.components.CatapultControls;
import com.elparole.feedThemAll.components.CursorMagnesMotion;
import com.elparole.feedThemAll.components.DisplayAddons;
import com.elparole.feedThemAll.components.Floor;
import com.elparole.feedThemAll.components.GoodCreature;
import com.elparole.feedThemAll.components.GravityComp;
import com.elparole.feedThemAll.components.GridPosition;
import com.elparole.feedThemAll.components.Heart;
import com.elparole.feedThemAll.components.MotionControls;
import com.elparole.feedThemAll.components.Obstacle;
import com.elparole.feedThemAll.components.PlayerCatapult;
import com.elparole.feedThemAll.components.StateComponent;
import com.elparole.feedThemAll.components.TargetPointer;
import com.elparole.feedThemAll.components.Weapon;
import com.elparole.feedThemAll.controller.BadCreatureStatesDefCreator;
import com.elparole.feedThemAll.model.ids.ProcessIds;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.view.SoundManager;
import com.elparole.levelEditor.model.vo.GameItemConfig;
import com.elparole.randomIntInRange;
import com.elparole.randomValFromArgs;
import com.elparole.randomValFromArr;

import flash.display.BitmapData;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.ui.Keyboard;

import org.casalib.math.geom.Point3d;

public class EntityCreator
{

	[Inject]
	public var soundManager:SoundManager;

	[Inject]
	public var gameState:GameState;

	[Inject]
	public var gameUtils:GameUtils;
	
	[Inject]
	public var mapModel:MapModel;

	[Inject]
	public var engine : Engine;

	[Inject]
	public var badCreaturesCreator:BadCreatureStatesDefCreator;

	public var sheepIdIter:int = 0;
	public var sheeps:Array;

	public function EntityCreator( game : Engine )
	{
		this.engine = game;
	}

	public function createBackground():void {

	}

	public function createBadCreature( skinID:String, animId:String, dirId:String, px:int,pz:int) : Entity	
	{
		
		if(!mapModel.isWalkable(Math.floor(px/AppConsts.xGrid),Math.floor(pz/AppConsts.zGrid))){
			return null;
		}
			
		var sheepId:int = 0;// = randomIntInRange(0,sheepIdIter-1);

		var drawCount:int = 0;
		do{
			sheepId = randomIntInRange(0,sheeps.length-1);
			drawCount++;
		}while(!sheeps[sheepId] && drawCount<1000);

		if(drawCount==1000)
			return null;

		var destSheep:Entity = sheeps[sheepId];
		var dx:int = (destSheep.get(Position) as Position).position.x;//AppConsts.xGrid;
		var dz:int = (destSheep.get(Position) as Position).position.z;//AppConsts.zGrid;
//		var mt:Motion3d =  new Motion3d( vx,0,vz);
//			var pBDclazz:Class = getClassByAlias('EnemyType' + getQualifiedClassName(pclazz).replace('Enemy',''));//getClassByAlias(type);
		var creature : Entity = new Entity();
//				.add( new BadCreature() )
//				.add( new Position( px, 0,pz) )
//				.add( mt )
//				.add( new Display(skinID,animId, dirId,
//				gameUtils.getSkinByID(skinID,animId, dirId, 0),
//				gameUtils.getSkinOffset(skinID,animId, dirId, 0)))
//				.add( gameUtils.getAnim(skinID,animId, dirId));

		var anim:Anim = gameUtils.getAnim(skinID, animId, dirId);
		var disp:DisplayBlis = new DisplayBlis(skinID, animId, dirId,
				gameUtils.getSkinByID(skinID, animId, dirId, 0),
				gameUtils.getSkinOffset(skinID, animId, dirId, 0));

		badCreaturesCreator.gameUtils = gameUtils;

		var sc:StateComponent = badCreaturesCreator.createStateComp(creature, disp, anim, px, pz, dx, dz, sheepId);
		engine.addEntity( creature );
		sc.fsm.runProcess(ProcessIds.DRAW_SHEEP);

		gameState.lastMonEatNum = (creature.get(BadCreature) as BadCreature).meatToEat;
		gameState.lastMonPos = new Point(px, pz);
		return creature;
	}

	public function explodeItem(node:Object):void {

		soundManager.play(Monster_ExplodeSnd);

		var svelocity:Point3d = new Point3d();
		var billsNum:int = 0;
		switch(node.entity.get(DisplayBlis).skinId){
			case AnimsIDs.MONSTER1:
				billsNum =3;
				break;
			case AnimsIDs.MONSTER2:
				billsNum =5;
				break;
			case AnimsIDs.MONSTER3:
				billsNum =8;
				break;
		}
		for (var i:int = 0; i < 6+billsNum; i++) {
			var ang:Number = randomIntInRange(-180, 180) * Math.PI / 180;
			var lstrength:Number = randomIntInRange(2, 8);
			svelocity.x = 1.5 * Math.cos(ang) * Math.sqrt(lstrength * 25) / 2;
			svelocity.z = 1.5 * Math.sin(ang) * Math.sqrt(lstrength * 25) / 2;
			svelocity.y = -2.5 * Math.sqrt(lstrength * 25);
			var ent:Entity;
			if(i<6){
				ent = createHeart(
						randomValFromArr([AnimsIDs.HEART1, AnimsIDs.MEAT2]),
						AnimsIDs.IDLE, AnimsIDs.DR,
						node.position.position.x,
						0.5,
						node.position.position.z, svelocity.x, svelocity.y, svelocity.z);
			}else{
				ent = createBill(
						AnimsIDs.BILL,
						AnimsIDs.IDLE, AnimsIDs.DR,
						node.position.position.x,
						0.5,
						node.position.position.z, svelocity.x, svelocity.y, svelocity.z);
			}
			ent.get(GravityComp).isFired = true;
		}
	}

	public function createCustomTextureItem(gic:GameItemConfig, px:Number, py:Number, pz:Number) : Entity{
		var mt:Motion3d =  new Motion3d( 0,0,0);
		var disp:DisplayBlis = new DisplayBlis('','', '',gic.bd,null);
		disp.textureSize.x = gic.bd.width;
		disp.textureSize.y = gic.bd.height;
//			var pBDclazz:Class = getClassByAlias('EnemyType' + getQualifiedClassName(pclazz).replace('Enemy',''));//getClassByAlias(type);
		var plane : Entity = new Entity()
				.add( new Obstacle() )
				.add( new Position( px, py,pz) )
				.add(mt)
				.add( disp);
		if(py<0)
			plane.add(new Floor());

		engine.addEntity( plane );
		return plane;
	}

	public function createItem( skinID:String, animId:String, dirId:String, px:int,py:int,pz:int) : Entity
	{
		
		var mt:Motion3d =  new Motion3d( 0,0,0);
		var disp:DisplayBlis = new DisplayBlis(skinID,animId, dirId,
				gameUtils.cloneBD(gameUtils.getSkinByID(skinID,animId, dirId)),
				gameUtils.getSkinOffset(skinID,animId, dirId));
//		disp.textureSize.x = 2048;
//		disp.textureSize.y = 1024;
//			var pBDclazz:Class = getClassByAlias('EnemyType' + getQualifiedClassName(pclazz).replace('Enemy',''));//getClassByAlias(type);
		var plane : Entity = new Entity()
				.add( new Obstacle() )
				.add( new Position( px, py,pz) )
				.add(mt)
				.add( disp);
		if(AnimsIDs.isFenceId(skinID)){
			plane.add(new Fence(8));
			plane.add( new GridPosition( Math.round(px/AppConsts.xGrid),Math.round(pz/AppConsts.zGrid)) )
		}

		engine.addEntity( plane );
		return plane;
	}


	public function createFloor( skinID:String, animId:String, dirId:String, px:int,py:int,pz:int) : Entity {
		var mt:Motion3d =  new Motion3d( 0,0,0);
		var disp:DisplayBlis = new DisplayBlis(skinID,animId, dirId,
				gameUtils.cloneBD(gameUtils.getSkinByID(skinID,animId, dirId)),
				gameUtils.getSkinOffset(skinID,animId, dirId));
		disp.textureSize.x = 2048;
		disp.textureSize.y = 1024;
//			var pBDclazz:Class = getClassByAlias('EnemyType' + getQualifiedClassName(pclazz).replace('Enemy',''));//getClassByAlias(type);
		var plane : Entity = new Entity()
				.add( new Floor() )
				.add( new Position( px, py,pz) )
				.add( mt )
				.add( disp);
		engine.addEntity( plane );
		return plane;
	}

	public function createGoodCreature( skinID:String, animId:String, dirId:String, px:int,pz:int,vx:int,vz:int) : Entity {

		if(!mapModel.isWalkable(Math.round(px/AppConsts.xGrid),Math.round(pz/AppConsts.zGrid))){
			return null;
		}
		
		trace('createGoodCreature',Math.round(px/AppConsts.xGrid),Math.round(pz/AppConsts.zGrid));
		var mt:Motion3d =  new Motion3d( vx,0,vz);
//			var pBDclazz:Class = getClassByAlias('EnemyType' + getQualifiedClassName(pclazz).replace('Enemy',''));//getClassByAlias(type);
		var plane : Entity = new Entity()
				.add( new GoodCreature(sheepIdIter) )
				.add( new Position( px, 0,pz) )
				.add( new GridPosition( Math.round(px/AppConsts.xGrid),Math.round(pz/AppConsts.zGrid)) )
				.add( mt )
				.add( new DisplayBlis(skinID,animId, dirId,
				gameUtils.getSkinByID(skinID,animId, dirId, 0),
				gameUtils.getSkinOffset(skinID,animId, dirId, 0)))
				.add( gameUtils.getAnim(skinID,animId, dirId));

		engine.addEntity( plane );
		sheeps[sheepIdIter] = plane;
		sheepIdIter++;
		return plane;
	}

	public function createHeart( skinID:String, animId:String, dirId:String, px:Number,py:Number,pz:Number,vx:Number=0,vy:Number=0,vz:Number=0) : Entity
	{
		var mt:Motion3d =  new Motion3d( vx,vy,vz);
//			var pBDclazz:Class = getClassByAlias('EnemyType' + getQualifiedClassName(pclazz).replace('Enemy',''));//getClassByAlias(type);
		var plane : Entity = new Entity()
				.add( new Heart(skinID) )
				.add( new GravityComp(gameState.currentWeapon.stayInGameDuration) )
				.add( new Position( px, py,pz) )
				.add(mt)
				.add( new DisplayBlis(skinID,animId, dirId,
				gameUtils.getSkinByID(skinID,animId, dirId, 0),
				gameUtils.getSkinOffset(skinID,animId, dirId, 0)))
//				.add( gameUtils.getAnim(skinID,animId, dirId));


		engine.addEntity( plane );
		return plane;
	}

	public function createBill( skinID:String, animId:String, dirId:String, px:Number,py:Number,pz:Number,vx:Number=0,vy:Number=0,vz:Number=0) : Entity
	{

		var mt:Motion3d =  new Motion3d( vx,vy,vz);
//			var pBDclazz:Class = getClassByAlias('EnemyType' + getQualifiedClassName(pclazz).replace('Enemy',''));//getClassByAlias(type);
		var plane : Entity = new Entity()
				.add( new Bill(randomValFromArgs(5,10,20)) )
				.add( new GravityComp() )
				.add( new Position( px, py,pz) )
				.add( new CursorMagnesMotion(20) )
				.add( mt )
				.add( new DisplayBlis(skinID,animId, dirId,
				gameUtils.getSkinByID(skinID,animId, dirId, 0),
				gameUtils.getSkinOffset(skinID,animId, dirId, 0)))
//				.add( gameUtils.getAnim(skinID,animId, dirId));


		engine.addEntity( plane );
		return plane;
	}

	public function createCamera(px:int, pz:int, vx:int, vz:int, bounds:Rectangle) : Entity{
		var mt:Motion3d =  new Motion3d( vx,0,vz);
		var camera : Entity = new Entity()
				.add( new CameraComp(bounds) )
				.add( new Position( px, 0, pz) )
				.add( new MotionControls( Keyboard.A, Keyboard.D, Keyboard.W,Keyboard.S,0,true) )
				.add(mt)
		engine.addEntity( camera );
		return camera;
	}
	public function createCatapult( skinID:String, animId:String, dirId:String, px:int,pz:int,vx:int,vz:int) : Entity
	{

		var mt:Motion3d =  new Motion3d( vx,0,vz);
		var spoonSkinID:String = AnimsIDs.CATAPULTSPOON;
		var spoonIAnimID:String = AnimsIDs.SHOOT;
		var spoonDirID:String = AnimsIDs.DR;
//		var heartDisp:Display = new Display(spoonSkinID,spoonIAnimID, spoonDirID,
//				gameUtils.getSkinByID(spoonSkinID,spoonIAnimID, spoonDirID, 0),
//				gameUtils.getSkinOffset(spoonSkinID,spoonIAnimID, spoonDirID, 0));
		var spoonIdleDisp:DisplayBlis = new DisplayBlis(spoonSkinID,spoonIAnimID, spoonDirID,
				gameUtils.getSkinByID(spoonSkinID,spoonIAnimID, spoonDirID, 0),
				gameUtils.getSkinOffset(spoonSkinID,spoonIAnimID, spoonDirID, 0));
//			var pBDclazz:Class = getClassByAlias('EnemyType' + getQualifiedClassName(pclazz).replace('Enemy',''));//getClassByAlias(type);
		var plane : Entity = new Entity()
				.add( new PlayerCatapult() )
				.add( new Weapon(500) )
				.add( new Position( px, 0, pz) )
				.add( new CatapultControls(0,0,12))
//				( Keyboard.A, Keyboard.Z, 0,0,0,true) )
				.add(mt)
				.add( new DisplayAddons([spoonIdleDisp]))
				.add( new DisplayBlis(skinID,animId, dirId,
				gameUtils.getSkinByID(skinID,animId, dirId, 0),
				gameUtils.getSkinOffset(skinID,animId, dirId, 0)))
				.add( gameUtils.getAnim(skinID,animId, dirId));


		engine.addEntity( plane );
		return plane;
	}

	public function createPointer( skinID:String, animId:String, dirId:String, px:int,pz:int,vx:int,vz:int) : Entity
	{
		var pointer : Entity = new Entity()
				.add( new TargetPointer() )
				.add( new Position( px, -0.5, pz) )
				.add( new DisplayBlis(skinID,animId, dirId,
				gameUtils.getSkinByID(skinID,animId, dirId, 0),
				gameUtils.getSkinOffset(skinID,animId, dirId, 0)))
				.add( gameUtils.getAnim(skinID,animId, dirId));


		engine.addEntity( pointer );
		return pointer;
	}

	public function createArrow( skinID:String, animId:String, dirId:String, px:int,pz:int,vx:int,vz:int) : Entity
	{
		var disp:DisplayBlis = new DisplayBlis(skinID,animId, dirId,
				gameUtils.getSkinByID(skinID,animId, dirId, 0),
				gameUtils.getSkinOffset(skinID,animId, dirId, 0));
		disp.textureSize.x = 1024;
		disp.textureSize.y = 512;
		var arrst:ArrowStrength = new ArrowStrength();
		arrst.arrowBD = disp.bitmap;
		disp.bitmap = new BitmapData(1024,512,true,0);
		arrst.textureBD = disp.bitmap;
		arrst.maskBD = gameUtils.getSkinByID('arrowMask',animId, dirId, 0);
		var pointer : Entity = new Entity()
				.add( arrst )
				.add( new Position( px, 0, pz) )
				.add( disp)
				.add( gameUtils.getAnim(skinID,animId, dirId));


		engine.addEntity( pointer );
		return pointer;
	}

	public function init():void {
		sheepIdIter = 0;
		sheeps = [];
	}
}
}
