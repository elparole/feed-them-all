/**
 * User: Elparole
 * Date: 04.09.12
 * Time: 14:57
 */
package com.elparole.feedThemAll.model
{
import flash.utils.Dictionary;

public class ShopConfig
{
	public var prizes:Dictionary = new Dictionary();

	public function ShopConfig() {

		var k:int = 6;
		prizes['meat11'] = k*30;//40; //50;//100;
		prizes['meat12'] = k*40; //60;//150;
		prizes['meat13'] = k*60; //90;//250;

		prizes['meat21'] = k*60;//40; //50;//100;
		prizes['meat22'] = k*70; //60;//150;
		prizes['meat23'] = k*90; //90;//250;

		prizes['meat31'] = k*80;//40; //50;//100;
		prizes['meat32'] = k*90; //60;//150;
		prizes['meat33'] = k*100; //90;//250;

		prizes['meat41'] = k*100;//40; //50;//100;
		prizes['meat42'] = k*120; //60;//150;
		prizes['meat43'] = k*140; //90;//250;

		prizes['meat51'] = k*180;//40; //50;//100;
		prizes['meat52'] = k*200; //60;//150;
		prizes['meat53'] = k*220; //90;//250;

		prizes['meat61'] = k*50;//40; //50;//100;
	}

	public function canAfford(coins:int, s:String, i:int):Boolean {
		return coins >= prizes[s+String(i)];
	}
}
}
