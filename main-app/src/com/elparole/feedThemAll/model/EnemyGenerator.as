/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 13:17
 */
package com.elparole.feedThemAll.model
{
import flash.utils.Dictionary;

import org.swiftsuspenders.Injector;

public class EnemyGenerator
{

	[Inject]
	public var inj:Injector;

	private var currentLevelNum:int = 0;
	
	public var definitions:Dictionary = new Dictionary();
	public var pathDefinitions:Array = [];
	public var levels:Array;
	private var squadrons:Array;

	private static var _instance:EnemyGenerator;
	public var currentLevelEnemyDensity:Number = 0.1;
	public var currentSquadronDensity:Number = 0.8;
	public var currentMovingSqDensity:Number = 0.1;
	public var bossId:int;
	public var bossSleepTime:int;
	public var rdFac:Number = 0.5;//2;
	public var sdFac:Number = 0;
	private var mdFac:Number = 0.5;
	public var gDeltaY:Number = -10;

	public var sinDensity:Number = 248;
	public var xSpeed:Number = 0.6;
	//2;

	public function EnemyGenerator() {}

	public static function get instance():EnemyGenerator {
		return _instance;
	}

	public function init():void {
//
	}
}
}
