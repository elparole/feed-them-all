/**
 * User: Elparole
 * Date: 06.02.13
 * Time: 17:19
 */
package com.elparole.feedThemAll.model
{
import flash.utils.Dictionary;

import utils.type.getClassName;

public class ComponentsConfig
{

	public var components:Dictionary = new Dictionary();

	public function ComponentsConfig() {
	}

	public function add(component:Object):void {
		components[getClassName(component)] = component;
	}
}
}
