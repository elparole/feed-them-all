/**
 * User: Elparole
 * Date: 07.02.13
 * Time: 15:11
 */
package com.elparole.feedThemAll.model
{
import ash.core.Entity;

import com.elparole.components.Motion3d;
import com.elparole.feedThemAll.components.BadCreature;
import com.elparole.feedThemAll.components.Direction;

public class PathAction
{
	private var destDir:int;
	private var velocityLength:int;
	private var actionId:int;


	public function PathAction(actionId:int,destDir:int, velocityLength:int) {
		this.actionId = actionId;
		this.destDir = destDir;
		this.velocityLength = velocityLength;
	}

	public function execute(entity:Entity):void {
		var motion:Motion3d = entity.get(Motion3d)||new Motion3d(0,0,0);
//		var dest:Destinations = entity.get(Destinations);
//		if(dest){
//			dest.currentTileX =
//			dest.currentTileZ =
//		}
//		trace('path action execute',actionId, destDir);
		switch(destDir){
			case 0:
				motion.velocity.z = 0;
				motion.velocity.y = 0;
				motion.velocity.x = velocityLength;
				break;
			case 1:
				motion.velocity.z = velocityLength;
				motion.velocity.y = 0;
				motion.velocity.x = 0;
				break;
			case 2:
				motion.velocity.z = 0;
				motion.velocity.y = 0;
				motion.velocity.x = -velocityLength;
				break;
			case 3:
				motion.velocity.z = -velocityLength;
				motion.velocity.y = 0;
				motion.velocity.x = 0;
				break;
		}

		if(entity.has(BadCreature)){
			trace('move to ',destDir,entity.get(BadCreature).id);
		}

		if(!entity.has(Motion3d)){
			entity.add(motion);
		}

		var direction:Direction = entity.get(Direction) || new Direction(destDir);
		direction.dir = destDir;
		if(entity.has(Direction)){
			entity.remove(Direction);
		}
		entity.add(direction, Direction);


	}
}
}
