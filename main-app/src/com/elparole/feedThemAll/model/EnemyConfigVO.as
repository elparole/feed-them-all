/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 13:23
 */
package com.elparole.feedThemAll.model
{
import flash.geom.Point;

public class EnemyConfigVO
{
	public var position:Point;
	public var velocity:Point;
	public var type:Class;
	public var lives:int;
	public var sqradius:int = 4096;
	public var points:int;
	public var hitType:Class;
	public var pathPts:Array;
	public var loops:int;
	public var isBoss:Boolean = false;
	public var creationTime:int = 0;

	public function EnemyConfigVO(position:Point, velocity:Point, type:Class, hitType:Class, lives:int, points:int, pathPts:Array = null, creationTime:int = 0) {
		this.position = position;
		this.velocity = velocity;
		this.type = type;
		this.hitType = hitType;
		this.lives = lives;
		this.points = points;
		this.pathPts = pathPts;
		this.creationTime = creationTime;
	}

	public function cloneToPos(x:int, y:int, path:Array = null, creationTime:int = 0):EnemyConfigVO {
		position.x = x;
		position.y = y;

//		velocity.x = vx;
//		velocity.y = vy;
		return new EnemyConfigVO(position.clone(),velocity.clone(), type, hitType,lives,points, path, creationTime);
	}
}
}
