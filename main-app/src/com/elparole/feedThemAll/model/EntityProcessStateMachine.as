/**
 * User: Elparole
 * Date: 06.02.13
 * Time: 16:58
 */
package com.elparole.feedThemAll.model
{
import ash.core.Entity;
import ash.fsm.EntityStateMachine;

import flash.utils.Dictionary;

import org.osflash.signals.Signal;

public class EntityProcessStateMachine extends EntityStateMachine
{
//	private var states:Dictionary;
	private var processes:Dictionary;
	private var _currentStateId:String;
	private var currentWithProcessToTarget:Dictionary;
	private var stateChanged:Signal = new Signal(String);

	public static var globalId:int =0;
	public var id:int;

	public function EntityProcessStateMachine( entity : Entity ) : void
	{
		super(entity);
		this.id = globalId++;
//		states = new Dictionary()
		processes = new Dictionary();
		currentWithProcessToTarget = new Dictionary()
	}
	
	public function configureProcess(processName:String, currentState:String, targetState:String):void{
//		states[currentStateId] ||= currentStateId;
//		states[targetState] ||= targetState;
		processes[processName] ||= processName;
		currentWithProcessToTarget[currentState+processName] = targetState;
	}
	
	public function setInitState(stateName:String):void{
		_currentStateId = stateName;
		changeState(stateName)
	}
	
	public function runProcess(processName:String):void{
		changeState(currentWithProcessToTarget[_currentStateId+processName]);
	}

	override public function changeState(name:String):void {
		trace('esm change state',id,name);
		super.changeState(name);
		_currentStateId = name;
	}

	public function getCurrentState():String{
		return _currentStateId;
	}

	public function hasProcess(name:String):Boolean{
		return processes[name] !=null;
	}

	public function get currentStateId():String {
		return _currentStateId;
	}
}
}
