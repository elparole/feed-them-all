/**
 * User: Elparole
 * Date: 07.02.13
 * Time: 13:31
 */
package com.elparole.feedThemAll.model
{
import ash.core.Entity;

public interface IAction
{
	function run(entity:Entity):void;
}
}
