/**
 * User: Elparole
 * Date: 26.01.13
 * Time: 11:48
 */
package com.elparole.feedThemAll.model
{
import ash.core.NodeList;

import com.blackmoondev.ashes.script.components.ScriptsHolderComponent;
import com.elparole.components.Position;
import com.elparole.feedThemAll.AppConsts;
import com.elparole.feedThemAll.components.Destinations;
import com.elparole.feedThemAll.components.PathComponent;
import com.elparole.feedThemAll.model.ids.ProcessIds;
import com.elparole.feedThemAll.model.ids.StatesIds;
import com.elparole.feedThemAll.model.vo.MeatWeaponVO;
import com.elparole.feedThemAll.nodes.DestNode;
import com.elparole.feedThemAll.service.SharedObjectService;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.view.SoundManager;
import com.elparole.randomIntInRange;

import flash.geom.Point;

import org.osflash.signals.Signal;

public class GameState
{
	[Inject]
	public var gameUtils:GameUtils;

	[Inject]
	public var levelModel:LevelModel;

	[Inject]
	public var creator:EntityCreator;

	[Inject]
	public var soundManager:SoundManager;

	[Inject]
	public var mapModel:MapModel;

	[Inject]
	public var scriptGenerator:ScriptGenerator;

	[Inject]
	public var soService:SharedObjectService;

	public static const debug:Boolean = false;
	public var setWin:Signal = new Signal(int);
	public var livesChanged:Signal = new Signal(int);
	public var scoreChanged:Signal = new Signal();
	public var isWin:Boolean;
	public var isRunning:Boolean;
	public var healthSkill:int;
	public var score:int;
	public var width:int;
	public var height:int;
	public var lameInit:Boolean = false;
	public var lameInitRunning:Boolean = false;
	public var heartID:String = AnimsIDs.HEART1;
	public var meat1Weapon:MeatWeaponVO;
	public var meat2Weapon:MeatWeaponVO;
	public var meat3Weapon:MeatWeaponVO;
	public var meat4Weapon:MeatWeaponVO;
	public var meat5Weapon:MeatWeaponVO;
	public var weapons:Array;
	
	public var showControls:Signal = new Signal();
	public var weaponsUpdate:Signal = new Signal(Array,MeatWeaponVO);
	public var currentWeapon:MeatWeaponVO;
	public var mouseBlockedByMenu:Boolean = false;
	public var sheepsNumLiving:int = 0;
	public var sheepNumChanged:Signal = new Signal(Number);
	public var cashChanged:Signal = new Signal(int);
	public var playerConfig:PlayerConfig = new PlayerConfig();
	public var currentLevel:int = -1;
	public var starsToShow:int;
	public var paused:Boolean = false;
	public var ctime:Number;
	public var lastMonEatNum:int;
	public var lastMonPos:Point;
	public var cameraMoved:Boolean;
	public var showCameraMoved:Signal = new Signal();

	public function GameState() {
		playerConfig.skillBuyed.add(onSkillBuyed);
		resetWeapons();
	}

	private function onSkillBuyed():void {
		upgradeWeapon();
		soundManager.play(Money_SpentSnd);
		weaponsUpdate.dispatch(weapons,currentWeapon);
		cashChanged.dispatch(playerConfig.coins);
		soService.saveWeapons(playerConfig.getWeaponsSkills());
		soService.saveMoney(playerConfig.coins);
	}

	private function upgradeWeapon():void {
		var weapon:MeatWeaponVO;
		var skill:int;
		for (var i:int = 1; i < 6; i++) {
			this['meat' + i + 'Weapon'].enabled = (i == 1) || (playerConfig['meat' + i + 'Skill'] > 0);
			weapon = this['meat' + i + 'Weapon'];
			skill = playerConfig['meat' + i + 'Skill'];
			if (weapon.enabled) {
				switch (i) {
					case 1:
						if (skill >= 1) {
							weapon.reloadDuration = 900;
						}
						if (skill >= 2) {
							weapon.stayInGameDuration = 4000;
						}
						if (skill >= 3) {
							weapon.reloadDuration = 600;
						}
						break;
					case 2:
						if (skill >= 2) {
							weapon.reloadDuration = 1500;
						}
						if (skill >= 3) {
							weapon.yummies = 10;
						}
						break;
					case 3:
						if (skill >= 2) {
							weapon.reloadDuration = 1800;
						}
						if (skill >= 3) {
							weapon.exploding = true;
						}
						break;
					case 4:
						if (skill >= 2) {
							weapon.reloadDuration = 2000;
						}
						if (skill >= 3) {
							weapon.reloadDuration = 1100;
						}
						break;
					case 5:
						if (skill >= 2) {
							weapon.range = 20;
						}
						if (skill >= 3) {
							weapon.reloadDuration = 2200;
						}
						break;
				}
//				weapon.reloadDuration = 3000*1/skill*Math.pow(i,0.5);
				trace('meat' + i + 'Weapon rd', weapon.reloadDuration);
			}
		}
	}

	public function initNodes():void {
		if(DestNode){

		}
	}

	/**
	 * reset before new gameplay
	 */
	public function reset():void {
		lameInit = false;
		sheepsNumLiving = 0;
		heartID = AnimsIDs.HEART1;
		levelModel.currentChildIndex = 0;
		isRunning = true;
		healthSkill = 8;
		score = 0;
		trace('game state reset');
//		playerConfig.coins = 0;
		currentWeapon = meat1Weapon;
		reloadWeapons();
	}

	public function resetWeapons():void {
		meat1Weapon = new MeatWeaponVO(AnimsIDs.HEART1, 1200);
		meat2Weapon = new MeatWeaponVO(AnimsIDs.MEAT2, 2000);
		meat3Weapon = new MeatWeaponVO(AnimsIDs.FROZEN_MEAT, 2300);
		meat4Weapon = new MeatWeaponVO(AnimsIDs.SHEEP_BOMB_MEAT, 2600, true);
		meat5Weapon = new MeatWeaponVO(AnimsIDs.SAUSAGE, 2900, true);
		meat1Weapon.enabled = true;
		meat2Weapon.yummies = 2;
		weapons = [meat1Weapon,meat2Weapon,meat3Weapon,meat4Weapon,meat5Weapon];
		currentWeapon = meat1Weapon;
	}

	public function updateWeaponProgress(time:Number){
		for each (var meatWeaponVO:MeatWeaponVO in weapons) {
			meatWeaponVO.percLoaded=Math.min(meatWeaponVO.percLoaded+time*1000/meatWeaponVO.reloadDuration,1);
		}
		weaponsUpdate.dispatch(weapons,currentWeapon);
	}

	public function reloadWeapons():void {
		for each (var meatWeaponVO:MeatWeaponVO in weapons) {
			meatWeaponVO.percLoaded=0;
		}
		weaponsUpdate.dispatch(weapons,currentWeapon);
	}

	public function selectWeapon(weaponIndex:int):void {
		if(weapons[weaponIndex].enabled)
			currentWeapon = weapons[weaponIndex];
	}

	public function removeCreatureDest(id:int,excluded:Destinations, destNodes:NodeList,goodNodes:NodeList):void {
			var dnode:Object;
			creator.sheeps[id] = null;
			var destination:Destinations;
			for ( dnode = destNodes.head; dnode; dnode = dnode.next ) {
				destination = dnode.entity.get(Destinations);
				if((destination!= excluded) &&
						(destination.destId == id) &&
						dnode.creature.sc.fsm.currentStateId ==StatesIds.GO_FOR_SHEEP){
//				toRemove.push(dnode);
					createPathToSheep(goodNodes,id,destNodes,destination,dnode);
//				shc.removeAll();
//				trace('calc different sheep', dnode.creature.id);
//				shc.addConcurentScriptList(
//						scriptGenerator.generateScriptListFromPathArray(
//								TestPathGenerator.generateDRPathFromIntPtGrid(
//										initArr.concat(
//										mapModel.solve(
//												Math.round(cx/AppConsts.xGrid),
//												Math.round(cz/AppConsts.zGrid),
////												0,
////												0))));
//												Math.round(destPos.position.x/AppConsts.xGrid),
//												Math.round(destPos.position.z/AppConsts.zGrid))))));
				}

//				if(!creator.sheeps[destination.destId] && dnode.creature.sc.fsm.currentStateId ==StatesIds.GO_FOR_SHEEP)
//					trace('fail');

			}

	}

	public function createPathToSheep(
			goodNodes:NodeList,
			id:int,
			destNodes:NodeList,
			destination:Destinations,
			dnode:Object):void {

		var newDestId:int;
		var position:Position;

		if(goodNodes.head){
			var drawCount:int = 0;
			do{
				newDestId = randomIntInRange(0,creator.sheeps.length-1);
				drawCount++;
			}while(!creator.sheeps[newDestId] && (drawCount<1000));
			if(drawCount==1000){
				return sendAllHome(destNodes);
			}
		}else{
			return sendAllHome(destNodes);
//			newDestId =id;
		}
//				if(drawCount>=1000)
		destination.destId = newDestId;
		destination.sheepDest.x = Math.round(creator.sheeps[newDestId].get(Position).position.x/AppConsts.xGrid);
		destination.sheepDest.y = Math.round(creator.sheeps[newDestId].get(Position).position.z/AppConsts.zGrid);

		var destPos:Position = creator.sheeps[newDestId].get(Position);
		position =  dnode.entity.get(Position);
//				var cx:int = position.position.x;
//				var cz:int = position.position.z;

//				position.position.x = Math.round(position.position.x/AppConsts.xGrid)*AppConsts.xGrid;
//				position.position.z = Math.round(position.position.z/AppConsts.zGrid)*AppConsts.zGrid;
		if(dnode.entity.get(PathComponent))
			dnode.entity.remove(PathComponent);

		dnode.entity.add(gameUtils.getPathComponent(
				position.position.x,
				position.position.z,
				destPos.position.x,
				destPos.position.z,mapModel));
		
		if(dnode.display.animId !=AnimsIDs.WALK ){

			dnode.entity.add(
					ScriptsHolderComponent.getSingleScriptComponent(
							scriptGenerator.getSetWalkAnimation(), true));
		}
	}


	public function sendAllHome(destNodes:NodeList):void {
		var dnode:Object;
		for ( dnode = destNodes.head; dnode; dnode = dnode.next )
		{
			if(dnode.creature.sc.fsm.currentStateId == StatesIds.GO_FOR_SHEEP)
				dnode.creature.sc.fsm.runProcess(ProcessIds.GO_HOME);
		}
	}

	public function addCash(value:int):void {
		playerConfig.coins+=value;
		trace('add cash',playerConfig.coins);
		cashChanged.dispatch(playerConfig.coins);
		soundManager.play(Money_CollectSnd);
	}

	public function setSheepsNum(num:Number):void {
		trace('setSheepsNum',num);
		sheepsNumLiving = num;
		sheepNumChanged.dispatch(num);
	}

	public function levelWon():void {
		setWin.dispatch(sheepsNumLiving);
	}

	public function isFenceEnabled():Boolean {
		return playerConfig.meat6Skill>0;
	}

	public function weaponsAvailable():int {
		var num:int = 0;
		for each (var meatWeaponVO:MeatWeaponVO in weapons) {
			if(meatWeaponVO.enabled)
				num++;
		}
		return num;
	}

	public function setWeaponsSkills(weapons:Array):void {
		for (var i:int = 1; i <= 6; i++) {
			playerConfig['meat' + i + 'Skill'] = weapons[i-1];
		}
		upgradeWeapon();
	}

	public function setCameraMoved():void {
		if(!cameraMoved)
			showCameraMoved.dispatch();

		cameraMoved = true;
	}
}
}
