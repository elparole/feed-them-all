/**
 * User: Elparole
 * Date: 07.02.13
 * Time: 14:46
 */
package com.elparole.feedThemAll.model
{
import ash.core.Entity;

import com.blackmoondev.ashes.script.impl.IEntityGuard;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.BadCreature;

import flash.geom.Rectangle;

public class PositionInRectGuard implements IEntityGuard
{

	private var rect:Rectangle;
	private var guardId:int;

	public function PositionInRectGuard(guardId:int, rect:Rectangle) {
		this.guardId = guardId;
		this.rect = rect;
	}


	public function approve(e:Entity):Boolean {

		var p:Position = e.get(Position);
//		if(rect.contains(p.position.x,p.position.z))
		trace('approve id:',guardId, rect.x, rect.y,
				p.position.x,p.position.z, e.get(BadCreature).id,
				rect.contains(p.position.x,p.position.z));

		return rect.contains(p.position.x,p.position.z);
//		return Math.random()<0.005;
//		return
//		return false;
	}
}
}
