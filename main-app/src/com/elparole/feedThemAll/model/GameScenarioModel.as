/**
 * User: Elparole
 * Date: 11.03.13
 * Time: 16:29
 */
package com.elparole.feedThemAll.model
{
import com.elparole.feedThemAll.service.SharedObjectService;

import de.polygonal.ds.ArrayUtil;

import org.casalib.math.geom.Point3d;
import org.osflash.signals.Signal;

public class GameScenarioModel
{

	[Inject]
	public var gameState:GameState;
	
	[Inject]
	public var levelModel:LevelModel;

	[Inject]
	public var soService:SharedObjectService;

	public var showInstructions:Signal = new Signal(int);
	public var moveCamera:Signal = new Signal(Point3d);

	public var instructionsShowed:Array = [];
	public var currentIndex:int = 0;

	public function GameScenarioModel() {
		instructionsShowed.length = 11;
		ArrayUtil.fill(instructionsShowed,false);
	}
	
	public function checkState(forceState:int = -1):void {
		if((gameState.currentLevel == 0) && (levelModel.currentChildIndex == 1) && !instructionsShowed[0]){
//			moveCamera.dispatch(new Point3d(gameState.lastMonPos.y-300,0,gameState.lastMonPos.x));
//			moveCamera.dispatch(new Point3d(5*50,0,27*50));
//			moveCamera.dispatch(new Point3d(100,0,100));
			showInstructions.dispatch(1);
			instructionsShowed[0] = true;
			currentIndex = 1;
			soService.saveInstructions(instructionsShowed);
		}else if((forceState == 2) && ((gameState.currentWeapon.percLoaded <1) && !instructionsShowed[1])){
			showInstructions.dispatch(2);
			instructionsShowed[1] = true;
			currentIndex = 2;
			soService.saveInstructions(instructionsShowed);
		}else if((gameState.currentLevel == 3) && !instructionsShowed[2]){
			showInstructions.dispatch(3);
			instructionsShowed[2] = true;
			currentIndex = 3;
			soService.saveInstructions(instructionsShowed);
		}else if(!instructionsShowed[3] && (gameState.weaponsAvailable() > 1)){
			showInstructions.dispatch(4);
			instructionsShowed[3] = true;
			currentIndex = 4;
			soService.saveInstructions(instructionsShowed);
		}else if(!instructionsShowed[4] && (gameState.lastMonEatNum > 2)){
//			moveCamera.dispatch(new Point3d(27*50,0,5*50));
			moveCamera.dispatch(new Point3d(gameState.lastMonPos.y-300,0,gameState.lastMonPos.x));
			instructionsShowed[4] = true;
			currentIndex = 5;
			soService.saveInstructions(instructionsShowed);
		}else if(!instructionsShowed[5] && (gameState.lastMonEatNum > 3)){
//			moveCamera.dispatch(new Point3d(27*50,0,5*50));
			moveCamera.dispatch(new Point3d(gameState.lastMonPos.y-300,0,gameState.lastMonPos.x));
			instructionsShowed[5] = true;
			currentIndex = 6;
			soService.saveInstructions(instructionsShowed);
		}
	}
}
}
