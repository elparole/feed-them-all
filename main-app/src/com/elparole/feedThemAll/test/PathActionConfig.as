/**
 * User: Elparole
 * Date: 06.02.13
 * Time: 19:05
 */
package com.elparole.feedThemAll.test
{
public class PathActionConfig
{
	public var destDir:int = 0;
	public var velocityLength:Number;
	public var destPosX:int;
	public var destPosY:int;


	public function PathActionConfig(dir:int, velocity:Number, destPosX:int,destPosY:int) {
		this.destDir = dir;
		this.velocityLength = velocity;
		this.destPosX = destPosX;
		this.destPosY = destPosY;
	}


	public function toString():String {
		return 'x:'+destPosX+', y:'+destPosY;
	}
}
}
