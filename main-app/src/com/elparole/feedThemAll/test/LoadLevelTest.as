/**
 * User: Elparole
 * Date: 14.03.13
 * Time: 13:17
 */
package com.elparole.feedThemAll.test
{
import com.blackmoondev.geom.IntPoint;
import com.blackmoondev.iso.isoModel.math.IsoMathUtils;
import com.elparole.levelEditor.model.vo.GameItemConfig;
import com.elparole.levelEditor.model.vo.LevelConfig;
import com.elparole.levelEditor.service.LevelFromFileDataService;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.utils.Dictionary;

[SWF(width=1024 , height=600)]
public class LoadLevelTest extends Sprite
{

	[Embed(source="../../../../assets/PNG_Assets/terrainPlain_idle_DR_49_24_1.png")]
	public var sheetC:Class;

	public var llser:LevelFromFileDataService = new LevelFromFileDataService();
	private var tile:BitmapData;

	public function LoadLevelTest() {
		addEventListener(Event.ADDED_TO_STAGE, init);
	}

	private function init(event:Event):void {
		trace('load');
		llser.loadConfig('lev1','D:/Flash/feed-grays/main-app/src/assets/levels/lev4.lev')
				.addResultProcessor(onLevel);
	}

	private function onLevel(lc:LevelConfig,err:Object):LevelConfig {
		trace('lc',lc);
		tile = (new sheetC()).bitmapData;
		var mat:Matrix = new Matrix();
		var ipt:IntPoint = new IntPoint();
		var iptc:IntPoint = new IntPoint();
		var pt:Point;
		var ptb:Point;
		var posDict:Dictionary = new Dictionary();
		var largeTiles:Dictionary = new Dictionary();

		lc.floorItems.sortOn('sortId',Array.DESCENDING|Array.NUMERIC);

		for each (var gameItemConfig:GameItemConfig in lc.floorItems) {
			posDict[gameItemConfig.pos.x+'_'+gameItemConfig.pos.z] = gameItemConfig;
			
			ipt.x = Math.floor(gameItemConfig.pos.x/70);
			ipt.y = Math.floor(gameItemConfig.pos.z/70);
			pt = IsoMathUtils.mapGridCoordToStage(50,ipt);
			ipt.x = Math.floor(gameItemConfig.pos.x/70/12);
			ipt.y = Math.floor(gameItemConfig.pos.z/70/12);

			for (var iptx:int = Math.max(0,ipt.x-1); iptx <= ipt.x+1; iptx++) {
				for (var ipty:int = Math.max(0,ipt.y-1); ipty <= ipt.y+1; ipty++) {
					iptc.x = iptx;
					iptc.y = ipty;
					ptb = IsoMathUtils.mapGridCoordToStage(50,iptc);

					ptb.x *= 9;
					ptb.y *= 11.7;
					mat.tx =pt.x+490-89-ptb.x;
					mat.ty =295*2-(pt.y)+ptb.y;
					largeTiles[iptc.x+'_'+iptc.y] ||= new Bitmap(new BitmapData(450*2,295*2,true,0x44ff0000));
					largeTiles[iptc.x+'_'+iptc.y].bitmapData.draw(tile,mat);
				}
			}
		}

		for (var key:String in largeTiles) {
			ipt.x = key.split('_')[1];
			ipt.y = key.split('_')[0];
			pt = IsoMathUtils.mapGridCoordToStage(50*9,ipt);
			
			addChild(largeTiles[key]);
			largeTiles[key].x = 450*5-pt.x;
			largeTiles[key].y = 295*8-pt.y*1.3;
		}

		this.scaleX = this.scaleY = 0.2;
		return lc;
	}
}
}
