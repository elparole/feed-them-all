/**
 * User: Elparole
 * Date: 06.02.13
 * Time: 18:57
 */
package com.elparole.feedThemAll.test
{
import com.blackmoondev.geom.IntPoint;
import com.elparole.feedThemAll.AppConsts;

import flash.geom.Rectangle;

import org.casalib.math.geom.Point3d;

public class TestPathGenerator
{
	public function TestPathGenerator() {

	}


	public static function generateDRPathFromIntPtGrid(tileArr:Array):Array {
		if(!tileArr||(tileArr.length ==0))
			return [];

		trace('generateDRPathFromIntPtGrid');
//		tileArr = tileArr.reverse();
		for each (var intPoint:IntPoint in tileArr) {
			trace(intPoint);
		}
		
		var arr:Array = [];
		var tile:IntPoint = tileArr.shift();
		var currentPos:Point3d = new Point3d(tile.x*AppConsts.xGrid,0,tile.y*AppConsts.zGrid);
		var newPos:Point3d = new Point3d(currentPos.x,0,currentPos.z);
		var speed:Number = 4;//4;
//		var dir1:int = 0;
		while(tileArr.length>0){

			tile = tileArr.shift();
			newPos.x = tile.x*AppConsts.xGrid;
			newPos.z = tile.y*AppConsts.zGrid;

			switch(true){
				case newPos.x >currentPos.x:
					arr.push(new PathActionConfig(1,speed, newPos.x, newPos.z));
					break;
				case newPos.x <currentPos.x:
					arr.push(new PathActionConfig(3,speed, newPos.x, newPos.z));
					break;
				case newPos.z>currentPos.z:
					arr.push(new PathActionConfig(0,speed, newPos.x, newPos.z));
					break;
				case newPos.z<currentPos.z:
					arr.push(new PathActionConfig(2,speed, newPos.x, newPos.z));
					break;
			}
			currentPos.x = newPos.x;
			currentPos.z = newPos.z;
		}
		trace('generateDRPathFromIntPtGrid');
		for each (var config:PathActionConfig in arr) {
			trace(config);
		}

		return arr;

	}

	public static function generateDRRandomPath(startX:int,startZ:int, destX:int, destZ:int, randomization:Number = 0):Array {
		var arr:Array = [];
		var currentPos:Point3d = new Point3d(startX,0,startZ);
		var newPos:Point3d = new Point3d(startX,0,startZ);
		var speed:Number = 4;//4;
		var mapRect:Rectangle = new Rectangle(-200,-1300,400,2150);
		if(Math.random()>0.5)
			return [new PathActionConfig(2,speed, startX, -270),
				new PathActionConfig(1,speed, 250, -270),
				new PathActionConfig(2,speed, 250, -350) ];
		else
			return [new PathActionConfig(2,speed, startX, -270),
				new PathActionConfig(3,speed, -250, -270),
				new PathActionConfig(2,speed, -250, -350) ];
		/**
		 * ----------------------------------------------------------------------------------------------------------------------
		 */
		var dir1:int = 0;
		while(currentPos.z>-300){
			var routeDelta:Number = dir1 ==0? Math.random()*randomization+80:Math.random()*randomization;
			if(dir1 == 1){
				newPos = currentPos.add(new Point3d(Math.random()<0.5?-routeDelta:routeDelta,0,0));
			}
			else
				newPos = currentPos.add(new Point3d(0,0,-routeDelta));
			


			if(isInRect(newPos,mapRect)){
				switch(true){
					case newPos.x >currentPos.x:
						arr.push(new PathActionConfig(1,speed, newPos.x, newPos.z));
						break;
					case newPos.x <currentPos.x:
						arr.push(new PathActionConfig(3,speed, newPos.x, newPos.z));
						break;
					case newPos.z>currentPos.z:
						arr.push(new PathActionConfig(0,speed, newPos.x, newPos.z));
						break;
					case newPos.z<currentPos.z:
						arr.push(new PathActionConfig(2,speed, newPos.x, newPos.z));
						break;
				}
				currentPos = newPos;
				dir1 = (dir1+1)%2;
			}
		}
		return arr;
	}
	
	public static function generateRandomPath2(length:int):Array {
		var arr:Array = [];
		var currentPos:Point3d = new Point3d(0,0,0);
		var newPos:Point3d = new Point3d(0,0,0);
		var speed:Number = 4;//4;
		var mapRect:Rectangle = new Rectangle(-200,-200,400,900);

		while(arr.length<length){
			var routeDelta:Number = Math.random()*100+80;
			if(Math.random()<0.5)
				newPos = currentPos.add(new Point3d(Math.random()<0.5?-routeDelta:routeDelta,0,0));
			else
				newPos = currentPos.add(new Point3d(0,0,Math.random()<0.5?-routeDelta:routeDelta));

			if(isInRect(newPos,mapRect)){
				switch(true){
					case newPos.x >currentPos.x:
						arr.push(new PathActionConfig(1,speed, newPos.x, newPos.z));
						break;
					case newPos.x <currentPos.x:
						arr.push(new PathActionConfig(3,speed, newPos.x, newPos.z));
						break;
					case newPos.z>currentPos.z:
						arr.push(new PathActionConfig(0,speed, newPos.x, newPos.z));
						break;
					case newPos.z<currentPos.z:
						arr.push(new PathActionConfig(2,speed, newPos.x, newPos.z));
						break;
				}
				currentPos = newPos;
			}
		}
		return arr;
	}

	public static function generateRandomPath1(length:int):Array {
		var arr:Array = [];
		var currentPos:Point3d = new Point3d(0,0,0);
		var newPos:Point3d = new Point3d(0,0,0);
		var speed:Number = 4;
		var mapRect:Rectangle = new Rectangle(-200,-200,400,900);

		while(arr.length<length){
			if(Math.random()<0.5)
				newPos = currentPos.add(new Point3d(Math.random()<0.5?-50:50,0,0));
			else
				newPos = currentPos.add(new Point3d(0,0,Math.random()<0.5?-50:50));

			if(isInRect(newPos,mapRect)){
				switch(true){
					case newPos.x >currentPos.x:
						arr.push(new PathActionConfig(1,speed, newPos.x, newPos.z));
						break;
					case newPos.x <currentPos.x:
						arr.push(new PathActionConfig(3,speed, newPos.x, newPos.z));
						break;
					case newPos.z>currentPos.z:
						arr.push(new PathActionConfig(0,speed, newPos.x, newPos.z));
						break;
					case newPos.z<currentPos.z:
						arr.push(new PathActionConfig(2,speed, newPos.x, newPos.z));
						break;
				}
				currentPos = newPos;
			}
		}
		return arr;
	}

	private static function isInRect(newPos:Point3d, rect:Rectangle):Boolean {
		return rect.contains(newPos.x, newPos.z);
	}

	public static function generatePathToHome():Array {
		return [];
	}
}
}
