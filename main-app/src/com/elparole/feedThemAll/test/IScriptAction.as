/**
 * User: Elparole
 * Date: 06.02.13
 * Time: 19:06
 */
package com.elparole.feedThemAll.test
{
import ash.core.Entity;

public interface IScriptAction
{
	function call(entity:Entity):void;
}
}
