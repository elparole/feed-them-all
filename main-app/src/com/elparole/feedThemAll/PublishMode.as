/**
 * User: Elparole
 * Date: 22.02.13
 * Time: 12:22
 */
package com.elparole.feedThemAll
{
import flash.display.SimpleButton;
import flash.display.Sprite;
import flash.text.TextField;
import flash.utils.Dictionary;

public class PublishMode
{
	public static const DEMO:Boolean = false;
	public static const isGer:Boolean = false;
	private static var gerLabels:Dictionary = new Dictionary();
	public static var mobile:Boolean = true;
	public static var devMode:Boolean = false;
//	public static var SponsorLink:String = "http://www.nowgamez.com/?utm_source=feed-them-all&utm_medium=currentLink&utm_campaign=feed-them-all";
	public static var SponsorLink:String = "http://armor.ag/MoreGames";
	public static var SponsorLikeLink:String = "http://www.facebook.com/ArmorGames";
//	public static var ShowBrand:Boolean = false;
	public static var ShowBrand:Boolean = true;
	public static var NowGamezBrand:Boolean = false;
	public static var AGBrand:Boolean = true;
//	public static var devMode:Boolean = true;

	/**
	 * flox
	 * elparole-Sky-Knight-2
	 * 8190cfed-075f-47c8-9d19-7659936cdde5
	 */
	public static function getText(s:String):String {
		return PublishMode.isGer? gerLabels[s]:s;
	}

	transInit();

	private static function transInit():void {

		gerLabels['START 1 PLAYER'] ="Start 1-Spieler";
		gerLabels['START 2 PLAYERS'] ="Start 2-Spieler";
		gerLabels['MORE GAMES'] ="Mehr Spiele";
		gerLabels['CONTINUE'] ="Weiter";
		gerLabels['MOVE'] ="Bewegen";
		gerLabels['SHOOT'] ="Schiessen";
		gerLabels['BOMB'] ="Bombe";
		gerLabels['BUY'] ="Kaufe";
		gerLabels['MUSIC ON'] ="musik an";
		gerLabels['MUSIC OFF'] ="musik aus";
		gerLabels['SOUND ON'] ="sound an";
		gerLabels['SOUND OFF'] ="sound aus";
//		gerLabels['BACK TO MAIN'] ="Back to main";
//		gerLabels['BACK TO MAIN'] ="Hauptansicht";
		gerLabels['BACK TO MAIN'] ="Hauptmenue";

		gerLabels['SHOP'] ="sound aus";
//		gerLabels['COOLER'] ="Kühler";
		gerLabels['COOLER'] ="Kuehler";
		gerLabels['GUN'] ="Waffe";
		gerLabels['HEALTH'] ="Leben";
		gerLabels['BOMBS'] ="Bombe";
		gerLabels['SCORE'] ="Punkte";
//		gerLabels['COINS'] ="Münzen";
		gerLabels['COINS'] ="Muenzen";
		gerLabels['BACK TO MAIN'] ="Hauptansicht";
		gerLabels['NEW GAME'] ="Neues Spiel";
		gerLabels['YOU HAVE SCORED'] ="Dein Punktestand";
		gerLabels['GUN TEMP'] ="Temperatur der Waffe";
		gerLabels['OR'] ="ODER";

		for (var i:int = 0; i < 16; i++) {
			gerLabels['WAVE '+String(i)] = 'Neuer Angriff';
		}
	}


	public static function setButtonText(bt:SimpleButton, s:String):void {
		changeAllTfs((bt.downState as Sprite),getText(s));
		changeAllTfs((bt.upState as Sprite),getText(s));
		changeAllTfs((bt.overState as Sprite),getText(s));
		changeAllTfs((bt.hitTestState as Sprite),getText(s));
	}

	private static function changeAllTfs(stateSprite:Sprite, s:String):void {
		for (var i:int = 0; i < stateSprite.numChildren; i++) {
			if(((stateSprite as Sprite).getChildAt(i) is TextField)){
				((stateSprite as Sprite).getChildAt(i) as TextField).text = s;
			}

		}
	}
}
}
