/**
 * User: Elparole
 * Date: 26.01.13
 * Time: 12:31
 */
package com.elparole.feedThemAll
{
import ash.core.Engine;
import ash.integration.swiftsuspenders.SwiftSuspendersEngine;
import ash.tick.FrameTickProvider;

import com.elparole.feedThemAll.controller.BadCreatureStatesDefCreator;
import com.elparole.feedThemAll.controller.ScreenNavigator;
import com.elparole.feedThemAll.controller.SetWinSignal;
import com.elparole.feedThemAll.model.EnemyGenerator;
import com.elparole.feedThemAll.model.EntityCreator;
import com.elparole.feedThemAll.model.GameScenarioModel;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.LevelModel;
import com.elparole.feedThemAll.model.MapModel;
import com.elparole.feedThemAll.model.ScriptGenerator;
import com.elparole.feedThemAll.model.SpriteSheetConfigParser;
import com.elparole.feedThemAll.service.SharedObjectService;
import com.elparole.feedThemAll.view.BlisContainer;
import com.elparole.feedThemAll.view.DevPanel;
import com.elparole.feedThemAll.view.SoundManager;
import com.elparole.feedThemAll.view.StartView;
import com.elparole.input.MousePoll;
import com.elparole.levelEditor.service.ILevelService;
import com.elparole.levelEditor.service.LevelFromFileDataService;
import com.elparole.levelEditor.service.LevelsEmbeddedDataService;

import flash.display.DisplayObjectContainer;

import net.richardlord.input.KeyPoll;

import org.swiftsuspenders.Injector;

public class FTAContext
{
	private var cont:DisplayObjectContainer;

	private var engine : Engine;
	private var tickProvider : FrameTickProvider;
	private var injector : Injector;
	private var screenNavigator:ScreenNavigator;
//	public static const dev:Boolean = true;
	public static const dev:Boolean = false;
	private var creaturesCreator:BadCreatureStatesDefCreator;
	private var scriptGenerator:ScriptGenerator;
	private var levelModel:LevelModel;
	private var llser:ILevelService;
	private var mapModel:MapModel;
	private var gameScrenarioModel:GameScenarioModel;
	private var soService:SharedObjectService;
	private var gameState:GameState = new GameState();

	public function FTAContext(cont:DisplayObjectContainer, width:int, height:int) {
		this.cont = cont;
		prepare( cont, width, height );
	}


	private function initMVC():void {
	}


	private var sm:SoundManager;

	private function prepare( container : DisplayObjectContainer, width : Number, height : Number ) : void
	{
		trace('context prepare');
		injector = new Injector();
		engine = new SwiftSuspendersEngine( injector );


//		var gamePlayLayer:PPVContainer = new PPVContainer(640, 480);
		var gamePlayLayer:BlisContainer = new BlisContainer();
		container.addChild(gamePlayLayer);
		gamePlayLayer.init();
		if(PublishMode.devMode){
			var devPanel:DevPanel = new DevPanel();
			container.stage.addChild(devPanel);
		}

//		if(dev){
//			var devCont:DevContainer = new DevContainer();
//			container.addChild(devCont);
//			devCont.init();
//		}

//		injector.injectInto(screenNavigator);
		var gameUtils:GameUtils = new GameUtils();

		gameUtils.parser = new SpriteSheetConfigParser();
		gameUtils.config = gameUtils.parser.parseConfig(gameUtils.config);

		injector.map( SetWinSignal ).toValue(new SetWinSignal());
		injector.map( SoundManager ).toValue(sm = new SoundManager());
		injector.map( GameUtils ).toValue(gameUtils);
		injector.map( BadCreatureStatesDefCreator ).toValue(creaturesCreator = new BadCreatureStatesDefCreator());
		injector.map( ScriptGenerator ).toValue(scriptGenerator = new ScriptGenerator());
		injector.map( GameState ).toValue(gameState);
		injector.map( Engine ).toValue( engine );
		injector.map( MapModel ).toValue( mapModel = new MapModel() );
		injector.map( GameScenarioModel ).toValue( gameScrenarioModel = new GameScenarioModel() );
		injector.map( LevelModel ).toValue( levelModel = new LevelModel() );
		injector.map( BlisContainer ).toValue( gamePlayLayer );

		PublishMode.devMode && injector.map( DevPanel ).toValue( devPanel );

		injector.map( EntityCreator ).asSingleton();
		injector.map( SharedObjectService ).toValue(soService = new SharedObjectService())
		injector.map( KeyPoll ).toValue( new KeyPoll( container.stage ) );
		var mousePoll:MousePoll = new MousePoll( container.stage );
		mousePoll.minDist = 5;
		injector.map( MousePoll ).toValue( mousePoll );
		var enemyGenerator:EnemyGenerator = new EnemyGenerator();
		injector.map( EnemyGenerator ).toValue( enemyGenerator );
		injector.map( Injector ).toValue( injector );

		if(PublishMode.devMode){
			llser = new LevelFromFileDataService();
			injector.map(ILevelService).toValue(llser);
		}else{
			llser = new LevelsEmbeddedDataService();
			injector.map(ILevelService).toValue(llser);
		}

		PublishMode.devMode && injector.injectInto(devPanel);
		injector.injectInto(levelModel);
		injector.injectInto(mapModel);
		injector.injectInto(gameScrenarioModel);
		injector.injectInto(enemyGenerator);
		injector.injectInto(gameState);
		injector.injectInto(gameUtils);
		injector.injectInto(scriptGenerator);
		injector.injectInto(creaturesCreator);
		injector.injectInto(sm);
//		creaturesCreator.scriptGenerator = scriptGenerator;
//		scriptGenerator.creaturesCreator = creaturesCreator;
//		scriptGenerator.gameUtils = gameUtils;

		tickProvider = new FrameTickProvider( container, 30);
//		tickProvider = new FixedTickProvider( container ,1/30);
		injector.map( FrameTickProvider ).toValue(tickProvider);
		tickProvider.stop();

		screenNavigator = new ScreenNavigator(this.cont);
		injector.map( ScreenNavigator ).toValue(screenNavigator);
		injector.injectInto(screenNavigator);
		screenNavigator.postInjectionInit();

		gameState.width = 640;//width;
		gameState.height = 480;//height;

		PublishMode.devMode &&

		llser.loadLevelsList('levelsList.txt')
				.addResultProcessor(devPanel.setLevelsList)
				.addResultProcessor(loadLev)
	}

	private function loadLev(data:*, err:Object):* {
//		llser.loadConfig('lev12','levels/lev12.lev')
		llser.loadConfig(data.list[0],'levels/'+data.list[0])
				.addResultProcessor(levelModel.parseConfig)
				.addResultProcessor(mapModel.parseObstacleMap);
		return data;
	}
//		gameState.gameUtils = gameUtils;


	public function start() : void{
		soService.init();
		levelModel.levelsProgress = soService.loadLevels();
		gameScrenarioModel.instructionsShowed = soService.loadInstructions();
		gameState.setWeaponsSkills(soService.loadWeapons());
		gameState.playerConfig.coins = soService.loadMoney();
		sm.soundOn = soService.loadSound();
		sm.musicOn = soService.loadMusic();

		screenNavigator.soundStatesInit();
		screenNavigator.setView(StartView);
		screenNavigator.startTicker();
	}
}
}
