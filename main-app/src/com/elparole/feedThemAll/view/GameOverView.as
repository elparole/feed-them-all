/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 17:17
 */
package com.elparole.feedThemAll.view
{
import com.elparole.feedThemAll.PublishMode;
import com.elparole.utils.RollScaleFeatures;
import com.greensock.TweenLite;

import flash.events.Event;

import flash.events.MouseEvent;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import org.osflash.signals.Signal;

public class GameOverView extends GameOverScr implements IScreen
{
	public var startGame:Signal = new Signal();

//	private var _dl:DataLoad = new DataLoad('http://projects.blackmoondev.com/SkyKnight/link.txt');
//	private var message:String;

	private var rsf:RollScaleFeatures = new RollScaleFeatures();
	public var backToMain:Signal = new Signal();

	public function GameOverView() {
//		graphics.beginFill(0x099000,1);
//		graphics.drawRect(0,0,640,480);
//		graphics.endFill();
	}

	private function onBackToMain():void {
		backToMain.dispatch();
	}

	private function onBackToMainClick(event:MouseEvent):void {
		outro();
		TweenLite.delayedCall(0.5,onBackToMain);
	}


	public function init():void {
		x = 0;
		sheepBg.scaleX = sheepBg.scaleY = 0.1;
		moreGamesBt.x = 187 - 500;
		restartBt.x = 441 + 500;
		backToMainBt.y = 433 + 500;

		TweenLite.to(sheepBg, 0.5, {scaleX:0.87, scaleY:0.87});
		TweenLite.to(moreGamesBt, 0.5, {x:187});
		TweenLite.to(restartBt, 0.5, {x:441});
		TweenLite.to(backToMainBt, 0.5, {y:433});

		restartBt.addEventListener(MouseEvent.CLICK, onStartClick,false,0,true);
		backToMainBt.addEventListener(MouseEvent.CLICK, onBackToMainClick,false,0,true);
		moreGamesBt.addEventListener(MouseEvent.CLICK, onGoSite,false,0,true);

		if(PublishMode.ShowBrand){
			sponsorLogoMC2.addEventListener(MouseEvent.CLICK, onGoSite,false,0,true);
			sponsorLogoMC2.buttonMode= true;
		}else{
			sponsorLogoMC2.visible = false;
		}

		rsf.addButtons([restartBt,moreGamesBt,backToMainBt]);
	}


	private function onGoSite(event:Event):void {
		navigateToURL(new URLRequest(PublishMode.SponsorLink),'_blank');
	}

	private function onStartClick(event:MouseEvent):void {
		outro();
		TweenLite.delayedCall(0.5,onStart);
//		GTweener.to(this, 0.1,{x:640},{onComplete:onStart});
	}

	private function outro():void {
		TweenLite.to(sheepBg, 0.5, {scaleX:0.1, scaleY:0.1});
		TweenLite.to(moreGamesBt, 0.5, {x:187 - 500});
		TweenLite.to(restartBt, 0.5, {x:441 + 500});
		TweenLite.to(backToMainBt, 0.5, {y:433 + 500});

		restartBt.removeEventListener(MouseEvent.CLICK, onStartClick);
		backToMainBt.removeEventListener(MouseEvent.CLICK, onBackToMainClick);
		sponsorLogoMC2.removeEventListener(MouseEvent.CLICK, onGoSite);
		moreGamesBt.removeEventListener(MouseEvent.CLICK, onGoSite);
		rsf.removeAllButtons();
	}

	private function onStart(...args):void {
		startGame.dispatch();
	}

	public function destroy():void {

	}
}
}
