/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 16:43
 */
package com.elparole.feedThemAll.view
{
import com.elparole.feedThemAll.PublishMode;
import com.elparole.feedThemAll.model.vo.MeatWeaponVO;
import com.elparole.utils.RollScaleFeatures;
import com.greensock.TweenLite;

import flash.events.Event;

import flash.events.MouseEvent;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import org.osflash.signals.Signal;

public class InGameView extends InGameScr implements IScreen
{

	public var blockMouse:Signal = new Signal(Boolean);
	public var weaponSelected:Signal = new Signal(int);
	public var pauseSelected:Signal = new Signal(Array);
	public var instructionsHided:Signal = new Signal();

	public var backToMain:Signal = new Signal();

	private var pauseScr:PauseScr = new PauseScr();
	private var instScr:InstructionsScr = new InstructionsScr();

	private var loaderScr:LoadingScr = new LoadingScr();
	private var rsf:RollScaleFeatures = new RollScaleFeatures();
	private var removingInfo:Boolean = false;

	public function InGameView() {
		loaderScr.percTf.visible = false;
		sponsorLogoMc.visible = false;
	}

	public function init():void {
		pauseScr.unpauseBackMC.addEventListener(MouseEvent.CLICK, onUnpause);
		for (var i:int = 1; i < 6; i++) {
			this["weaponBt"+i].iconsMC.gotoAndStop(i);
			this["weaponBt"+i].indexMC.gotoAndStop(i);
			this["weaponBt"+i].selectionMC.visible = false;
			this["weaponBt"+i].progressMC.gotoAndStop(1);
			this["weaponBt"+i].buttonMode=true;
			this["weaponBt"+i].y = 416+200;
			this["weaponBt"+i].addEventListener(MouseEvent.ROLL_OVER, onOver);
			this["weaponBt"+i].addEventListener(MouseEvent.ROLL_OUT, onOut);
			this["weaponBt"+i].addEventListener(MouseEvent.CLICK, onSelectWeapon);
			rsf.addButton(this["weaponBt"+i]);
		}
		rsf.addButtons([pauseBt, pauseScr.backToMainBt, pauseScr.moreGamesBt, instScr.continueBt]);

		statsInfoMC.statsInfo2.sheepsTf.text = "0";
		statsInfoMC.statsInfo2.moneyTf.text = "$0";

		statsInfoMC.addEventListener(MouseEvent.ROLL_OVER, onOver);
		statsInfoMC.addEventListener(MouseEvent.ROLL_OUT, onOut);
//		soundsPanel.addEventListener(MouseEvent.ROLL_OVER, onOver);
//		soundsPanel.addEventListener(MouseEvent.ROLL_OUT, onOut);
		pauseBt.addEventListener(MouseEvent.ROLL_OVER, onOver);
		pauseBt.addEventListener(MouseEvent.ROLL_OUT, onOut);
		pauseBt.addEventListener(MouseEvent.CLICK, onPause);
		pauseScr.backToMainBt.addEventListener(MouseEvent.CLICK, onBackToMain)
		pauseScr.moreGamesBt.addEventListener(MouseEvent.CLICK, onGoSite,false,0,true);
		pauseScr.sponsorLogoMC2.addEventListener(MouseEvent.CLICK, onGoSite,false,0,true);
		pauseScr.sponsorLogoMC2.buttonMode = true;

		instScr.continueBt.addEventListener(MouseEvent.CLICK, onUnpause, false,0,true);
		if(PublishMode.ShowBrand){
			sponsorLogoMC2.addEventListener(MouseEvent.CLICK, onGoSite,false,0,true);
			sponsorLogoMC2.buttonMode= true;
		}else{
			sponsorLogoMC2.visible = false;
		}


		this.weaponBt1.selectionMC.visible = true;

		statsInfoMC.y = 10-200;
		pauseBt.y = 32-200;
		sponsorLogoMc.y = 4-200;

		addChild(loaderScr);

	}


	private function onGoSite(event:Event):void {
		navigateToURL(new URLRequest(PublishMode.SponsorLink),'_blank');
	}

	private function onBackToMain(event:MouseEvent):void {
		removeChild(pauseScr);
		backToMain.dispatch();
	}

	private function onPause(event:MouseEvent):void {
		pauseSelected.dispatch([true, -1]);
	}

	private function removePause(...args):void {

		removingInfo = false;
		pauseScr.parent && removeChild(pauseScr);
		instScr.parent && removeChild(instScr);
	}

	private function onSelectWeapon(event:MouseEvent):void {
		for (var i:int = 1; i < 6; i++) {
			this["weaponBt"+i].selectionMC.visible = false;
		}
		event.currentTarget.selectionMC.visible = true;
		weaponSelected.dispatch(int(event.currentTarget.name.charAt(8)));
	}

	private function onOut(event:MouseEvent):void {
		blockMouse.dispatch(false);
	}

	private function onOver(event:MouseEvent):void {
		blockMouse.dispatch(true);
	}


	public function reset():void{

	}

	public function destroy() :void {
		pauseScr.parent && removeChild(pauseScr);
		rsf.removeAllButtons();
	}

	public function setLives(value:int):void {
		statsInfoMC.statsInfo2.sheepsTf.text = String(value);
	}

	public function setScore(score:int):void {
		statsInfoMC.statsInfo2.moneyTf.text = "$"+String(score);
//		scoreMc.scoreTf.text = StringUtils.renderZeroScore(7, score);
	}

	public function onWeaponsUpdate(weapons:Array, meatWeapon:MeatWeaponVO):void {
		for (var i:int = 1; i < 6; i++) {
			this["weaponBt"+i].visible = weapons[i-1].enabled;
			this["weaponBt"+i].selectionMC.visible = (meatWeapon==weapons[i-1]);
			this["weaponBt"+i].progressMC.gotoAndStop(
					Math.floor(this["weaponBt"+i].progressMC.totalFrames*weapons[i-1].percLoaded));
		}
	}

	public function onCashChanged(value:int):void {
		statsInfoMC.statsInfo2.moneyTf.text= "$"+value;
	}

	public function onSheepsChanged(value:int):void {
		statsInfoMC.statsInfo2.sheepsTf.text= String(value);
	}

	public function showPause():void {
		addChild(pauseScr);
		pauseScr.alpha = 0;
		TweenLite.to(pauseScr,0.5,{alpha:1});
	}

	private function onUnpause(event:MouseEvent = null):void {

		if(removingInfo)
			return;

		if(event){
			event.stopImmediatePropagation();
			event.preventDefault();
		}
		removingInfo = true;

		if(pauseScr.parent){
			pauseScr.alpha = 1;
			TweenLite.to(pauseScr,0.5,{alpha:0});
			TweenLite.delayedCall(0.5,removePause);
			pauseSelected.dispatch([false,-1]);
		}else if(instScr.parent){
			instScr.alpha = 1;
			TweenLite.to(instScr,0.5,{alpha:0});
			TweenLite.delayedCall(0.5,removePause);
			pauseSelected.dispatch([false,-1]);
		}
	}

	public function showInstructions(frame:int):void{
		instScr.infoMC.gotoAndStop(frame);
		this.addChild(instScr);
		instScr.alpha = 0;
		if(frame == 3){
			instScr.mouseChildren = instScr.mouseEnabled = false;
			instScr.continueBt.mouseChildren = instScr.continueBt.mouseEnabled = false;
			instScr.infoMC.mouseChildren = instScr.infoMC.mouseEnabled = false;
		}else{
			instScr.mouseChildren = instScr.mouseEnabled = true;
			instScr.continueBt.mouseChildren = instScr.continueBt.mouseEnabled = true;
			instScr.infoMC.mouseChildren = instScr.infoMC.mouseEnabled = true;
		}
			TweenLite.to(instScr, 0.4,{alpha:1});
	}

	public function onShowControls():void {
		loaderScr.parent && removeChild(loaderScr);

		for (var i:int = 1; i < 6; i++) {
			TweenLite.to(this["weaponBt"+i], 0.4,{delay:0.5,y:416});
//			trace('frame',this["weaponBt"+i].progressMC.totalFrames*weapons[i-1].percLoaded);
		}
		TweenLite.to(sponsorLogoMc, 0.4,{delay:0.5,y:4});
		TweenLite.to(statsInfoMC, 0.4,{delay:0.5,y:10});
		TweenLite.to(pauseBt, 0.4,{delay:0.5,y:32});
	}

	public function onExternalHideInstructions():void {
		if(instScr.parent)
			onUnpause();
	}
}
}
