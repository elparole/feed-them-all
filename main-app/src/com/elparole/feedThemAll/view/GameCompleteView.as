/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 17:17
 */
package com.elparole.feedThemAll.view
{
import com.elparole.utils.RollScaleFeatures;
import com.greensock.TweenLite;
import com.greensock.easing.Strong;

import flash.events.MouseEvent;
import flash.utils.setTimeout;

import org.osflash.signals.Signal;

public class GameCompleteView extends GameCompleteScr implements IScreen
{
	public const showStart:Signal = new Signal();

	private var rsf:RollScaleFeatures = new RollScaleFeatures();


	public function GameCompleteView() {
		rsf.addButtons([continueBt]);
	}

	private function onStart(event:*):void {


		TweenLite.to(elem2Bg, 0.4,{y:168-500, ease:Strong.easeIn});
		TweenLite.to(elem1Bg, 0.4,{delay:0.6,x:189-500, ease:Strong.easeIn});
		TweenLite.to(elem3Bg, 0.4,{delay:0.6,x:74-500, ease:Strong.easeIn});
		TweenLite.to(continueBt, 0.4,{delay:0.6,x:509+500, ease:Strong.easeIn});
		setTimeout(onShowStart,1400);
	}

	private function onShowStart():void {
		showStart.dispatch();
	}

	public function destroy():void {

	}

	public function init():void {
		trace('init gcv');

//		elem2Bg.scaleX = elem2Bg.scaleY = 1.17;
		elem2Bg.y = 168-500;
		elem1Bg.x = 189-500;
		elem3Bg.x = 74-500;
		continueBt.x = 509+500;
//		var tlp:TimelineLite = new TimelineLite();
//		tlp.addChild(TweenLite.to(elem2Bg, 1,{y:168}));
		TweenLite.to(elem2Bg, 1,{y:168});
//		tlp.addChild(TweenLite.to(elem2Bg, 1,{scaleX:1.5,scaleY:1.5}));
//		tlp.addChild(TweenLite.to(elem2Bg, 0.4,{scaleX:1.17,scaleY:1.17}));
//		tlp.play();
		TweenLite.to(elem1Bg, 0.6,{delay:1.4,x:189, ease:Strong.easeIn});
		TweenLite.to(elem3Bg, 0.6,{delay:1.4,x:74, ease:Strong.easeIn});
		TweenLite.to(continueBt, 0.6,{delay:1.4,x:509, ease:Strong.easeIn});
		continueBt.addEventListener(MouseEvent.CLICK, onStart, false,0,true);
//
	}

}
}
