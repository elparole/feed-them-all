/**
 * User: Elparole
 * Date: 31.07.12
 * Time: 01:22
 */
package com.elparole.feedThemAll.view
{
import com.elparole.feedThemAll.service.SharedObjectService;

import flash.events.TimerEvent;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;
import flash.utils.Dictionary;
import flash.utils.Timer;

public class SoundManager
{
	[Inject]
	public var soService:SharedObjectService;

	public static var instance:SoundManager;
	
	private var sounds:Dictionary;
	private var soundTransform:SoundTransform;
	private var musicTransform:SoundTransform;
	private var currentChannels:Array = [];
	private var _soundOn:Boolean = true;
	private var _musicOn:Boolean = true;
	private var fadeOutTimer:Timer = new Timer(500/20,20);
	private var fadeInTimer:Timer = new Timer(500/20,20);
	private var musicVolume:Number = 0.5;
	private var maxMusicVolume:Number = 0.5;
	private var soundVolume:Number = 0.5;
	private var currentMusic:SoundChannel;

	private var currentMusicClass:Class;
	public const globalSoundOn:Boolean = true;
//	public const globalSoundOn:Boolean = false;
	public const globalMusicOn:Boolean = true;
//	public const globalMusicOn:Boolean = false;

	public function SoundManager() {
		instance =this;

		sounds = new Dictionary();
		sounds[Click_04Snd] = new Click_04Snd();
		sounds[CoverMusicSnd] = new CoverMusicSnd();
		sounds[feedmmainthemeSnd] = new feedmmainthemeSnd();
		sounds[GameOverLouderSnd] = new GameOverLouderSnd();
		sounds[levelwinSnd] = new levelwinSnd();
		sounds[sheep_05Snd] = new sheep_05Snd();
		sounds[sheep_06Snd] = new sheep_06Snd();
		sounds[sheep_07Snd] = new sheep_07Snd();
		sounds[monster_eating_meat_02Snd] = new monster_eating_meat_02Snd();
		sounds[monster_eating_meat_03Snd] = new monster_eating_meat_03Snd();
		sounds[monster_eating_meat_04Snd] = new monster_eating_meat_04Snd();
		sounds[monster_eating_meat_05Snd] = new monster_eating_meat_05Snd();
		sounds[monster_eating_sheep_03Snd] = new monster_eating_sheep_03Snd();
		sounds[monster_eating_sheep_04Snd] = new monster_eating_sheep_04Snd();
		sounds[MonsterHittingFence_04Snd] = new MonsterHittingFence_04Snd();
		sounds[MonsterHittingFence_05Snd] = new MonsterHittingFence_05Snd();
		sounds[MonsterHittingFence_06Snd] = new MonsterHittingFence_06Snd();
		sounds[FenceBeingDestroyed_02Snd] = new FenceBeingDestroyed_02Snd();
		sounds[MonsterGrumble_03Snd] = new MonsterGrumble_03Snd();
		sounds[MonsterGrumble_04Snd] = new MonsterGrumble_04Snd();
		sounds[MonsterGrumble_05Snd] = new MonsterGrumble_05Snd();
		sounds[MonsterGrumble_06Snd] = new MonsterGrumble_06Snd();
		sounds[MonsterGrumble_07Snd] = new MonsterGrumble_07Snd();

		sounds[BetterMeat_HittingGroundSnd] = new BetterMeat_HittingGroundSnd();
		sounds[BetterMeat_SelectSnd] = new BetterMeat_SelectSnd();
		sounds[CantDropMeatYetSnd] = new CantDropMeatYetSnd();
		sounds[FrozenMeat_HittingGroundSnd] = new FrozenMeat_HittingGroundSnd();
		sounds[FrozenMeat_SelectSnd] = new FrozenMeat_SelectSnd();
		sounds[Money_CollectSnd] = new Money_CollectSnd();
		sounds[Money_SpentSnd] = new Money_SpentSnd();
		sounds[Monster_ExplodeSnd] = new Monster_ExplodeSnd();
		sounds[RegularMeat_HittingGroundSnd] = new RegularMeat_HittingGroundSnd();
		sounds[RegularMeat_SelectSnd] = new RegularMeat_SelectSnd();
		sounds[SheepMeat_HittingGroudSnd] = new SheepMeat_HittingGroudSnd();
		sounds[SheepMeat_SelectSnd] = new SheepMeat_SelectSnd();
		sounds[TNTSausage_HittingGroundSnd] = new TNTSausage_HittingGroundSnd();
		sounds[TNTSausage_SelectSnd] = new TNTSausage_SelectSnd();
		sounds[BounceofFrozenmonsterSnd] = new BounceofFrozenmonsterSnd();
//		sounds[CoinHit2Snd] = new CoinHit2Snd();
////		sounds[CoinHitSnd] = new CoinHitSnd();
//		sounds[CoinSnd] = new CoinSnd();

		soundTransform = new SoundTransform(0.5);
		musicTransform = new SoundTransform(0.5);

		fadeOutTimer.addEventListener(TimerEvent.TIMER, onSilent);
		fadeInTimer.addEventListener(TimerEvent.TIMER, onLoud);
		fadeInTimer.addEventListener(TimerEvent.TIMER_COMPLETE, onCompleteFadeIn);
		
	}

	private function onCompleteFadeIn(event:TimerEvent):void {
		trace('sm, onCompleteFadeIn');

		if(!musicOn)
			return;

		if(currentChannels.indexOf(currentMusic,1)>=0)
			currentChannels.splice(currentChannels.indexOf(currentMusic,1));
		stopAllMusic();
		currentChannels.push(currentMusic);		
		currentMusic = null;
	}

	private function onLoud(event:TimerEvent):void {
		try {
			currentMusic.soundTransform = new SoundTransform(fadeInTimer.currentCount * (musicVolume / 20));
		} catch (e:Error) { }
	}

	private function onSilent(event:TimerEvent):void {
		try {
			currentMusic.soundTransform = new SoundTransform(musicVolume-fadeInTimer.currentCount * (musicVolume / 20));
		} catch (e:Error) { }

//		for each (var soundChannel:SoundChannel in currentChannels) {
//			soundChannel.soundTransform = new SoundTransform(musicVolume - fadeOutTimer.currentCount*(musicVolume/20));
//		}
	}

	public function play(soundClazz:Class):void {
		if(globalSoundOn)
//		if(soundClazz == Hit11Snd)
			(sounds[soundClazz] as Sound).play(0,0,soundTransform);
	}

	public function playGameOver():void {
		trace('sm, playGameOver');
		if(!globalMusicOn || currentMusicClass == GameOverLouderSnd)
			return;

		currentMusicClass = GameOverLouderSnd;
		initFades();
//		var musicTransform:SoundTransform = new SoundTransform(0);
		currentMusic = sounds[GameOverLouderSnd].play(0,1,musicTransform);
//		currentChannels.push(currentMusic);
	}

	public function playLevelWin():void {
		trace('sm, playGameOver');
		if(!globalMusicOn || currentMusicClass == levelwinSnd)
			return;

		currentMusicClass = levelwinSnd;
		initFades();
		currentMusic = sounds[levelwinSnd].play(0,1,musicTransform);
//		currentChannels.push(currentMusic);
	}

	public function playCover():void {
		trace('sm, playCover');
		if(!globalMusicOn || currentMusicClass == CoverMusicSnd)
			return;

		currentMusicClass = CoverMusicSnd;
		initFades();
		currentMusic = sounds[CoverMusicSnd].play(0,10000,musicTransform)
//		currentChannels.push(currentMusic);
	}

	public function playGame():void {
		trace('sm, playgame');
		if(!globalMusicOn || currentMusicClass == feedmmainthemeSnd)
			return;

		currentMusicClass = feedmmainthemeSnd;
		initFades();
		currentMusic = sounds[feedmmainthemeSnd].play(0,10000,musicTransform);
//		currentChannels.push(currentMusic);
	}

	private function initFades():void {
		trace('initFades');
		currentMusic && currentMusic.stop();
		currentMusic = null;
//		fadeOutTimer.reset();
//		fadeOutTimer.start();
		fadeInTimer.reset();
		fadeInTimer.start();
	}
	
	private function stopAllMusic():void {
		trace('stopAllMusic');
		for each (var soundChannel:SoundChannel in currentChannels) {
			soundChannel.stop();
		}
		currentChannels.length = 0;
	}

	public function get soundOn():Boolean {
		return _soundOn;
	}

	public function set soundOn(value:Boolean):void {
		soundTransform.volume = value? 0.5:0;
		soService.setSound(value);
		_soundOn = value;
	}

	public function get musicOn():Boolean {
		return _musicOn;
	}

	public function set musicOn(value:Boolean):void {

		soService.setMusic(value);
		if(!globalMusicOn)
			return;

		musicVolume = musicTransform.volume = value? 0.5:0;
		for each (var soundChannel:SoundChannel in currentChannels) {
//			soundChannel.soundTransform = null;
			soundChannel.soundTransform = musicTransform;
		}
		
		if(currentMusic)
			currentMusic.soundTransform = musicTransform;
		
		_musicOn = value;
	}
}
}
