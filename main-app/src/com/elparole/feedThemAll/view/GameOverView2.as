/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 17:17
 */
package com.elparole.feedThemAll.view
{
import com.gskinner.motion.GTweener;

import flash.display.MovieClip;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.net.URLRequest;
import flash.net.URLRequestMethod;
import flash.net.URLVariables;
import flash.net.navigateToURL;

import org.casalib.events.LoadEvent;
import org.casalib.load.DataLoad;
import org.osflash.signals.Signal;

public class GameOverView2 extends MovieClip implements IScreen
{
	public var startGame:Signal = new Signal();

	private var _dl:DataLoad = new DataLoad('http://projects.blackmoondev.com/SkyKnight/link.txt');
	private var message:String;

	private var score:int;


	public function GameOverView2() {

	}


	public function init():void {
		continueBt.addEventListener(MouseEvent.CLICK, onStartClick, false,0,true);
		tweetBt.addEventListener(MouseEvent.CLICK, onTweet, false,0,true);
		onGetTweetLink();
		playMoreGames.addEventListener(MouseEvent.CLICK, onMoreGames, false,0,true);
		this.x = -640;
		gameWinLooseMc.stop();
		GTweener.to(this, 0.1,{x:0});
	}

	private function onStartClick(event:MouseEvent):void {
		GTweener.to(this, 0.1,{x:640},{onComplete:onStart});
	}

	private function onStart(event:*):void {
		startGame.dispatch();
	}

	public function destroy():void {
		continueBt.removeEventListener(MouseEvent.CLICK, onStartClick);
		tweetBt.removeEventListener(MouseEvent.CLICK, onTweet);
		playMoreGames.removeEventListener(MouseEvent.CLICK, onMoreGames);
	}

	private function onMoreGames(event:MouseEvent):void {
		navigateToURL(new URLRequest("http://blackmoondev.com/"));
	}

	private function onGetTweetLink():void {
		_dl.addEventListener(LoadEvent.COMPLETE, onLoadedTweet,false,0,true);
		_dl.start();
	}

	private function onLoadedTweet(event:LoadEvent):void {
		message = _dl.dataAsString;
	}
	
	private function onTweet(event:Event):void {

		
		//The path to the status page
		var path:URLRequest = new URLRequest("http://twitter.com/home");

		//using the GET method means you will use a QueryString to send the variables
		//twitter likes this
		path.method = URLRequestMethod.GET;

		//The custom status message to send. Might as well let twitter worry about the char count
		var tweetVars:URLVariables = new URLVariables();//escape("Major Callisto rocks my world http://www.majorcallisto.com");

		//Twitter uses a "status" variable to prepopulate the page
//		tweetVars.status = "I scored "+score+" in Sky Knight. It's so much FUN! FUN! FUN! ->"+_dl.dataAsString;//_ http://tinyurl.com/bp2lesc ";
		tweetVars.status = message;//_ http://tinyurl.com/bp2lesc ";

		//Add the variables to the URLRequest
		path.data = tweetVars;

		//Open the page in a "_blank" (new) window and let the user worry about closing
		navigateToURL(path, "_blank");
	}

	public function setResult(score:int, renderedScore:String, isWin:Boolean):void {
		this.score = score;
		resultTf.text = String(renderedScore);
		gameWinLooseMc.gotoAndStop(isWin? 1:2);
	}
}
}
