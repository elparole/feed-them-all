/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 16:43
 */
package com.elparole.feedThemAll.view
{
import com.elparole.feedThemAll.PublishMode;
import com.elparole.gamingUtils.SoundControls;
import com.elparole.utils.RollScaleFeatures;
import com.greensock.TweenLite;
import com.greensock.easing.Quad;
import com.gskinner.motion.GTweener;

import flash.events.Event;

import flash.events.MouseEvent;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import org.osflash.signals.Signal;

public class StartView extends StartScr implements IScreen
{

	private var soundsControls:SoundControls = new SoundControls();

	public var startGame:Signal;
	private var playesNum:int = 1;
	public var hideEnemies:Signal;
	private var rsf:RollScaleFeatures = new RollScaleFeatures();


	public function StartView() {
		startGame = new Signal();//players number
		hideEnemies = new Signal();

		playBt.addEventListener(MouseEvent.CLICK, onPlayOne, false,0,true);

		rsf.addButtons([moreGamesBt,playBt,continueBt]);
		backgroundMc.bmLogoMc.visible = false;
	}

//	[Live]
	public function init():void {
//		var lc:LevelCompleteView = new LevelCompleteView();
//		addChild(lc);
//		lc.init();
//		lc.showStars(3);
//		return;
		
//		if(!continueBt.hasEventListener(MouseEvent.CLICK))
		continueBt.addEventListener(MouseEvent.CLICK, onContinue, false,0,true);

		this.x = 0;
//		GTweener.to(this, 0.3,{x:0});
//		playBt.scaleX = playBt.scaleY = 0;
//		moreGamesBt.scaleX = moreGamesBt.scaleY = 0;
		moreGamesBt.x = 144-300;
		moreGamesBt.y = 426;
		playBt.x = 465+400;
		playBt.y = 403;
		instructionsMc.y = 15;
		continueBt.y = 421;
		backgroundMc.bmLogoMc.x = 500+300;
		backgroundMc.rightTopMonsterMc.scaleX = backgroundMc.rightTopMonsterMc..scaleY = 0;
		backgroundMc.leftBottomMonsterMc.scaleX = backgroundMc.leftBottomMonsterMc.scaleY = 0;
		backgroundMc.titleMc.scaleX = backgroundMc.titleMc.scaleY = 0;

		backgroundMc.rightTopMonsterMc.x = 525;
		backgroundMc.rightTopMonsterMc.y = 94;
		backgroundMc.titleMc.x = 412;
		backgroundMc.titleMc.y = 189;
		backgroundMc.bmLogoMc.y = 237;

		backgroundMc.leftBottomMonsterMc.x = 131;
		backgroundMc.leftBottomMonsterMc.y = 274;
		sponsorLogoMc.visible = !PublishMode.ShowBrand;
		moreGamesBt.y = 429;
		playBt.y = 403;

		moreGamesBt.addEventListener(MouseEvent.CLICK, onGoSite,false,0,true);
		moreGamesBt.buttonMode = true;

		sponsorLogoMC2.visible = PublishMode.ShowBrand;
		sponsorLogoMC2.addEventListener(MouseEvent.CLICK, onGoSite,false,0,true);
		sponsorLogoMC2.buttonMode= true;

		likeUsBt.visible = PublishMode.AGBrand;
		likeUsBt.addEventListener(MouseEvent.CLICK, onLikeSite,false,0,true);
		likeUsBt.buttonMode= true;

		TweenLite.to(backgroundMc.rightTopMonsterMc,0.5,{scaleX:1,scaleY:1,ease:Quad.easeOut});
		TweenLite.to(backgroundMc.leftBottomMonsterMc,0.5,{delay:0.4,scaleX:1,scaleY:1,ease:Quad.easeOut});
		TweenLite.to(backgroundMc.titleMc,0.5,{delay:0.8,scaleX:0.88,scaleY:0.88/*,ease:Bounce.easeOut*/});

		TweenLite.to(playBt,0.3,{delay:1.2,x:465/*,ease:Bounce.easeOut*/});
		TweenLite.to(moreGamesBt,0.3,{delay:1.4,x:144/*,ease:Bounce.easeOut*/});

		TweenLite.to(backgroundMc.bmLogoMc,0.3,{delay:1.7,x:500/*,ease:Bounce.easeOut*/});
	}

	private function onGoSite(event:Event):void {
		navigateToURL(new URLRequest(PublishMode.SponsorLink),'_blank');
	}

	private function onLikeSite(event:Event):void {
		navigateToURL(new URLRequest(PublishMode.SponsorLikeLink),'_blank');
	}

	private function onContinue(event:MouseEvent):void {
		onStart();
	}

	private function onPlayOne(event:MouseEvent):void {
//		rsf.removeButton(playBt);
//		rsf.removeButton(moreGamesBt);
//		instructionsMc.visible = true;
//		instructionsMc.players2AssetsMc.visible = false;
		continueBt.visible = true;
//		this.x = 640;

		TweenLite.to(backgroundMc.rightTopMonsterMc, 0.3,{y:backgroundMc.rightTopMonsterMc.y-500});
		TweenLite.to(backgroundMc.titleMc, 0.3,{y:backgroundMc.titleMc.y-500});
		TweenLite.to(backgroundMc.bmLogoMc, 0.3,{y:backgroundMc.bmLogoMc.y-500});

		TweenLite.to(backgroundMc.leftBottomMonsterMc, 0.3,{y:backgroundMc.leftBottomMonsterMc.y+500});
		TweenLite.to(playBt, 0.3,{y:playBt.y+500});
		TweenLite.to(moreGamesBt, 0.3,{y:moreGamesBt.y+500});

		TweenLite.to(this, 0.3,{delay:0.3,x:640});

		playesNum = 1;
//		hideEnemies.dispatch();
	}

	private function onPlayTwo(event:MouseEvent):void {
//		instructionsMc.visible = true;
//		instructionsMc.players2AssetsMc.visible = true;
		continueBt.visible = true;
		GTweener.to(this, 0.3,{x:640});
		playesNum = 2;
//		hideEnemies.dispatch();
	}

	private function onStart(event:* = null):void {
		TweenLite.to(instructionsMc, 0.3,{y:instructionsMc.y-500});
		TweenLite.to(continueBt, 0.3,{y:continueBt.y+500});

		TweenLite.delayedCall(0.3,function():void{startGame.dispatch();});
//		if(continueBt.hasEventListener(MouseEvent.CLICK))
//			continueBt.removeEventListener(MouseEvent.CLICK, onStart);
		
//		stage.quality = StageQuality.LOW;

	}

	public function destroy():void {
//		startGame.removeAll();
	}
}
}
