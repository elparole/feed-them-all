/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 17:17
 */
package com.elparole.feedThemAll.view
{
import com.deviant.HueColorMatrixFilter;
import com.elparole.feedThemAll.PublishMode;
import com.elparole.feedThemAll.model.PlayerConfig;
import com.elparole.feedThemAll.model.ShopConfig;
import com.elparole.utils.RollScaleFeatures;
import com.greensock.TweenLite;

import flash.display.MovieClip;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import org.osflash.signals.Signal;

public class ShopView extends ShopScr implements IScreen
{
	public var startGame:Signal = new Signal();

	private var playerConfig:PlayerConfig;

	private var shopConfig:ShopConfig = new ShopConfig();
	private var rsf:RollScaleFeatures = new RollScaleFeatures();

//	private var introMask:MovieClip = new IntroMaskScr();
//	private var outroMask:MovieClip = new OutroMaskScr();

	private var hcm:HueColorMatrixFilter = new HueColorMatrixFilter();
	public static var _instance:ShopView;


	public function ShopView() {
//		graphics.beginFill(0x099000,1);
//		graphics.drawRect(0,0,640,480);
//		graphics.endFill();

		_instance ||= this;
		for (var i:int = 1; i < 6; i++) {
			shopControlsMC['meat'+i+'BuyMc'].meatPrizeTf.mouseEnabled = false;
		}
//		shopControlsMC['fencePrizeTf'].mouseEnabled = false;
		continueBt.addEventListener(MouseEvent.CLICK, onStartClick, false,0,true);
	}

	private function onStartClick(event:MouseEvent):void {
		rsf.removeAllButtons();
		TweenLite.to(meatsBg,0.5,{x:meatsBg.x-500});
		TweenLite.to(shopControlsMC,0.5,{x:shopControlsMC.x-500});
		TweenLite.to(infoBg,0.5,{x:infoBg.x+500});
		TweenLite.to(moneyTf,0.5,{x:moneyTf.x+500});
		TweenLite.to(moneyBg,0.5,{x:moneyBg.x+500});
		TweenLite.to(weaponsInfoMc,0.5,{x:weaponsInfoMc.x+500});
		TweenLite.to(continueBt,0.5,{x:continueBt.x+500});

		TweenLite.delayedCall(0.5,onStart);
//		GTweener.to(this, 0.1,{x:640},{onComplete:onStart});
	}

	private function onStart(...args):void {

//		meatsBg.x+=500;
//		shopControlsMC.x+=500;
//		infoBg.x-=500;
//		weaponsInfoMc.x-=500;
//		continueBt.x-=500;
		startGame.dispatch();
	}

	public function destroy():void {
	}
//
	public function setConfig(playerConfig:PlayerConfig):void {
		this.playerConfig = playerConfig;
		moneyTf.text = "$"+playerConfig.coins;
		renderSkills();
	}
//
	public function init():void {
//		var lc:LevelCompleteView = new LevelCompleteView();
//		addChild(lc);
//		lc.init();
//		lc.showStars(3);
//		return;

//		var lc:GameOverView = new GameOverView();
//		addChild(lc);
//		lc.init();
//		return;

//		if(PublishMode.GameGab && !gabLogo){
//			gabLogo = new GabLogoScr();
//			addChild(gabLogo);
//			gabLogo.x=486+87;
//			gabLogo.y=0;
//
//			gabLogo.width=63;
//			gabLogo.height=50;
//		}

		x = 0;
		weaponsInfoMc.gotoAndStop(1);
		for (var i:int = 1; i < 7; i++) {
			setButton(shopControlsMC['meat'+i+'BuyMc']);
			shopControlsMC['meat'+i+'BuyMc'].gotoAndStop(i);
			shopControlsMC['meat'+i+'BuyMc'].y = -300;

			TweenLite.to(shopControlsMC['meat'+i+'BuyMc'],0.5,{delay:0.9-0.1*i, y:76*(i-1)});
			rsf.addButton(shopControlsMC['meat'+i+'BuyMc'].meatBuyBt);
		}
		rsf.addButton(continueBt);

		shopControlsMC.scaleX = shopControlsMC.scaleY = 1;
		shopControlsMC.meat6BuyMc.meatUpgrade.up2.visible = false;
		shopControlsMC.meat6BuyMc.meatUpgrade.up3.visible = false;

		shopControlsMC.x = 9;
		meatsBg.x = 200;
		weaponsInfoMc.x = 409;
		infoBg.x = 518;
		continueBt.x = 515;
		moneyBg.x = 412;
		moneyTf.x = 460;


		meatsBg.alpha = 0;
		infoBg.alpha = 0;
		moneyBg.alpha = 0;
		continueBt.alpha = 0;
		weaponsInfoMc.alpha = 0;
		moneyBg.alpha = 0;
		moneyTf.alpha = 0;
		TweenLite.to(moneyBg,0.5,{alpha:1});
		TweenLite.to(moneyTf,0.5,{alpha:1});
		TweenLite.to(weaponsInfoMc,0.5,{alpha:1});
		TweenLite.to(moneyBg,0.5,{alpha:1});
		TweenLite.to(meatsBg,0.5,{alpha:1});
		TweenLite.to(infoBg,0.5,{delay:0.2,alpha:1});
		TweenLite.to(infoBg,0.5,{delay:0.4,alpha:1});
		TweenLite.to(continueBt,0.5,{delay:0.6,alpha:1});

//		sponsorLogoMC2.addEventListener(MouseEvent.CLICK, onGoSite,false,0,true);
//		sponsorLogoMC2.buttonMode= true;
//		sponsorLogoMC.visible = false;

//		setButton(shopControlsMC['fenceBuyBt']);

		trace('shop view init'+"$"+String(playerConfig.coins));
		moneyTf.text = "$"+String(playerConfig.coins);

//		gunBuyBt.addEventListener(MouseEvent.CLICK, onBuy);
//		healthBuyBt.addEventListener(MouseEvent.CLICK, onBuy);
//		coolerBuyBt.addEventListener(MouseEvent.CLICK, onBuy);
//		bombsBuyBt.addEventListener(MouseEvent.CLICK, onBuy);

//		gunLb.text = PublishMode.getText('GUN');
//		bombsLb.text = PublishMode.getText('BOMBS');
//		healthLb.text = PublishMode.getText('HEALTH');
//		coolerLb.text = PublishMode.getText('COOLER');

//		PublishMode.setButtonText(backToMenuBt,'BACK TO MAIN');
//		PublishMode.setButtonText(gunBuyBt,'BUY');
//		PublishMode.setButtonText(healthBuyBt,'BUY');
//		PublishMode.setButtonText(coolerBuyBt,'BUY');
//		PublishMode.setButtonText(bombsBuyBt,'BUY');
//		PublishMode.setButtonText(continueBt,'CONTINUE');

//		maskCont.addChild(introMask);
//		introMask.gotoAndPlay(1);
	}


	private function onGoSite(event:Event):void {
		navigateToURL(new URLRequest(PublishMode.SponsorLink),'_blank');
	}
//
//
//	private function onContinue(event:MouseEvent):void {
//		showCountDown();
//	}
//
//	private function onBuy(event:MouseEvent):void {
//		soundManager.play(CoinSnd);
//		var skillType:String = (event.currentTarget.name as String).slice(0,event.currentTarget.name.length-5);
//		playerConfig.buy(skillType, shopConfig.prizes);
//		renderSkills();
//	}
//
	public function renderSkills():void {
		for (var j:int = 1; j < 7; j++) {
			for (var i:int = 1; i < 4; i++) {
				renderSkill(j,i);
			}
		}
//		renderSkill('fence',1);

	}
//
	private function renderSkill(s:int, i:int):void {
		if(playerConfig['meat'+s+'Skill']>=i){
			shopControlsMC['meat'+s+'BuyMc'].meatUpgrade['up'+i].gotoAndStop(2);
			shopControlsMC['meat'+s+'BuyMc'].meatUpgrade['up'+i].mouseEnabled = shopControlsMC['meat'+s+'BuyMc'].meatUpgrade['up'+i].mouseChildren = false;
			shopControlsMC['meat'+s+'BuyMc'].meatUpgrade['up'+i].alpha =1;
		}else{
			shopControlsMC['meat'+s+'BuyMc'].meatUpgrade['up'+i].gotoAndStop(1);
			if(i>playerConfig['meat'+s+'Skill']){
				if((i==playerConfig['meat'+s+'Skill']+1) && shopConfig.canAfford(playerConfig.coins, String('meat'+s), i)){
					shopControlsMC['meat'+s+'BuyMc'].meatUpgrade['up'+i].alpha =1;
					shopControlsMC['meat'+s+'BuyMc'].meatUpgrade['up'+i].mouseEnabled = shopControlsMC['meat'+s+'BuyMc'].meatUpgrade['up'+i].mouseChildren = true;
//					this[s+i+'Bt'].visible =true;
				}else{
					shopControlsMC['meat'+s+'BuyMc'].meatUpgrade['up'+i].alpha =0.5;
					shopControlsMC['meat'+s+'BuyMc'].meatUpgrade['up'+i].mouseEnabled = shopControlsMC['meat'+s+'BuyMc'].meatUpgrade['up'+i].mouseChildren = false;
//					this[s+i+'Bt'].visible =false;
				}
			}
		}
////		var ct:ColorTransform= new ColorTransform();
		if(i==playerConfig['meat'+s+'Skill']+1 ){
			if(shopConfig.canAfford(playerConfig.coins, String('meat'+s), i)){
				enableButton(true, String(s));
			}else{
				enableButton(false, String(s));
			}
			shopControlsMC['meat'+s+'BuyMc'].meatPrizeTf.text = '$'+shopConfig.prizes['meat'+s+i];
		}else if(playerConfig['meat'+s+'Skill'] == playerConfig['meat'+s+'SkillMax']){
			shopControlsMC['meat'+s+'BuyMc'].meatPrizeTf.text = '';
			enableButton(false, String(s));
		}
	}
//
	private function enableButton(enabled:Boolean, s:String):void {
		hcm = new HueColorMatrixFilter();
		hcm.Saturation = enabled? 1:0;
		(shopControlsMC['meat'+s+'BuyMc'].meatBuyBt as Object).alpha = enabled? 1:0.5;
		(shopControlsMC['meat'+s+'BuyMc'].meatBuyBt as Object).mouseEnabled = enabled;
		(shopControlsMC['meat'+s+'BuyMc'].meatBuyBt as Object).filters = [hcm.Filter];
	}
//
	public function setButton(mc:MovieClip):void {
		mc.buttonMode = true;
		mc.mouseChildren = true;
		mc.gotoAndStop(1);
		mc.addEventListener(MouseEvent.CLICK, onSelect);

		if(!mc.hasEventListener(MouseEvent.ROLL_OVER))
			mc.addEventListener(MouseEvent.ROLL_OVER, onRollOver,false,0,true);

	}

	private function onRollOver(event:MouseEvent):void {
		weaponsInfoMc.gotoAndStop(event.currentTarget.name=='fenceBuyBt'? 6:event.currentTarget.name.charAt(4));
	}
//
	private function onSelect(event:MouseEvent):void {
//		event.currentTarget.gotoAndStop(2);
//		soundManager.play(CoinSnd);
		var skillType:String = (event.currentTarget.name as String).split('BuyMc')[0];
		trace('skill type', skillType);
		playerConfig.buy(skillType, shopConfig.prizes);
		renderSkills();
//		switch(skillType){
//			case 'gun':
//				playerConfig.buy('gun');
//				break;
//		}
	}
//
//	private function getSkill(s:String):int {
//		var skill:int = 0;
//		for (var i:int = 1; i < 6; i++) {
//			skill = this[s+i+'Bt'].currentFrame ==2? skill+1:skill;
//		}
//		return skill;
//	}
//
//	public function destroy():void {
//
////		backToMenuBt.removeEventListener(MouseEvent.CLICK, onBackToMenu);
//		gunBuyBt.removeEventListener(MouseEvent.CLICK, onBuy);
//		healthBuyBt.removeEventListener(MouseEvent.CLICK, onBuy);
//		coolerBuyBt.removeEventListener(MouseEvent.CLICK, onBuy);
//		bombsBuyBt.removeEventListener(MouseEvent.CLICK, onBuy);
//	}
//
//	public function showCountDown():void {
//		GTweener.to(this, 0.1, {x:640});
//		countDownTimer.reset();
//		countDownTimer.start();
//		countDownTf.text = '3';
//	}
//
//	public function get pauseEnded():Signal {
//		return _pauseEnded;
//	}
//
//
//	public function get backToMenu():Signal {
//		return _backToMenu;
//	}
	public function onCashChanged(value:int):void {
		moneyTf.text = '$'+value;
		renderSkills();
	}
}
}
