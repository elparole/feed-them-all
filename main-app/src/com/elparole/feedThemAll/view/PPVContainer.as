/**
 * User: Elparole
 * Date: 26.01.13
 * Time: 14:20
 */
package com.elparole.feedThemAll.view
{

import com.elparole.components.DisplayPPV;
import com.elparole.components.IBitmapPlane;

import flash.display.BitmapData;
import flash.display.Sprite;
import flash.geom.Point;

import org.casalib.math.geom.Point3d;
import org.papervision3d.cameras.*;
import org.papervision3d.core.math.*;
import org.papervision3d.materials.*;
import org.papervision3d.objects.*;
import org.papervision3d.objects.primitives.*;
import org.papervision3d.render.*;
import org.papervision3d.scenes.*;
import org.papervision3d.view.*;

public class PPVContainer extends Sprite
{

	/** Camera Yaw */
	private var _camYaw				: Number = 45;//-Math.PI/2;

	/** Camera Pitch */
	private var _camPitch			: Number = -60;//Math.PI/2;

	/** Camera distance */
	private var _camDist			: Number = 100;//2000;

	/** Camera target */
	private var _camTarget			: DisplayObject3D = DisplayObject3D.ZERO;
	/** The PV3D scene to render */
	public var scene : Scene3D;

	/** This PV3D camera */
	public var camera : Camera3D;

	/** The PV3D renderer */
	public var renderer : BasicRenderEngine;

	/** The PV3D viewport */
	public var viewport : Viewport3D;

	private var ISOW:Number = 500;
	private var ISOZ:Number = 500;

	private var bm:BitmapMaterial;
	private var fnum:int = 0;
	private var n3d:Number3D;
	private var w:int;
	private var h:int;

	/**
	 * Constructor.
	 */
	public function PPVContainer(w:int = 640, h:int = 480) {
		this.w = w;
		this.h = h;
	}

	/**
	 * Init.
	 */
	public function init(isInteractive:Boolean = false) : void {


		// create a viewport
		viewport = new Viewport3D(w, h);

//		viewport.interactive = isInteractive;
		// add
		addChild(viewport);

		// create a frustum camera with FOV=60, near=10, far=10000
		camera = new Camera3D(60, 10, 10000);
		camera.ortho = true;
		camera.orbit(_camPitch, _camYaw, _camDist,_camTarget);

		// create a renderer
		renderer = new BasicRenderEngine();

		// create the scene
		scene = new Scene3D();

		// init the scene
		initScene();
	}

	/**
	 * Init the scene.
	 */
	private function initScene() : void {

	}

	/**
	 * Render!
	 */
	public function render() : void {

		fnum++;
//		moveSprites();

		// render
		renderer.renderScene(scene, camera, viewport);
	}

	public function movePlaneTo(plane:Plane, pos:Number3D = null):void{
		if(pos)
			plane.position = pos.clone();

		n3d = new Number3D(
				(plane.position.x + 0 + ISOW/2) % ISOW,
				0,
				(plane.position.z + -2 + ISOZ/2) % ISOZ);
		if (n3d.x < 0)
			n3d.x = ISOW - n3d.x;
		if (n3d.z < 0)
			n3d.z = ISOZ - n3d.z;

		n3d.x -= ISOW/2;
		n3d.z -= ISOZ/2;

		plane.position = n3d;		
	}
	
	public function getScreenCoords(display:DisplayPPV):Point{
		display.plane.calculateScreenCoords(camera);
		return new Point(display.plane.screen.x, display.plane.screen.y);
	}

	/**
	 *
	 * @param	event
	 */

	public function add3DBitmap(bitmapCont:IBitmapPlane, isInteractive:Boolean = false):void {
//		trace('add3DBitmap 1');
		var mbd:BitmapData = bitmapCont.bitmap;//(new TexturesAssets.mc1()).bitmapData;
		bm = new BitmapMaterial(mbd, false);
		bm.interactive = isInteractive;
//		offsets.push(Math.floor(Math.random()*5.999999));

//		trace('add3DBitmap 2');
		var plane:Plane = new Plane(
//					new BitmapMaterial(mbd,false),
//				new ColorMaterial(0xff0000+Math.random()*256*256),
				bm,
				128,128,0,0);
		plane.yaw(45);
		plane.pitch(-60+90);

//		trace('add3DBitmap 3');
		scene.addChild(plane);
//			plane.position= new Number3D(Math.floor(i/Math.sqrt(cubeNum))*25-100,0,Math.floor(i%Math.sqrt(cubeNum))*25-100);
//		plane.position= new Number3D(Math.random()*ISOW-ISOW/2,0,Math.random()*ISOZ-ISOZ/2);
		bitmapCont.plane = plane;
		bitmapCont.material = bm;
//		render();
		trace('add3DBitmap 4', scene.numChildren);
	}

	public function remove3DBitmap(plane:Plane):void {
		scene.removeChild(plane);
	}

	public function movePlaneToPoint(plane:Plane, position:Point3d):void {

		n3d = new Number3D(
				position.x,
				position.y,
				position.z);
		plane.position = n3d;
	}

	public function add3DBitmapWithSize(display:DisplayPPV, w:int, h:int):void {
		var mbd:BitmapData = display.bitmap;//(new TexturesAssets.mc1()).bitmapData;
		bm = new BitmapMaterial(mbd, false);
//		offsets.push(Math.floor(Math.random()*5.999999));
		var plane:Plane = new Plane(
//					new BitmapMaterial(mbd,false),
				bm,
				w,h,0,0);
		plane.yaw(45);
//		plane.yaw(0);
		plane.pitch(-60+90);
		scene.addChild(plane);
//			plane.position= new Number3D(Math.floor(i/Math.sqrt(cubeNum))*25-100,0,Math.floor(i%Math.sqrt(cubeNum))*25-100);
		plane.position= new Number3D(445,-200,445);
//		plane.position= new Number3D(Math.random()*ISOW-ISOW/2,0,Math.random()*ISOZ-ISOZ/2);
		display.plane = plane;
		display.material = bm;
	}

	public function getScreenTo3DCoords2(mouseX:Number, mouseY:Number, fixedY:Number = 0):Point3d {
		var ray:Number3D = camera.unproject(viewport.containerSprite.mouseX,viewport.containerSprite.mouseY);
		var cameraPosition:Number3D = new Number3D(camera.x,camera.y,camera.z);
		ray = Number3D.add(ray,cameraPosition);

		var deltY:Number = fixedY - ray.y;
		ray.x-=deltY*1.22;
		ray.z-=deltY*1.22;
		ray.y=fixedY;

		return new Point3d(ray.x,ray.y, ray.z);

	}
	public function getScreenTo3DCoords(mouseX:Number, mouseY:Number, fixedY:Number = 0):Point3d {
		var ray:Number3D = camera.unproject(viewport.containerSprite.mouseX*640/16,viewport.containerSprite.mouseY*640/16);
		var cameraPosition:Number3D = new Number3D(camera.x,camera.y,camera.z);
		ray = Number3D.add(ray,cameraPosition);

		var deltY:Number = fixedY - ray.y;
		ray.x-=deltY*1.22;
		ray.z-=deltY*1.22;
		ray.y=fixedY;

		return new Point3d(ray.x,ray.y, ray.z);
	}
}
}