/**
 * User: Elparole
 * Date: 26.01.13
 * Time: 11:55
 */
package com.elparole.feedThemAll.view
{
public interface IScreen
{
	function destroy():void;
	function init():void;
}
}
