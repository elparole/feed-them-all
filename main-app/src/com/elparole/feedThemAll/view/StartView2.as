
/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 16:43
 */
package com.elparole.feedThemAll.view
{
import com.gskinner.motion.GTweener;

import flash.display.Sprite;
import flash.events.MouseEvent;

import org.osflash.signals.Signal;

public class StartView2 extends Sprite implements IScreen
{
	public var startGame:Signal = new Signal();
	private var playesNum:int = 1;
	public var hideEnemies:Signal = new Signal();

	public function StartView2() {
		graphics.beginFill(0x990000,1);
		graphics.drawRect(0,0,640,480);
		graphics.endFill();

		addEventListener(MouseEvent.CLICK, onStart);
	}

	public function init():void {
		this.x = -640;
		GTweener.to(this, 0.3,{x:0});
	}

	private function onContinue(event:MouseEvent):void {
		onStart();
	}

	private function onPlayOne(event:MouseEvent):void {
	}

	private function onPlayTwo(event:MouseEvent):void {
	}

	private function onStart(event:* = null):void {
		startGame.dispatch();
	}

	public function destroy():void {
//		startGame.removeAll();
	}
}
}
