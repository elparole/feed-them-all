/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 17:17
 */
package com.elparole.feedThemAll.view
{
import com.greensock.TweenLite;

import org.casalib.load.DataLoad;
import org.osflash.signals.Signal;

public class LoadingView extends LoadingScr implements IScreen
{
	public var startGame:Signal = new Signal();

	private var _dl:DataLoad = new DataLoad('http://projects.blackmoondev.com/SkyKnight/link.txt');
	private var message:String;

	public function LoadingView() {
//		graphics.beginFill(0x099000,1);
//		graphics.drawRect(0,0,640,480);
//		graphics.endFill();
	}


	public function init():void {
		this.alpha = 0;
		TweenLite.to(this, 0.5, {alpha:1});

	}

	private function onStart(...args):void {
		startGame.dispatch();
	}

	public function destroy():void {

	}
}
}
