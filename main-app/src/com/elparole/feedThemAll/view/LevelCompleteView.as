/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 17:17
 */
package com.elparole.feedThemAll.view
{
import com.elparole.feedThemAll.PublishMode;
import com.elparole.utils.RollScaleFeatures;
import com.greensock.TweenLite;
import com.greensock.easing.Strong;

import flash.events.Event;

import flash.events.MouseEvent;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import org.osflash.signals.Signal;

public class LevelCompleteView extends LevelCompleteScr implements IScreen
{
	public const continueLevelComplete:Signal = new Signal();

	private var rsf:RollScaleFeatures = new RollScaleFeatures();
	private var currentInfoToShow:int;


	public function LevelCompleteView() {
		rsf.addButtons([continueBt,moreGamesBt]);
		monsterInfoMC.alpha = 0;
		monsterInfoMC.gotoAndStop(0);
	}

	private function onStart(event:*):void {

		if((currentInfoToShow == -1) || (monsterInfoMC.alpha ==1))
			outro();
		else{
			monsterInfoMC.gotoAndStop(currentInfoToShow);
			TweenLite.to(monsterInfoMC, 0.5,{alpha:1});
			TweenLite.to(star1MC, 0.5,{alpha:0});
			TweenLite.to(star2MC, 0.5,{alpha:0});
			TweenLite.to(star3MC, 0.5,{alpha:0});
			TweenLite.to(completedLb, 0.5,{alpha:0});
		}
	}

	private function outro():void {
		TweenLite.to(completedLb, 0.5, {y:32 - 500, ease:Strong.easeIn});
		TweenLite.to(star1MC, 0.5, {y:268 - 500, ease:Strong.easeIn});
		TweenLite.to(star2MC, 0.5, {y:268 - 500, ease:Strong.easeIn});
		TweenLite.to(star3MC, 0.5, {y:268 - 500, ease:Strong.easeIn});
		TweenLite.to(continueBt, 0.5, {delay:0, x:510 + 500});
		TweenLite.to(moreGamesBt, 0.5, {delay:0, x:140 - 500});
		TweenLite.to(monsterInfoMC, 0.5, {alpha:0});
		TweenLite.delayedCall(0.5, function (...args) {continueLevelComplete.dispatch()});
	}

	public function destroy():void {

	}
//

	public function showInfo(num:int):void {
		switch(num){
			case 0:
				currentInfoToShow = 1;
				break;
			case 3:
				currentInfoToShow = 2;
				break;
			case 7:
				currentInfoToShow = 3;
				break;
			case 11:
				currentInfoToShow = 4;
				break;
			case 15:
				currentInfoToShow = 5;
				break;
			default:
				currentInfoToShow = -1;
				break;
		}
	}
	
	public function showStars(num:int):void {
		trace('showStars',num)
		star1MC.gotoAndStop(2);
		star2MC.gotoAndStop(1);
		star3MC.gotoAndStop(1);

		TweenLite.to(star1MC, 0.5,{y:268});
		TweenLite.to(star2MC, 0.5,{y:268});
		TweenLite.to(star3MC, 0.5,{y:268});

//		TweenLite.to(star1MC, 0.5,{scaleX:1.5,scaleY:1.5, ease:Strong.easeIn})
//		TweenLite.to(star1MC, 0.5,{delay:0.6,scaleX:1,scaleY:1, ease:Linear.easeOut})
		switch(num){
			case 3:
				star3MC.gotoAndStop(2);
//				TweenLite.to(star3MC, 0.5,{delay:5.2,y:268,scaleX:1.5,scaleY:1.5})
//				TweenLite.to(star3MC, 0.5,{delay:1.8,scaleX:1,scaleY:1})
			case 2:
				star2MC.gotoAndStop(2);
//				TweenLite.to(star2MC, 0.5,{delay:0.8,scaleX:1.5,scaleY:1.5})
//				TweenLite.to(star2MC, 0.5,{delay:1.4,scaleX:1,scaleY:1})
				break;
//			case 1:
//				star1MC.gotoAndStop(2);
//				break;
		}
	}

	public function init():void {

		this.addChild(sponsorLogoMC2);

		completedLb.y = 32-500;
		TweenLite.to(completedLb, 0.5,{y:32, ease:Strong.easeIn});
		star1MC.y = 268-500;
		star2MC.y = 268-500;
		star3MC.y = 268-500;
		star1MC.gotoAndStop(1);
		star2MC.gotoAndStop(1);
		star3MC.gotoAndStop(1);

		star1MC.alpha = 1;
		star2MC.alpha = 1;
		star3MC.alpha = 1;
		completedLb.alpha = 1;
		monsterInfoMC.alpha = 0;

		continueBt.x = 510+500;
		moreGamesBt.x = 115-500;
		TweenLite.to(continueBt, 0.5,{delay:1,x:510});
		TweenLite.to(moreGamesBt, 0.5,{delay:1,x:115});
		continueBt.addEventListener(MouseEvent.CLICK, onStart, false,0,true);
		moreGamesBt.addEventListener(MouseEvent.CLICK, onGoSite,false,0,true);
		moreGamesBt.buttonMode = true;

		if(PublishMode.ShowBrand){
			sponsorLogoMC2.addEventListener(MouseEvent.CLICK, onGoSite,false,0,true);
			sponsorLogoMC2.buttonMode= true;
			sponsorLogoMC3.addEventListener(MouseEvent.CLICK, onGoSite,false,0,true);
			sponsorLogoMC3.buttonMode= true;
		}else{
			sponsorLogoMC2.visible = sponsorLogoMC3.visible = false;
		}
//
	}



	private function onGoSite(event:Event):void {
		navigateToURL(new URLRequest(PublishMode.SponsorLink),'_blank');
	}

}
}
