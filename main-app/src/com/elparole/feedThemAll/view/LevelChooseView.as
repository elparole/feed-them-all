/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 16:43
 */
package com.elparole.feedThemAll.view
{
import com.elparole.feedThemAll.PublishMode;
import com.elparole.feedThemAll.model.LevelModel;
import com.elparole.utils.RollScaleFeatures;
import com.greensock.TweenLite;

import flash.events.Event;

import flash.events.MouseEvent;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import org.osflash.signals.Signal;

public class LevelChooseView extends LevelChooseScr implements IScreen
{
	public var goBack:Signal = new Signal();
	public var levelSelected:Signal = new Signal(int);
	private var lnum:int;

	private var rsf:RollScaleFeatures = new RollScaleFeatures();


	public function LevelChooseView() {
	}


	public function init():void {
		for (var i:int = 1; i < 21; i++) {
			this["lev"+i].levelNumbersMC.gotoAndStop(i);
			this["lev"+i].lockMC.visible = false;
			this["lev"+i].starsMC.gotoAndStop(3);
			this["lev"+i].buttonMode=true;
			this["lev"+i].addEventListener(MouseEvent.CLICK, onSelectLevel);
			this["lev"+i].scaleX = this["lev"+i].scaleY = 1;
			rsf.addButton(this["lev"+i]);
			this["lev"+i].scaleX = this["lev"+i].scaleY = 0.1;
			this["lev"+i].alpha = 0;
			TweenLite.to(this["lev"+i],0.3,{delay:-0.02+0.02*i, scaleX:1, scaleY:1, alpha:1});
//			TweenLite.to(this["lev"+i],0.04,{delay:-0.01+0.02*i, alpha:1});
		}
		backBt.scaleX = backBt.scaleY = 1;
		rsf.addButton(backBt);
		backBt.scaleX = backBt.scaleY = 0;
		TweenLite.to(backBt,0.3,{delay:0.3, scaleX:1, scaleY:1});
		backBt.addEventListener(MouseEvent.CLICK, onGoBack);
		if(PublishMode.ShowBrand){
			sponsorLogoMC2.addEventListener(MouseEvent.CLICK, onGoSite,false,0,true);
			sponsorLogoMC2.buttonMode= true;
			sponsorLogoMC.visible = false;
		}


//		statsInfoMC.statsInfo2.sheepsTf.text = "0";
//		statsInfoMC.statsInfo2.moneyTf.text = "$0";
//
//		statsInfoMC.addEventListener(MouseEvent.ROLL_OVER, onOver);
//		statsInfoMC.addEventListener(MouseEvent.ROLL_OUT, onOut);
//		soundsPanel.addEventListener(MouseEvent.ROLL_OVER, onOver);
//		soundsPanel.addEventListener(MouseEvent.ROLL_OUT, onOut);
//		pauseBt.addEventListener(MouseEvent.ROLL_OVER, onOver);
//		pauseBt.addEventListener(MouseEvent.ROLL_OUT, onOut);
	}



	private function onGoSite(event:Event):void {
		navigateToURL(new URLRequest(PublishMode.SponsorLink),'_blank');
	}

	private function onGoBack(event:MouseEvent):void {

		rsf.removeAllButtons();

		for (var i:int = 1; i < 21; i++) {
			TweenLite.to(this["lev"+i],0.3,{delay:-0.02+0.02*i, scaleX:0, scaleY:0, alpha:0});
		}
		TweenLite.to(backBt,0.5,{delay:0.3, scaleX:0, scaleY:0});

		TweenLite.delayedCall(0.8,function(){goBack.dispatch();});


	}

	private function onSelectLevel(event:MouseEvent):void {
		lnum = int(event.currentTarget.name.split('lev').join(''))-1;

		if(this["lev"+String(lnum+1)].lockMC.visible)
			return;

		rsf.removeAllButtons();

		for (var i:int = 1; i < 21; i++) {
			TweenLite.to(this["lev"+i],0.3,{delay:-0.02+0.02*i, scaleX:0, scaleY:0, alpha:0});
		}

		TweenLite.to(backBt,0.5,{delay:0.3, scaleX:0, scaleY:0});

		TweenLite.delayedCall(0.8,function(){
			levelSelected.dispatch(lnum);});

	}

	public function reset():void{

	}

	public function destroy() :void {

	}

	public function enableLevels(levelsProgress:Array):void {

		for (var i:int = 1; i < 21; i++) {
			this["lev"+i].levelNumbersMC.gotoAndStop(i);
			this["lev"+i].lockMC.visible = !(levelsProgress[i-1]>=LevelModel.LEVEL_ENABLED_ID);
			if(levelsProgress[i-1]>=LevelModel.LEVEL_ENABLED_ID)
				this["lev"+i].starsMC.gotoAndStop(levelsProgress[i-1]+1);
			else
				this["lev"+i].starsMC.gotoAndStop(0);
		}
	}
}
}
