/**
 * User: Elparole
 * Date: 04.06.13
 * Time: 18:50
 */
package com.elparole.feedThemAll.view
{


//import com.elparole.skyKnight2.model.GameUtils;
//import com.gamua.flox.Flox;

import com.elparole.feedThemAll.PublishMode;
import com.greensock.TweenLite;

import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.DataEvent;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.net.URLRequest;
import flash.net.navigateToURL;
import flash.system.ApplicationDomain;
import flash.system.Security;
import flash.utils.getDefinitionByName;
import flash.utils.getTimer;
import flash.utils.setTimeout;

import mochi.as3.MochiAd;

//import mochi.as3.MochiServices;

public dynamic class AppLoader extends MovieClip
{
	private var _mc:LoadingScr;
	private var _ngp:NowGamezPreloader;
	private var _agi:AGIntroMC;
	private var _main:Object;

	private var _t:int;

	public function AppLoader()
	{
		_t = getTimer();
		if(PublishMode.NowGamezBrand){
			_ngp = new NowGamezPreloader();
			_ngp.scaleX = 480/640;
			_ngp.scaleY = 480/640;
			_ngp.x = -40;
			_ngp.btSPlay.mouseEnabled = false;
			_ngp.ply.visible = false;
			_ngp.siteLinkBt.addEventListener(MouseEvent.CLICK, onGoSite);
			this.addChild(_ngp);
			_ngp.preloaderMc.gotoAndStop(2);
			PublishMode.SponsorLink = PublishMode.SponsorLink.split("currentLink").join(root.loaderInfo.url);
		}else {
//			_ngp.preloaderMc.gotoAndStop(2);
			_mc = new LoadingScr();
			addChild(_mc);
		}

		addEventListener(Event.ENTER_FRAME, onEnterFrame);

//            mc.graphics.beginFill(0xff0000,1);
//            mc.graphics.drawRect(0,0,100,100);
//            mc.graphics.endFill();

//            FontUtil.setTCEFont(_mc.percValTf,30);
//            FontUtil.setTCEFont(_mc.percTf,26);
//            _mc.percValTf.text = '10';

	}

	public function onEnterFrame(event:Event=null):void
	{
		if(framesLoaded == totalFrames) {
			setPercent(100);
			removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			nextFrame();
			init();
		} else {
			var percent:Number = (stage.loaderInfo.bytesLoaded / stage.loaderInfo.bytesTotal) * 100;
			setPercent(percent);
//			trace('percent',percent);
		}
	}

	private function setPercent(percent:Number):void {
		if(PublishMode.NowGamezBrand){
			if(!_ngp)
				return;
			if(percent*100)
				_ngp.ply.visible = true;
			_ngp.preloaderMc.gotoAndStop(Math.min(Math.ceil(percent)*240/100,_ngp.preloaderMc.totalFrames));

		}else {
			_mc.percTf.text = String(Math.floor(percent)) + '%';
		}



//		_ngp.preloaderMc.gotoAndStop(Math.floor(percent)*240/100);
//		_mc.mc.percValTf.text = String(Math.floor(percent)) + ' ';
//		_mc.mc.percTf.x = _mc.mc.percValTf.x + _mc.mc.percValTf.textWidth+20;
	}

	public static function Domain(root:Sprite):String {
		var currentDomain:String = root.loaderInfo.url.split("/")[2];
		var fqdn:Array = currentDomain.split(".");
		var rdi:int = 1;
		var tli:int = 2;
		if (fqdn.length == 2) {
			rdi--;
			tli--;
		}

		return fqdn[rdi] + "." + fqdn[tli];
	}

	private function init(...args):void {
		//if class is inside package you'll have use full path ex.org.actionscript.Main
//		removeChild(_mc);

//		_mc = PublishMode.GameGab? new GameGabLdr():new LoaderScr();
//		GameUtils.niceScale(_mc, 640,520);
//		GameUtils.center(_mc, 640,520);
//		addChild(_mc);


		if((Domain(this).indexOf('games.armorgames.com') !=0 )&&
				(Domain(this).indexOf('cache.armorgames.com') !=0 )&&
				(Domain(this).indexOf('preview.armorgames.com') !=0 )&&
				(Domain(this).indexOf('cdn.armorgames.com') !=0 )&&
				(Domain(this).indexOf('gamemedia.armorgames.com') !=0 )&&
				(Domain(this).indexOf('files.armorgames.com') !=0 )&&
				((Domain(this).split('.')[1]!='armorgames') ||(Domain(this).split('.')[2]!='com'))){
			return;
		}


		stage.frameRate = 25;
		Security.allowDomain('*');
		Security.allowInsecureDomain("*");

		if(PublishMode.NowGamezBrand){
			_ngp.preloaderMc.visible = false;
			_ngp.btSPlay.mouseEnabled = true;
			_ngp.addEventListener("PlayClicked",onClickInit);


	//		MochiBot.track(this,"0a5de62778a2f4c7");
			MochiBot.track(this, "3091a243");
			MochiBot.track(this, "46b0bad9");
			MochiAd.showPreGameAd({clip:root, id:"0a5de62778a2f4c7", res:"640x480", background:0x04D520, color:0x33FF00, outline:0x33FF00, no_bg:true});
		}else if(PublishMode.AGBrand){

			_agi = new AGIntroMC();
			_agi.siteLinkBt.addEventListener(MouseEvent.CLICK, onGoSite);
			_agi.siteLinkBt.buttonMode = true;
			_agi.addEventListener('introCompleted', onClickInit);
			this.addChild(_agi);
			this.removeChild(_mc);

			_agi.x = 640/2;
			_agi.y = 480/2;
		}

//		Log.View(972713,"6c1a85c00998475d","f987a55a072a478183d6f382b3cb75", stage.loaderInfo);
//		Flox.init("elparole-Sky-Knight-2", "8190cfed-075f-47c8-9d19-7659936cdde5", "0.9");
//		if(PublishMode.GameGab){
//
//			_mc.addEventListener('goGame', onClickInit);
//
////			setTimeout(delayedInit,4000);
//		}else


	}

	private function onGoSite(event:MouseEvent):void {
		navigateToURL(new URLRequest(PublishMode.SponsorLink),'_blank');
	}

	private function onClickInit(event:Event = null):void {
//		return;
		if(PublishMode.NowGamezBrand){
			_ngp.preloaderMc.visible = false;

		}else if(PublishMode.AGBrand){
			_agi.stop();
			this.removeChild(_agi);
		}

//		_mc.removeEventListener('goGame', onClickInit);
//		stage.frameRate = 60;
		var mainClass:Class = Class(getDefinitionByName("com.elparole.feedThemAll.FTACreatures"));
		if(mainClass){
			_main = new mainClass();
			this.addChildAt(_main as DisplayObject,0);
//			this.removeChild(_ngp);
//			_main.addEventListener('floxPostScore', onFlexPostScore);
//			_main.addEventListener('floxLogEvent', onFlexLogEvent);
		}

		if(PublishMode.NowGamezBrand){
			this.addChild(_ngp);
			TweenLite.to(_ngp,1,{delay:7,alpha:0, onComplete:delayedInit});
		}else
			setTimeout(delayedInit,1000);
	}

	private function delayedInit(...args):void {

		if(PublishMode.NowGamezBrand){
			this.removeChild(_ngp);
		}else if(!PublishMode.AGBrand){
			this.removeChild(_mc);
		}
		stage.frameRate = 30;
		_main.start();

//		addChild(_main as DisplayObject);
//		(_main as Object).start();
//		_main = null;
		//if class is inside package you'll have use full path ex.org.actionscript.Main
//		var mainClass:Class = Class(getDefinitionByName("com.elparole.skyKnight2.MainEmbedded"));
	}

	private function onFlexLogEvent(event:DataEvent):void {
//		Flox.logEvent(event.data);
	}
	private function onFlexPostScore(event:DataEvent):void {
//		Flox.postScore(event.data.split(';')[0],event.data.split(';')[1],event.data.split(';')[2]);
	}
}
}
