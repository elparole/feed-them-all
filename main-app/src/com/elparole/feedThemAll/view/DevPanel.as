/**
 * User: Elparole
 * Date: 18.03.13
 * Time: 14:23
 */
package com.elparole.feedThemAll.view
{
import com.bit101.components.ComboBox;
import com.bit101.components.HSlider;
import com.bit101.components.Label;
import com.bit101.components.PushButton;
import com.bit101.components.RangeSlider;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.LevelModel;
import com.elparole.feedThemAll.model.MapModel;
import com.elparole.levelEditor.service.ILevelService;

import flash.display.Sprite;
import flash.events.Event;

public class DevPanel extends Sprite
{
	[Inject]
	public var gameState:GameState;

	[Inject]
	public var llser:ILevelService;

	[Inject]
	public var levelModel:LevelModel;

	[Inject]
	public var mapModel:MapModel;

	private var levels:ComboBox;
	private var levelIndex:int = -1;
	private var refreshBt:PushButton;
	private var sensSlider:RangeSlider
	private var rangeSlider:HSlider
	private var sensLabel:Label;
	private var rangeLabel:Label;
	private var addCoins:PushButton;

	public static var sensLow:Number = 5;
	public static var sensHigh:Number = 14;
	public static var range:Number = 60;

	public function DevPanel() {
		addEventListener(Event.ADDED_TO_STAGE, init)
	}

	private function init(event:Event):void {
		this.alpha = 0.5;
		levels = new ComboBox(this,640-250,10,'levels',['lev1.lev','lev2.lev','lev3.lev','lev4.lev','lev5.lev']);
		refreshBt = new PushButton(this,640-350,10,'refesh',onRefresh);
		addCoins = new PushButton(this,200,10,'$$',onCoins);
		levels.addEventListener('select', onSelectLevel);

//		sensLabel = new Label(this, 640-400,0,'0');
//		sensSlider = new RangeSlider('horizontal',this, 640-400,20,onSensSlider);
//		rangeLabel = new Label(this, 640-400,30,'0');
//		rangeSlider = new HSlider(this, 640-400,50,onRangeSlider);
	}

	private function onSensSlider(event:Event):void {
		sensLabel.text = String(sensSlider.lowValue+":"+sensSlider.highValue);
		sensLow = sensSlider.lowValue;
		sensHigh = sensSlider.highValue;
	}

	private function onRangeSlider(event:Event):void {
		rangeLabel.text = String(rangeSlider.value);
		range = rangeSlider.value;
	}

	private function onCoins(event:Event):void {
		gameState.addCash(1000);
	}

	private function onRefresh(event:Event):void {
		gameState.livesChanged.dispatch(0);

		llser.loadLevelsList('levelsList.txt')
				.addResultProcessor(this.setLevelsList)
				.addResultProcessor(loadLev);
	}


	private function loadLev(data:*, err:Object):* {

		llser.loadConfig(llser.currentLevelName,
//				'D:/Flash/feed-grays/main-app/src/assets/levels/'+llser.currentLevelName+'.lev')
				'levels/'+llser.currentLevelName+'.lev')
				.addResultProcessor(levelModel.parseConfig)
				.addResultProcessor(mapModel.parseObstacleMap);
		return data;
	}

	private function onSelectLevel(event:Event):void {
		if(levels.selectedItem){
			levelIndex = levels.selectedIndex;
			gameState.livesChanged.dispatch(0);
			llser.loadConfig(levels.selectedItem.split('.')[0],
//					'D:/Flash/feed-grays/main-app/src/assets/levels/'+levels.selectedItem)
					'levels/'+levels.selectedItem)
					.addResultProcessor(levelModel.parseConfig)
					.addResultProcessor(mapModel.parseObstacleMap);
//			selectedPlane.skin.x = selectedPlane.pathPts[levelIndex].x;//-selectedPlane.skin.width/2;
//			selectedPlane.skin.y = selectedPlane.pathPts[levelIndex].y;//-selectedPlane.skin.height/2;
		}
		else
			levelIndex = -1;
	}

	public function setLevelsList(listObj:Object, err:Object):Object {
		(listObj.list as Array).sort(Array.CASEINSENSITIVE);
		levels.items = listObj.list;
		return listObj;
	}
}
}
