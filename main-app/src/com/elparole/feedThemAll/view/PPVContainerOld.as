/**
 * User: Elparole
 * Date: 26.01.13
 * Time: 14:20
 */
package com.elparole.feedThemAll.view
{

import com.elparole.components.DisplayBlis;

import flash.display.BitmapData;
import flash.display.Sprite;
import flash.geom.Point;

import org.casalib.math.geom.Point3d;
import org.papervision3d.cameras.*;
import org.papervision3d.core.math.*;
import org.papervision3d.materials.*;
import org.papervision3d.objects.*;
import org.papervision3d.objects.primitives.*;
import org.papervision3d.render.*;
import org.papervision3d.scenes.*;
import org.papervision3d.view.*;

public class PPVContainerOld extends Sprite
{


	/** Are we orbiting the camera? */
	private var _orbiting			: Boolean = false;

	/** Are we zooming the camera? */
	private var _zooming			: Boolean = false;

	/** A Point to keep track of mouse coords */
	private var _lastMouse			: Point;

	/** Camera Yaw */
	private var _camYaw				: Number = 45;//-Math.PI/2;

	/** Camera Pitch */
	private var _camPitch			: Number = -60;//Math.PI/2;

	/** Camera distance */
	private var _camDist			: Number = 100;//2000;

	/** Camera target */
	private var _camTarget			: DisplayObject3D = DisplayObject3D.ZERO;
	/** The PV3D scene to render */
	public var scene : Scene3D;

	/** This PV3D camera */
	public var camera : Camera3D;

	/** The PV3D renderer */
	public var renderer : BasicRenderEngine;

	/** The PV3D viewport */
	public var viewport : Viewport3D;

	private var cubeNum:int = 0;//60;
	private var cubes:Array = [];
	private var speeds:Array = [];
	private var ISOW:Number = 500;
	private var ISOZ:Number = 500;

	private var bm:BitmapMaterial;
	private var ta:TexturesAssets = new TexturesAssets();
	private var fnum:int = 0;
	private var materials:Array = [];
	private var offsets:Array = [];

	private var floorPlane:Plane;
	private var n3d:Number3D;

	/**
	 * Constructor.
	 */
	public function PPVContainerOld() : void {

	}

	/**
	 * Init.
	 */
	public function init(...args) : void {


		// create a viewport
		viewport = new Viewport3D(640, 480);

		// add
		addChild(viewport);

		// create a frustum camera with FOV=60, near=10, far=10000
		camera = new Camera3D(60, 10, 10000);
		camera.ortho = true;
		camera.orbit(_camPitch, _camYaw, _camDist,_camTarget);

		// create a renderer
		renderer = new BasicRenderEngine();

		// create the scene
		scene = new Scene3D();

		// init the scene
		initScene();
	}

	/**
	 * Init the scene.
	 */
	private function initScene() : void {

//		this.graphics.beginFill(0x999999);
//		this.graphics.drawRect(0,0,640,480);
//		this.graphics.endFill();



	}

	/**
	 * Render!
	 */
	public function render() : void {

		fnum++;
//		moveSprites();

		// render
		renderer.renderScene(scene, camera, viewport);
	}

	private function moveSprites():void {
		var n3d:Number3D;
		for (var i:int = 0; i < cubeNum; i++) {

//			materials[i].bitmap.fillRect(bm.bitmap.rect, 0);
			materials[i].bitmap = ta['bm' + ((Math.floor(fnum/2) + offsets[i]) % 5 + 1)].bitmapData;

//			if(i%7==0){
			//			(cubes[i] as Plane).moveLeft(Math.random()*10-5);
			//			(cubes[i] as Plane).moveForward(Math.random()*10-5);
			n3d = new Number3D(
					((cubes[i] as Plane).position.x + speeds[i].x + ISOW/2) % ISOW,
					0,
					((cubes[i] as Plane).position.z + speeds[i].y + ISOZ/2) % ISOZ);
			if (n3d.x < 0)
				n3d.x = ISOW - n3d.x;
			if (n3d.z < 0)
				n3d.z = ISOZ - n3d.z;

			n3d.x -= ISOW/2;
			n3d.z -= ISOZ/2;

			(cubes[i] as Plane).position = n3d;
//			trace(i,n3d);
//			}
//			(cubes[i] as Plane).lookAt(camera);
//					.position.x +=Math.random()*6-3;
//			cubes[i].position.z +=Math.random()*6-3;
//			cubes[i].position = cubes[i].position.clone();
		}
	}

	/**
	 * Show some info;
	 */
//	private function showInfo() : void {
//		return;
//		var msg : String = "usage:\ndrag to rotate, drag-shift to zoom\n";
//		msg += "- o: toggle ortho / perspective mode\n";
//		msg += "- v: increase / decrease(+shift) FOV\n";
//		msg += "- f: increase / decrease(+shift) camera far-plane\n";
//		msg += "- n: increase / decrease(+shift) camera near-plane\n\n";
//
//		msg += "camera\n- projection mode: " + (camera.ortho?"ortho":"perspective") + "\n";
//		msg += "- fov: " + camera.fov + " near: " + camera.near + " far: " + camera.far + "\n";
//
//		status.text = msg;
//	}

	public function movePlaneTo(plane:Plane, pos:Number3D = null):void{
		if(pos)
			plane.position = pos.clone();

		n3d = new Number3D(
				(plane.position.x + 0 + ISOW/2) % ISOW,
				0,
				(plane.position.z + -2 + ISOZ/2) % ISOZ);
		if (n3d.x < 0)
			n3d.x = ISOW - n3d.x;
		if (n3d.z < 0)
			n3d.z = ISOZ - n3d.z;

		n3d.x -= ISOW/2;
		n3d.z -= ISOZ/2;

		plane.position = n3d;		
	}
	
	public function getScreenCoords(display:DisplayBlis):Point{
		display.renderItem.calculateScreenCoords(camera);
		return new Point(display.renderItem.screen.x, display.renderItem.screen.y);
	}

	public function addFloorOld(display:DisplayBlis){
		var mbd:BitmapData = display.bitmap;//(new TexturesAssets.mc1()).bitmapData;

		var bm:BitmapMaterial = new BitmapMaterial(mbd, false);
		floorPlane = new Plane(bm, 2048, 1024, 0, 0);

		scene.addChild(floorPlane);

//		floorPlane.yaw(90);
		floorPlane.pitch(90);

		floorPlane.position = new Number3D(200,-200,200);
		
	}

//	public function addFloor(display:Display):void {
//		var mbd:BitmapData = display.bitmap;//(new TexturesAssets.mc1()).bitmapData;
//		bm = new BitmapMaterial(mbd, false);
//		materials.push(bm);
////		offsets.push(Math.floor(Math.random()*5.999999));
//		var plane:Plane = new Plane(
////					new BitmapMaterial(mbd,false),
//				bm,
//				2048,1024,0,0);
//		plane.yaw(45);
////		plane.yaw(0);
//		plane.pitch(-60+90);
//		scene.addChild(plane);
////			plane.position= new Number3D(Math.floor(i/Math.sqrt(cubeNum))*25-100,0,Math.floor(i%Math.sqrt(cubeNum))*25-100);
//		plane.position= new Number3D(445,-200,445);
////		plane.position= new Number3D(Math.random()*ISOW-ISOW/2,0,Math.random()*ISOZ-ISOZ/2);
//		display.plane = plane;
//		display.material = bm;
//
//	}

	/**
	 *
	 * @param	event
	 */

	public function add3DBitmap(display:DisplayBlis):void {
		var mbd:BitmapData = display.bitmap;//(new TexturesAssets.mc1()).bitmapData;
		bm = new BitmapMaterial(mbd, false);
		materials.push(bm);
//		offsets.push(Math.floor(Math.random()*5.999999));

		var plane:Plane = new Plane(
//					new BitmapMaterial(mbd,false),
//				new ColorMaterial(0xff0000+Math.random()*256*256),
				bm,
				128,128,0,0);
		plane.yaw(45);
		plane.pitch(-60+90);
		scene.addChild(plane);
//			plane.position= new Number3D(Math.floor(i/Math.sqrt(cubeNum))*25-100,0,Math.floor(i%Math.sqrt(cubeNum))*25-100);
//		plane.position= new Number3D(Math.random()*ISOW-ISOW/2,0,Math.random()*ISOZ-ISOZ/2);
		display.renderItem = plane;
		display.material = bm;
	}

	public function remove3DBitmap(plane:Plane):void {
		scene.removeChild(plane);
	}

	public function movePlaneToPoint(plane:Plane, position:Point3d):void {

		n3d = new Number3D(
				position.x,
				position.y,
				position.z);
		plane.position = n3d;
	}

	public function add3DBitmapWithSize(display:DisplayBlis, w:int, h:int):void {
		var mbd:BitmapData = display.bitmap;//(new TexturesAssets.mc1()).bitmapData;
		bm = new BitmapMaterial(mbd, false);
		materials.push(bm);
//		offsets.push(Math.floor(Math.random()*5.999999));
		var plane:Plane = new Plane(
//					new BitmapMaterial(mbd,false),
				bm,
				w,h,0,0);
		plane.yaw(45);
//		plane.yaw(0);
		plane.pitch(-60+90);
		scene.addChild(plane);
//			plane.position= new Number3D(Math.floor(i/Math.sqrt(cubeNum))*25-100,0,Math.floor(i%Math.sqrt(cubeNum))*25-100);
		plane.position= new Number3D(445,-200,445);
//		plane.position= new Number3D(Math.random()*ISOW-ISOW/2,0,Math.random()*ISOZ-ISOZ/2);
		display.renderItem = plane;
		display.material = bm;
	}

	public function getScreenTo3DCoords(mouseX:Number, mouseY:Number, fixedY:Number = 0):Point3d {
		var ray:Number3D = camera.unproject(viewport.containerSprite.mouseX*640/16,viewport.containerSprite.mouseY*640/16);
		var cameraPosition:Number3D = new Number3D(camera.x,camera.y,camera.z);
		ray = Number3D.add(ray,cameraPosition);

		var deltY:Number = fixedY - ray.y;
		ray.x-=deltY*1.22;
		ray.z-=deltY*1.22;
		ray.y=fixedY;

		return new Point3d(ray.x,ray.y, ray.z);
	}

	public function getScreenTo3DCoordsOld(mouseX:Number, mouseY:Number):Point3d {
		var ray:Number3D = camera.unproject(
				viewport.containerSprite.mouseX + (640/2-viewport.containerSprite.mouseX)*0.4,
				viewport.containerSprite.mouseY + (480/2-viewport.containerSprite.mouseY)*0.4);
//		var ray:Number3D = camera.unproject(mouseX,mouseY);
		var cameraPosition:Number3D = new Number3D(camera.x,camera.y,camera.z);
		ray = Number3D.add(ray,cameraPosition);
		var p:Plane3D = new Plane3D();
		p.setNormalAndPoint(new Number3D(0, 1, 0), new Number3D(0, 0, 0));
		var intersect:Number3D = p.getIntersectionLineNumbers(cameraPosition,ray);
		return new Point3d(intersect.x,intersect.y, intersect.z);
	}
}
}