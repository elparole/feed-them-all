/**
 * User: Elparole
 * Date: 27.07.12
 * Time: 01:00
 */
package com.elparole.feedThemAll.components
{
public class BadCreature
{
	public static const GOFORSHEEP:String = 'goForSheep';
	public static const GOBACK:String = 'goBack';
	public static const EAT:String = 'eat';
	public var state:String = 'goForSheep';
	public var ttl:int = 2000;
	public var sc:StateComponent;
	public var eaten:int = 0;
	public var id:int;

	public static var currentId:int = 0;
	public var meatImEatingNow:String;
	public var meatToEat:int;

	public function BadCreature(meatToEat:int) {
		this.meatToEat = meatToEat;
		currentId++;
		id = currentId;
	}
}
}
