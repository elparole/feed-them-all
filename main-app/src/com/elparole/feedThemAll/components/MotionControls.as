package com.elparole.feedThemAll.components
{
	public class MotionControls
	{
		public var left1 : int = -1;
		public var left2 : int = -1;
		public var right1 : int = -1;
		public var right2 : int = -1;
		public var forward1 : int = -1;
		public var forward2 : int = -1;
		public var back1 : int = -1;
		public var back2 : int = -1;

		public var accelerationRate : Number = 0;
		public var rotationRate : Number = 0;
		public var mouseModeAvailable:Boolean = false;
		public var mouseModeOn:Boolean = false;
		
		public function MotionControls( left : uint, right : uint, forward : uint, back : uint, accelerationRate : Number, mouseMode:Boolean = false)
		{
			this.mouseModeAvailable = mouseMode;
			this.left1 = left;
			this.right1 = right;
			this.forward1 = forward;
			this.back1 = back;
			this.accelerationRate = accelerationRate;
			this.rotationRate = rotationRate;
		}
	}
}
