/**
 * User: Elparole
 * Date: 26.03.13
 * Time: 19:47
 */
package com.elparole.feedThemAll.components
{
public class GridPosition
{
	public var x:int;
	public var z:int;


	public function GridPosition(x:int, z:int) {
		this.x = x;
		this.z = z;
	}
}
}
