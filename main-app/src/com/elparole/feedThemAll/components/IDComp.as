/**
 * User: Elparole
 * Date: 15.05.13
 * Time: 15:35
 */
package com.elparole.feedThemAll.components
{
public class IDComp
{
	public var id:String;
	public static const LOST:String = 'lost';
	public static const WON:String = 'won';

	public function IDComp(id:String) {
		this.id = id;
	}
}
}
