/**
 * User: Elparole
 * Date: 18.02.13
 * Time: 18:30
 */
package com.elparole.feedThemAll.components
{
public class Scalable
{
	public var scaleX:Number = 1;
	public var scaleY:Number = 1;


	public function Scalable(scaleX:Number, scaleY:Number) {
		this.scaleX = scaleX;
		this.scaleY = scaleY;
	}
}
}
