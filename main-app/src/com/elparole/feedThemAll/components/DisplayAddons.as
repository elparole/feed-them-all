/**
 * User: Elparole
 * Date: 27.01.13
 * Time: 11:20
 */
package com.elparole.feedThemAll.components
{
public class DisplayAddons
{
	public var addons:Array = [];
	public var addonsOn:Array=[];

	public function DisplayAddons(displays:Array) {
		addons = addons.concat(displays);
		addonsOn = [displays[0]];
	}
}
}
