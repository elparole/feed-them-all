/**
 * User: Elparole
 * Date: 06.02.13
 * Time: 16:55
 */
package com.elparole.feedThemAll.components
{
import com.elparole.feedThemAll.model.ComponentsConfig;
import com.elparole.feedThemAll.model.EntityProcessStateMachine;

import flash.utils.Dictionary;

public class StateComponent
{
	public var fsm:EntityProcessStateMachine;
	private var stateComponentsConfigs:Dictionary = new Dictionary()

	public function StateComponent() {
	}

	public function setStateComponentsConfigs(stateName:String, entCompConfig:ComponentsConfig):void {
		stateComponentsConfigs[stateName] = entCompConfig;
	}
}
}
