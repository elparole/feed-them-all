package com.elparole.feedThemAll.components
{
	public class Bullet
	{
		public var lifeRemaining : Number;
		public var owner:PlayerCatapult


		public function Bullet(lifeRemaining:Number, owner:PlayerCatapult) {
			this.lifeRemaining = lifeRemaining;
			this.owner = owner;
		}
	}
}
