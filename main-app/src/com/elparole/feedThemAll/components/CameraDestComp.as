/**
 * User: Elparole
 * Date: 19.02.13
 * Time: 18:46
 */
package com.elparole.feedThemAll.components
{
import org.casalib.math.geom.Point3d;

public class CameraDestComp
{
	public var destination:Point3d;


	public function CameraDestComp(destination:Point3d) {
		this.destination = destination;
	}
}
}
