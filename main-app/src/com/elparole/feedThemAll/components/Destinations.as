/**
 * User: Elparole
 * Date: 12.03.13
 * Time: 12:30
 */
package com.elparole.feedThemAll.components
{
import flash.geom.Point;

public class Destinations
{
	public var startPos:Point;
	public var sheepDest:Point;
	private var _destId:int;


	public function Destinations(destId:int,startPos:Point, sheepDest:Point) {
		this._destId = destId;
		this.startPos = startPos;
		this.sheepDest = sheepDest;
	}

	public function get destId():int {
		return _destId;
	}

	public function set destId(value:int):void {
		trace('set destID from',_destId, 'to',value);
		_destId = value;
	}
}
}
