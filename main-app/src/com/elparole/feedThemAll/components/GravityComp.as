/**
 * User: Elparole
 * Date: 18.04.13
 * Time: 16:11
 */
package com.elparole.feedThemAll.components
{
public class GravityComp
{

	public var isFired:Boolean = false;
	public var isLanded:Boolean = false;
	public var ttl:Number = 2000;
	public var gravity:Number = 4;

	public function GravityComp(ttl:Number = 2000) {
		this.ttl = ttl;
	}
}
}
