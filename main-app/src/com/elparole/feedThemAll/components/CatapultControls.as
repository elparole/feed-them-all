/**
 * User: Elparole
 * Date: 20.02.13
 * Time: 12:53
 */
package com.elparole.feedThemAll.components
{
import org.casalib.math.geom.Point3d;

public class CatapultControls
{
	public var maxVelocity:Point3d;

	public function CatapultControls(x:Number, y:Number, z:Number) {
		maxVelocity = new Point3d(x,y,z);
	}


}
}
