/**
 * User: Elparole
 * Date: 26.01.13
 * Time: 11:23
 */
package com.elparole.feedThemAll
{
import com.elparole.feedThemAll.nodes.CameraControlNode;
import com.elparole.feedThemAll.nodes.MovementNode;
import com.elparole.feedThemAll.nodes.Render3DONode;

import flash.display.Sprite;
import flash.display.StageScaleMode;
import flash.events.Event;

[Frame(factoryClass="com.elparole.feedThemAll.view.AppLoader")]
[SWF(width = 640,height = 480, frameRate = 30, backgroundColor=0x999999)]
public class FTACreatures extends Sprite
{
	private var context:FTAContext;

	public function FTACreatures() {
		addEventListener(Event.ADDED_TO_STAGE, onAdded);
	}

	private function onAdded(event:Event):void {
		removeEventListener(Event.ADDED_TO_STAGE, onAdded);
		addEventListener( Event.ENTER_FRAME, init );
	}

	private function init( event : Event ) : void {

		context = new FTAContext( this, stage.stageWidth, stage.stageHeight );


		this.opaqueBackground = 0xA29A77;

//		stage.addChild(new FTAStats());
//		stage.addChild(new Stats());

		graphics.beginFill(0xA29A77);
		graphics.drawRect(0,0,640,520);
		graphics.endFill();

//		var dmask:Sprite = new Sprite();
//		dmask.graphics.beginFill(0);
//		dmask.graphics.drawRect(0,0,640,520);
//		dmask.graphics.endFill();
//		addChild(dmask);
//		mask = dmask;

		stage.scaleMode = StageScaleMode.NO_SCALE;
		removeEventListener( Event.ENTER_FRAME, init );
	}

	public function start():void{
		context.start();
	}
	
	public function initClazz():void{
		trace(MovementNode, CameraControlNode, Render3DONode);
	}
}
}
