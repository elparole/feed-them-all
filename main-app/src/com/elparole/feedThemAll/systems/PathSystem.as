package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.NodeList;
import ash.core.System;

import com.blackmoondev.ashes.script.components.ScriptsHolderComponent;
import com.elparole.feedThemAll.components.BadCreature;
import com.elparole.feedThemAll.components.MarkedToRemove;
import com.elparole.feedThemAll.controller.PathManager;
import com.elparole.feedThemAll.model.EntityCreator;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.ScriptGenerator;
import com.elparole.feedThemAll.model.ids.StatesIds;
import com.elparole.feedThemAll.nodes.PathNode;
import com.elparole.feedThemAll.view.AnimsIDs;

public class PathSystem extends System
	{
		[Inject]
		public var gameState:GameState;
	
		[Inject]
		public var gameUtils:GameUtils;

		[Inject]
		public var creator:EntityCreator;

		[Inject]
		public var scriptGenerator:ScriptGenerator;
		
		[Inject(nodeType="com.elparole.feedThemAll.nodes.PathNode")]
		public var nodes : NodeList;
	
		private var pathManager:PathManager = new PathManager();


		override public function addToEngine(engine:Engine):void {
			nodes.nodeAdded.add(onPathAdded);
			super.addToEngine(engine);
		}


		override public function removeFromEngine(engine:Engine):void {
			nodes.nodeAdded.remove(onPathAdded);
			super.removeFromEngine(engine);
		}

	private function onPathAdded(pnode:PathNode):void {
			pnode.pathComp.timeElapsed = 0;
		}

	override public function update( time : Number ) : void
		{
			var node : PathNode;

			for ( node = nodes.head; node; node = node.next ) {

//				trace('time to wait', node.pathComp.timeToWait, node.display.animId, node.pathComp.pathPts.length);
				
				if(node.pathComp.timeToWait>0){
					node.pathComp.timeToWait-=time;
					if(node.pathComp.timeToWait<=0){

						node.entity.add(
								ScriptsHolderComponent.getSingleScriptComponent(
										scriptGenerator.getSetWalkAnimation(), true));

						if(node.entity.has(BadCreature) &&
							(node.entity.get(BadCreature).meatImEatingNow == AnimsIDs.FROZEN_MEAT) &&
								gameState.meat3Weapon.exploding){

							creator.explodeItem(node);
							node.entity.add(new MarkedToRemove());
						}

						node.entity.get(BadCreature).meatImEatingNow = null;
					}
					if(node.entity.has(BadCreature) &&
						(node.entity.get(BadCreature).meatImEatingNow == AnimsIDs.FROZEN_MEAT))
						continue;
				}else
					node.pathComp.timeElapsed += time;

				if (!node.pathComp.pathPts)
					continue;


				pathManager.solvePosition(node.pathComp);
//				if(Math.sqrt(gameUtils.distanceSquared(pathManager.px,pathManager.pz,node.position.position.x,node.position.position.z))>15){
//					try {
//						trace('suspicious', node.pathComp.currentIndex-1,
//								node.pathComp.velocity,
//								node.pathComp.pathPts[node.pathComp.currentIndex-1].pos.x,
//								node.pathComp.pathPts[node.pathComp.currentIndex-1].pos.y);
//						trace('suspicious2', node.pathComp.currentIndex,
//								node.pathComp.velocity,
//								node.pathComp.pathPts[node.pathComp.currentIndex].pos.x,
//								node.pathComp.pathPts[node.pathComp.currentIndex].pos.y);
//						trace('suspicious3', node.pathComp.currentIndex+1,
//								node.pathComp.velocity,
//								node.pathComp.pathPts[node.pathComp.currentIndex+1].pos.x,
//								node.pathComp.pathPts[node.pathComp.currentIndex+1].pos.y);
//
//						trace(node.pathComp.pathPts[node.pathComp.currentIndex - 1],
//								node.pathComp.pathPts[node.pathComp.currentIndex],
//								node.pathComp.pathPts[node.pathComp.currentIndex + 1])
//						if(node.pathComp.pathPts[node.pathComp.currentIndex].pos.x==350){
//							trace('stop');
//						}
//					} catch (e:Error) {
//					}
//				}
//				trace('path dist',Math.sqrt(gameUtils.distanceSquared(pathManager.px,pathManager.pz,node.position.position.x,node.position.position.z)));
				node.position.position.x = pathManager.px;
				node.position.position.z = pathManager.pz;
				node.display.dirId = pathManager.dir;

				node.anim.totalFrames = gameUtils.getTotalFrames(node.display.skinId, node.display.animId, node.display.dirId);

				node.motion.velocity.x = 0;
				node.motion.velocity.y = 0;
				node.motion.velocity.z = 0;

				if ((node.pathComp.currentIndex == node.pathComp.pathPts.length - 1) &&
						(node.entity.get(BadCreature) as BadCreature).sc.fsm.currentStateId == StatesIds.GO_HOME)
					node.entity.add(new MarkedToRemove());
			}
		}

//	public function oldupdate( time : Number ) : void
//		{
//			var node : PathNode;
//
//			for ( node = nodes.head; node; node = node.next ) {
//
////				node.position.position.x = node.pathComp.startPt.x;
////				node.position.position.z = node.pathComp.startPt.y;
//				node.pathComp.timeElapsed+=time;
//				if(!node.pathComp.pathPts)
//					continue;
//
//				if(node.pathComp.timeElapsed*node.pathComp.velocity >
//						node.pathComp.pathPts[Math.min(node.pathComp.currentIndex+1,node.pathComp.pathPts.length-1)].distanceFromStart)
//					node.pathComp.currentIndex = Math.min(node.pathComp.currentIndex+1, node.pathComp.pathPts.length-1);
//
//
////				node.pathComp.timeElapsed*node.pathComp.velocity;
//				var currentPt:PathPoint = (node.pathComp.pathPts[node.pathComp.currentIndex] as PathPoint);
//				node.position.position.x =currentPt.pos.x*AppConsts.xGrid;
//				node.position.position.z =currentPt.pos.y*AppConsts.zGrid;
//				var extraDist:Number;
//
////				if(node.pathComp.currentIndex>0)
//					extraDist = node.pathComp.timeElapsed*node.pathComp.velocity - currentPt.distanceFromStart;
////				else
////					extraDist = node.pathComp.timeElapsed*node.pathComp.velocity;
//
//				switch(currentPt.dirId){
//					case 'UL':
//						node.position.position.x +=extraDist;
//						break;
//					case 'UR':
//						node.position.position.z +=extraDist;
//						break;
//					case 'DL':
//						node.position.position.z -=extraDist;
//						break;
//					case 'DR':
//						node.position.position.x -=extraDist;
//						break;
//				}
//
////				trace('path pos',node.position.position.x,
////						node.position.position.z,
////						'pt',currentPt.pos.x,
////						currentPt.pos.y,
////						node.pathComp.currentIndex,currentPt.distanceFromStart);
////				node.position.position.x=node.position.position.x*1/0.706+2.5*AppConsts.xGrid;
////				node.position.position.z*=0.706;
//
//				node.display.dirId = currentPt.dirId;
//				node.anim.totalFrames = gameUtils.getTotalFrames(node.display.skinId,node.display.animId,node.display.dirId);
//
////				node.position.position.z =//node.pathComp.pathPts[0].distanceFromStart+
////											node.pathComp.timeElapsed*node.pathComp.velocity*10;
//
//				node.motion.velocity.x = 0;
//				node.motion.velocity.y = 0;
//				node.motion.velocity.z = 0;
//
//				if((node.pathComp.currentIndex==node.pathComp.pathPts.length-1)&&
//						(node.entity.get(BadCreature) as BadCreature).sc.fsm.currentStateId ==StatesIds.GO_HOME)
//				node.entity.add(new MarkedToRemove());
//			}
//		}
	}
}
