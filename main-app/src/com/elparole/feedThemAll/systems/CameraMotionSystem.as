package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.Entity;
import ash.core.NodeList;
import ash.core.System;

import com.elparole.feedThemAll.components.CameraDestComp;
import com.elparole.feedThemAll.model.GameScenarioModel;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.nodes.CameraMotionNode;

import org.casalib.math.geom.Point3d;

public class CameraMotionSystem extends System
	{
		[Inject]
		public var gameState:GameState;
		
		[Inject]
		public var gameUtils:GameUtils;

		[Inject]
		public var gameScenario:GameScenarioModel;
		
		[Inject(nodeType="com.elparole.feedThemAll.nodes.CameraMotionNode")]
		public var destNodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.CameraControlNode")]
		public var cameraNodes : NodeList;

		private var param:Number = 20;

		override public function addToEngine(engine:Engine):void {
			super.addToEngine(engine);
			gameScenario.moveCamera.add(onStartMovingCamera);
		}

		private function onStartMovingCamera(destPt:Point3d):void {
			(cameraNodes.head.entity as Entity).add(new CameraDestComp(destPt));
		}

		override public function removeFromEngine(engine:Engine):void {
			super.removeFromEngine(engine);
		}

		override public function update( time : Number ) : void
		{
			var node : CameraMotionNode;

			for ( node = destNodes.head; node; node = node.next ) {
				var normLength:Number = Math.sqrt(gameUtils.distanceSquaredPt3D(node.position.position,node.cameraDest.destination));
				node.position.position.x += param*(node.cameraDest.destination.x-node.position.position.x)/normLength;
				node.position.position.y += param*(node.cameraDest.destination.y-node.position.position.y)/normLength;
				node.position.position.z += param*(node.cameraDest.destination.z-node.position.position.z)/normLength;

				if(normLength<25){
					node.position.position.x = node.cameraDest.destination.x;
					node.position.position.y = node.cameraDest.destination.y;
					node.position.position.z = node.cameraDest.destination.z;
					node.entity.remove(CameraDestComp);
					gameScenario.showInstructions.dispatch(gameScenario.currentIndex);
				}
			}
		}
	}
}
