package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.Entity;
import ash.core.NodeList;
import ash.core.System;

import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.nodes.MarkedToRemoveNode;

public class RemoveMarkedSystem extends System
	{
		[Inject]
		public var gameState:GameState;
		
		[Inject(nodeType="com.elparole.feedThemAll.nodes.MarkedToRemoveNode")]
		public var nodes : NodeList;

		[Inject]
		public var engine:Engine;

		private var toRemove:Array = [];

		override public function update( time : Number ) : void
		{
			var node : MarkedToRemoveNode;

			toRemove.length = 0;
			for ( node = nodes.head; node; node = node.next ) {
				toRemove.push(node.entity);
			}

			for each (var entity:Entity in toRemove) {
				engine.removeEntity(entity);
			}
		}
	}
}
