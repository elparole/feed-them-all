package com.elparole.feedThemAll.systems
{
import ash.core.NodeList;
import ash.core.System;

import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.nodes.MovementNode;

public class MovementSystem extends System
	{
		[Inject]
		public var gameState:GameState;
		
		[Inject(nodeType="com.elparole.feedThemAll.nodes.MovementNode")]
		public var nodes : NodeList;

		private var param:Number = 1;//0.05;

		override public function update( time : Number ) : void
		{
			var node : MovementNode;

			for ( node = nodes.head; node; node = node.next ) {
				node.position.position.x +=node.motion.velocity.x*param;
				node.position.position.y +=node.motion.velocity.y*param;
				node.position.position.z +=node.motion.velocity.z*param;
			}
		}
	}
}
