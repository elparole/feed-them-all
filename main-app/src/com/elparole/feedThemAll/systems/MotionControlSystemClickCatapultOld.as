package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.Entity;
import ash.core.NodeList;
import ash.core.System;

import com.elparole.feedThemAll.components.GravityComp;
import com.elparole.feedThemAll.components.MotionControls;
import com.elparole.feedThemAll.model.EntityCreator;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.ids.ProcessIds;
import com.elparole.feedThemAll.model.ids.StatesIds;
import com.elparole.feedThemAll.nodes.CameraControlNode;
import com.elparole.feedThemAll.nodes.EnemyMotionControlNode;
import com.elparole.feedThemAll.nodes.GravityTTLMotionControlNode;
import com.elparole.feedThemAll.nodes.PlayerControlNode;
import com.elparole.feedThemAll.nodes.PointerNode;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.view.BlisContainer;
import com.elparole.input.MousePoll;
import com.elparole.randomIntInRange;

import flash.geom.Point;

import net.richardlord.input.KeyPoll;

import org.casalib.math.geom.Point3d;

public class MotionControlSystemClickCatapultOld extends System
	{
		[Inject]
		public var mousePoll : MousePoll;
	
		[Inject]
		public var keyPoll: KeyPoll;
		
		[Inject]
		public var gameUtils:GameUtils;

		[Inject]
		public var gameSate:GameState;

		[Inject]
		public var creator : EntityCreator;

		[Inject]
		public var engine : Engine;

		[Inject]
		public var container:BlisContainer;
		
		[Inject(nodeType="com.elparole.feedThemAll.nodes.CameraControlNode")]
		public var cameraNodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.PlayerControlNode")]
		public var playerNodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.EnemyMotionControlNode")]
		public var enemiesNodes : NodeList;
	
		[Inject(nodeType="com.elparole.feedThemAll.nodes.GravityTTLMotionControlNode")]
		public var heartNodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.PointerNode")]
		public var pointersNodes : NodeList;

		private var strength:Number;
		private var toRemove:Array = [];
		private var firedHearts:int = 0;

		private static const SQRT3:Number = Math.sqrt(3);
		private var mouseStart:Point;
		private var destPt:Point;

		public static function initStatic():void {
			trace(PointerNode);
		}

		override public function addToEngine(engine:Engine):void {
			super.addToEngine(engine);
			mousePoll.clicked = false;
		}

		override public function update( time : Number ) : void
		{
			toRemove.length = 0;
			moveEnemies(time);
			movePlayer(time);

			for each (var obj:Object in toRemove) {
				engine.removeEntity(obj.entity);
			}
		}

		private function moveHearts(z:Number, time:Number):void {
		var node : GravityTTLMotionControlNode;
		firedHearts = 0;
		for ( node = heartNodes.head; node; node = node.next )
		{
			if(!node.gravityComp.isFired){
				node.position.position.z = z;
			}else if(!node.gravityComp.isLanded){
				firedHearts++;
				if(node.position.position.y<=-0.1){
					node.position.position.y = 0.01;
					node.motion.velocity.x = 0;
					node.motion.velocity.y = 0;
					node.motion.velocity.z = 0;
					node.gravityComp.isLanded = true;
				}else{
//					node.motion.velocity.x = 0;
					node.motion.velocity.y -= 2;
				}
			}else{
				firedHearts++;
				node.gravityComp.ttl-=time*1000;
			}
			if(node.gravityComp.ttl<0)
				toRemove.push(node);
		}
	}
	
	private function fireNonFiredHearts():void {
		var node : GravityTTLMotionControlNode;

		var isFIred:Boolean = false;
		for ( node = heartNodes.head; node; node = node.next )
		{
			if(!node.gravityComp.isFired){
				node.gravityComp.isFired = true;
				node.motion.velocity.x = Math.min(9.5*18/10,1.5*Math.sqrt(strength*25)/2);
				node.motion.velocity.y = Math.min(9.5*36/10,1.5*Math.sqrt(strength*25));
				isFIred = true;
			}
		}
//		isFIred && creator.createHeart(AnimsIDs.HEART,AnimsIDs.IDLE,AnimsIDs.DR,-305,-278,0,0);
		isFIred && creator.createHeart(AnimsIDs.HEART,AnimsIDs.IDLE,AnimsIDs.DR,
				playerNodes.head.position.position.x-30,
				playerNodes.head.position.position.z-45,0,0);
	}

	private function movePlayer(time:Number):void {
		var node : CameraControlNode;
		var delta:int = 10;
		for ( node = cameraNodes.head; node; node = node.next )
		{
			/**
			 * zmax -230
			 */
			if(keyPoll.isDown(node.control.left1)){
//				if(node.position.position.z<750)
				node.motion.velocity.x = -delta;
				node.motion.velocity.z = delta;
//				else{
//					node.motion.velocity.z = 0;
//					node.position.position.z = 750;
//				}
			}else if(keyPoll.isDown(node.control.right1)){
//				if(node.position.position.z>10)
				node.motion.velocity.x = delta;
				node.motion.velocity.z = -delta;
//				else{
//					node.motion.velocity.z = 0;
//					node.position.position.z = 0;
//				}
			}else if(keyPoll.isDown(node.control.forward1)){
//				if(node.position.position.z<750)
				node.motion.velocity.z = delta*1.5;
				node.motion.velocity.x = delta*1.5;
//				else{
//					node.motion.velocity.z = 0;
//					node.position.position.z = 750;
//				}
			}else if(keyPoll.isDown(node.control.back1)){
//				if(node.position.position.z>10)
//				else{
				node.motion.velocity.z = -delta*1.5;
				node.motion.velocity.x = -delta*1.5;
//					node.motion.velocity.z = 0;
//					node.position.position.z = 0;
//				}
			}else{
				node.motion.velocity.z = 0;
				node.motion.velocity.x = 0;
			}
		}

		var pnode : PlayerControlNode;
		for ( pnode = playerNodes.head; pnode; pnode = pnode.next ){

			moveHearts(pnode.position.position.z, time);
			pnode.weapon.reloadingTime -=time*1000;

			if(destPt && pnode.weapon.reloadingTime<=0){
				if(pnode.position.position.z<destPt.y)
					pnode.motion.velocity.z = pnode.control.maxVelocity.z;
				else
					pnode.motion.velocity.z = -pnode.control.maxVelocity.z;

				if(Math.abs(pnode.position.position.z - destPt.y)<pnode.control.maxVelocity.z){
					pnode.motion.velocity.z = 0;
					pnode.position.position.z = destPt.y;
					strength = 0;
					while(dirtyCalcS(Math.min(9.5*18/10,1.5*Math.sqrt(strength*25)/2),
							Math.min(9.5*36/10,1.5*Math.sqrt(strength*25)))<Math.min(500,destPt.x - pnode.position.position.x)){
						strength += 0.2;
					}
					fireNonFiredHearts();
					pnode.weapon.reloadingTime = pnode.weapon.maxReloadingTime;
					destPt = null;
				}
			}
		}

		var pointPos:Point = container.getScreenTo3DCoords(mousePoll.mouseX, mousePoll.mouseY,-1);
		if(pointersNodes.head){

			(pointersNodes.head as PointerNode).position.position.y = -1;
			(pointersNodes.head as PointerNode).position.position.z = -pointPos.y;
			(pointersNodes.head as PointerNode).position.position.x = -pointPos.x;


			if(mousePoll.clicked){
				destPt = container.getScreenTo3DCoords(mousePoll.mouseX, mousePoll.mouseY,0);
				mousePoll.clicked = false;
			}
		}
	}

	private function dirtyCalcS(x:Number, y:Number):Number {
//		trace('dirtyCalcS',x, y);
		var px:Number = -40;
		var py:Number = 0;
		while(py>-0.1){
			px+=x;
			py+=y;
			y-= 2;
		}
		trace('strength',strength,'py',py);
		return px;
	}

	private function moveEnemies(time:Number):void {

		var node : EnemyMotionControlNode;
		for ( node = enemiesNodes.head; node; node = node.next )
		{

//			trace('current state',node.creature.sc.fsm.id,node.creature.sc.fsm.getCurrentState());

//			if(node.creature.sc.fsm.getCurrentState()==StatesIds.EAT){
//				trace('anim loops counter',node.anim.loopsCounter);
//			}
			
			if(node.creature.sc.fsm.getCurrentState()==StatesIds.EAT && node.anim.loopsCounter>3){
//				trace('run process',ProcessIds.GO_HOME);
				if(node.creature.eaten<node.creature.meatToEat)
					node.creature.sc.fsm.runProcess(ProcessIds.GO_HOME);
				else{
					var svelocity:Point3d = new Point3d();
					for (var i:int = 0; i < 6; i++) {
						var ang:Number = randomIntInRange(-180,180)*Math.PI/180;
						var lstrength:Number = randomIntInRange(2,8);
						svelocity.x = 1.5*Math.cos(ang)*Math.sqrt(lstrength*25)/2;
						svelocity.z = 1.5*Math.sin(ang)*Math.sqrt(lstrength*25)/2;
						svelocity.y = 1.5*Math.sqrt(lstrength*25);
						var heartEnt:Entity = creator.createHeart(AnimsIDs.HEART,AnimsIDs.IDLE,AnimsIDs.DR,
								node.position.position.x,
								node.position.position.z,svelocity.x,svelocity.y, svelocity.z);
						heartEnt.get(GravityComp).isFired = true;
					}
					engine.removeEntity(node.entity);
				}

			}

//			switch(node.creature.state){
//				case BadCreature.GOFORSHEEP:
//						node.motion.velocity.x = 0;
//						node.motion.velocity.y = 0;
//						node.motion.velocity.z = -2;
//					break;
//
//				case BadCreature.EAT:
//					node.creature.ttl-=time*1000;
//					node.display.animId = AnimsIDs.EAT;
//					node.display.dirId = AnimsIDs.DR;
//					node.anim.totalFrames = gameUtils.getTotalFrames(node.display.skinId,
//							node.display.animId, node.display.dirId)
//
//						node.motion.velocity.x = 0;
//						node.motion.velocity.y = 0;
//						node.motion.velocity.z = 0;
//					break;
//
//				case BadCreature.GOBACK:
//						node.motion.velocity.x = 0;
//						node.motion.velocity.y = 0;
//						node.motion.velocity.z = 2;
//					break;
//			}
			if(node.position.position.z>1150||
					node.position.position.z<0 ||
					node.position.position.x>1150||
					node.position.position.x<0 ||
					node.creature.ttl<0){

				if(node.position.position.z<0 || node.position.position.x<0)
					trace('off screen',
							node.creature.id,
							'x',
							node.position.position.x,
							'z',node.position.position.z );
				toRemove.push(node)
			}
		}
	}

	private function movedByKeyBoard(control:MotionControls):Boolean {
		return keyPoll.isDown(control.right1) ||
			   keyPoll.isDown(control.right2) ||
			   keyPoll.isDown(control.left1) ||
			   keyPoll.isDown(control.left2) ||
			   keyPoll.isDown(control.forward1) ||
			   keyPoll.isDown(control.forward2) ||
			   keyPoll.isDown(control.back1) ||
			   keyPoll.isDown(control.back2)
	}

	private function keyBoardMotion(node:CameraControlNode, af:Number, time:Number):void {

	}

	private function mouseMotion(position:Point):void {

	}

}
}
