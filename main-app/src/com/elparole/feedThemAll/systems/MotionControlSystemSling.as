package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.NodeList;
import ash.core.System;

import com.elparole.feedThemAll.components.MotionControls;
import com.elparole.feedThemAll.model.EntityCreator;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.ids.ProcessIds;
import com.elparole.feedThemAll.model.ids.StatesIds;
import com.elparole.feedThemAll.nodes.ArrowStrengthNode;
import com.elparole.feedThemAll.nodes.CameraControlNode;
import com.elparole.feedThemAll.nodes.EnemyMotionControlNode;
import com.elparole.feedThemAll.nodes.GravityTTLMotionControlNode;
import com.elparole.feedThemAll.nodes.PointerNode;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.input.MousePoll;

import flash.geom.Point;

import net.richardlord.input.KeyPoll;

public class MotionControlSystemSling extends System
	{
		[Inject]
		public var mousePoll : MousePoll;
	
		[Inject]
		public var keyPoll: KeyPoll;
		
		[Inject]
		public var gameUtils:GameUtils;

		[Inject]
		public var gameSate:GameState;
		
		[Inject(nodeType="com.elparole.feedThemAll.nodes.CameraControlNode")]
		public var nodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.EnemyMotionControlNode")]
		public var enemiesNodes : NodeList;
	
		[Inject(nodeType="com.elparole.feedThemAll.nodes.GravityTTLMotionControlNode")]
		public var heartNodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.PointerNode")]
		public var pointersNodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.ArrowStrengthNode")]
		public var arrowNodes : NodeList;

		[Inject]
		public var creator : EntityCreator;

		[Inject]
		public var engine : Engine;

		private var strength:Number;
		private var toRemove:Array = [];
		private var firedHearts:int = 0;

		override public function update( time : Number ) : void
		{
			toRemove.length = 0;
			moveEnemies(time);
			movePlayer(time);

			for each (var obj:Object in toRemove) {
				engine.removeEntity(obj.entity);
			}
		}

	private function moveHearts(z:Number, time:Number):void {
		var node : GravityTTLMotionControlNode;
		firedHearts = 0;
		for ( node = heartNodes.head; node; node = node.next )
		{
			if(!node.gravityComp.isFired){
				node.position.position.z = z;
			}
			else if(!node.gravityComp.isLanded){
				firedHearts++;
				if(node.position.position.y<=-0.1){
					node.position.position.y = 0.01;
					node.motion.velocity.x = 0;
					node.motion.velocity.y = 0;
					node.motion.velocity.z = 0;
					node.gravityComp.isLanded = true;
				}else{
//					node.motion.velocity.x = 0;
					node.motion.velocity.y -= 2;
				}
			}else{
				firedHearts++;
				node.gravityComp.ttl-=time*1000;
			}
			if(node.gravityComp.ttl<0)
				toRemove.push(node);
		}
	}
	
	private function fireNonFiredHearts():void {
		var node : GravityTTLMotionControlNode;

		var isFIred:Boolean = false;
		for ( node = heartNodes.head; node; node = node.next )
		{
			if(!node.gravityComp.isFired){
				node.gravityComp.isFired = true;
				node.motion.velocity.x = Math.min(9.5*18/10,1.5*Math.sqrt(strength*25)/2);
				node.motion.velocity.y = Math.min(9.5*36/10,1.5*Math.sqrt(strength*25));
				isFIred = true;
			}
		}
		isFIred && creator.createHeart(AnimsIDs.HEART,AnimsIDs.IDLE,AnimsIDs.DR,-305,-278,0,0);
	}

	private static const SQRT3:Number = Math.sqrt(3);
	private var mouseStart:Point;

	private function movePlayer(time:Number):void {
		var node : CameraControlNode;
		for ( node = nodes.head; node; node = node.next )
		{
			/**
			 * zmax -230
			 */
			if(keyPoll.isDown(node.control.left1)){
				if(node.position.position.z<450)
					node.motion.velocity.z = 8;
				else{
					node.motion.velocity.z = 0;
					node.position.position.z = 450;
				}
			}else if(keyPoll.isDown(node.control.right1)){
				if(node.position.position.z>-232)
					node.motion.velocity.z = -8;
				else{
					node.motion.velocity.z = 0;
					node.position.position.z = -233;
				}
			}else{
				node.motion.velocity.z = 0;
			}
			
			if(mousePoll.isMouseDown){
				mouseStart||= new Point(mousePoll.mouseX,mousePoll.mouseY);
				if(mouseStart.x>mousePoll.mouseX &&  mouseStart.y<mousePoll.mouseY)
					strength = Math.sqrt(gameUtils.distanceSquared(mouseStart.x, mouseStart.y, mousePoll.mouseX,mousePoll.mouseY))/6;
//						Math.sqrt(strength*25)*Math.sqrt(strength*25)/2;
//				(pointersNodes.head as PointerNode).position.position.x = node.position.position.x+24*strength;
			
			}else if(strength>0){
				fireNonFiredHearts();
				strength=0;
			}
			if(!mousePoll.isMouseDown)
				mouseStart = null;

			var dist = dirtyCalcS(Math.min(9.5*18/10,1.5*Math.sqrt(strength*25)/2),
					Math.min(9.5*36/10,1.5*Math.sqrt(strength*25)));

			if(mousePoll.isMouseDown || (firedHearts>0)){
				(pointersNodes.head as PointerNode).position.position.y = -1-100;
				if(mousePoll.isMouseDown){
					(pointersNodes.head as PointerNode).position.position.z = node.position.position.z+112;
					(pointersNodes.head as PointerNode).position.position.x = 110 + dist - 35 + node.position.position.x;
				}
			}

			if(arrowNodes.head && mousePoll.isMouseDown){
				(arrowNodes.head as ArrowStrengthNode).arrow.textureBD.fillRect((arrowNodes.head as ArrowStrengthNode).arrow.textureBD.rect,0);

				dist *= 0.712;
//				(arrowNodes.head as ArrowStrengthNode).arrow.textureBD.draw(
//						(arrowNodes.head as ArrowStrengthNode).arrow.arrowBD,
//						new Matrix(1,0,0,1,-341*1.1+dist*(SQRT3/2+0.1),172*1.1-dist/2));

				var pt:Point = new Point(-341*1.1+dist*(SQRT3/2+0.1),172*1.1-dist/2);
//				pt.x = -pt.x;
//				pt.y = -pt.y;
				(arrowNodes.head as ArrowStrengthNode).arrow.textureBD.copyPixels(
						(arrowNodes.head as ArrowStrengthNode).arrow.arrowBD,(arrowNodes.head as ArrowStrengthNode).arrow.arrowBD.rect,
						pt,(arrowNodes.head as ArrowStrengthNode).arrow.maskBD,/*new Point(-pt.x, -pt.y)*/pt,false);


				(arrowNodes.head as ArrowStrengthNode).position.position.y = -1-200;
				(arrowNodes.head as ArrowStrengthNode).position.position.z = node.position.position.z+212;
				(arrowNodes.head as ArrowStrengthNode).position.position.x = 210+node.position.position.x;
			}

			moveHearts(node.position.position.z, time);
		}
	}

	private function dirtyCalcS(x:Number, y:Number):Number {
//		trace('dirtyCalcS',x, y);
		var px:Number = 0;
		var py:Number = 0;
		while(py>-0.1){
			px+=x;
			py+=y;
			y-= 2.08;
		}
//		trace('strength',strength,'py',py);
		return px;
	}

	private function moveEnemies(time:Number):void {

		var node : EnemyMotionControlNode;
		for ( node = enemiesNodes.head; node; node = node.next )
		{

//			trace('current state',node.creature.sc.fsm.id,node.creature.sc.fsm.getCurrentState());

			if(node.creature.sc.fsm.getCurrentState()==StatesIds.EAT && node.anim.loopsCounter>3){
//				trace('run process',ProcessIds.GO_HOME);
				node.creature.sc.fsm.runProcess(ProcessIds.GO_HOME);
			}

//			switch(node.creature.state){
//				case BadCreature.GOFORSHEEP:
//						node.motion.velocity.x = 0;
//						node.motion.velocity.y = 0;
//						node.motion.velocity.z = -2;
//					break;
//
//				case BadCreature.EAT:
//					node.creature.ttl-=time*1000;
//					node.display.animId = AnimsIDs.EAT;
//					node.display.dirId = AnimsIDs.DR;
//					node.anim.totalFrames = gameUtils.getTotalFrames(node.display.skinId,
//							node.display.animId, node.display.dirId)
//
//						node.motion.velocity.x = 0;
//						node.motion.velocity.y = 0;
//						node.motion.velocity.z = 0;
//					break;
//
//				case BadCreature.GOBACK:
//						node.motion.velocity.x = 0;
//						node.motion.velocity.y = 0;
//						node.motion.velocity.z = 2;
//					break;
//			}
			if(node.position.position.z>550||node.position.position.z<-280 || node.creature.ttl<0){
				toRemove.push(node)
			}
		}


	}

	private function movedByKeyBoard(control:MotionControls):Boolean {
		return keyPoll.isDown(control.right1) ||
			   keyPoll.isDown(control.right2) ||
			   keyPoll.isDown(control.left1) ||
			   keyPoll.isDown(control.left2) ||
			   keyPoll.isDown(control.forward1) ||
			   keyPoll.isDown(control.forward2) ||
			   keyPoll.isDown(control.back1) ||
			   keyPoll.isDown(control.back2)
	}

	private function keyBoardMotion(node:CameraControlNode, af:Number, time:Number):void {

	}

	private function mouseMotion(position:Point):void {

	}

}
}
