package com.elparole.feedThemAll.systems
{
import ash.core.NodeList;
import ash.core.System;

import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.nodes.ScalableNode;

import flash.geom.Matrix;

public class ScalableSystem extends System
	{
		[Inject]
		public var gameState:GameState;
	
		[Inject]
		public var gameUtils:GameUtils;
		
		[Inject(nodeType="com.elparole.feedThemAll.nodes.ScalableNode")]
		public var nodes : NodeList;

		private var param:Number = 1;//0.05;

		override public function update( time : Number ) : void
		{
			var node : ScalableNode;

			for ( node = nodes.head; node; node = node.next ) {
				var mat:Matrix = new Matrix();
				mat.scale(node.scalable.scaleX, node.scalable.scaleY);
				mat.translate(
						(1-node.scalable.scaleX)*node.display.bitmap.width/2+0.5*(1-node.scalable.scaleX)*node.display.offset.x,
						(1-node.scalable.scaleY)*node.display.bitmap.height/2+0.5*(1-node.scalable.scaleY)*node.display.offset.y);
//						node.display.offset.x*(1-node.scalable.scaleX),
//						node.display.offset.y*(1-node.scalable.scaleY));
						
				node.display.bitmap.fillRect(node.display.bitmap.rect,0);
				node.display.bitmap.draw(gameUtils.getSkinByID(node.display.skinId,
						node.display.animId, node.display.dirId),mat);
				node.display.offset = gameUtils.getSkinOffset(node.display.skinId,
						node.display.animId, node.display.dirId);
//				node.position.position.x =node.position.position.x;
//				node.position.position.y +=node.motion.velocity.y*param;
//				node.position.position.z +=node.position.position.z
			}
		}
	}
}
