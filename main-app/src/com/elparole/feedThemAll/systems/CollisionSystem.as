package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.Entity;
import ash.core.NodeList;
import ash.core.System;

import com.blackmoondev.ashes.script.components.ScriptsHolderComponent;
import com.elparole.feedThemAll.AppConsts;
import com.elparole.feedThemAll.components.GoodCreature;
import com.elparole.feedThemAll.components.GridPosition;
import com.elparole.feedThemAll.components.IDComp;
import com.elparole.feedThemAll.components.TTLComp;
import com.elparole.feedThemAll.model.EntityCreator;
import com.elparole.feedThemAll.model.Fence;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.MapModel;
import com.elparole.feedThemAll.model.ScriptGenerator;
import com.elparole.feedThemAll.model.ids.ProcessIds;
import com.elparole.feedThemAll.model.ids.SoundIds;
import com.elparole.feedThemAll.model.ids.StatesIds;
import com.elparole.feedThemAll.nodes.BadCollisionNode;
import com.elparole.feedThemAll.nodes.BouncingObstacleNode;
import com.elparole.feedThemAll.nodes.FastGridCollisionNode;
import com.elparole.feedThemAll.nodes.GoodCollisionNode;
import com.elparole.feedThemAll.nodes.HeartCollisionNode;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.view.SoundManager;
import com.elparole.randomValFromArr;

import flash.utils.Dictionary;

import net.richardlord.input.KeyPoll;

public class CollisionSystem extends System
{
	[Inject]
	public var creator : EntityCreator;

	[Inject]
	public var keyPoll:KeyPoll;

	[Inject]
	public var soundManager:SoundManager;

	[Inject]
	public var gameUtils:GameUtils;


	[Inject]
	public var game : Engine;

	[Inject]
	public var gameState:GameState;

	[Inject]
	public var mapModel:MapModel;

	[Inject]
	public var scriptGenerator:ScriptGenerator;

	private var ghostTime:int = 0;


	[Inject(nodeType="com.elparole.feedThemAll.nodes.HeartCollisionNode")]
	public var heartNodes : NodeList;

	[Inject(nodeType="com.elparole.feedThemAll.nodes.GoodCollisionNode")]
	public var goodNodes : NodeList;

	[Inject(nodeType="com.elparole.feedThemAll.nodes.BadCollisionNode")]
	public var badNodes : NodeList;
	
	[Inject(nodeType="com.elparole.feedThemAll.nodes.FastGridCollisionNode")]
	public var gridNodes: NodeList;

	[Inject(nodeType="com.elparole.feedThemAll.nodes.DestNode")]
	public var destNodes : NodeList;

	[Inject(nodeType="com.elparole.feedThemAll.nodes.BouncingObstacleNode")]
	public var bouncingNodes : NodeList;


	[Inject(nodeType="com.elparole.feedThemAll.nodes.IDNode")]
	public var idNodes:NodeList;

	public var tiles:Dictionary = new Dictionary();

	private var toRemove:Array = [];

	override public function removeFromEngine(game:Engine):void {
		gridNodes.nodeAdded.remove(onGridNodeAdded);
		gridNodes.nodeRemoved.remove(onGridNodeRemoved);

		goodNodes.nodeAdded.remove(onSheepAdded);
		goodNodes.nodeRemoved.remove(onSheepRemoved);
	}

	override public function addToEngine(game:Engine):void {
		gridNodes.nodeAdded.add(onGridNodeAdded);
		gridNodes.nodeRemoved.add(onGridNodeRemoved);

		goodNodes.nodeAdded.add(onSheepAdded);
		goodNodes.nodeRemoved.add(onSheepRemoved);
	}

	private function onGridNodeRemoved(node:FastGridCollisionNode):void {
		tiles[node.gridPos.x+'_'+node.gridPos.z].splice(tiles[node.gridPos.x+'_'+node.gridPos.z].indexOf(node.entity),1);
	}

	private function onSheepRemoved(node:GoodCollisionNode):void {
		gameState.setSheepsNum(countSheeps());
	}

	private function onSheepAdded(node:GoodCollisionNode):void {
		gameState.setSheepsNum(countSheeps());
	}

	private function onGridNodeAdded(node:FastGridCollisionNode):void {
		tiles[node.gridPos.x+'_'+node.gridPos.z] ||=[];
		tiles[node.gridPos.x+'_'+node.gridPos.z].push(node);
	}

	private function countSheeps():Number {

		var gnode:GoodCollisionNode;
		var num:int = 0;
		for ( gnode = goodNodes.head; gnode; gnode = gnode.next )
		{
			num++;
		}
		return num;
	}

//	private function onDebugPlayerAdded(player:PlayerPlaneCollisionNode):void {
//		trace('added', player);
//	}

	private function initNodes():void {
		if(BouncingObstacleNode){

		}
	}

	override public function update( time : Number ) : void
	{
		var gnode : GoodCollisionNode;
		var bnode : BadCollisionNode;
		var hnode : HeartCollisionNode;
		
		var tileArr:Array;

		toRemove.length = 0;
		for ( bnode = badNodes.head; bnode; bnode = bnode.next )
		{
			tileArr = tiles[
					Math.round(bnode.position.position.x/AppConsts.xGrid)+'_'+
							Math.round(bnode.position.position.z/AppConsts.zGrid)];

			if(tileArr && tileArr.length>0){
				for (var i:int = 0; i < tileArr.length; i++) {
					var fnode:FastGridCollisionNode = tileArr[i];
					if(fnode.display.skinId==AnimsIDs.SHEEP){
						if(gameUtils.dist(fnode.position.position,bnode.position.position)<bnode.collision.radius*0.7 &&
								toRemove.indexOf(fnode)<0 &&
								bnode.badCreatur.eaten<bnode.badCreatur.meatToEat-1 &&
								bnode.badCreatur.sc.fsm.currentStateId !=StatesIds.EAT &&
								!bnode.entity.has(GridPosition)){

							bnode.badCreatur.sc.fsm.runProcess(ProcessIds.EAT_SHEEP);
							var id:int = (fnode.entity.get(GoodCreature) as GoodCreature).id;
							creator.sheeps[id] = null;

							if(goodNodes.head && goodNodes.head.next)
								gameState.removeCreatureDest(id,bnode.destinations,destNodes,goodNodes);
							else
								gameState.sendAllHome(destNodes);


							soundManager.play(randomValFromArr(SoundIds.eatingSheepSnds));
							soundManager.play(randomValFromArr(SoundIds.sheepSnds));
							toRemove.push(fnode);
						}
					}else if(gameUtils.dist(fnode.position.position,bnode.position.position)<bnode.collision.radius &&
							(bnode.path.timeToWait<=0) &&
							(fnode.entity!=bnode.entity)){
						bnode.path.timeElapsed -= 0.2+(bnode.collision.radius-gameUtils.dist(fnode.position.position,bnode.position.position))*0.1/bnode.collision.radius
						bnode.path.timeElapsed = Math.max(0,bnode.path.timeElapsed);
						bnode.path.timeToWait = 0.4;
						bnode.entity.add(
								ScriptsHolderComponent.getSingleScriptComponent(
										scriptGenerator.getSetIdleAnimation(), true));
								
						bnode.path.currentIndex = 0;
						if(AnimsIDs.isFenceId(fnode.display.skinId)){
							var fenceComp:Fence = fnode.entity.get(Fence);
							fenceComp.health--;
							if(fenceComp.health<=0){
								soundManager.play(FenceBeingDestroyed_02Snd);
								toRemove.push(fnode);
							}else{
								soundManager.play(randomValFromArr(SoundIds.hittingFenceSnds));
								if(fenceComp.health<=4){
									fnode.display.skinId = AnimsIDs.isRuined(fnode.display.skinId)?
											fnode.display.skinId:
											fnode.display.skinId+'Rozwalony';
									fnode.display.setBitmap(gameUtils.getSkinByID(
											fnode.display.skinId,
											fnode.display.animId,
											fnode.display.dirId  ))
								}
							}
						}else
							soundManager.play(BounceofFrozenmonsterSnd);
					}
				}
			}

			for ( hnode = heartNodes.head; hnode; hnode = hnode.next )
			{
				if(gameUtils.dist(hnode.position.position,bnode.position.position)<bnode.collision.radius &&
						toRemove.indexOf(hnode)<0 &&
						(hnode.gravity.isFired || hnode.gravity.isLanded) && // eat landed meat
						bnode.badCreatur.sc.fsm.currentStateId !=StatesIds.EAT && // don't eat in this state
						!((bnode.badCreatur.meatImEatingNow == AnimsIDs.FROZEN_MEAT) &&
							(bnode.entity.has(GridPosition)))){

					bnode.badCreatur.meatImEatingNow = hnode.display.skinId;
					bnode.badCreatur.sc.fsm.runProcess(ProcessIds.EAT_MEAT);
					soundManager.play(randomValFromArr(SoundIds.eatingMeatSnds));
					toRemove.push(hnode);
				}
			}
		}

		for each (var obj:Object in toRemove) {
			game.removeEntity(obj.entity);
		}

		if(!goodNodes.head && gameState.isRunning && !idNodes.head && !gameState.lameInitRunning){
			trace("dispatch lost end game");
			game.addEntity(new Entity().add(new TTLComp(2)).add(new IDComp(IDComp.LOST)))
		}
	}

}
}
