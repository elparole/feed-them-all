package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.NodeList;
import ash.core.System;

import com.elparole.feedThemAll.AppConsts;
import com.elparole.feedThemAll.components.Destinations;
import com.elparole.feedThemAll.components.Heart;
import com.elparole.feedThemAll.model.EntityCreator;
import com.elparole.feedThemAll.model.GameScenarioModel;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.LevelModel;
import com.elparole.feedThemAll.model.ScriptGenerator;
import com.elparole.feedThemAll.model.ids.ProcessIds;
import com.elparole.feedThemAll.model.ids.StatesIds;
import com.elparole.feedThemAll.nodes.CameraControlNode;
import com.elparole.feedThemAll.nodes.CashMotionNode;
import com.elparole.feedThemAll.nodes.EnemyMotionControlNode;
import com.elparole.feedThemAll.nodes.GoodCollisionNode;
import com.elparole.feedThemAll.nodes.GravityTTLMotionControlNode;
import com.elparole.feedThemAll.nodes.PointerNode;
import com.elparole.feedThemAll.test.IsoAnimItem;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.view.BlisContainer;
import com.elparole.feedThemAll.view.DevPanel;
import com.elparole.feedThemAll.view.SoundManager;
import com.elparole.input.MousePoll;

import flash.geom.Point;
import flash.ui.Keyboard;

import net.richardlord.input.KeyPoll;

public class MotionControlSystemClick extends System
	{
		[Inject]
		public var mousePoll : MousePoll;
	
		[Inject]
		public var keyPoll: KeyPoll;

		[Inject]
		public var gameScenario:GameScenarioModel;
		
		[Inject]
		public var gameUtils:GameUtils;

		[Inject]
		public var soundManager:SoundManager;

		[Inject]
		public var scriptGenerator:ScriptGenerator;

		[Inject]
		public var gameState:GameState;

		[Inject]
		public var creator : EntityCreator;

		[Inject]
		public var engine : Engine;

		[Inject]
		public var levelModel:LevelModel;

		[Inject]
		public var container:BlisContainer;
		
		[Inject(nodeType="com.elparole.feedThemAll.nodes.CameraControlNode")]
		public var cameraNodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.EnemyMotionControlNode")]
		public var enemiesNodes : NodeList;
	
		[Inject(nodeType="com.elparole.feedThemAll.nodes.GravityTTLMotionControlNode")]
		public var heartNodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.GoodCollisionNode")]
		public var sheepsNodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.DestNode")]
		public var destNodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.PointerNode")]
		public var pointersNodes : NodeList;
	
		[Inject(nodeType="com.elparole.feedThemAll.nodes.CashMotionNode")]
		public var cashMotions : NodeList;

		private var toRemove:Array = [];
		private var isDragging:Boolean = false;
		private var startPt:Point ;
		private var startCamPt:Point;

		public static function initStatic():void {
			if(PointerNode){

			}
		}

		override public function removeFromEngine(engine:Engine):void {
			super.removeFromEngine(engine);
			keyPoll.keyDownSignal.remove(onMeatType);
		}

		override public function addToEngine(engine:Engine):void {
			super.addToEngine(engine);
			mousePoll.clicked = false;
			keyPoll.keyDownSignal.add(onMeatType);			
		}

		private function onMeatType(kcode:uint):void {
			switch(kcode){
				case Keyboard.NUMBER_1:
					if(gameState.meat1Weapon.enabled){
						gameState.currentWeapon = gameState.meat1Weapon;
						soundManager.play(AnimsIDs.selectSounds[gameState.currentWeapon.id]);
					}
					break;
				case Keyboard.NUMBER_2:
					if(gameState.meat2Weapon.enabled){
						gameState.currentWeapon = gameState.meat2Weapon;
						soundManager.play(AnimsIDs.selectSounds[gameState.currentWeapon.id]);
					}
					break;
				case Keyboard.NUMBER_3:
					if(gameState.meat3Weapon.enabled){
						gameState.currentWeapon = gameState.meat3Weapon;
						soundManager.play(AnimsIDs.selectSounds[gameState.currentWeapon.id]);
					}
					break;
				case Keyboard.NUMBER_4:
					if(gameState.meat4Weapon.enabled){
						gameState.currentWeapon = gameState.meat4Weapon;
						soundManager.play(AnimsIDs.selectSounds[gameState.currentWeapon.id]);
					}
					break;
				case Keyboard.NUMBER_5:
					if(gameState.meat5Weapon.enabled){
						gameState.currentWeapon = gameState.meat5Weapon;
						soundManager.play(AnimsIDs.selectSounds[gameState.currentWeapon.id]);
					}
					break;

				gameState.weaponsUpdate.dispatch(gameState.weapons);
			}
		}

		override public function update( time : Number ) : void
		{
			toRemove.length = 0;
			moveEnemies(time);

			moveCamera();
			moveHearts(time);
			moveCash(time);

			movePlayer(time);
			for each (var obj:Object in toRemove) {
				engine.removeEntity(obj.entity);
			}
		}

		private function moveCash(time:Number):void {

			var node : CashMotionNode;

			for ( node = cashMotions.head; node; node = node.next )
			{
				if(gameUtils.dist(node.position.position, (pointersNodes.head as PointerNode).position.position)<20){
					gameUtils.followPlayer(node.position.position,
							(pointersNodes.head as PointerNode).position.position,
							time, 
							8500/gameUtils.dist(node.position.position, (pointersNodes.head as PointerNode).position.position))
				}
				if(gameUtils.dist(node.position.position, (pointersNodes.head as PointerNode).position.position)<10){
					toRemove.push(node);
					gameState.addCash(node.bill.value);
				}
			}
		}
	
		private function moveHearts(time:Number):void {
		var node : GravityTTLMotionControlNode;		
		for ( node = heartNodes.head; node; node = node.next )
		{
			if(!node.gravityComp.isLanded){				
				if(node.position.position.y>=-node.motion.velocity.y-0.01){
					node.position.position.y = 0.01;
					node.motion.velocity.x = 0;
					node.motion.velocity.y = 0;
					node.motion.velocity.z = 0;
					node.gravityComp.isLanded = true;
					if(node.entity.has(Heart))
						soundManager.play(AnimsIDs.hitGroundSounds[node.entity.get(Heart).id]);
//					else if(node.entity.has(Bill))
//						soundManager.play();

				}else{
//					node.motion.velocity.x = 0;
					node.motion.velocity.y += node.gravityComp.gravity;
				}
			}else{				
				node.gravityComp.ttl-=time*1000;
			}
			if(node.gravityComp.ttl<0)
				toRemove.push(node);
		}
	}
	
	private function movePlayer(time:Number):void {
		if(levelModel.destPt){

			creator.createHeart(gameState.currentWeapon.id,AnimsIDs.IDLE,AnimsIDs.DR,
					-levelModel.destPt.x,
					-600,
					-levelModel.destPt.y,0,20);

			levelModel.destPt = null;
		}

		var pointPos:Point = container.getScreenTo3DCoords(mousePoll.mouseX, mousePoll.mouseY,-1);
		if(pointersNodes.head){

			((pointersNodes.head as PointerNode).display._renderItem as IsoAnimItem).visible = !gameState.mouseBlockedByMenu;

			(pointersNodes.head as PointerNode).position.position.y = -1;
			(pointersNodes.head as PointerNode).position.position.z = -pointPos.y+cameraNodes.head.motion.velocity.x;
			(pointersNodes.head as PointerNode).position.position.x = -pointPos.x+cameraNodes.head.motion.velocity.z;

			if(mousePoll.clicked){
				if(!gameState.mouseBlockedByMenu && gameState.currentWeapon.percLoaded>=1 && !isDragging){
					levelModel.destPt = container.getScreenTo3DCoords(mousePoll.mouseX, mousePoll.mouseY,-1);
					levelModel.destPt.x+=cameraNodes.head.motion.velocity.x;
					levelModel.destPt.y+=cameraNodes.head.motion.velocity.z;
					gameState.currentWeapon.percLoaded = 0;
					trace('mousePoll.clicked', levelModel.destPt);
//					gameState.reloadWeapons();
				}else if(!gameState.mouseBlockedByMenu &&gameState.currentWeapon.percLoaded<1){
					soundManager.play(CantDropMeatYetSnd);
					gameScenario.checkState(2);
				}
				mousePoll.clicked = false;
			}
		}

		if(!mousePoll.isMouseDown && startPt){
			startPt = null;
			isDragging = false;
			startCamPt = null;
		}
	}

	private function moveCamera():void {
		var node:CameraControlNode;
//		var delta:int = 10;
		var delta:int = DevPanel.sensLow;
		node = cameraNodes.head;
		if (gameState.mouseBlockedByMenu)
			return;
		if(node){
			var pos:Point = new Point(-mousePoll.mouseX,mousePoll.mouseY);
			if(mousePoll.isMouseDown && !startPt){
	//				startPt = new Point(node.position.position.x, node.position.position.z);
	//				startPt = new Point(mousePoll.mouseX, mousePoll.mouseY);
					startPt = new Point(-mousePoll.mouseX,mousePoll.mouseY);
					startCamPt = new Point(node.position.position.x, node.position.position.z);
			}else if(mousePoll.isMouseDown && startPt &&
					gameUtils.distanceSquaredPt(container.getScreenTo3DCoords(startPt.x, startPt.y,0),
												container.getScreenTo3DCoords(pos.x, pos.y,0))>81){
				isDragging = true;
				pos = container.getScreenTo3DCoords(pos.x, pos.y,0).subtract(container.getScreenTo3DCoords(startPt.x, startPt.y,0));
				node.position.position.x = startCamPt.x+pos.x;
				node.position.position.z = startCamPt.y+pos.y;
				gameState.setCameraMoved();
			}
		}
	}

	private function moveCameraOld():void {
		var node:CameraControlNode;
//		var delta:int = 10;
		var delta:int = DevPanel.sensLow;

		for (node = cameraNodes.head; node; node = node.next) {
			node.motion.velocity.x = 0;
			node.motion.velocity.y = 0;
			node.motion.velocity.z = 0;
			if (gameState.mouseBlockedByMenu)
				return;

			/**
			 * zmax -230
			 */
			if (mousePoll.mouseX < DevPanel.range &&
					-node.position.position.x + node.position.position.z <
							(node.camera.bounds.width + node.camera.bounds.height) * 0.5 * AppConsts.xGrid + 500) {
				delta = DevPanel.sensLow +
						(DevPanel.range - mousePoll.mouseX) * (DevPanel.sensHigh - DevPanel.sensLow) / DevPanel.range
				node.motion.velocity.x = -delta;
				node.motion.velocity.z = delta;
//				else{
//					node.motion.velocity.z = 0;
//					node.position.position.z = 750;
//				}
			} else if (mousePoll.mouseX > gameState.width - DevPanel.range &&
					-node.position.position.x + node.position.position.z >
							-200) {
//				if(node.position.position.z>10)
				delta = DevPanel.sensLow +
						(DevPanel.range + mousePoll.mouseX - gameState.width) * (DevPanel.sensHigh - DevPanel.sensLow) / DevPanel.range
				node.motion.velocity.x = delta;
				node.motion.velocity.z = -delta;
//				else{
//					node.motion.velocity.z = 0;
//					node.position.position.z = 0;
//				}
			}
			if (mousePoll.mouseY < DevPanel.range &&
					node.position.position.x + node.position.position.z <
							(node.camera.bounds.width + node.camera.bounds.height) * 0.5 * AppConsts.xGrid + 300) {
				delta = DevPanel.sensLow +
						(DevPanel.range - mousePoll.mouseY) * (DevPanel.sensHigh - DevPanel.sensLow) / DevPanel.range;
//				if(node.position.position.z<750)

				node.motion.velocity.z += delta * 1.5;
				node.motion.velocity.x += delta * 1.5;
//				else{
//					node.motion.velocity.z = 0;
//					node.position.position.z = 750;
//				}
			} else if (mousePoll.mouseY > gameState.height - DevPanel.range &&
					node.position.position.x + node.position.position.z >
							-300) {
				delta = DevPanel.sensLow +
						(DevPanel.range + mousePoll.mouseY - gameState.height) * (DevPanel.sensHigh - DevPanel.sensLow) / DevPanel.range;
//				if(node.position.position.z>10)
//				else{
				node.motion.velocity.z += -delta * 1.5;
				node.motion.velocity.x += -delta * 1.5;
//					node.motion.velocity.z = 0;
//					node.position.position.z = 0;
//				}
			}
		}
	}

	private function dirtyCalcS(x:Number, y:Number):Number {
//		trace('dirtyCalcS',x, y);
		var px:Number = -40;
		var py:Number = 0;

		while(py>-0.1){
			px += x;
			py += y;
			y  -= 2;
		}

//		trace('strength',strength,'py',py);
		return px;
	}

	private function moveEnemies(time:Number):void {

		var node : EnemyMotionControlNode;
		for ( node = enemiesNodes.head; node; node = node.next )
		{

//			trace('current state',node.creature.sc.fsm.id,node.creature.sc.fsm.getCurrentState());

//			if(node.creature.sc.fsm.getCurrentState()==StatesIds.EAT){
//				trace('anim loops counter',node.anim.loopsCounter);
//			}
//			trace('meatImEatingNow',node.creature.meatImEatingNow);
			if((node.creature.sc.fsm.getCurrentState()==StatesIds.EAT &&
					node.anim.loopsCounter>3)
//					||((node.creature.meatImEatingNow == AnimsIDs.FROZEN_MEAT) //&&
//							gameState.meat3Weapon.exploding &&
//							(node.creature.sc.fsm.currentStateId == StatesIds.GO_HOME)
//							 )
					){
//				trace('run process',ProcessIds.GO_HOME);
				if(node.creature.meatImEatingNow == AnimsIDs.SAUSAGE ){
					
				    var enode:EnemyMotionControlNode
					for ( enode = enemiesNodes.head; enode; enode = enode.next )
					{
						if(Math.pow(gameUtils.distanceSquaredPt3D(enode.position.position,node.position.position),0.333)
								<gameState.currentWeapon.range){
							creator.explodeItem(enode);
							toRemove.push(enode);
						}
					}

				    var gnode:GoodCollisionNode;
					for ( gnode = sheepsNodes.head; gnode; gnode = gnode.next )
					{
						if(Math.pow(gameUtils.distanceSquaredPt3D(gnode.position.position,node.position.position),0.333)
								<gameState.currentWeapon.range){
							creator.explodeItem(gnode);
							toRemove.push(gnode);


							if(sheepsNodes.head && sheepsNodes.head.next)
								gameState.removeCreatureDest(
										gnode.goodCreature.id,
										node.entity.get(Destinations),
										destNodes,
										sheepsNodes);
							else
								gameState.sendAllHome(destNodes);
						}
					}

					creator.explodeItem(node);
					toRemove.push(node);
				}else if(node.creature.eaten==node.creature.meatToEat-1 ||(node.creature.meatImEatingNow==AnimsIDs.FROZEN_MEAT)){
//					if(node.entity.has(PathComponent)
//							&& (node.entity.get(PathComponent) as PathComponent).timeToWait>0){
//						node.entity.add(
//								ScriptsHolderComponent.getSingleScriptComponent(
//										scriptGenerator.getSetIdleAnimation(), true));
//					}
					if(node.creature.sc.fsm.currentStateId !=ProcessIds.GO_HOME)
						node.creature.sc.fsm.runProcess(ProcessIds.GO_HOME);
				}
				else if(node.creature.eaten<node.creature.meatToEat-1) {
					//					}

//					if(node.creature.sc.fsm.currentStateId !=ProcessIds.GO_HOME)
//						node.creature.sc.fsm.runProcess(ProcessIds.GO_HOME);

					if(node.creature.sc.fsm.currentStateId !=StatesIds.GO_FOR_SHEEP && sheepsNodes.head){
						node.creature.sc.fsm.runProcess(ProcessIds.DRAW_SHEEP);
						gameState.createPathToSheep(sheepsNodes,0,destNodes,node.entity.get(Destinations) as Destinations,node)

						if(node.creature.sc.fsm.currentStateId !=StatesIds.GO_FOR_SHEEP)
							node.creature.sc.fsm.runProcess(ProcessIds.DRAW_SHEEP);

					}else if(node.creature.sc.fsm.currentStateId !=ProcessIds.GO_HOME)
						node.creature.sc.fsm.runProcess(ProcessIds.GO_HOME);


//					if(node.creature.sc.fsm.currentStateId !=ProcessIds.GO_HOME)
//						node.creature.sc.fsm.runProcess(ProcessIds.GO_HOME);

//					if(node.creature.sc.fsm.currentStateId !=ProcessIds.DRAW_SHEEP)
//						node.creature.sc.fsm.runProcess(ProcessIds.DRAW_SHEEP);
				}
				else if(node.creature.eaten>=node.creature.meatToEat) {
					creator.explodeItem(node);
					toRemove.push(node);
//					engine.removeEntity(node.entity);
				}

			}
			if(node.creature.ttl<0){

				if(node.position.position.z<0 || node.position.position.x<0)
					trace('off screen',
							node.creature.id,
							'x',
							node.position.position.x,
							'z',node.position.position.z );
				toRemove.push(node)
			}
		}
	}



}
}
