package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.NodeList;
import ash.core.System;

import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.nodes.DrawableFloorNode;
import com.elparole.feedThemAll.nodes.RedrawToBackgroundNode;
import com.elparole.feedThemAll.view.BlisContainer;

import flash.geom.Matrix;
import flash.geom.Point;

public class RedrawToBackgroundSystem extends System
	{
		[Inject]
		public var gameState:GameState;
		
		[Inject(nodeType="com.elparole.feedThemAll.nodes.RedrawToBackgroundNode")]
		public var nodes : NodeList;
	
		[Inject(nodeType="com.elparole.feedThemAll.nodes.DrawableFloorNode")]
		public var floorNodes : NodeList;

		[Inject]
		public var engine:Engine;

		[Inject]
		public var container : BlisContainer;


		override public function addToEngine(engine:Engine):void {
			super.addToEngine(engine);
			nodes.nodeAdded.add(onAdded);
		}

		private function onAdded(toDrawNode:RedrawToBackgroundNode):void {
			var node : DrawableFloorNode;

			for ( node = floorNodes.head; node; node = node.next ) {
				var screen1Pt:Point = container.getScreenCoords(node.disp);
//				screen1Pt.offset(-node.disp.bitmap.width/2,-node.disp.bitmap.height/2);
				var screen2Pt:Point = container.getScreenCoords(toDrawNode.disp);
//				screen2Pt.offset(-toDrawNode.disp.bitmap.width/2,-toDrawNode.disp.bitmap.height/2);
				var mat:Matrix = new Matrix(1,0,0,1,screen2Pt.x-screen1Pt.x, screen2Pt.y-screen1Pt.y);
//				trace('draw puddle', mat.tx, mat.ty)
//				node.disp.bitmap.draw(toDrawNode.disp.bitmap);
//				node.disp.bitmap.fillRect(new Rectangle(mat.tx, mat.ty, 100,100),0);
				node.disp.bitmap.draw(toDrawNode.disp.bitmap, mat);
			}
		}
	
		override public function removeFromEngine(engine:Engine):void {
			super.removeFromEngine(engine);
			nodes.nodeAdded.remove(onAdded);
		}
}
}
