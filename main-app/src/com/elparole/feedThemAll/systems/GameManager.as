package com.elparole.feedThemAll.systems
{
import ash.core.NodeList;
import ash.core.System;

import com.elparole.feedThemAll.model.EnemyGenerator;
import com.elparole.feedThemAll.model.EntityCreator;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.GameUtils;

public class GameManager extends System
	{
		[Inject]
		public var gameState : GameState;
		[Inject]
		public var creator : EntityCreator;

		[Inject]
		public var enemyGenerator:EnemyGenerator;

		[Inject]
		public var gameUtils:GameUtils;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.Render3DONode")]
		public var renders : NodeList;

		private var isNextWaveWaitingMode:Boolean = false;
		private var WAVE_DELAY:Number = 2;
		private var timeToNextWave:Number = -1;
		private const SAFE_TIME_TO_CLEAR_ENEMIES:Number = 30;
		private const FIRST_LEVEL_DELAY:Number = 3;

		public function initClazz():void {
//			trace(PlayerPlaneCollisionNode);
//			trace(BulletCollisionNode);
		}

		override public function update( time : Number ) : void
		{

		}
}
}
