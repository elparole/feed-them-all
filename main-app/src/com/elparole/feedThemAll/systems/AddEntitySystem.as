package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.NodeList;
import ash.core.System;

import com.elparole.components.Position;
import com.elparole.feedThemAll.components.AddEntity;
import com.elparole.feedThemAll.components.Direction;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.nodes.AddEntityNode;

import org.casalib.math.geom.Point3d;

public class AddEntitySystem extends System
	{
		[Inject]
		public var gameState:GameState;
		
		[Inject(nodeType="com.elparole.feedThemAll.nodes.AddEntityNode")]
		public var nodes : NodeList;

		[Inject]
		public var engine : Engine;

		override public function addToEngine(engine:Engine):void {
			super.addToEngine(engine);
			nodes.nodeAdded.add(onAdded);
		}

		private function onAdded(node:AddEntityNode):void {
			engine.addEntity(node.addEntity.entity);
			
//			var point3d:Point3d = new Point3d(220,-182,220);
			var point3d:Point3d = new Point3d(0,0,0);
//			var dirAmp:int = 40;
			var dirAmp:int = 20;
			switch(node.addEntity.entity.get(Direction).dir){
				case 0:
					point3d.offset(0,0,dirAmp);
					break;
				case 1:
					point3d.offset(dirAmp,0,0);
					break;
				case 2:
					point3d.offset(0,0,-dirAmp);
					break;
				case 3:
					point3d.offset(-dirAmp,0,0);
					break;
			}
			
			(node.addEntity.entity.get(Position) as Position).position =
					(node.entity.get(Position) as Position).position.clone().add(point3d);
			node.entity.remove(AddEntity);
		}

		override public function removeFromEngine(engine:Engine):void {
			super.removeFromEngine(engine);
			nodes.nodeAdded.remove(onAdded);
		}

		override public function update( time : Number ) : void
		{


		}
	}
}
