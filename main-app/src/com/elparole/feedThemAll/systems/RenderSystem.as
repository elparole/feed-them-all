package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.NodeList;
import ash.core.System;

import com.elparole.components.DisplayBlis;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.nodes.AddonsNode;
import com.elparole.feedThemAll.nodes.AnimNode;
import com.elparole.feedThemAll.nodes.CameraControlNode;
import com.elparole.feedThemAll.nodes.CameraNode;
import com.elparole.feedThemAll.nodes.Render3DONode;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.view.BlisContainer;

import org.casalib.math.geom.Point3d;

public class RenderSystem extends System
	{
		[Inject]
		public var container : BlisContainer;

		[Inject]
		public var gameState:GameState;
	
		[Inject]
		public var gameUtils:GameUtils;
		
		[Inject(nodeType="com.elparole.feedThemAll.nodes.AnimNode")]
		public var animNodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.Render3DONode")]
		public var nodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.AddonsNode")]
		public var addonsNodes : NodeList;
	
		[Inject(nodeType="com.elparole.feedThemAll.nodes.CameraControlNode")]
		public var playerNodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.CameraNode")]
		public var cameraNodes : NodeList;

		override public function addToEngine(engine:Engine):void {
			super.addToEngine(engine);
			nodes.nodeRemoved.add(removeFromDisplay);
			nodes.nodeAdded.add(addToDisplay);
			addonsNodes.nodeAdded.add(addAddonsToDisplay);
			addonsNodes.nodeRemoved.add(removeAddonsFromDisplay);
		}

		override public function removeFromEngine(engine:Engine):void {
			super.removeFromEngine(engine);

			nodes.nodeRemoved.remove(removeFromDisplay);
			nodes.nodeAdded.remove(addToDisplay);
			addonsNodes.nodeAdded.remove(addAddonsToDisplay);
			addonsNodes.nodeRemoved.remove(removeAddonsFromDisplay);
		}

		private function addToAnim( node : AnimNode ) : void
		{

		}
		
		private function removeFromAnim( node : AnimNode ) : void
		{

		}

		private function addToFloor( node : Render3DONode ) : void
		{
			container.add3DBitmapWithSize(node.display, node.position.position.y);
		}

		private function removeAddonsFromDisplay( node : AddonsNode ) : void{
			for each (var display:DisplayBlis in node.addons.addonsOn) {
				container.remove3DBitmap(display.renderItem);
			}
		}

		private function addAddonsToDisplay( node : AddonsNode ) : void
		{
			for each (var display:DisplayBlis in node.addons.addonsOn) {
				container.add3DBitmap(display, node.position.position.y);
			}
		}
		private function addToDisplay( node : Render3DONode ) : void
		{
			container.add3DBitmapWithSize(node.display,node.position.position.y);
		}

		private function removeFromDisplay( node : Render3DONode ) : void
		{
			container.remove3DBitmap(node.display.renderItem);
		}

		override public function update( time : Number ) : void
		{
			var node : Render3DONode;
			var animNode : AnimNode;
			var addonsNode : AddonsNode;
			var playerNode : CameraControlNode;
			var cameraNode : CameraNode;
			var display : DisplayBlis;
			

			for ( cameraNode  = cameraNodes.head; cameraNode ; cameraNode  = cameraNode .next )
			{
//				container.cameraY = -712+233+cameraNode.position.position.z;
//				container.cameraX = cameraNode.position.position.x;

//				trace('cam',-100-cameraNode.position.position.x, -275-cameraNode.position.position.z);
//				container.cameraY = -100-cameraNode.position.position.x;
//				container.cameraX = -275-cameraNode.position.position.z;
				container.moveCameraTo(
						-300-cameraNode.position.position.x,
						-cameraNode.position.position.z);
			}
			
			for ( addonsNode = addonsNodes.head; addonsNode; addonsNode = addonsNode.next )
			{
				for each (var display1:DisplayBlis in addonsNode.addons.addonsOn) {
					container.movePlaneToPoint(display1.renderItem, addonsNode.position.position.add(new Point3d(0,1,0)));
				}
			}

			for ( node = nodes.head; node; node = node.next )
			{
				container.movePlaneToPoint(node.display.renderItem, node.position.position);
			}

			for ( animNode = animNodes.head; animNode; animNode = animNode.next )
			{
				if(AnimsIDs.isBadCreature(animNode.display.skinId)){
//					trace('fnum',animNode.anim.frameNum);
				}
				if(!AnimsIDs.isMeat(animNode.display.skinId)){
					if(animNode.motion.velocity.z<0){
						animNode.display.skinId = AnimsIDs.SHEEP;
						animNode.display.animId = AnimsIDs.WALK;
						animNode.display.dirId = AnimsIDs.DL;
						animNode.anim.totalFrames = gameUtils.getTotalFrames(animNode.display.skinId,
								animNode.display.animId, animNode.display.dirId)
					}else if(animNode.motion.velocity.z>0){
						animNode.display.animId = AnimsIDs.WALK;
						animNode.display.dirId = AnimsIDs.UR;
						animNode.anim.totalFrames = gameUtils.getTotalFrames(animNode.display.skinId,
								animNode.display.animId, AnimsIDs.UR)
	//							animNode.display.animId, animNode.display.dirId)
					}else if(animNode.motion.velocity.x>0){
						animNode.display.animId = AnimsIDs.WALK;
						animNode.display.dirId = AnimsIDs.UL;
						animNode.anim.totalFrames = gameUtils.getTotalFrames(animNode.display.skinId,
								animNode.display.animId, animNode.display.dirId)
					}else if(animNode.motion.velocity.x<0){
						animNode.display.animId = AnimsIDs.WALK;
						animNode.display.dirId = AnimsIDs.DR;
						animNode.anim.totalFrames = gameUtils.getTotalFrames(animNode.display.skinId,
								animNode.display.animId, AnimsIDs.DR)
	//							animNode.display.animId, animNode.display.dirId)
					}else if(animNode.motion.velocity.z==0){
						if(animNode.display.skinId == AnimsIDs.CATAPULTWHEELS){
							animNode.display.animId = AnimsIDs.IDLE;
							animNode.anim.totalFrames = gameUtils.getTotalFrames(animNode.display.skinId,
									animNode.display.animId, animNode.display.dirId)
						}
					}
				}

				animNode.anim.frameNum= animNode.anim.frameNum+time*animNode.anim.animSpeed;
				if(animNode.anim.frameNum>animNode.anim.totalFrames){
					animNode.anim.loopsCounter++;
//					if(animNode.anim.animationID == AnimsIDs.EAT)
//						trace('set eat animation ended true');
				}
				animNode.anim.frameNum= animNode.anim.frameNum%animNode.anim.totalFrames;
//				trace('frame',Math.floor(animNode.anim.frameNum));
				var fn:int = animNode.anim.frameNum;


				animNode.display.setBitmap(gameUtils.getSkinByID(animNode.display.skinId,
						animNode.display.animId, animNode.display.dirId, Math.floor(fn)));

				animNode.display.setOffset(gameUtils.getSkinOffset(animNode.display.skinId,
						animNode.display.animId, animNode.display.dirId, Math.floor(fn)
						));
			}
			
			gameState.updateWeaponProgress(time);

//			for ( addonsNode = addonsNodes.head; addonsNode; addonsNode = addonsNode.next )
//			{
//				for each (var disp:Display in addonsNode.addons.addonsOn) {
//					container.movePlaneTo(disp.plane, addonsNode.display.plane.position);
//				}
//			}

			container.render(time);
		}
	}
}
