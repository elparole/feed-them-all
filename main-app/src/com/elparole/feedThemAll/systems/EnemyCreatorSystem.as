package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.Entity;
import ash.core.NodeList;
import ash.core.System;

import com.elparole.feedThemAll.components.IDComp;
import com.elparole.feedThemAll.components.TTLComp;
import com.elparole.feedThemAll.model.EnemyGenerator;
import com.elparole.feedThemAll.model.EntityCreator;
import com.elparole.feedThemAll.model.GameScenarioModel;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.LevelModel;
import com.elparole.feedThemAll.model.MapModel;
import com.elparole.feedThemAll.model.ids.SoundIds;
import com.elparole.feedThemAll.nodes.EnemyMotionControlNode;
import com.elparole.feedThemAll.nodes.IDNode;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.view.SoundManager;
import com.elparole.levelEditor.model.vo.GameItemConfig;
import com.elparole.randomValFromArr;

import flash.utils.getTimer;

public class EnemyCreatorSystem extends System
	{
		[Inject]
		public var gameState:GameState;

		[Inject]
		public var creator:EntityCreator;

		[Inject]
		public var gameScenario:GameScenarioModel;

		[Inject]
		public var enemyGenerator:EnemyGenerator;

		[Inject]
		public var soundManager:SoundManager;

		[Inject]
		public var engine:Engine;

		[Inject]
		public var gameUtils:GameUtils;
	
		[Inject]
		public var levelModel:LevelModel;

		[Inject]
		public var mapModel:MapModel;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.EnemyMotionControlNode")]
		public var nodes : NodeList;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.GoodCollisionNode")]
		public var goodNodes:NodeList;
	
		[Inject(nodeType="com.elparole.feedThemAll.nodes.IDNode")]
		public var idNodes:NodeList;
	
		private var toRemove:Array = [];
		private var hold:Boolean = false;
		private var monstersNum:int = 0;
		private var _mfloor:Entity;
		private var deltaTime:Number = 2800;
		private var maxMonsterNum:Number = 2;


		public function initClazz():void {
//			trace(PlayerPlaneaCollisionNode);
//			trace(BulletCollisionNode);
		}


		override public function removeFromEngine(engine:Engine):void {
			super.removeFromEngine(engine);
			idNodes.nodeRemoved.remove(onIDRemoved);
		}

	override public function addToEngine(engine:Engine):void {
			super.addToEngine(engine);
			nodes.nodeRemoved.add(onRemoved);
			idNodes.nodeRemoved.add(onIDRemoved);
			deltaTime = 5;
			monstersNum = 0;
//			maxMonsterNum = 8;
			maxMonsterNum = 6;//6;
			gameState.ctime = 0;
		}
	
		private function onIDRemoved(node:IDNode):void {
			if(node.idComp.id == IDComp.LOST){
				gameState.livesChanged.dispatch(0);
			}else if(node.idComp.id == IDComp.WON){
				levelModel.currentChildIndex = 0;
				gameState.ctime = 0;
				gameState.levelWon();
			}
		}
	
		private function onRemoved(node:EnemyMotionControlNode):void {
			monstersNum--;
		}

		override public function update( time : Number ) : void {
			gameState.ctime+=time*1000;
			var t:int = getTimer();
			if(!gameState.lameInit){
				creator.init();
				trace('ecs lame init');
				levelModel.levelConfig.floorItems =
						levelModel.rewriteFloorsBasic(levelModel.levelConfig.backupFloorItems, levelModel.levelConfig.size);
				levelModel.levelConfig.backupItems ||= levelModel.levelConfig.items.concat();
				levelModel.levelConfig.items = levelModel.levelConfig.backupItems.concat();
				gameState.lameInitRunning = true;
				gameState.lameInit = true;
			}

			if(gameState.lameInitRunning){
				var gameItemConfig:GameItemConfig
				while(levelModel.levelConfig.floorItems.length>0 && (getTimer()-t<time*1000)){

					gameItemConfig = levelModel.levelConfig.floorItems.pop();

					if(gameItemConfig.bd){
						creator.createCustomTextureItem(gameItemConfig,
								gameItemConfig.pos.x,
								-1,
								gameItemConfig.pos.z);
					}else{
					}
				}

//				levelModel.levelConfig.items.length = 1;

//				levelModel.levelConfig.items.length = Math.min(levelModel.levelConfig.items.length,2);
				trace('items length',levelModel.levelConfig.items.length);
				while(levelModel.levelConfig.items.length>0 && (getTimer()-t<time*1000)){
					gameItemConfig = levelModel.levelConfig.items.pop();				
					switch(gameItemConfig.skinId){
						case AnimsIDs.SHEEP:
							creator.createGoodCreature(AnimsIDs.SHEEP, AnimsIDs.IDLE,AnimsIDs.DL,
									gameItemConfig.pos.x,
//									gameItemConfig.pos.z*0.706 + 3.25 * AppConsts.xGrid*1/0.706,
									gameItemConfig.pos.z,
//									gameItemConfig.pos.x*0.706 + 3.25 * AppConsts.zGrid*1/0.706,
									0,0);
							break;
						default:
								if(!AnimsIDs.isBadCreature(gameItemConfig.skinId) &&
										(!AnimsIDs.isFenceId(gameItemConfig.skinId) || gameState.isFenceEnabled()))
									creator.createItem(gameItemConfig.skinId,
											gameItemConfig.animId,
											gameItemConfig.dirId,
											gameItemConfig.pos.x,
											gameItemConfig.pos.y,
											gameItemConfig.pos.z);
							break;
					}
				}

				if(levelModel.levelConfig.items.length>0){
					return;
				}
				gameState.ctime = 0;

//				creator.createGoodCreature(AnimsIDs.SHEEP, AnimsIDs.IDLE,AnimsIDs.DL,
//						12*AppConsts.xGrid-2,
//						12*AppConsts.zGrid,
//						0,0);
//				creator.createGoodCreature(AnimsIDs.SHEEP, AnimsIDs.IDLE,AnimsIDs.DL,
//						0,0,0,0);

//				if(levelModel.levelConfig.items.length==0){
//					for (var ix:Number = 14; ix < 16; ix+=1) {
//						for (var iy:Number = 6; iy < 8; iy+=1) {
////							creator.createGoodCreature(AnimsIDs.SHEEP, AnimsIDs.IDLE,AnimsIDs.DL,-240+i*71,-278,0,0);
////							creator.createGoodCreature(AnimsIDs.SHEEP, AnimsIDs.IDLE,AnimsIDs.DL,35+ix*71,-45+iy*71,0,0);
//							creator.createGoodCreature(AnimsIDs.SHEEP, AnimsIDs.IDLE,AnimsIDs.DL,
//									ix*AppConsts.xGrid,
//									iy*AppConsts.zGrid,
//									0,0);
////							creator.createGoodCreature(AnimsIDs.SHEEP, AnimsIDs.IDLE,AnimsIDs.DL,
////									randomIntInRange(1,16)*70*0.706,
////									randomIntInRange(1,16)*70*0.706,
////									0,0);
//						}
//					}
//				}
				creator.createCamera(-100+(mapModel.bounds.width-9)*25,200+(mapModel.bounds.height-9)*25,0,0,mapModel.bounds);
				creator.createPointer(AnimsIDs.TARGETPOINTER, AnimsIDs.IDLE,AnimsIDs.DR,0,0,0,0);
				gameState.lameInitRunning = false;
				gameState.playerConfig.meat6Skill = 0;
				gameState.showControls.dispatch();
				trace('lameinit time',getTimer()-t)
			}
			levelModel.levelConfig.sheepsNum=creator.sheepIdIter;
			
			if(!gameState.lameInitRunning)
				addMonster();
		}

	private function addMonster():void {

		gameScenario.checkState();
		if(levelModel.monsterAvailable()){
			while(levelModel.monstersEnabled && (levelModel.monsterAvailable() && levelModel.getNextMonster().startTime<gameState.ctime)){
				var ent:Entity = creator.createBadCreature(levelModel.getNextMonster().skinId, 'walk', 'DL',
						levelModel.getNextMonster().x,
						levelModel.getNextMonster().y);
				levelModel.currentChildIndex++;
				soundManager.play(randomValFromArr(SoundIds.monsterGrumbles))
			}
		}else if(levelModel.currentChildIndex>0 && !nodes.head  && gameState.isRunning && !idNodes.head){
			trace("dispatch win end game");
			engine.addEntity(new Entity().add(new TTLComp(2)).add(new IDComp(IDComp.WON)));
//			gameState.livesChanged.dispatch(0);
		}

		if(Math.random()<Math.min(0.01+0.001*levelModel.currentChildIndex,0.02)){
			soundManager.play(randomValFromArr(SoundIds.monsterGrumbles))			
		}

		if(Math.random()<(Math.min(0.01+0.001*gameState.sheepsNumLiving,0.02))){
			soundManager.play(randomValFromArr(SoundIds.sheepSnds))
		}
		
	}
}
}
