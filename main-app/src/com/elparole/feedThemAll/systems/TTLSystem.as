package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.Entity;
import ash.core.NodeList;
import ash.core.System;

import com.elparole.feedThemAll.nodes.TTLNode;

public class TTLSystem extends System
	{

		[Inject]
		public var engine:Engine;

		[Inject(nodeType="com.elparole.feedThemAll.nodes.TTLNode")]
		public var nodes : NodeList;


		private var toRemove:Array = [];
	//0.05;

		override public function update( time : Number ) : void
		{
			var node : TTLNode;

			toRemove.length = 0;
			for ( node = nodes.head; node; node = node.next ) {
				node.ttlComp.time-=time;
				if(node.ttlComp.time<=0)
					toRemove.push(node.entity);
			}

			for each (var ttl:Entity in toRemove) {
				engine.removeEntity(ttl);
			}
		}
	}
}
