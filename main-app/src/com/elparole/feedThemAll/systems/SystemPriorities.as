/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 01:31
 */
package com.elparole.feedThemAll.systems
{
public class SystemPriorities
{

	public static const preUpdate : int = 1;
	public static const update : int = 2;
	public static const move : int = 3;
	public static const resolveCollisions : int = 4;
	public static const render : int = 5;

	public function SystemPriorities() {
	}
}
}
