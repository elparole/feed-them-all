package com.elparole.feedThemAll.systems
{
import ash.core.Engine;
import ash.core.NodeList;
import ash.core.System;

import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.nodes.FastGridCollisionNode;

public class TilesSystem extends System
	{
		[Inject]
		public var gameState:GameState;
	
		[Inject]
		public var gameUtils:GameUtils;
		
		[Inject(nodeType="com.elparole.feedThemAll.nodes.FastGridCollisionNode")]
		public var nodes : NodeList;


		override public function addToEngine(engine:Engine):void {
			super.addToEngine(engine);
			nodes.nodeAdded.add(onAdded);
			nodes.nodeRemoved.add(onRemoved);
		}

		private function onRemoved(node:FastGridCollisionNode):void {

		}
		private function onAdded(node:FastGridCollisionNode):void {

		}

	override public function update( time : Number ) : void
		{
			var node : FastGridCollisionNode;

			for ( node = nodes.head; node; node = node.next ) {
				
			}
		}
	}
}
