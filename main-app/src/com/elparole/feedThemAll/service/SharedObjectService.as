/**
 * User: Elparole
 * Date: 10.06.13
 * Time: 15:59
 */
package com.elparole.feedThemAll.service
{

import com.elparole.feedThemAll.model.LevelModel;

import flash.net.SharedObject;

import utils.assert.assertIsTrue;

/**
 * jeśli nie ma neta, to save score do kolejki
 *
 * jeśli jest, to nie do kolejki
 *
 * na init zapisać wszystkie wyniki do neta
 *
 * usuwać zapisane na necie z so
 *
 */

public class SharedObjectService
{

	private var name:String;
	private var so:SharedObject;
	private var currentIndex:int;
	private var uniqueName:String = 'FeedThemAll';

	/*
	shared object:
	levels:Array(int)
	money:iny;
	weapons:Array(int)
	instructions:Array(Boolean)

	 */
	public function SharedObjectService()
	{
		assertIsTrue(uniqueName && uniqueName.length > 0, 'null or empty ScoreService SharedObject name');
		name = uniqueName;
		so = SharedObject.getLocal(name);
	}

	public function init():void {
		if(!so || !so.data['levels']){
			var levels:Array = [LevelModel.LEVEL_ENABLED_ID];
			for (var i:int = 1; i < 20; i++) {
				levels[i] = LevelModel.LEVEL_DISABLED_ID;
			}
			var weapons:Array = [0];
			for (var i:int = 0; i < 6; i++) {
				weapons[i] = 0;
			}
			var instructions:Array = [];
			for (var i:int = 0; i < 6; i++) {
				instructions[i] = false;
			}
			so.data['levels'] = levels;
			so.data['weapons'] = weapons;
			so.data['instructions'] = instructions;
			so.data['money'] = 0;
			so.data['music'] = true;
			so.data['sound'] = true;
			so.flush();
		}
	}

	public function clearOverallResult():void{
		so.clear();
	}

	public function saveLevels(value:Array):void {
		so.data['levels'] = value.concat();
		so.flush();
	}

	public function saveWeapons(value:Array):void {
		so.data['weapons'] = value.concat();
		so.flush();
	}

	public function saveInstructions(value:Array):void {
		so.data['instructions'] = value.concat();
		so.flush();
	}

	public function saveMoney(value:int):void {
		so.data['money'] = value;
		so.flush();
	}

	public function get tutorialShown():Boolean {
		return so.data['tutorialShown'];
	}

	public function clearSO():void{
		so.clear();
		so.flush();
	}

	public function set showLead(v:Boolean):void {
		so.data['showLead'] = v;
		so.flush();
	}

	public function clearFence():void {
		so.data['weapons'] ||= [];
		so.data['weapons'][5] = 0;
		so.flush();
	}

	public function setMusic(value:Boolean):void {
		so.data['music'] = value;
	}

	public function setSound(value:Boolean):void {
		so.data['sound'] = value;
	}

	public function loadMusic():Boolean {
		return so.data['music'];
	}

	public function loadSound():Boolean {
		return so.data['sound']
	}

	public function loadLevels():Array {
		return so.data['levels'];
	}

	public function loadInstructions():Array {
		return so.data['instructions'];
	}

	public function loadWeapons():Array {
		return so.data['weapons'];
	}

	public function loadMoney():int {
		return so.data['money'];
	}
}
}
