/**
 * User: Elparole
 * Date: 23.01.13
 * Time: 12:33
 */
package com.elparole.feedThemAll
{
import com.bit101.components.Label;

import flash.display.Sprite;
import flash.events.Event;

public class FTAStats extends Sprite
{
	public var planesLB:Label;
	public var planesSkinsLB:Label;
	public var maxYLB:Label;
	private static var _instance:FTAStats;
	private var waveLB:Label;

	public function SKStats() {
		_instance = this;
		addEventListener(Event.ADDED_TO_STAGE, onAdded)
	}

	private function onAdded(...args):void {
		waveLB = new Label(this,10,40, 'planes');
//		planesLB = new Label(this,10,40, 'planes');
		planesSkinsLB = new Label(this,10,60, 'planes skins');
		maxYLB = new Label(this,10,80, 'max plane y:');
	}

	public static function setPlanesSkins(v:int):void {
//		_instance.planesLB.text = 'planes skins: '+v;
	}

	public static function setPlanes(v:int):void {
//		_instance.planesLB.text = 'planes: '+v;
	}

	public static function setMaxY(v:int):void {
		_instance.maxYLB.text = 'max plane y: '+v;
	}

	public static function setWaveText(str:String):void {
		_instance.waveLB.text = str;
	}
}
}
