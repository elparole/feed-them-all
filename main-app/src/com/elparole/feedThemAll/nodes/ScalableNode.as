package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.DisplayBlis;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.Scalable;

public class ScalableNode extends Node
	{
		public var scalable : Scalable;
		public var display : DisplayBlis;
		public var position:Position;
	}
}
