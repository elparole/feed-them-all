/**
 * User: Elparole
 * Date: 19.02.13
 * Time: 18:46
 */
package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.Position;
import com.elparole.feedThemAll.components.CameraComp;

public class CameraNode extends Node
{
	public var camera:CameraComp;
	public var position:Position;
}
}
