/**
 * User: Elparole
 * Date: 18.02.13
 * Time: 18:58
 */
package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.DisplayBlis;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.Floor;

public class DrawableFloorNode extends Node
{
	public var floor:Floor;
	public var disp:DisplayBlis;
	public var postition:Position;
}
}
