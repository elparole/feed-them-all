/**
 * User: Elparole
 * Date: 08.02.13
 * Time: 18:50
 */
package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.DisplayBlis;
import com.elparole.components.Motion3d;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.Anim;
import com.elparole.feedThemAll.components.PathComponent;

public class PathNode extends Node
{
	public var pathComp:PathComponent;
	public var position:Position;
	public var motion:Motion3d;
	public var display : DisplayBlis;
	public var anim : Anim;
}
}
