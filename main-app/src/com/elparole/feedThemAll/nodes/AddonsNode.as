package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.DisplayBlis;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.DisplayAddons;

public class AddonsNode extends Node
	{
		public var position : Position;
		public var display : DisplayBlis;
		public var addons : DisplayAddons;
	}
}
