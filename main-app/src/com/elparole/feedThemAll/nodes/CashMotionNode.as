/**
 * User: Elparole
 * Date: 18.04.13
 * Time: 16:47
 */
package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.Position;
import com.elparole.feedThemAll.components.Bill;
import com.elparole.feedThemAll.components.CursorMagnesMotion;

public class CashMotionNode extends Node
{
	public var motion:CursorMagnesMotion;
	public var bill:Bill;
	public var position:Position

	public function CashMotionNode() {
	}
}
}
