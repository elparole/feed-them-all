package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.DisplayBlis;
import com.elparole.components.Position;

public class Render3DONode extends Node
	{
		public var position : Position;
		public var display : DisplayBlis;
	}
}
