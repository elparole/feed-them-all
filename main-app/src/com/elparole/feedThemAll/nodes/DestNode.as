package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.DisplayBlis;
import com.elparole.components.Motion3d;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.BadCreature;
import com.elparole.feedThemAll.components.Destinations;

public class DestNode extends Node
	{
		public var position : Position;
		public var display : DisplayBlis;
		public var creature : BadCreature;
		public var destination : Destinations;
		public var motion:Motion3d;
	}
}
