/**
 * User: Elparole
 * Date: 08.02.13
 * Time: 18:50
 */
package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.Position;
import com.elparole.feedThemAll.components.ArrowStrength;

public class ArrowStrengthNode extends Node
{
	public var arrow:ArrowStrength;
	public var position:Position;
}
}
