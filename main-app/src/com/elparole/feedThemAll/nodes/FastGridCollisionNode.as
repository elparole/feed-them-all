package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.DisplayBlis;
import com.elparole.components.Motion3d;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.GridPosition;

public class FastGridCollisionNode extends Node
	{
		public var display : DisplayBlis;
		public var gridPos : GridPosition;
		public var position : Position;
		public var motion : Motion3d;
	}
}
