package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.Position;
import com.elparole.feedThemAll.components.CameraComp;
import com.elparole.feedThemAll.components.CameraDestComp;

public class CameraMotionNode extends Node
	{
		public var position : Position;
		public var camera : CameraComp;
		public var cameraDest : CameraDestComp;
	}
}
