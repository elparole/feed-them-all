package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.Motion3d;
import com.elparole.components.Position;

public class MovementNode extends Node
	{
		public var position : Position;
		public var motion : Motion3d;
	}
}
