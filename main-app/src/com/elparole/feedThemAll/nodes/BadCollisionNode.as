package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.Motion3d;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.BadCreature;
import com.elparole.feedThemAll.components.CollisionSphere;
import com.elparole.feedThemAll.components.Destinations;
import com.elparole.feedThemAll.components.PathComponent;

public class BadCollisionNode extends Node
	{
		public var badCreatur : BadCreature;
		public var position : Position;
		public var motion : Motion3d;
		public var collision : CollisionSphere;
		public var destinations : Destinations;
		public var path:PathComponent
	}
}
