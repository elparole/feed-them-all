/**
 * User: Elparole
 * Date: 19.02.13
 * Time: 18:53
 */
package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.Motion3d;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.CatapultControls;
import com.elparole.feedThemAll.components.PlayerCatapult;
import com.elparole.feedThemAll.components.Weapon;

public class PlayerControlNode extends Node
{
	public var player:PlayerCatapult;
	public var control : CatapultControls;
	public var position : Position;
	public var motion : Motion3d;
	public var weapon:Weapon;
}
}
