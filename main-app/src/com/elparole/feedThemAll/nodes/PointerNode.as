/**
 * User: Elparole
 * Date: 08.02.13
 * Time: 18:50
 */
package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.DisplayBlis;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.TargetPointer;

public class PointerNode extends Node
{
	public var pointer:TargetPointer;
	public var display:DisplayBlis;
	public var position:Position;
}
}
