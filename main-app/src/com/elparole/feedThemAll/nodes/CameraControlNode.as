package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.Motion3d;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.CameraComp;
import com.elparole.feedThemAll.components.MotionControls;

public class CameraControlNode extends Node
	{

		//public static const excluded:Array = [Gun];
		public var camera:CameraComp;
		public var control : MotionControls;
		public var position : Position;
		public var motion : Motion3d;
	}
}
