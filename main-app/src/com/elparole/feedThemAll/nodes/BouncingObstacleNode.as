package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.DisplayBlis;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.BouncingObstacle;

public class BouncingObstacleNode extends Node
	{
		public var position : Position;
		public var display : DisplayBlis;
		public var bouncingObstacle:BouncingObstacle
	}
}
