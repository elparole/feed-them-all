package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.DisplayBlis;
import com.elparole.components.Motion3d;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.Anim;
import com.elparole.feedThemAll.components.BadCreature;

public class EnemyMotionControlNode extends Node
	{

		//public static const excluded:Array = [Gun];
		public var creature:BadCreature;
		public var position : Position;
		public var anim : Anim;
		public var display : DisplayBlis;
		public var motion : Motion3d;



	}



}
