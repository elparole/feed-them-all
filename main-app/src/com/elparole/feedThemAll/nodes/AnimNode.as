package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.DisplayBlis;
import com.elparole.components.Motion3d;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.Anim;

public class AnimNode extends Node
	{
		public var position : Position;
		public var display : DisplayBlis;
		public var anim : Anim;
		public var motion:Motion3d;
	}
}
