package com.elparole.feedThemAll.nodes
{
import ash.core.Node;

import com.elparole.components.Motion3d;
import com.elparole.components.Position;
import com.elparole.feedThemAll.components.GoodCreature;

public class GoodCollisionNode extends Node
	{
		public var goodCreature : GoodCreature;
		public var position : Position;
		public var motion : Motion3d;
	}
}
