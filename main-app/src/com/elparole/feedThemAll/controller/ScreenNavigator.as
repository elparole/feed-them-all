/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 17:15
 */
package com.elparole.feedThemAll.controller
{
import ash.core.Engine;
import ash.tick.FrameTickProvider;

import com.blackmoondev.ashes.script.systems.ScriptRunnerSystem;
import com.elparole.feedThemAll.PublishMode;
import com.elparole.feedThemAll.model.GameScenarioModel;
import com.elparole.feedThemAll.model.GameState;
import com.elparole.feedThemAll.model.LevelModel;
import com.elparole.feedThemAll.model.MapModel;
import com.elparole.feedThemAll.service.SharedObjectService;
import com.elparole.feedThemAll.systems.AddEntitySystem;
import com.elparole.feedThemAll.systems.CameraMotionSystem;
import com.elparole.feedThemAll.systems.CollisionSystem;
import com.elparole.feedThemAll.systems.EnemyCreatorSystem;
import com.elparole.feedThemAll.systems.GameManager;
import com.elparole.feedThemAll.systems.MotionControlSystemClick;
import com.elparole.feedThemAll.systems.MovementSystem;
import com.elparole.feedThemAll.systems.PathSystem;
import com.elparole.feedThemAll.systems.RedrawToBackgroundSystem;
import com.elparole.feedThemAll.systems.RemoveMarkedSystem;
import com.elparole.feedThemAll.systems.RenderSystem;
import com.elparole.feedThemAll.systems.ScalableSystem;
import com.elparole.feedThemAll.systems.TTLSystem;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.view.GameCompleteView;
import com.elparole.feedThemAll.view.GameOverView;
import com.elparole.feedThemAll.view.IScreen;
import com.elparole.feedThemAll.view.InGameView;
import com.elparole.feedThemAll.view.LevelChooseView;
import com.elparole.feedThemAll.view.LevelCompleteView;
import com.elparole.feedThemAll.view.LoadingView;
import com.elparole.feedThemAll.view.ShopView;
import com.elparole.feedThemAll.view.SoundManager;
import com.elparole.feedThemAll.view.StartView;
import com.elparole.gamingUtils.SoundControls;
import com.elparole.input.MousePoll;
import com.elparole.levelEditor.model.vo.LevelConfig;
import com.elparole.levelEditor.service.ILevelService;
import com.elparole.levelEditor.service.LevelFromFileDataService;

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.Sprite;
import flash.utils.Dictionary;
import flash.utils.setTimeout;

public class ScreenNavigator
{

	[Inject]
	public var tickProvider:FrameTickProvider;

	[Inject]
	public var gameState:GameState;

	[Inject]
	public var soService:SharedObjectService;

	[Inject]
	public var mousePoll:MousePoll;

	[Inject]
	public var game:Engine;

	[Inject]
	public var soundManager:SoundManager;

	[Inject]
	public var llser:ILevelService;

	[Inject]
	public var levelModel:LevelModel;

	[Inject]
	public var mapModel:MapModel;
	
	[Inject]
	public var gameScenario:GameScenarioModel;

	private var views:Dictionary;
	private var cont:DisplayObjectContainer;
	private var currentViews:Array;
	private var startView:StartView;
	private var gameOverView:GameOverView;
	private var inGameView:InGameView;
	private var currentView:IScreen;
	private var levelChooseView:LevelChooseView;
	private var shopView:ShopView;
	private var levelCompleteView:LevelCompleteView;
	private var gameCompleteView:GameCompleteView;
	private var soundPanel:SoundPanelComp = new SoundPanelComp();
	private var soundsControls:SoundControls = new SoundControls();
	private var screenMask:Sprite;
	
	public function ScreenNavigator(cont:DisplayObjectContainer) {

		this.cont = cont;
		currentViews =[];

		cont.stage.stageFocusRect = false;

		soundPanel.x = 580;
		soundPanel.y = 4;

		views = new Dictionary();
		views[MenuBackScr] = new MenuBackScr();
		views[LoadingView] = new LoadingView();
		views[LevelChooseView] = levelChooseView = new LevelChooseView();
		views[GameOverView] = gameOverView = new GameOverView();
		views[ShopView] = shopView = new ShopView();
		views[LevelCompleteView] = levelCompleteView = new LevelCompleteView();
		views[GameCompleteView] = gameCompleteView = new GameCompleteView();

		screenMask = new Sprite();
		screenMask.graphics.beginFill(0);
		screenMask.graphics.drawRect(0,0,640,480);
		screenMask.graphics.endFill();


		cont.addChildAt(views[MenuBackScr],0);
//		gameState.autoShowShop.add(onShowShop);

		if(GameState.debug){

		}
	}

	public function soundStatesInit():void {
		soundsControls.showSoundMusicStates();
	}

	public function postInjectionInit():void {

		views[StartView] = startView = new StartView();
		views[InGameView] = inGameView = new InGameView();
		soundsControls.init(soundPanel,soundManager);

		gameScenario.showInstructions.add(onShowInstructions);
		
		gameState.setWin.add(onWin);
//		gameState.waveInfoChanged.add(onWaveInfoChanged);
		gameState.livesChanged.add(onLivesChanged);
		gameState.scoreChanged.add(onScoreChanged);
		gameState.weaponsUpdate.add(inGameView.onWeaponsUpdate);
		gameState.showControls.add(inGameView.onShowControls);
		gameState.cashChanged.add(inGameView.onCashChanged);
		gameState.cashChanged.add(shopView.onCashChanged);
		gameState.sheepNumChanged.add(inGameView.onSheepsChanged);
		gameState.showCameraMoved.add(inGameView.onExternalHideInstructions);

		inGameView.blockMouse.add(onBlockMouse);
		inGameView.weaponSelected.add(onChangeWeapon);
		inGameView.pauseSelected.add(showHidePause);

		levelChooseView.goBack.add(onGoStart);
		inGameView.backToMain.add(onGoStart);
		gameOverView.backToMain.add(onGoStart);
		levelChooseView.levelSelected.add(onLevelSelected);
//		startView.startGame.add(onTestView);
		startView.startGame.add(onStart);
		startView.hideEnemies.add(onHideEnemies);
		levelCompleteView.continueLevelComplete.add(continueLevelCompleted);
		shopView.setConfig(gameState.playerConfig);
		shopView.startGame.add(onShowLevels);
		gameCompleteView.showStart.add(onGoStart);
		gameOverView.startGame.add(onRestart);
	}

	private function onShowInstructions(id:int):void {
		gameState.mouseBlockedByMenu = id!=3;
		if(id == 3){
			gameState.cameraMoved = false;
			levelModel.monstersEnabled = false;
		}
		showHidePause([true,id]);
	}

	public function continueLevelCompleted():void {
		if(gameState.currentLevel<19)
			setView(ShopView,true);
		else
			setView(GameCompleteView);
		
	}
	public function onTestView():void {
		setView(GameCompleteView);
	}
	public function onGoStart():void {
		setView(StartView);
	}

	public function onGoBack():void {
		setView(ShopView);
	}

	private function onChangeWeapon(weaponIndex:int):void {
		gameState.selectWeapon(weaponIndex-1);
		soundManager.play(AnimsIDs.selectSounds[gameState.currentWeapon.id]);
	}

	private function onBlockMouse(blocked:Boolean):void {
		gameState.mouseBlockedByMenu = blocked;
	}

	private function onHideEnemies():void {
		game.removeSystem(game.getSystem(EnemyCreatorSystem ));
		game.addSystem( new MotionControlSystemClick(), 78 );
		game.removeSystem(game.getSystem(MotionControlSystemClick ));
	}

	private function onWin(sheepsLeft:int):void {
		trace('win sheepsLeft',sheepsLeft);
		gameState.isWin = true;
		gameState.isRunning = false;		
		gameState.starsToShow = Math.max(1,Math.floor(3*sheepsLeft/levelModel.levelConfig.sheepsNum));
		endGame();
	}

//	private function onWaveInfoChanged(messageText:String, visible:Boolean):void {
//		visible? inGameView.showWaveInfo(messageText):
//				 inGameView.hideWaveInfo();
//	}



	private function onShowLevels():void {
		setView(LevelChooseView,true)
	}

	private function onRestart():void {
//		soundManager.play(ClickSnd);
		game.removeAllEntities();
//		(game.getSystem(RenderSystem) as RenderSystem).destroy();
		game.removeAllSystems();
//		setView(StartView);
		setView(InGameView);
		setTimeout(function():void{
			initSystems();
			gameState.reset();
		},1000);
//		initSystems();

//		onStart();
	}


	private function onScoreChanged(score:int):void {
		inGameView.setScore(score);
	}

	private function onLivesChanged(value:int):void {
		trace('onLivesChanged',value,gameState.isRunning);
		inGameView.setLives(value+1);
		if((value == 0) && gameState.isRunning){


			gameState.isRunning = false;
			gameState.isWin = false;
			setTimeout(function () {
//								tickProvider.stop();
				gameState.starsToShow = 0;
				endGame();
			}, 1000);
		}
	}

	private function endGame():void {
		trace('endGame');
//		game.removeSystem(game.getSystem(MotionControlSystemClick));
//		game.removeSystem(game.getSystem(CollisionSystem));
//		game.removeSystem(game.getSystem(GunControlSystem));

		game.removeAllEntities();
		game.removeAllSystems();
		tickProvider.stop();
//		game.removeSystem(game.getSystem(GameManager));
//		game.removeSystem(game.getSystem(EnemyCreatorSystem));
//		game.removeSystem(game.getSystem(AddEntitySystem));
//		game.removeSystem(game.getSystem(MotionControlSystemClick));
//		game.removeSystem(game.getSystem(GunControlSystem));
//		game.removeSystem(game.getSystem(MovementSystem));
//		game.removeSystem(game.getSystem(CollisionSystem));
//		game.removeSystem(game.getSystem(ScriptRunnerSystem));
//		game.removeSystem(game.getSystem(RedrawToBackgrounSystem));
//		game.removeSystem(game.getSystem(ScalableSystem));
//		game.removeSystem(game.getSystem(RenderSystem));
//		game.removeSystem(game.getSystem(RemoveMarkedSystem));

		if(gameState.isWin){
			levelModel.setLevelsProgress(gameState.starsToShow);
			setView(LevelCompleteView);
		}
		else
			setView(GameOverView);
	}

//	private function stopGame():void {
//		tickProvider.stop();
//	}

	private function onLevelSelected(nr:int):void {
		//		views[InGameView] = inGameView = new InGameView(soundManager);

		if(PublishMode.devMode){
			if(llser.hasLevelNR(nr)){
				gameState.currentLevel = nr;
				llser.loadConfig((llser as LevelFromFileDataService).list[nr],'levels/'+(llser as LevelFromFileDataService).list[nr])
						.addResultProcessor(levelModel.parseConfig)
						.addResultProcessor(mapModel.parseObstacleMap)
						.addResultProcessor(onShowSelectedLevel)
			}
		}else{
			if(llser.hasLevelNR(nr)){
				gameState.currentLevel = nr;
				llser.loadConfigByNR(nr)
						.addResultProcessor(levelModel.parseConfig)
						.addResultProcessor(mapModel.parseObstacleMap)
						.addResultProcessor(onShowSelectedLevel)
			}
		}
	}
	private function onShowSelectedLevel(lc:LevelConfig,err:Object):LevelConfig {

		gameState.healthSkill = 3;
//		soundManager.play(ClickSnd);

		if(cont.stage)
			cont.stage.focus = cont.stage;

		views[InGameView].reset();
		runGame();
		return lc;
	}

	private function onStart():void {
//		setView(LevelChooseView);
		if(levelModel.levelsProgress[1]==LevelModel.LEVEL_DISABLED_ID)
			setView(LevelChooseView)
		else
			setView(ShopView);
	}

	public function showHidePause(argums:Array):void {
		var pause:Boolean = argums[0];
		var instructionsId:int = argums[1];

		if(pause){
			if((gameState.paused && gameState.isRunning) || !(currentView is InGameView)){
				return;
			}
			
			if((instructionsId != 3) && game.getSystem(MotionControlSystemClick))
				game.removeSystem(game.getSystem(MotionControlSystemClick));

			if(instructionsId<0)
				inGameView.showPause();
			else
				inGameView.showInstructions(instructionsId);

			if((instructionsId != 3)){
				gameState.paused = true;
				tickProvider.stop();
			}
		}else{
			gameState.paused = false;
			if(!game.getSystem(MotionControlSystemClick))
				game.addSystem( new MotionControlSystemClick(),78);

			if(!levelModel.monstersEnabled){
				levelModel.monstersEnabled = true;
				gameState.ctime = 0;
			}

			gameState.mouseBlockedByMenu = false;
			tickProvider.start();
		}
		mousePoll.clicked = false;
	}

	public function setView(view:Class,removePrevious:Boolean = true):void {
		trace('set view',view)
		if(removePrevious && currentView && (currentView as DisplayObject).parent){
			for each (var screen:IScreen in currentViews) {
				screen.destroy();
				cont.removeChild(screen as DisplayObject);
			}
			currentViews.length = 0;
		}
		
		currentView = views[view];
		currentViews.push(currentView);
		cont.addChild(currentView as DisplayObject);

		if(view!=InGameView){
			cont.addChild(screenMask);
			(currentView as DisplayObject).mask = screenMask;
		}else{
			screenMask.parent && screenMask.parent.removeChild(screenMask);
		}

		currentView.init();

		switch(view){
			case GameOverView:
				views[MenuBackScr].parent && cont.removeChild(views[MenuBackScr]);
				soundManager.playGameOver();
				break;
			case LevelCompleteView:
				views[MenuBackScr].parent && cont.removeChild(views[MenuBackScr]);

				levelCompleteView.showStars(gameState.starsToShow);
				levelCompleteView.showInfo(gameState.currentLevel);

				soService.saveLevels(levelModel.levelsProgress)
				soService.saveMoney(gameState.playerConfig.coins);
				soService.clearFence();

				soundManager.playLevelWin();
				break;
			case InGameView:
				views[MenuBackScr].parent && cont.removeChild(views[MenuBackScr]);
				inGameView.onCashChanged(gameState.playerConfig.coins);
				if(!gameState.isRunning){
					tickProvider.start();
					soundManager.playGame();
				}
				break;
			case ShopView:
				if(gameState.isRunning){
					gameState.paused = false;
					gameState.isRunning = false;
					game.removeAllEntities();
					game.removeAllSystems();
					tickProvider.stop();
				}

				cont.addChildAt(views[MenuBackScr],0);
				soundManager.playCover();
				shopView.setConfig(gameState.playerConfig);
				break;
			case LevelChooseView:
				cont.addChildAt(views[MenuBackScr],0);
				levelChooseView.enableLevels(levelModel.levelsProgress);
				soundManager.playCover();
				break;
			case GameCompleteView:
				cont.addChildAt(views[MenuBackScr],0);
				soundManager.playCover();
				break;
			case StartView:
				if(gameState.isRunning){
					gameState.paused = false;
					gameState.isRunning = false;
					game.removeAllEntities();
					game.removeAllSystems();
					tickProvider.stop();
				}
				cont.addChildAt(views[MenuBackScr],0);
				soundManager.playCover();
				break;
		}
		cont.addChild(soundPanel);
	}

	private function runGame():void {

		if(game.getSystem(EnemyCreatorSystem))
			game.removeSystem(game.getSystem(EnemyCreatorSystem) );

		setView(InGameView);
		setTimeout(function():void{
			initSystems();
			gameState.reset();
		},1000);
	}

	public function startTicker():void{
		trace('start ticker');
		tickProvider.add(game.update);
		tickProvider.start();
	}

	public function initSystems():void{
		game.removeAllEntities();
		game.removeAllSystems();
//		if(game.getSystem(GameManager))
//			return;

		game.addSystem( new GameManager(), 75 );
		game.addSystem( new EnemyCreatorSystem(), 76 );
		game.addSystem( new AddEntitySystem(), 77 );
		game.addSystem( new MotionControlSystemClick(), 78 );
		game.addSystem( new PathSystem(), 79 );
		game.addSystem( new CameraMotionSystem(), 80 );
		game.addSystem( new MovementSystem(), 81 );
		game.addSystem( new CollisionSystem(), 82 );
		game.addSystem( new ScriptRunnerSystem(), 83 );
		game.addSystem( new RedrawToBackgroundSystem(), 84 );
		game.addSystem( new ScalableSystem(), 85 );
		game.addSystem( new RenderSystem(), 86 );
		game.addSystem( new RemoveMarkedSystem(), 87 );
		game.addSystem( new TTLSystem(), 88 );
	}
}
}
