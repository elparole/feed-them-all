/**
 * User: Elparole
 * Date: 06.02.13
 * Time: 16:54
 */
package com.elparole.feedThemAll.controller
{
import ash.core.Entity;

import com.blackmoondev.ashes.script.components.ScriptsHolderComponent;
import com.elparole.components.DisplayBlis;
import com.elparole.components.Motion3d;
import com.elparole.components.Position;
import com.elparole.feedThemAll.AppConsts;
import com.elparole.feedThemAll.components.Anim;
import com.elparole.feedThemAll.components.BadCreature;
import com.elparole.feedThemAll.components.CollisionSphere;
import com.elparole.feedThemAll.components.Destinations;
import com.elparole.feedThemAll.components.PathComponent;
import com.elparole.feedThemAll.components.Puddle;
import com.elparole.feedThemAll.components.Scalable;
import com.elparole.feedThemAll.components.StateComponent;
import com.elparole.feedThemAll.model.EntityProcessStateMachine;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.MapModel;
import com.elparole.feedThemAll.model.ScriptGenerator;
import com.elparole.feedThemAll.model.ids.ProcessIds;
import com.elparole.feedThemAll.model.ids.StatesIds;
import com.elparole.feedThemAll.view.AnimsIDs;

import flash.geom.Point;

public class BadCreatureStatesDefCreator
{
	[Inject]
	public var scriptGenerator:ScriptGenerator;

	[Inject]
	public var mapModel:MapModel;

//	[Inject]
//	public var astar:Astar

	public var gameUtils:GameUtils;

	public function BadCreatureStatesDefCreator() {
		
	}
	
	public function createStateComp(entity:Entity, disp:DisplayBlis, anim:Anim, px:int, pz:int, dx:int, dz:int, destId:int):StateComponent{
		var sc:StateComponent = new StateComponent();
		sc.fsm = new EntityProcessStateMachine(entity);

		var position:Position;

//		position = new Position(randomIntInRange(-200,200),0,500);
		position = new Position(px,0,pz);

		var badCreature:BadCreature = new BadCreature(0);
		switch(disp.skinId){
			case AnimsIDs.MONSTER1:
				badCreature.meatToEat = 2;
				break;
			case AnimsIDs.MONSTER2:
				badCreature.meatToEat = 3;
				break;
			case AnimsIDs.MONSTER3:
				badCreature.meatToEat = 4;
				break;
		}

		var mt:Motion3d = new Motion3d(0,0,0);
		var collision:CollisionSphere = new CollisionSphere(10);
		anim.animSpeed = 25;//25 for speed 4

		var destinations:Destinations =  new Destinations(destId,
				new Point(Math.floor(position.position.x/AppConsts.xGrid),
						Math.floor(position.position.z/AppConsts.zGrid)),
				new Point(Math.round(dx/AppConsts.xGrid),
						Math.round(dz/AppConsts.zGrid)));
//	.add( gameUtils.getAnim(skinID,animId, dirId))
//	.add( mt )
//				.add( )

		sc.fsm.createState(StatesIds.IDLE)
				.add(Position).withInstance(position)
				.add(DisplayBlis).withInstance(disp)
				.add(Anim).withInstance(anim)
				.add(Motion3d).withInstance(mt)
				.add(CollisionSphere).withInstance(collision)
				.add(BadCreature).withInstance(badCreature);

		var pc:PathComponent = gameUtils.getPathComponent(position.position.x,position.position.z, dx, dz,mapModel);
		anim.animSpeed = pc.velocity/60*30;

		sc.fsm.createState(StatesIds.GO_FOR_SHEEP)
				.add(Position).withInstance(position)
				.add(BadCreature).withInstance(badCreature)
				.add(Destinations).withInstance(destinations)
				.add(DisplayBlis).withInstance(disp)
				.add(Anim).withInstance(anim)
				.add(Motion3d).withInstance(mt)
				.add(CollisionSphere).withInstance(collision)
				.add(PathComponent).withInstance(pc)
				.add(ScriptsHolderComponent).withInstance(
					ScriptsHolderComponent.getSingleScriptComponent(
							scriptGenerator.goForSheepScript(), false));
//				.add(ScriptsHolderComponent).withInstance(
//					ScriptsHolderComponent.getSingleScriptListComponent(
//							scriptGenerator.generateScriptListFromPathArray(
////									TestPathGenerator.generateDRPathFromIntPtGrid(
////											mapModel.solve(
////													Math.floor(position.position.x/AppConsts.xGrid),
////													Math.floor(position.position.z/AppConsts.zGrid),
////													Math.floor(dx/AppConsts.xGrid),
////													Math.floor(dz/AppConsts.zGrid))))));
//								TestPathGenerator.generateDRRandomPath(position.position.x,
//																		position.position.z,
//																		randomIntInRange(-20,20),-30,50))));

		var puddle:Entity = getPuddle(entity);
		sc.fsm.createState(StatesIds.EAT)
				.add(Position).withInstance(position)
				.add(BadCreature).withInstance(badCreature)
				.add(DisplayBlis).withInstance(disp)
				.add(Anim).withInstance(anim)
				.add(Motion3d).withInstance(mt)
				.add(ScriptsHolderComponent).withInstance(
					ScriptsHolderComponent.getSingleScriptComponent(
							scriptGenerator.stopAndPlayEat(), false));
		badCreature.sc = sc;

		sc.fsm.createState(StatesIds.GO_HOME)
				.add(Position).withInstance(position)
				.add(BadCreature).withInstance(badCreature)
				.add(Destinations).withInstance(destinations)
				.add(DisplayBlis).withInstance(disp)
				.add(Anim).withInstance(anim)
				.add(Motion3d).withInstance(mt)
				.add(CollisionSphere).withInstance(collision)
				.add(ScriptsHolderComponent).withInstance(
					ScriptsHolderComponent.getSingleScriptComponent(
						scriptGenerator.getGoHomeScript(), true));

		sc.fsm.setInitState(StatesIds.GO_FOR_SHEEP);


//				new ActionsComponent(
//					ScriptGenerator.generateScriptListFromPathArray(
//							TestPathGenerator.generateRandomPath(10))));
		
		sc.fsm.configureProcess(ProcessIds.DRAW_SHEEP, StatesIds.IDLE, StatesIds.GO_FOR_SHEEP);
		sc.fsm.configureProcess(ProcessIds.DRAW_SHEEP, StatesIds.GO_FOR_SHEEP, StatesIds.GO_FOR_SHEEP);
		sc.fsm.configureProcess(ProcessIds.DRAW_SHEEP, StatesIds.EAT, StatesIds.GO_FOR_SHEEP);
		sc.fsm.configureProcess(ProcessIds.DRAW_SHEEP, StatesIds.GO_HOME, StatesIds.GO_FOR_SHEEP);
		sc.fsm.configureProcess(ProcessIds.EAT_MEAT, StatesIds.GO_FOR_SHEEP, StatesIds.EAT);
		sc.fsm.configureProcess(ProcessIds.EAT_MEAT, StatesIds.GO_HOME, StatesIds.EAT);
		sc.fsm.configureProcess(ProcessIds.EAT_MEAT, StatesIds.EAT, StatesIds.EAT);
		sc.fsm.configureProcess(ProcessIds.EAT_SHEEP, StatesIds.EAT, StatesIds.EAT);
		sc.fsm.configureProcess(ProcessIds.EAT_SHEEP, StatesIds.GO_FOR_SHEEP, StatesIds.EAT);
		sc.fsm.configureProcess(ProcessIds.GO_HOME, StatesIds.EAT, StatesIds.GO_HOME);
		sc.fsm.configureProcess(ProcessIds.GO_HOME, StatesIds.GO_FOR_SHEEP, StatesIds.GO_HOME);
//		sc.fsm.configureProcess(ProcessIds.KILL, StatesIds.GO_FOR_SHEEP, StatesIds.);

		
//		sc.setStateComponentsConfigs(StatesIds.IDLE, createIdleCompConf());
//		sc.setStateComponentsConfigs(StatesIds.GO_FOR_SHEEP, createGoForSheepCompConf());
//		sc.setStateComponentsConfigs(StatesIds.GO_HOME, createGoHomeCompConf());
//		sc.setStateComponentsConfigs(StatesIds.EAT, createEatCompConf());

		return sc;
	}

	

	public function getPuddle(refEnt:Entity):Entity {

		var disp:DisplayBlis = new DisplayBlis(AnimsIDs.PUDDLE, AnimsIDs.IDLE, AnimsIDs.DR,
				gameUtils.cloneBD(gameUtils.getSkinByID(AnimsIDs.PUDDLE, AnimsIDs.IDLE, AnimsIDs.DR, 0)),
				gameUtils.getSkinOffset(AnimsIDs.PUDDLE, AnimsIDs.IDLE, AnimsIDs.DR, 0));

		var puddle:Entity = new Entity();
		puddle.add(new Puddle(refEnt))
			.add(new Scalable(0.1,0.1))
			.add(new Position(0,-1,-5))
//			.add(new Direction())
			.add(disp)
			.add(ScriptsHolderComponent.getSingleScriptComponent(
						scriptGenerator.getGrowPuddleScript(), false));

		return puddle;
	}

	private function createEatCompConf(entity:Entity, stateComponent:StateComponent):void {
//		entity.add(Position)
//		cconf.add(new Position( 0, 0,0));
//		cconf.add(new BadCreature());
//		return cconf;
	}

//	private function createGoHomeCompConf():ComponentsConfig {
//		var cconf:ComponentsConfig = new ComponentsConfig();
//		cconf.add(new Position( 0, 0,0));
//		cconf.add(new BadCreature());
//		return cconf;
//	}
//
//	private function createGoForSheepCompConf():ComponentsConfig {
//		var cconf:ComponentsConfig = new ComponentsConfig();
//		cconf.add(new Position( 0, 0,0));
//		cconf.add(new BadCreature());
//		return cconf;
//	}
//
//	private function createIdleCompConf():ComponentsConfig {
//		var cconf:ComponentsConfig = new ComponentsConfig();
//		cconf.add(new Position( 0, 0,0));
//		cconf.add(new BadCreature());
//
//		return cconf;
//	}
}
}
