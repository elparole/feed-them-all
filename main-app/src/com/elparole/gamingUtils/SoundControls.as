/**
 * User: Elparole
 * Date: 28.01.13
 * Time: 21:58
 */
package com.elparole.gamingUtils
{
import com.elparole.feedThemAll.view.SoundManager;

import flash.events.MouseEvent;

public class SoundControls
{
	private var target:Object;
	private var soundManager:SoundManager;


	public function SoundControls() {

	}

	public function init(target:Object, soundManager:SoundManager):void{

		this.target = target;
		this.soundManager = soundManager;
//		PublishMode.setButtonText(this.target.soundOnBt, 'SOUND ON')
//		PublishMode.setButtonText(this.target.soundOffBt, 'SOUND OFF')
//		PublishMode.setButtonText(this.target.musicOnBt, 'MUSIC ON')
//		PublishMode.setButtonText(this.target.musicOffBt, 'MUSIC OFF')

//		this.target.soundOffBt.visible = !soundManager.soundOn;
//		this.target.musicOnBt.visible = soundManager.musicOn;
//		this.target.musicOffBt.visible = !soundManager.musicOn;

		this.target.soundOnBt.visible = soundManager.soundOn;
		this.target.soundOffBt.visible = !soundManager.soundOn;
		this.target.musicOnBt.visible = soundManager.musicOn;
		this.target.musicOffBt.visible = !soundManager.musicOn;

		this.target.soundOffBt.addEventListener(MouseEvent.CLICK, onSetSoundOn, false,0,true);
		this.target.soundOnBt.addEventListener(MouseEvent.CLICK, onSetSoundOff, false,0,true);
		this.target.musicOffBt.addEventListener(MouseEvent.CLICK, onSetMusicOn, false,0,true);
		this.target.musicOnBt.addEventListener(MouseEvent.CLICK, onSetMusicOff, false,0,true);
	}

	private function onSetMusicOn(event:MouseEvent):void {
		soundManager.play(Click_04Snd);
		soundManager.musicOn = true;
		this.target.musicOffBt.visible = false;
		this.target.musicOnBt.visible = true;
	}

	private function onSetSoundOff(event:MouseEvent):void {
		soundManager.play(Click_04Snd);
		soundManager.soundOn = false;
		this.target.soundOffBt.visible = true;
		this.target.soundOnBt.visible = false;
	}

	private function onSetSoundOn(event:MouseEvent):void {
		soundManager.play(Click_04Snd);
		soundManager.soundOn = true;
		this.target.soundOffBt.visible = false;
		this.target.soundOnBt.visible = true;
	}

	private function onSetMusicOff(event:MouseEvent):void {
		soundManager.play(Click_04Snd);
		soundManager.musicOn = false;
		this.target.musicOffBt.visible = true;
		this.target.musicOnBt.visible = false;
	}

	public function showSoundMusicStates():void {
		this.target.musicOffBt.visible = !soundManager.musicOn;
		this.target.musicOnBt.visible = soundManager.musicOn;

		this.target.soundOffBt.visible = !soundManager.soundOn;
		this.target.soundOnBt.visible = soundManager.soundOn;
	}
}
}
