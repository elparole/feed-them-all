package com.elparole
{
import flash.display.BitmapData;
import flash.display.Shape;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.geom.Vector3D;
import flash.ui.Keyboard;
import flash.utils.getTimer;

/**
 * Sample Away3D Lite Stage3DRenderer
 * @author Philippe
 */
[SWF(width="800", height = "600")]
public class TestAwayLiteIso extends Sprite
{
//	[Embed(source="/../assets/skyboxlite.jpg")]
//	private const SkyImage:Class;
//	[Embed(source="/../assets/texture.jpg")]
//	private const TestTex:Class;
//	[Embed(source="/../assets/cloud.png")]
//	private const TestTex2:Class;

	private var useStage3D:Boolean;
	public var view3D:View3D;
	private var cube:Skybox6;
	private var plane:Plane;
	private var plane2:Plane;
	private var t0:int;
	private var shape:Shape;

	public function SkyboxLite()
	{
		addEventListener(Event.ADDED_TO_STAGE, addedToStage);
	}

	private function addedToStage(e:Event):void
	{
		removeEventListener(Event.ADDED_TO_STAGE, addedToStage);

		this.useStage3D = hasStage3D();
		initializeScene();
		initializeUI();

		addEventListener(Event.ENTER_FRAME, enterFrame);
		t0 = getTimer();
	}

	private function hasStage3D():Boolean
	{
		if (loaderInfo.parameters.fp10 == "1") // force FP10
			return false;
		try { return stage.stage3Ds != null && stage.stage3Ds.length > 0; }
		catch (e:Error) { return false; }
		return false;
	}

	private function initializeUI():void
	{
		shape = new Shape();
		shape.graphics.beginFill(0xff0000);
		shape.graphics.drawCircle(0, 0, 10);
		addChild(shape);
	}

	private function initializeScene():void
	{
		view3D = new View3D();
		view3D.mouseEnabled3D = false; // not very usable for now
		addChild(view3D);

		if (useStage3D) view3D.renderer = new Stage3DRenderer(); // easy ;)

		// skybox
		var skyTex:BitmapMaterial = new BitmapMaterial(new BitmapData(512,512,true,0xffff0000));
		cube = new Skybox6(skyTex);
		view3D.scene.addChild(cube);

		// a plane using mip-mapping
		var mat1:BitmapMaterial = new BitmapMaterial(new BitmapData(512,512,true,0xff00ff00));
		mat1.mipmap = true;

		plane = new Plane(mat1);
		plane.bothsides = true;
		plane.rotationX = 90;
		plane.scaleX = 2;
		plane.scaleZ = 2;
		view3D.scene.addChild(plane);

		// another simple plane
		var mat2:BitmapMaterial = new BitmapMaterial(new BitmapData(512,512,true,0xff0000ff));

		plane2 = new Plane(mat2);
		plane2.blendMode = "add"; // additive!
		plane2.bothsides = true;
		plane2.rotationY = -90;
		plane2.rotate(-30, Vector3D.Z_AXIS);
		plane2.scaleX = 4;
		plane2.scaleZ = 4;
		view3D.scene.addChild(plane2);

		stage.addEventListener(KeyboardEvent.KEY_DOWN, stage_keyDown);
	}

	private function stage_keyDown(e:KeyboardEvent):void
	{
		if (e.keyCode == Keyboard.UP) plane.y -= 100;
		if (e.keyCode == Keyboard.DOWN) plane.y += 100;
		if (e.keyCode == Keyboard.LEFT) plane.x -= 100;
		if (e.keyCode == Keyboard.RIGHT) plane.x += 100;
		if (e.keyCode == Keyboard.NUMPAD_ADD) plane.z += 100;
		if (e.keyCode == Keyboard.NUMPAD_SUBTRACT) plane.z -= 100;
	}

	private function enterFrame(e:Event):void
	{
		var t:int = getTimer();
		var elapsed:int = t - t0;
		t0 = t;
		var k:Number = Math.min(4, elapsed / 50);

		// animations
		if (cube) cube.rotationY -= 0.2 * k;
		plane.rotationZ -= 1 * k;
		plane.rotationX -= 1 * k;
		plane.rotationY -= 1 * k;
		plane2.rotationZ -= 1 * k;
		plane2.rotationX -= 1 * k;
		plane2.rotationY -= 1 * k;
		var sh:Number = stage.stageHeight / 2;
		view3D.scene.rotationX = 45 * (stage.mouseY - sh) / sh;

		// render
		view3D.x = stage.stageWidth/2;
		view3D.y = stage.stageHeight/2;
		view3D.render();

		// using the new 'screenPosition' property
		shape.x = plane.screenPosition.x + stage.stageWidth/2;
		shape.y = plane.screenPosition.y + stage.stageHeight/2;
	}
}

}
