/**
 * User: Elparole
 * Date: 05.03.13
 * Time: 16:00
 */
package com.elparole.components
{
import flash.display.BitmapData;

import org.papervision3d.materials.BitmapMaterial;
import org.papervision3d.objects.primitives.Plane;

public interface IBitmapPlane
{
	function get bitmap():BitmapData;

	function get plane():Plane;

	function set plane(plane:Plane):void;

	function set material(material:BitmapMaterial):void;

	function get material():BitmapMaterial;
}
}
