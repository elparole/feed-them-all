package com.elparole.components
{
import flash.display.BitmapData;
import flash.geom.Point;

import org.papervision3d.materials.BitmapMaterial;
import org.papervision3d.objects.primitives.Plane;

public class DisplayPPV implements IBitmapPlane
	{
		private var _bitmap : BitmapData = null;
		public var _plane:Plane;
		public var _material:BitmapMaterial;
		public var skinId:String;
		public var animId:String;
		public var dirId:String;
		public var offset:Point;
		public var type:int = 0;
		public var textureSize:Point = new Point(128,128);

	/**
	 * todo - refactor to floor3d and item3d
	 * @param skinId
	 * @param animId
	 * @param dirId
	 * @param bitmap
	 * @param offset
	 */

		public function DisplayPPV(skinId:String, animId:String, dirId:String, bitmap:BitmapData,offset:Point)
		{
			this.skinId = skinId;
			this.animId = animId;
			this.dirId = dirId;
			this._bitmap = bitmap;
			if(offset)
				this.offset = offset.clone();
			else
				this.offset = new Point();

//			if((displayObject as Object).core)
//				(displayObject as Object).core.visible = false;
		}

	public function get bitmap():BitmapData {
		return _bitmap;
	}

	public function set bitmap(value:BitmapData):void {
		_bitmap = value;
	}

	public function get plane():Plane {
		return _plane;
	}

	public function set plane(plane:Plane):void {
		_plane = plane;
	}

	public function set material(material:BitmapMaterial):void {
		_material = material;
	}

	public function get material():BitmapMaterial {
		return _material;
	}
}
}
