package com.elparole.components
{
import flash.geom.Point;

import org.casalib.math.geom.Point3d;

public class Motion3d
	{
		public var velocity : Point3d = new Point3d();
		public var accelleration : Point = new Point();
		public var damping : Number = 0;
		
		public function Motion3d( velocityX : Number, velocityY : Number, velocityZ : Number, damping : Number=0 )
		{
			velocity = new Point3d( velocityX, velocityY, velocityZ );
			this.damping = damping;
		}
	}
}
