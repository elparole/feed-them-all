package com.elparole.components
{
import flash.display.Sprite;

public class Display2
	{
		public var displayObject : Sprite = null;

		public var offsetX:int = 0;
		public var offsetY:int = 0;

		public function Display2(displayObject:Sprite,offsetX:int = 0, offsetY:int = 0, startPlaying:Boolean = false)
		{
			this.displayObject = displayObject;
			this.displayObject.cacheAsBitmap = true;

			this.offsetX = offsetX;
			this.offsetY = offsetY;

//			if((displayObject as Object).core)
//				(displayObject as Object).core.visible = false;
		}
	}
}
