/**
 * User: Elparole
 * Date: 26.07.12
 * Time: 00:51
 */
package com.elparole.components
{
import org.casalib.math.geom.Point3d;

public class Position
{
	public var position:Point3d = new Point3d();

	public function Position(x:Number, y:Number, z:Number =0) {
		this.position.x = x;
		this.position.y = y;
		this.position.z = z;
	}

	public function clone():Position {
		return new Position(position.x, position.y, position.z);
	}
}
}
