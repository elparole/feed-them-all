/**
 * User: Elparole
 * Date: 05.09.12
 * Time: 10:21
 */
package com.elparole
{
import com.gskinner.performance.ptest;

import de.polygonal.ds.SLL;
import de.polygonal.ds.SLLNode;

import flash.display.Sprite;
import flash.events.Event;
import flash.text.TextField;

public class TestList extends Sprite
{
	private var sll:SLL;
	private var arr:Array;
	private const contSize:int = 100000;
	private var vec:Vector.<String>;

	public function TestList() {
		addEventListener(Event.ADDED_TO_STAGE, onAdded)
	}

	private function onAdded(event:Event):void {
		writeList();
		writeArr();
		writeVec();
		var tf:TextField = new TextField();
		addChild(tf);
		tf.appendText(String(ptest(readList,null,'read list', 20,1))+'\n');
		//tf.appendText(String(ptest(readVec,null,'read vec', 1,20))+'\n');
		tf.appendText(String(ptest(readArrFor,null,'read arr for', 20,1))+'\n');
		tf.appendText(String(ptest(readArrForEach,null,'read arr for each', 20,1))+'\n');
		//tf.appendText(String(ptest(writeArr,null,'write arr', 20,1))+'\n');
		//tf.appendText(String(ptest(writeVec,null,'write vec', 20,1))+'\n');
		tf.width = 640;
		tf.height = 520;
//		trace(PerformanceTest.run(this, "readSLL", 20));
//		trace(PerformanceTest.run(this, "readVec", 20));
//		trace(PerformanceTest.run(this, "readArrForEach", 20));
//		var time:int = getTimer();
//		testSLL();
//		trace('sll ' + (getTimer()-time));
//		time = getTimer();
//		testVec();
//		trace('vec ' + (getTimer()-time));
//		time = getTimer();
//		testArr();
//		trace('arr ' + (getTimer()-time));

	}

	private function testVec():void {
		for (var i:int = 0; i < 20; i++) {
			readVec();
		}
	}

	private function readVec():void {
		var anum:int = 0;
		for each (var string:String in vec) {
			anum += string.length;
		}
	}

	private function writeVec():void {
		vec = new Vector.<String>(contSize, true);
		for (var i:int = 0; i < contSize; i++) {
			vec[i] = ('node' + i);
		}
	}

	private function testArr():void {
		for (var i:int = 0; i < 20; i++) {
			readArrForEach();
		}
	}

	private function writeArr():void {
		arr = [];
		for (var i:int = 0; i < contSize; i++) {
			arr.push('node' + i);
		}
	}

	private function readArrFor():void {
		var anum:int = 0;
		for (var i:int = 0; i < arr.length; i++) {
			anum += arr[i].length;

		}
//		trace(anum);
	}

	private function readArrForEach():void {
		var anum:int = 0;
		for each (var string:String in arr) {
			anum += string.length;
		}
//		trace(anum);
	}

	private function testSLL():void {
		for (var i:int = 0; i < 20; i++) {
			readList();
		}
	}

	private function readList():void {
		var anum:int = 0;
		var node:SLLNode = sll.head;
		while (node) {
			//var value:String = node.val as String;
			anum += node.val.length;//value.length;
			node = node.next;
		}
//		trace(anum);
	}

	private function writeList():void {
		sll = new SLL();
		for (var i:int = 0; i < contSize; i++) {
			sll.append('node' + i);
		}
	}
}
}
