/**
 * User: Elparole
 * Date: 05.02.13
 * Time: 16:44
 */
package com.elparole.sme
{
import by.blooddy.crypto.serialization.JSON;

import org.casalib.events.LoadEvent;
import org.casalib.load.DataLoad;
import org.osflash.signals.Signal;

public class SLService
{
	private var loadSignal:Signal;
	private var dl:DataLoad;
	
	public function SLService() {
		
	}
	
	public function saveToJSON():void{
		
	}

	public function loadFromJSON():Signal {
		loadSignal = new Signal(Object);
		dl = new DataLoad('states.txt');
		dl.addEventListener(LoadEvent.COMPLETE, onLoaded);
		dl.start();
		
		return loadSignal;
	}

	private function onLoaded(event:LoadEvent):void {
		loadSignal.dispatch(JSON.decode(dl.dataAsString));
	}
}
}
