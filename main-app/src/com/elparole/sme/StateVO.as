/**
 * User: Elparole
 * Date: 04.02.13
 * Time: 16:45
 */
package com.elparole.sme
{
import com.bit101.components.Label;

import flash.display.Sprite;

public class StateVO
{
	public var skin:Sprite;
	public var label:Label;
	public var name:String;
	public var processesOut:Array = [];
	public var processesIn:Array = [];
	public var id:int;

	public static var id:int = 0;

	public function StateVO(skin:Sprite, label:Label, name:String) {
		this.skin = skin;
		this.label = label;
		this.name = name;
		this.id = StateVO.id;
		StateVO.id++;
	}


	public function toString():String {
		return name;
	}
}
}
