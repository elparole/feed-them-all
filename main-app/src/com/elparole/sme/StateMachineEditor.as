/**
 * User: Elparole
 * Date: 04.02.13
 * Time: 16:07
 */
package com.elparole.sme
{
import by.blooddy.crypto.serialization.JSON;

import com.bit101.components.ComboBox;
import com.bit101.components.Label;
import com.bit101.components.NumericStepper;
import com.bit101.components.PushButton;
import com.bit101.components.Style;
import com.bit101.components.TextArea;
import com.elparole.cartesianToPolarCoords;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.filters.GlowFilter;
import flash.geom.Point;
import flash.utils.Dictionary;

import utils.geom.polarToCartesianCoordinates;

[SWF(width='1024', height='768', frameRate='30', backgroundColor='#dddddd')]
public class StateMachineEditor extends Sprite
{
	private var cont:Sprite;
	private var loadFromClassBt:PushButton;
	private var saveToAS3Bt:PushButton;
	private var addStateBt:PushButton;
	private var selectedState:StateVO;
	private var bf:GlowFilter = new GlowFilter(0xffffff);
	private var addProcessBt:PushButton;
	private var startStateCB:ComboBox;
	private var endStateCB:ComboBox;
	private var processNameTF:TextArea;
	private var removeStateBt:PushButton;

	private var states:Array = [];
	private var processes:Array = [];
	private var curvNS:NumericStepper;
	private var selectedProcess:ProcessVO;
	private var removeProcessBt:PushButton;
	private var stateNameTF:TextArea;
	private var changeStateNameBt:PushButton;
	private var renameProcessBt:PushButton;
	private var service:SLService = new SLService();
	private var outputTF:TextArea;
	private var showCodeBt:PushButton;

	public function StateMachineEditor() {
		addEventListener(Event.ADDED_TO_STAGE, onInit);
	}

	private function onInit(event:Event):void {
		cont = new Sprite();
		cont.y = 100;
		addChild(cont);

		cont.graphics.lineStyle(1);
		cont.graphics.drawRect(0,0,640,520);
		cont.graphics.lineStyle(0);

		loadFromClassBt = new PushButton(this, 650,10,'load', onLoad);
		saveToAS3Bt = new PushButton(this, 770,10,'save', onSaveToAS3);
		addStateBt = new PushButton(this, 650,40,'add state', onAddState);
		removeStateBt = new PushButton(this, 770,40,'remove state', onRemoveState);
		stateNameTF = new TextArea(this,890,40,'state name');
		stateNameTF.height = 25;
		stateNameTF.width = 100;
		changeStateNameBt = new PushButton(this, 990,40,'rename', onRenameState);
		changeStateNameBt.width = 70;

		startStateCB = new ComboBox(this,650,70,'',[]);
		endStateCB = new ComboBox(this,770,70,'',[]);
		processNameTF = new TextArea(this,890,70,'process name');
		processNameTF.height = 25;
		processNameTF.width = 100;
		renameProcessBt = new PushButton(this, 990,70,'rename', onRenameProcess);
		renameProcessBt.width = 70;
		addProcessBt = new PushButton(this, 650,100,'add process', onAddProcess);
		removeProcessBt = new PushButton(this, 770,100,'remove process', onRemoveProcess);
		Style.LABEL_TEXT = 0;
//		Style.fontSize = 14;

		curvNS = new NumericStepper(this, 650,130, onChangeCurve);

		showCodeBt = new PushButton(this,650,160,'render code', onRenderCode);
		outputTF = new TextArea(this,650,190);
		outputTF.width = 300;
		outputTF.height = 300;
	}

	private function onRenderCode(...args):void {
		var code:String = '';
		var namesUsed:Dictionary = new Dictionary();
		for each (var processVO:ProcessVO in processes) {
			namesUsed[processVO.name] = processVO.name;
			namesUsed[processVO.startState.name] = processVO.startState.name;
			namesUsed[processVO.endState.name] = processVO.endState.name;
			code+="fsmConfig.configureProcess( "+processVO.name+", "+processVO.startState.name+", "+processVO.endState.name+");\n";
		}

		for each (var string:String in namesUsed) {
			code+='publis static const '+string.toUpperCase()+' = '+string+';\n';
		}
		outputTF.text = code;
	}

	private function onRenameState(...args):void {
		if(!selectedState)
			return;
		
		selectedState.name = stateNameTF.text;
		selectedState.label.text =selectedState.name; 
	}
	
	private function onRenameProcess(...args):void {
		if(!selectedProcess)
			return;
		
		selectedProcess.name = processNameTF.text;
		selectedProcess.label.text =processNameTF.text;
	}

	private function onChangeCurve(...args):void {
		if(!selectedProcess)
			return;

		selectedProcess.anchorDist = curvNS.value;
		redraw();

	}

	private function onRemoveProcess(...args):void {
		if(!selectedProcess)
			return;

		selectedProcess.skin.removeEventListener(MouseEvent.CLICK, onSelectProcess);
		cont.removeChild(selectedProcess.skin);
		selectedProcess.startState.processesOut.splice(selectedProcess.startState.processesOut.indexOf(selectedProcess),1);
		selectedProcess.endState.processesIn.splice(selectedProcess.endState.processesIn.indexOf(selectedProcess),1);

		selectedProcess = null;
	}
	
	private function onAddProcess(...args):void {
		if(!startStateCB.selectedItem ||! endStateCB.selectedItem){
			return;
		}else{
			var pvo:ProcessVO = addProcess(StateVO(startStateCB.selectedItem),StateVO(endStateCB.selectedItem));
			selectProcessF(pvo.skin);
		}
		redraw();
	}

	private function addProcess(startState:StateVO,endState:StateVO,name:String = null):ProcessVO {
		var pvo:ProcessVO = new ProcessVO(startState,
				endState, name||processNameTF.text);
		pvo.skin = new Sprite();
		pvo.label = new Label(pvo.skin, 0, 0, pvo.name);
		pvo.skin.addEventListener(MouseEvent.CLICK, onSelectProcess);

		cont.addChild(pvo.skin);

		processes.push(pvo);
		startState.processesOut.push(pvo);
		endState.processesIn.push(pvo);
		return pvo;
	}

	private function onSelectProcess(event:MouseEvent):void {

		selectProcessF(event.target as Sprite);
	}

	private function selectProcessF(skin:Sprite):void {
		if(selectedProcess){
			selectedProcess.skin.filters = [];
		}

		for each (var processVO:ProcessVO in processes) {
			if ((processVO.skin == skin) || (processVO.skin == skin.parent))
				selectedProcess = processVO;
		}

		curvNS.value = selectedProcess.anchorDist;

		selectedProcess.skin.filters = [bf];
		
		processNameTF.text = selectedProcess.name;
	}

	/**
	 *
	 * @param args
	 */

	private function onRemoveState(...args):void {
		if(selectedState){
			selectedState.skin.filters = [];
		}else{
			return;
		}
		
		states.splice(states.indexOf(selectedState,1));
		var processSkinsToRemove:Array = [];
		for each (var processVO:ProcessVO in selectedState.processesIn) {
			processSkinsToRemove.push(processes.splice(processes.indexOf(processVO),1)[0]);
			processVO.startState.processesOut.splice(processVO.startState.processesOut.indexOf(processVO),1);
		}

		for each (processVO in selectedState.processesOut) {

			processSkinsToRemove.push(processes.splice(processes.indexOf(processVO),1)[0]);
			processVO.endState.processesIn.splice(processVO.endState.processesIn.indexOf(processVO),1);
		}

		for each (var processVO1:ProcessVO in processSkinsToRemove) {

			processVO1.skin.parent && cont.removeChild(processVO1.skin);
		}
		
		cont.removeChild(selectedState.skin);
		selectedState = null;
		redraw();
	}


	private function redraw():void {
//		cont.graphics.clear();
		for each (var processVO:ProcessVO in processes) {
			processVO.skin.graphics.clear();
			processVO.skin.graphics.lineStyle(5);
			var x1:Number = processVO.startState.skin.x;
			var y1:Number = processVO.startState.skin.y;
			var x2:Number = processVO.endState.skin.x;
			var y2:Number = processVO.endState.skin.y;
			
			var polars:Array = cartesianToPolarCoords(x1-x2,y1-y2);
			polars[0]-=28;
			var sx1:Point = polarToCartesianCoordinates(polars[0],polars[1]*180/Math.PI);
			
			x1 = sx1.x+x2;
			y1 = sx1.y+y2;

			polars = cartesianToPolarCoords(x2-x1,y2-y1);
			polars[0]-=28;
			sx1 = polarToCartesianCoordinates(polars[0],polars[1]*180/Math.PI);

			x2 = sx1.x+x1;
			y2 = sx1.y+y1;

			polars[1] +=Math.PI/2;
			
			var apt:Point =  polarToCartesianCoordinates(processVO.anchorDist*10,polars[1]*180/Math.PI);
			apt.offset((x1+x2)/2,(y1+y2)/2);

			processVO.skin.graphics.moveTo(x1,y1);
			processVO.skin.graphics.curveTo(apt.x, apt.y, x2, y2);
			processVO.label.x = apt.x;
			processVO.label.y = apt.y;
//			processVO.skin.graphics.lineTo(x2,y2);
			
			
			drawArrow(processVO.skin,apt.x,apt.y, x2, y2);
		}
//		processVO.skin.graphics.lineStyle(0);
	}

	private function drawArrow(cont:Sprite,x1:Number, y1:Number, x2:Number, y2:Number):void {

//		var dist:Number = Math.sqrt(GameUtils.distanceSquaredStat(x2,y2, x1,y1));


		var polars:Array = cartesianToPolarCoords(x1-x2,y1-y2);
		polars[0]-=25;
		var sx1:Point = polarToCartesianCoordinates(polars[0],polars[1]*180/Math.PI);
		x1 = sx1.x+x2;
		y1 = sx1.y+y2;
		var sx2:Point = polarToCartesianCoordinates(polars[0],polars[1]*180/Math.PI);
//		polars[1]+=/*6*/anchorAngMod-Math.PI/2;
		polars[1]-=Math.PI/8;
		var x3:int = polarToCartesianCoordinates(20,polars[1]*180/Math.PI).x;
		var y3:int = polarToCartesianCoordinates(20,polars[1]*180/Math.PI).y;
		cont.graphics.lineStyle(5);
		cont.graphics.moveTo(x2, y2);
		cont.graphics.lineTo(x2+x3, y2+y3);

//		polars[1]-=Math.PI*2-Math.PI/2;
		polars[1]+=Math.PI/4;
		x3 = polarToCartesianCoordinates(20,polars[1]*180/Math.PI).x;
		y3 = polarToCartesianCoordinates(20,polars[1]*180/Math.PI).y;

		cont.graphics.moveTo(x2, y2);
		cont.graphics.lineTo(x2+x3, y2+y3);
	}

	private function onAddState(...args):void {
		addState();
	}

	private function addState(aname:String = null):StateVO {
		var stateView:Sprite = new Sprite();
//		var name:String = 'state '+String(states.length+1);
		var name:String = aname || stateNameTF.text;
		var label:Label = new Label(stateView, -20, -10, name);
		var svo:StateVO = new StateVO(stateView, label, name); 
		states.push(svo);
		cont.addChild(stateView);
		stateView.x = 25;
		stateView.y = 25;
		stateView.graphics.beginFill(0x888888, 1);
		stateView.graphics.drawCircle(0, 0, 25);
		stateView.graphics.endFill();
		stateView.addEventListener(MouseEvent.MOUSE_DOWN, onSelectState);

		startStateCB.items = states;
		endStateCB.items = states;
		
		return svo;
	}

	private function onSelectState(event:MouseEvent):void {
		if(selectedState){
			selectedState.skin.filters = [];
		}

		var skin:Sprite = event.target as Sprite;

		for each (var stateVO:StateVO in states) {
			if(stateVO.skin == skin){
				selectedState = stateVO;
				selectedState.skin.filters = [bf];
			}
		}
		stage.addEventListener(MouseEvent.MOUSE_UP, onStopDragState);
		selectedState.skin.startDrag();

		stateNameTF.text = selectedState.name;
	}

	private function onStopDragState(event:MouseEvent):void {
		selectedState.skin.stopDrag();
		stage.removeEventListener(MouseEvent.MOUSE_UP, onStopDragState);
		redraw();
	}

	private function onSaveToAS3(...args):void {
		var obj:Object = {};
		obj.states = [];
		obj.processes = [];

		for each (var processVO:ProcessVO in processes) {
			obj.processes.push({name:processVO.name, 
								id:processVO.id, 
								anchorDist:processVO.anchorDist,
								startStateId:processVO.startState.id,
								endStateId:processVO.endState.id
								})
		}

		for each (var stateVO:StateVO in states) {
			var sobj:Object = {
				name:stateVO.name,
				id:stateVO.id,
				x:stateVO.skin.x,
				y:stateVO.skin.y,
				processesIn:[],
				processesOut:[]
			}
					
			for each (processVO in stateVO.processesIn) {
				sobj.processesIn.push(processVO.id);
			}
			for each (processVO in stateVO.processesOut) {
				sobj.processesOut.push(processVO.id);
			}
			obj.states.push(sobj);
		}
		trace(JSON.encode(obj));		
	}
	
	private function onLoad(...args):void{
		service.loadFromJSON().add(parse);
	}
	
	private function parse(obj:Object):void{

		var statesToRemove:Array = states.concat();
		var processToRemove:Array = processes.concat();
		for each (var stateVO:StateVO in statesToRemove) {
			selectedState = stateVO;
			onRemoveState();
		}


		var stateIdsToSVOs:Dictionary = new Dictionary();
		
		for each (var state:Object in obj.states) {
			var svo:StateVO = addState(state.name);
			svo.skin.x = state.x;
			svo.skin.y = state.y;
			svo.id = state.id;
			stateIdsToSVOs[state.id] = svo;
		}
		
		for each (var pobj:Object in obj.processes) {
			var pvo:ProcessVO = addProcess(stateIdsToSVOs[pobj.startStateId],stateIdsToSVOs[pobj.endStateId],pobj.name);
//			pvo.name = pobj.name;
			pvo.id = pobj.id;
			pvo.anchorDist = pobj.anchorDist;
//			pvo.startState = stateIdsToSVOs[pobj.startStateId];
//			pvo.endState = stateIdsToSVOs[pobj.endStateId];
//			stateIdsToSVOs[pobj.startStateId].processesOut.push(pvo);
//			stateIdsToSVOs[pobj.endStateId].processesIn.push(pvo);
		}

		redraw();
	}
}
}
