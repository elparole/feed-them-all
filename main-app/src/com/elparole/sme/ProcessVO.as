/**
 * User: Elparole
 * Date: 04.02.13
 * Time: 16:45
 */
package com.elparole.sme
{
import com.bit101.components.Label;

import flash.display.Sprite;

public class ProcessVO
{

	public var startState:StateVO;
	public var endState:StateVO;
	public var name:String;
	public var skin:Sprite;
	public var label:Label;
	public var anchorDist:Number = 0;
	public var id:int = 0;

	public static var id:int = 0;

	public function ProcessVO(startState:StateVO = null, endState:StateVO = null, name:String = '') {
		this.startState = startState;
		this.endState = endState;
		this.name = name;
		this.id = ProcessVO.id;
		ProcessVO.id++;
	}
}
}
