/**
 * User: Elparole
 * Date: 22.03.12
 * Time: 14:29
 */
package com.elparole.utils {
import com.greensock.TweenLite;
import com.greensock.plugins.FrameLabelPlugin;
import com.greensock.plugins.TweenPlugin;

import flash.display.MovieClip;
import flash.events.MouseEvent;

public class RollClickFeatures {

    private var currentBts:Array = [];
    
    public static var initiated:Boolean = false;

    public function RollClickFeatures() {
        !initiated && TweenPlugin.activate([FrameLabelPlugin]);
        initiated = true;
    }

    public function removeButton(bt:MovieClip):void {
        if(currentBts.indexOf(bt)>=0){
            bt.removeEventListener(MouseEvent.ROLL_OVER, onMouseOver)
            bt.removeEventListener(MouseEvent.CLICK, onMouseClick);
            bt.stop();
            currentBts.splice(currentBts.indexOf(bt),1);
        }
    }

    public function addButton(bt:MovieClip):void {
        bt.mouseChildren = false;
        bt.buttonMode = bt.useHandCursor = true;
        if(!currentBts.indexOf(bt)>=0){
            bt.addEventListener(MouseEvent.ROLL_OVER, onMouseOver)
            bt.addEventListener(MouseEvent.CLICK, onMouseClick);
            bt.stop();
            currentBts.push(bt);
        }
    }

    private function onMouseClick(event:MouseEvent):void {
        var bt:MovieClip = event.currentTarget as MovieClip;
//        tween = new GTween(event.currentTarget,0.5,)
        bt.gotoAndStop('click');
        TweenLite.to(bt, 0.5, {frameLabel:"clickEnd"});
    }

    private function onMouseOver(event:MouseEvent):void {

        var bt:MovieClip = event.currentTarget as MovieClip;
//        tween = new GTween(event.currentTarget,0.5,)
        bt.gotoAndStop('roll');
        TweenLite.to(bt, 1, {frameLabel:"click"});
    }

    public function addButtons(arr:Array):void {
        for each (var movieClip:MovieClip in arr) {
            addButton(movieClip);
        }
    }

    public function removeAllButtons():void {
        for each (var movieClip:MovieClip in currentBts.concat()) {
            removeButton(movieClip);
        }
    }
}
}
