/**
 * User: Elparole
 * Date: 22.03.12
 * Time: 14:29
 */
package com.elparole.utils {
import com.elparole.feedThemAll.view.SoundManager;

import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.utils.Dictionary;

public class RollScaleFeatures {

    private var currentBts:Array = [];
    
    public static var initiated:Boolean = false;
	private var scales:Dictionary = new Dictionary();
	public var endScale:Number = 1.05;
//	private var currentTL:TweenLite;

    public function RollScaleFeatures() {
//        !initiated && TweenPlugin.activate([FrameLabelPlugin]);
        initiated = true;
    }

    public function removeButton(bt:Sprite):void {
        if(currentBts.indexOf(bt)>=0){
//	        currentTL && currentTL.pause();
	        bt.scaleX = scales[bt].x;
	        bt.scaleY = scales[bt].y;

            bt.removeEventListener(MouseEvent.ROLL_OVER, onMouseOver);
	        bt.removeEventListener(MouseEvent.ROLL_OUT, onMouseOut);
	        bt.removeEventListener(MouseEvent.CLICK, onMouseClick);
            bt.scaleX = scales[bt].x;
            bt.scaleY = scales[bt].y;
	        delete scales[bt];
            currentBts.splice(currentBts.indexOf(bt),1);
        }
    }

    public function addButton(bt:Sprite):void {
        bt.mouseChildren = false;
        bt.buttonMode = bt.useHandCursor = true;
        if(!currentBts.indexOf(bt)>=0){
            bt.addEventListener(MouseEvent.ROLL_OVER, onMouseOver);
            bt.addEventListener(MouseEvent.ROLL_OUT, onMouseOut);
            bt.addEventListener(MouseEvent.CLICK, onMouseClick);
	        scales[bt] = new Point(bt.scaleX,bt.scaleY);
            currentBts.push(bt);
        }
    }

    private function onMouseClick(event:MouseEvent):void {
	    SoundManager.instance.play(Click_04Snd);
//	    currentTL && currentTL.pause();
//	    var bt:Sprite = event.currentTarget as Sprite;
//	    bt.scaleX = scales[bt].x;
//	    bt.scaleY = scales[bt].y;
    }
	
    private function onMouseOut(event:MouseEvent):void {

	    var bt:Sprite = event.currentTarget as Sprite;
	    bt.scaleX = scales[bt].x;
	    bt.scaleY = scales[bt].y;
//	    currentTL = TweenLite.to(bt, 0.3, {scaleX:scales[bt].x, scaleY:scales[bt].y});
    }
	
    private function onMouseOver(event:MouseEvent):void {

        var bt:Sprite = event.currentTarget as Sprite;
//        currentTL = TweenLite.to(bt, 0.3, {scaleX:scales[bt].x*endScale, scaleY:scales[bt].y*endScale});
	    bt.scaleX = scales[bt].x*endScale;
	    bt.scaleY = scales[bt].y*endScale;
    }

    public function addButtons(arr:Array):void {
        for each (var movieClip:Sprite in arr) {
            addButton(movieClip);
        }
    }

    public function removeAllButtons():void {
        for each (var movieClip:Sprite in currentBts.concat()) {
            removeButton(movieClip);
        }
    }
}
}
