package {

import flash.display.BitmapData;
import flash.display.Sprite;
import flash.display.StageQuality;
import flash.events.*;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.text.TextField;
import flash.text.TextFormat;

import net.hires.Stats;

import org.papervision3d.Papervision3D;
import org.papervision3d.cameras.*;
import org.papervision3d.core.math.*;
import org.papervision3d.core.proto.*;
import org.papervision3d.materials.*;
import org.papervision3d.materials.utils.*;
import org.papervision3d.objects.*;
import org.papervision3d.objects.primitives.*;
import org.papervision3d.render.*;
import org.papervision3d.scenes.*;
import org.papervision3d.view.*;

/**
 * @author timknip
 */
public class TutoFrustumCamera3D extends Sprite {

	/** The PV3D scene to render */
	public var scene : Scene3D;

	/** This PV3D camera */
	public var camera : Camera3D;

	/** The PV3D renderer */
	public var renderer : BasicRenderEngine;

	/** The PV3D viewport */
	public var viewport : Viewport3D;

	/** Show info */
	public var status : TextField;

	/**
	 * Constructor.
	 */
	public function TutoFrustumCamera3D() : void {
		addEventListener(Event.ADDED_TO_STAGE, init);
	}

	/**
	 * Init.
	 */
	private function init(...args) : void {
		stage.quality = StageQuality.LOW;

		stage.addChild(new Stats());
		// add a textfield
		initStatusLine();

		// used by orbiting / zooming
		_lastMouse = new Point();

		// create a viewport
		viewport = new Viewport3D(800, 600);

		// add
		addChild(viewport);

		// create a frustum camera with FOV=60, near=10, far=10000
		camera = new Camera3D(60, 10, 10000);
		camera.ortho = true;
		camera.orbit(_camPitch, _camYaw, _camDist,_camTarget);

		// create a renderer
		renderer = new BasicRenderEngine();

		// create the scene
		scene = new Scene3D();

		// init the scene
		initScene();

		// mouse listeners
//		stage.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
//		stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
//		stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);

		// key listener
		stage.addEventListener(KeyboardEvent.KEY_UP, keyUpHandler);

		// render on each frame
		addEventListener(Event.ENTER_FRAME, render);

		showInfo();
	}

	private var cubeNum:int = 60;
	private var cubes:Array = [];
	private var speeds:Array = [];
	private var ISOW:Number = 1000;
	private var ISOZ:Number = 1000;

	private var bm:BitmapMaterial;
	private var ta:TexturesAssets = new TexturesAssets();
	private var fnum:int = 0;
	private var materials:Array = [];
	private var offsets:Array = [];

	private var floorPlane:Plane;

	/**
	 * Init the scene.
	 */
	private function initScene() : void {

//		var materials : MaterialsList = new MaterialsList();
//
//
//		materials.addMaterial(new ColorMaterial(0xff0000), "front");
//		materials.addMaterial(new ColorMaterial(0x0000ff), "back");
//		materials.addMaterial(new ColorMaterial(0xffff00), "left");
//		materials.addMaterial(new ColorMaterial(0x00ffff), "right");
//		materials.addMaterial(new ColorMaterial(0x00ff00), "top");
//		materials.addMaterial(new ColorMaterial(0xff00ff), "bottom");
//		this.graphics.beginBitmapFill(ta.bm1.bitmapData, new Matrix(1,0,0,1,200,200));
//		this.graphics.drawRect(400,400,100,100);
//		this.graphics.endFill();

		this.graphics.beginFill(0x999999);
		this.graphics.drawRect(0,0,640,480);
		this.graphics.endFill();

		floorPlane = new Plane(new ColorMaterial(0x777777), 1000, 1000, 0, 0);

//		scene.addChild(floorPlane);

//		floorPlane.yaw(90);
		floorPlane.pitch(90);

		floorPlane.position = new Number3D(200,-200,200);

		var mbd:BitmapData;// = new BitmapData(100,100);

		for (var i:int = 0; i < cubeNum; i++) {

//			var cube : Cube = new Cube(materials,20,20,20);
//			scene.addChild(cube);
//			cube.position= new Number3D(Math.floor(i/Math.sqrt(cubeNum))*20-100,0,Math.floor(i%Math.sqrt(cubeNum))*20-100);
//			cubes.push(cube);
//			new Plane3D()
//			var plane:Plane = new Plane(new ColorMaterial(0x555555 + Math.random()*0x999999),40,40,1,1);
			mbd = (new TexturesAssets.mc1()).bitmapData;
			bm = new BitmapMaterial(mbd, false);
			materials.push(bm);
			offsets.push(Math.floor(Math.random()*5.999999));
			var plane:Plane = new Plane(
//					new BitmapMaterial(mbd,false),
					bm,
					100,100,0,0);
			plane.yaw(45);
			plane.pitch(-60+90);
			scene.addChild(plane);
//			plane.position= new Number3D(Math.floor(i/Math.sqrt(cubeNum))*25-100,0,Math.floor(i%Math.sqrt(cubeNum))*25-100);
			plane.position= new Number3D(Math.random()*ISOW-ISOW/2,0,Math.random()*ISOZ-ISOZ/2);
			cubes.push(plane);
			var step:Number = 1.5;
			speeds[i] = new Point(-step, 0);
//			speeds[i] = new Point(Math.random()*4+2,Math.random()*4+2);
//			speeds[i].x = Math.random()>0.5? speeds[i].x:-speeds[i].x;
//			speeds[i].y = Math.random()>0.5? speeds[i].y:-speeds[i].y;

		}

	}

	/**
	 * Add a textfield.
	 */
	private function initStatusLine() : void {
		status = new TextField();
		addChild(status);
		status.x = status.y = 5;
		status.width = 500;
		status.height = 200;
		status.multiline = true;
		status.selectable = false;
		status.defaultTextFormat = new TextFormat("Arial", 12, 0xaa0000);
		status.text = "";
	}

	/**
	 * Render!
	 */
	private function render( event : Event = null ) : void {

//		floorPlane.yaw((fnum/100)%360)


		fnum++;// = (fnum+1)%5;
//		fnum = (fnum+1)%5;

		// orbit the camera
//		camera.orbit(_camPitch, _camYaw, _camDist,_camTarget);
		moveSprites();

		// render
		renderer.renderScene(scene, camera, viewport);
	}

	private function moveSprites():void {
		var n3d:Number3D;
		for (var i:int = 0; i < cubeNum; i++) {

//			materials[i].bitmap.fillRect(bm.bitmap.rect, 0);
			materials[i].bitmap = ta['bm' + ((Math.floor(fnum/2) + offsets[i]) % 5 + 1)].bitmapData;

//			if(i%7==0){
			//			(cubes[i] as Plane).moveLeft(Math.random()*10-5);
			//			(cubes[i] as Plane).moveForward(Math.random()*10-5);
			n3d = new Number3D(
					((cubes[i] as Plane).position.x + speeds[i].x + ISOW/2) % ISOW,
					0,
					((cubes[i] as Plane).position.z + speeds[i].y + ISOZ/2) % ISOZ);
			if (n3d.x < 0)
				n3d.x = ISOW - n3d.x;
			if (n3d.z < 0)
				n3d.z = ISOZ - n3d.z;

			n3d.x -= ISOW/2;
			n3d.z -= ISOZ/2;

			(cubes[i] as Plane).position = n3d;
//			trace(i,n3d);
//			}
//			(cubes[i] as Plane).lookAt(camera);
//					.position.x +=Math.random()*6-3;
//			cubes[i].position.z +=Math.random()*6-3;
//			cubes[i].position = cubes[i].position.clone();
		}
	}

	/**
	 * Show some info;
	 */
	private function showInfo() : void {
		return;
		var msg : String = "usage:\ndrag to rotate, drag-shift to zoom\n";
		msg += "- o: toggle ortho / perspective mode\n";
		msg += "- v: increase / decrease(+shift) FOV\n";
		msg += "- f: increase / decrease(+shift) camera far-plane\n";
		msg += "- n: increase / decrease(+shift) camera near-plane\n\n";

		msg += "camera\n- projection mode: " + (camera.ortho?"ortho":"perspective") + "\n";
		msg += "- fov: " + camera.fov + " near: " + camera.near + " far: " + camera.far + "\n";

		status.text = msg;
	}

	/**
	 *
	 * @param	event
	 */
	private function keyUpHandler( event : KeyboardEvent ) : void {

		switch(event.keyCode) {
//			case 70: // f
//				if(!camera.ortho) {
//					if(event.shiftKey)
//						camera.far -= 10;
//					else
//						camera.far += 10;
//				}
//				break;
//
//			case 78: // n
//				if(!camera.ortho) {
//					if(event.shiftKey)
//						camera.near -= 10;
//					else
//						camera.near += 10;
//				}
//				break;

			case 79: // o
				camera.orthoScale -= 0.005;
				break;
			case 86: // v
				camera.orthoScale += 0.005;
				break;

//			case 86: // v
//				if(!camera.ortho) {
//					if(event.shiftKey)
//						camera.fov -= 5;
//					else
//						camera.fov += 5;
//				}
//				break;

			default:
				break;
		}

		showInfo();
	}

	/**
	 *
	 * @param	event
	 */
	private function mouseDownHandler( event : MouseEvent ) : void {
		_lastMouse.x = event.stageX;
		_lastMouse.y = event.stageY;
		if(event.shiftKey)
			_zooming = true;
		else
			_orbiting = true;
	}

	/**
	 *
	 * @param	event
	 */
	private function mouseMoveHandler( event : MouseEvent ) : void {
		var dx:Number = _lastMouse.x - event.stageX;
		var dy:Number = _lastMouse.y - event.stageY;

		if(_orbiting)	{
			_camYaw += dx * (Math.PI/180);

			_camPitch -= dy * (Math.PI/180);
			_camPitch = _camPitch ==  Math.PI/2 ? Math.PI/2 : _camPitch;

			_lastMouse.x = event.stageX;
			_lastMouse.y = event.stageY;
		} else if(!camera.ortho && _zooming) {
			if(_camDist - dy > 10)
				_camDist -= dy;
		} else if(camera.ortho && _zooming) {
			// this is the essential bit to zoom the camera in ortho-mode!
			camera.orthoScale += (dy * 0.0005);
		}

		render();

		event.updateAfterEvent();
	}

	/**
	 *
	 * @param	event
	 */
	private function mouseUpHandler( event : MouseEvent ) : void {
		_orbiting = _zooming = false;
	}

	/** Are we orbiting the camera? */
	private var _orbiting			: Boolean = false;

	/** Are we zooming the camera? */
	private var _zooming			: Boolean = false;

	/** A Point to keep track of mouse coords */
	private var _lastMouse			: Point;

	/** Camera Yaw */
	private var _camYaw				: Number = 45;//-Math.PI/2;

	/** Camera Pitch */
	private var _camPitch			: Number = -60;//Math.PI/2;

	/** Camera distance */
	private var _camDist			: Number = 100;//2000;

	/** Camera target */
	private var _camTarget			: DisplayObject3D = DisplayObject3D.ZERO;
}
}