package com.blackmoondev.iso.isoModel.math {
import com.blackmoondev.geom.IntPoint;

import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.Dictionary;

public class IsoMathUtils {
	public function IsoMathUtils() {
		throw new Error("Do not instantiate this class. Use Static Methods.");
	}

	public static function mapStageToGrid(cellSize:int, _mouseX:int, _mouseY:int, throwOnLowerThan0:Boolean = true):IntPoint {
		var _a1:Number = 0.5;
		var _a2:Number = -0.5;

		var b1_new:int = _mouseY - (_a1 * _mouseX);
		var b2_new:int = _mouseY - (_a2 * _mouseX);

		if (b1_new < 0 || b2_new < 0) {
			if (throwOnLowerThan0) {
				throw new Error("Provided position invalid. You are outside ISO MAP")
			}
			else {
				//b1_new = Math.max(b1_new,0);
				//b2_new = Math.max(b2_new,0);
			}
		}

		var invCellSize:Number = 1 / cellSize;//0.03125;

		var isoY:int = Math.floor(b1_new * invCellSize);
		var isoX:int = Math.floor(b2_new * invCellSize);

		return new IntPoint(isoX, isoY);
	}

	/*public static function mapGridRectToStage(cellSize:int, rect:Rectangle):Rectangle{
	 return new Rectangle((rect.x - rect.y) * cellSize,
	 (rect.x + rect.y) * (cellSize>>1),
	 rect.width * (cellSize<<1),
	 rect.height * cellSize);
	 }*/

	public static function mapGridCoordToStage(cellSize:int, point:IntPoint):Point {
		return new Point((point.x - point.y) * cellSize, (point.x + point.y) * (cellSize >> 1));
	}

	public static function getIsoRectToStageSize(cellSize:int, isoRect:IntPoint):Point {
		return new Point((isoRect.x + isoRect.y) * cellSize, (isoRect.x + isoRect.y) * (cellSize >> 1))
	}

	/**
	 *
	 * @param    point items iso dimention
	 * @return rotation axis coordinates
	 */
	public static function getItemRotationAxisCoord(cellSize:int, point:IntPoint):Point {
		var negativeRotationPoint:IntPoint = new IntPoint(
				-(point.y % 2 == 0 ? point.y + 1 : point.y),
				-(point.x % 2 == 0 ? point.x + 1 : point.x));
		return new Point(
				IsoMathUtils.mapGridCoordToStage(cellSize, new IntPoint(0, 0)).x - (IsoMathUtils.mapGridCoordToStage(cellSize, negativeRotationPoint).x >> 1),
				IsoMathUtils.mapGridCoordToStage(cellSize, new IntPoint(0, 0)).y - (IsoMathUtils.mapGridCoordToStage(cellSize, negativeRotationPoint).y >> 1));

	}

	public static function generateHexObstacleMapFromDict(dict:Dictionary):String {
		var arr:Array = [];
		for (var key:String in dict) {
			arr[key.split('_')[0]] ||= [];
			arr[key.split('_')[0]][key.split('_')[1]] = dict[key];
		}
		return generateHexObstacleMap(arr);
	}

	public static function getMapSizePixelFromGrid(cellSize:int, width:int, height:int):IntPoint {
		return new IntPoint((width + height) * cellSize, (height * cellSize + width * cellSize) >> 1);
	}

	public static function getGridBounds(cellSize:int, width:int, height:int):Rectangle {
		var gridBounds:Rectangle = new Rectangle();
		gridBounds.left = -height * cellSize;
		gridBounds.top = 0;
		gridBounds.width = (width + height) * cellSize;
		gridBounds.height = (height * cellSize + width * cellSize) >> 1;
		return gridBounds;
	}

	public static function isItemOutOfMap(itemX:int, itemY:int, itemW:int, itemH:int, mapSize:IntPoint):Boolean {
		return (itemX + itemW - 1 >= mapSize.x || itemY + itemH - 1 >= mapSize.y || itemX < 0 || itemY < 0);
	}

	public static function isItemInRect(itemX:int, itemY:int, itemW:int, itemH:int, rect:Rectangle):Boolean {
		var startX:int = rect.x;
		var startY:int = rect.y;
		var endX:int = rect.x + rect.width - 1;
		var endY:int = rect.y + rect.height - 1;
		return !(itemX > endX || itemY > endY || itemX + itemW - 1 < startX || itemY + itemH - 1 < startY);
	}

	private static const STR32:String = '0123456789abcdefghijklmnopqrstuv';


	/**
	 * Converts String of number coded in numeral system 32 to Array of Booleans in given length
	 * @param    str
	 * @param    length
	 * @return
	 */

	public static function string32ToBooleanArray(str:String, length:int):Array {
		var numArr:Array = [];
		var allStr:String = '';
		for (var i:int = 0; i < str.length; i++) {
			var cs:String = parseInt(str.charAt(i), 32).toString(2);
			var l:uint = Math.min(5, length - i * 5);
			while (cs.length < l)
				cs = '0' + cs;
			allStr += cs;
		}
		for (i = 0; i < length; i++)
			numArr[i] = allStr.charAt(i) == '1'
		return numArr;
	}

	/**
	 * Converts Array of Booleans to String coded in numeral system 32
	 *
	 * @param    arr - array of Booleans
	 * @return String - converted array
	 */
	public static function booleanArrayToHexString(arr:Array):String {
		var str:String = '';
		for (var i:int = 0; i < arr.length; i += 5) {
			var num:uint = 0;
			var l:uint = Math.min(4, arr.length - 1 - i);
			for (var j:int = 0; j <= l; j++)
				num |= arr[i + j] << l - j;
			str += STR32.charAt(num)
		}
		return str;
	}

	public static function generateHexObstacleMap(arr:Array):String {
		var arrOfRows:Vector.<String> = new Vector.<String>(arr.length, true);
		for (var j:int = 0; j < arr.length; j++) {
			arrOfRows[j] = booleanArrayToHexString(arr[j]);
		}
		return arrOfRows.join('-');
	}



	public static function generateObstacleMapFromHexToDict(str:String, width:int, height:int):Dictionary {
		var obstacleMap:Array = generateObstacleMapFromHex(str,width,height);
		var dict:Dictionary = new Dictionary();
		for (var ix:int = 0; ix < width; ix++) {
			for (var iy:int = 0; iy < height; iy++) {
				dict[ix+'_'+iy] = obstacleMap[ix][iy];
			}
		}
		return dict;
	}

	public static function generateObstacleMapFromHex(str:String, width:int, height:int):Array {
		var obstacleMap:Array = new Array(width);
		if (str) {
			var tempSplit:Array = str.split('-');
			var i:int = 0;
			for each (var col:String in tempSplit) {
				obstacleMap[i] = string32ToBooleanArray(col, height);
				i++;
			}
		} else {
			throw new Error('Provided string is empty or null')
		}
		return obstacleMap;
	}

}
}