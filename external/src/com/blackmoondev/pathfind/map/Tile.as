﻿package com.blackmoondev.pathfind.map
{

    public class Tile
    {
        //needed for AStar only
        public var f:Number = 0;
        public var g:Number = 0;
        public var h:Number = 0;
        public var parent:Tile;
        public var travelCost:Number;
        public var x:int;
        public var y:int;

		public var closed:Boolean = false;
		public var open:Boolean = false;
		public var visited:Boolean = false;

        public function Tile(x:int, y:int)
        {
            this.x = x;
            this.y = y;
        }// end function
    }
}
