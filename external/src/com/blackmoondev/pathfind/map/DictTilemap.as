/**
 * Created by IntelliJ IDEA.
 * User: Elparole
 * Date: 16.06.11
 * Time: 12:18
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.pathfind.map {
import com.blackmoondev.geom.IntPoint;

import flash.geom.Rectangle;
import flash.utils.Dictionary;

import org.osflash.signals.Signal;

public class DictTilemap {
    private var h:int;
    private var _data:Dictionary;
    private var w:int;

    private var _resized:Signal = new Signal(DictTilemap);

    public function DictTilemap(width:int = 30, height:int = 30) {
        resize(new IntPoint(width, height));
    }

    public function setExternalData(_data:Array):void {
//        var resized:Boolean = !(_data.length == this._data.length && _data[0].length == this._data[0].length);
//        this._data = _data;
//        if(resized)
//            _resized.dispatch(this)
    }

    public function isWalkable(xPos:int, yPos:int):Boolean {
        return _data[String(xPos) + '_'+String(yPos)] != -1;
//        return _data[xPos][yPos];
    }

    public function isWalkablePoint(target:IntPoint):Boolean {
        return _data[String(target.x) + '_'+String(target.y)] != -1;
    }

    public function setWalkable(gridX:int, gridY:int, walkable:Boolean):void {
        _data[String(gridX) + '_'+String(gridY)] = walkable? 1:-1;
    }

    public function setWalkablePoint(pos:IntPoint, walkable:Boolean):void {
         _data[String(pos.x) + '_'+String(pos.y)] = walkable? 1:-1;
    }

    public function clearTilemap():void {
        _data = new Dictionary();
    }

    public function resize(newSize:IntPoint):void {
//        w = newSize.x;
//        h = newSize.y;
        _data = new Dictionary();
         for (var i:int = 0; i < newSize.x; i++) {
            for (var j:int = 0; j < newSize.y; j++) {
                _data[i+'_'+j]=1;
            }
         }
    }

    public function getWidth():int {
        return w;
    }

    public function getHeight():int {
        return h;
    }

    public function trimAreaToWorldSize(rect:Rectangle):Rectangle {
        var tempRect:Rectangle = rect.clone();

        tempRect.left = Math.max(tempRect.left, 0);
        tempRect.top = Math.max(tempRect.top, 0);
        tempRect.right = Math.min(tempRect.right, w);
        tempRect.bottom = Math.min(tempRect.bottom, h);

        return tempRect;
    }


    public function isMapPoint(pos:IntPoint):Boolean {
        try {
            return _data[pos.x+'_'+pos.y] != null
        } catch(e:Error) {
            return false
        }
        return false
    }

    public function get resized():Signal {
        return _resized;
    }

    public function isWalkableArea(x:int, y:int,width:int, height:int):Boolean {
        return isWalkableRect(new Rectangle(x,y,width,height));
    }

    public function isWalkableRect(rectangle:Rectangle):Boolean {
//        var bounds:Rectangle = new Rectangle(0,0,w,h);

        var isWalkable:Boolean = true;
//        if(bounds.containsRect(rectangle)){
            for (var i:int = rectangle.x; i < rectangle.x+rectangle.width; i++) {
                for (var j:int = rectangle.y; j < rectangle.y+rectangle.height; j++) {
                    isWalkable &&=_data[i+'_'+j]!=-1;
                }
            }
//        }else{
//            throw new Error('Rectangle is outside tilemap!');
//        }
        return isWalkable;
    }

    public function setWalkableArea(x:int, y:int,width:int, height:int, isWalkable:Boolean):void {
        setWalkableRect(new Rectangle(x,y,width, height), isWalkable);
    }

    public function setWalkableRect(rectangle:Rectangle,isWalkable:Boolean):void {
//        var bounds:Rectangle = new Rectangle(0,0,w,h);
//
//        if(bounds.containsRect(rectangle)){
            for (var i:int = rectangle.x; i < rectangle.x+rectangle.width; i++) {
                for (var j:int = rectangle.y; j < rectangle.y+rectangle.height; j++) {
                    _data[i+'_'+j] = isWalkable? 1:-1;
                }
            }
//        }else{
//            throw new Error('Rectangle is outside tilemap!');
//        }
    }
}
}
