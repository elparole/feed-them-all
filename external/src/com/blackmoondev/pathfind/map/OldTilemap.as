/**
 * Created by IntelliJ IDEA.
 * User: work
 * Date: 15.03.11
 * Time: 13:31
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.pathfind.map {
import com.blackmoondev.geom.IntPoint;

import flash.geom.Rectangle;

public class OldTilemap {
    private var h:int;
    private var _data:Array;
    private var w:int;

    public function get data():Array {
        return _data;
    }

    public function OldTilemap(width:int = 30, height:int = 30) {
        setSize(new IntPoint(width, height));
    }

    public function isWalkable(xPos:int, yPos:int):Boolean {
        return _data[xPos][yPos];
    }

    public function isWalkablePoint(target:IntPoint):Boolean {
        return _data[target.x][target.y];
    }

    public function setWalkable(gridX:int, gridY:int, walkable:Boolean):void {
        _data[gridX][gridY] = walkable;
    }

    public function setWalkablePoint(pos:IntPoint, walkable:Boolean):void {
        _data[pos.x][pos.y] = walkable;
    }

    public function clearTilemap():void {
        for (var h:int = 0; h < _data.length; h++) {
            var col:Array = _data[h];
            for (var w:int = 0; w < col.length; w++) {
                col[w] = true;
            }
        }
    }

    public function setSize(newSize:IntPoint):void {
        var i:int = 0;
        var j:int = 0;

        // if data doesn't exists yet create new array
        if (!_data)
            _data = new Array(newSize.x);

        //iterate over all y values.
        while (i < newSize.x) {
            j = 0;

            //if current column doesn't exists create new array
            //else move iterator to the end of column list.
            if (!_data[i])
                _data[i] = new Array(newSize.y);
            else
                j = _data[i].length;

            //iterate over all x values
            while (j < newSize.y) {
                //create empty tiles for non existing tiles.
                _data[i][j] = true
                j++;
            }
            //trim array to the new/init width value
            _data[i].length = newSize.y;
            i++;
        }
        //trim array to the new/init height value
        _data.length = newSize.x;

        this.w = newSize.x;
        this.h = newSize.y;
    }

    public function getWidth():int {
        return w;

    }


    public function getHeight():int {
        return h;

    }

    public function trimAreaToWorldSize(rect:Rectangle):Rectangle {
        var tempRect:Rectangle = rect.clone();

        tempRect.left = Math.max(tempRect.left, 0);
        tempRect.top = Math.max(tempRect.top, 0);
        tempRect.right = Math.min(tempRect.right, w);
        tempRect.bottom = Math.min(tempRect.bottom, h);

        return tempRect;
    }


    public function isMapPoint(pos:IntPoint):Boolean {
        try {
            return _data[pos.x][pos.x] != null
        } catch(e:Error) {
            return false
        }
        return false
    }
}
}