package com.blackmoondev.pathfind.map {
import com.blackmoondev.geom.IntPoint;
import com.blackmoondev.geom.IntPoint;
import com.blackmoondev.pathfind.astar.IAStarSearchable;

import flash.geom.Rectangle;

import org.osflash.signals.Signal;

public class Tilemap extends Object implements IAStarSearchable {
    private var h:int;
    private var _data:Array;
    private var w:int;

    private var _resized:Signal = new Signal(Tilemap);

    public function get data():Array {
        return _data;
    }

    public function Tilemap(width:int = 30, height:int = 30) {
        resize(new IntPoint(width, height));
    }

    public function setExternalData(_data:Array):void {
        var resized:Boolean = !(_data.length == this._data.length && _data[0].length == this._data[0].length);
        this._data = _data;
        if (resized)
            _resized.dispatch(this)
    }

    public function isWalkable(xPos:int, yPos:int):Boolean {
        return _data[xPos][yPos];
    }

    public function isWalkablePoint(target:IntPoint):Boolean {
        return _data[target.x][target.y];
    }

    public function setWalkable(gridX:int, gridY:int, walkable:Boolean):void {
        _data[gridX][gridY] = walkable;
    }

    public function setWalkablePoint(pos:IntPoint, walkable:Boolean):void {
        _data[pos.x][pos.y] = walkable;
    }

    public function clearTilemap():void {
        for (var h:int = 0; h < _data.length; h++) {
            var col:Array = _data[h];
            for (var w:int = 0; w < col.length; w++) {
                _data[h][w] = true;
            }
        }
    }

    public function resize(newSize:IntPoint):void {
        var i:int = 0;
        var j:int = 0;

        // if data doesn't exists yet create new array
//        if (!_data)
            _data = new Array(newSize.x);

        //iterate over all y values.
        while (i < newSize.x) {
            j = 0;

            //if current column doesn't exists create new array
            //else move iterator to the end of column list.
            if (!_data[i])
                _data[i] = new Array(newSize.y);
            else
                j = _data[i].length;

            //iterate over all x values
            while (j < newSize.y) {
                //create empty tiles for non existing tiles.
                _data[i][j] = true;
                j++;
            }
            //trim array to the new/init width value
            _data[i].length = newSize.y;
            i++;
        }
        //trim array to the new/init height value
        _data.length = newSize.x;

        this.w = newSize.x;
        this.h = newSize.y;

        _resized.dispatch(this)
    }

    public function getWidth():int {
        return w;
    }

    public function getHeight():int {
        return h;
    }

    public function trimAreaToWorldSize(rect:Rectangle):Rectangle {
        var tempRect:Rectangle = rect.clone();

        tempRect.left = Math.max(tempRect.left, 0);
        tempRect.top = Math.max(tempRect.top, 0);
        tempRect.right = Math.min(tempRect.right, w);
        tempRect.bottom = Math.min(tempRect.bottom, h);

        return tempRect;
    }


    public function isMapPoint(pos:IntPoint):Boolean {
        try {
            return _data[pos.x][pos.y] != null
        } catch(e:Error) {
            return false
        }
        return false
    }

    public function get resized():Signal {
        return _resized;
    }

    public function isWalkableArea(x:int, y:int, width:int, height:int):Boolean {
        return isWalkableRect(new Rectangle(x, y, width, height));
    }

    public function isWalkableRect(rectangle:Rectangle):Boolean {
        var bounds:Rectangle = new Rectangle(0, 0, w, h);

        var isWalkable:Boolean = true;
        if (bounds.containsRect(rectangle)) {
            for (var i:int = rectangle.x; i < rectangle.width; i++) {
                for (var j:int = rectangle.y; j < rectangle.height; j++) {
                    isWalkable &&= data[i][j];
                }
            }
        } else {
            throw new Error('Rectangle is outside tilemap!');
        }
        return isWalkable;
    }

    public function setWalkableArea(x:int, y:int, width:int, height:int, isWalkable:Boolean):void {
        setWalkableArea(x, y, width, height, isWalkable);
    }

    public function setWalkableRect(rectangle:Rectangle, isWalkable:Boolean):void {
        var bounds:Rectangle = new Rectangle(0, 0, w, h);

        if (bounds.containsRect(rectangle)) {
            for (var i:int = rectangle.x; i < rectangle.width; i++) {
                for (var j:int = rectangle.y; j < rectangle.height; j++) {
                    data[i][j] = isWalkable;
                }
            }
        } else {
            throw new Error('Rectangle is outside tilemap!');
        }
    }
}
}
