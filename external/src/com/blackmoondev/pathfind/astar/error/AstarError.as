package com.blackmoondev.pathfind.astar.error
{
	public class AstarError extends Error
	{
		public function AstarError(message:*="", id:*=0)
		{
			super(message, id);
		}
	}
}