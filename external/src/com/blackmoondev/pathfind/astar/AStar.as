﻿package com.blackmoondev.pathfind.astar
{
import com.blackmoondev.geom.IntPoint;
import com.blackmoondev.pathfind.astar.error.AstarError;
    import com.blackmoondev.pathfind.map.Tile;

    import com.blackmoondev.pathfind.map.Tilemap;

public class AStar extends Object
	{
		private var _width:int;
		private var _height:int;
		public var distanceFunction:Function;
		private var _open:Vector.<Tile>;
		private var _closed:Vector.<Tile>;
		private var _visited:Vector.<Tile>;
		private var _map:IAStarSearchable;
		private var _mapData:Array;
		private var _astarData:Array;
		private static const COST_DIAGONAL:Number = COST_ORTHOGONAL * Math.sqrt(2);
		private static const COST_ORTHOGONAL:Number = 1;

		public function AStar(inputMap:IAStarSearchable)
		{
			distanceFunction = distManhattan;
			map = inputMap;
		}

		public function updateMap() : void
		{
			_width = _map.getWidth();
			_height = _map.getHeight();
			_astarData = [];
            for (var i:int = 0; i < _width; i++) {
                _astarData[i] = [];
                for (var j:int = 0; j < _height; j++)
                    _astarData[i][j] = new Tile(i, j);
            }
		}

        /*public function refreshMap():void {
			var xPos:int = 0;
			var yPos:int = 0;
			width = userMap.getWidth();
			height = userMap.getHeight();
			while (xPos < width)
			{
				yPos = 0;
				while (yPos < height)
				{
					(map[xPos][yPos] as AStarNode).walkable = userMap.isWalkable(xPos, yPos)
					yPos++;
				}
				xPos++;
			}
        }

        public function refreshMapPoint(pt:IntPoint):void {
            try{
                (map[pt.x][pt.y] as AStarNode).walkable = userMap.isWalkable(pt.x, pt.y)
            } catch(e:Error) {}
        }

		public function setMap(inputMap:IAStarSearchable) : void
		{
            if(userMap == inputMap || userMap && inputMap && userMap.getWidth() == inputMap.getWidth() && userMap.getHeight() == inputMap.getHeight())
                refreshMap();
            else {
                userMap = inputMap;
                updateMap();
            }
		}*/

		private function createSolution(startNode:Tile, endNode:Tile) : Array
		{
			var path:Array = [];
			var node:Tile = endNode;
			while (node.parent)	{
				path.push(new IntPoint(node.x, node.y));
				node = node.parent;
			}
			//path.push(new IntPoint(startNode.x, startNode.y));
			return path;
		}

		private function neighbors(param1:Tile) : Array
		{
			/**
			 * 4 way pathfinding faster by 20-30 ms than 9 way
			 * before time:  200-205 ms
			 * current time: 180-185 ms
			 */
			
			var node:Tile = null;
			var xPos:* = param1.x;
			var yPos:* = param1.y;
			var neighborNodes:* = new Array(4);
			if (xPos > 0)
			{
				node = _astarData[xPos - 1][yPos] as Tile;
				if (_mapData[xPos - 1][yPos])
				{
					node.travelCost = COST_ORTHOGONAL;
					neighborNodes.push(node);
				}
			}
			if (xPos < _width - 1)
			{
				node = _astarData[xPos + 1][yPos] as Tile;
				if (_mapData[xPos + 1][yPos])
				{
					node.travelCost = COST_ORTHOGONAL;
					neighborNodes.push(node);
				}
			}
			if (yPos > 0)
			{
				node = _astarData[xPos][yPos - 1] as Tile;
				if (_mapData[xPos][yPos - 1])
				{
					node.travelCost = COST_ORTHOGONAL;
					neighborNodes.push(node);
				}
			}
			if (yPos < _height - 1)
			{
				node = _astarData[xPos][yPos + 1] as Tile;
				if (_mapData[xPos][yPos + 1])
				{
					node.travelCost = COST_ORTHOGONAL;
					neighborNodes.push(node);
				}
			}
			
			/** / //PART NEEDED FOR DIAGONAL
			if (xPos > 0 && yPos > 0)
			{
				node = _mapData[(xPos - 1)][(yPos - 1)];
				if (node.walkable && _mapData[(xPos - 1)][yPos].walkable && _mapData[xPos][(yPos - 1)].walkable)
				{
					node.travelCost = COST_DIAGONAL;
					neighborNodes.push(node);
				}
			}
			if (xPos < (_width - 1) && yPos > 0)
			{
				node = _mapData[(xPos + 1)][(yPos - 1)];
				if (node.walkable && _mapData[(xPos + 1)][yPos].walkable && _mapData[xPos][(yPos - 1)].walkable)
				{
					node.travelCost = COST_DIAGONAL;
					neighborNodes.push(node);
				}
			}
			if (xPos > 0 && yPos < (_height - 1))
			{
				node = _mapData[(xPos - 1)][(yPos + 1)];
				if (node.walkable && _mapData[(xPos - 1)][yPos].walkable && _mapData[xPos][(yPos + 1)].walkable)
				{
					node.travelCost = COST_DIAGONAL;
					neighborNodes.push(node);
				}
			}
			if (xPos < (_width - 1) && yPos < (_height - 1))
			{
				node = _mapData[(xPos + 1)][(yPos + 1)];
				if (node.walkable && _mapData[(xPos + 1)][yPos].walkable && _mapData[xPos][(yPos + 1)].walkable)
				{
					node.travelCost = COST_DIAGONAL;
					neighborNodes.push(node);
				}
			}/**/
			return neighborNodes;
		}

		public function solve(startPoint:IntPoint, endPoint:IntPoint) : Array
		{
			var smallestF:Number = NaN;
			var smallestNode:Tile = null;
			var cOpenIndex:int = 0;
			var neighbor:Tile = null;
			var neigborG:Number = NaN;
			var needsUpdateOrFillWithData:Boolean = false;
			_open = new Vector.<Tile>();
			_closed = new Vector.<Tile>();
			_visited = new Vector.<Tile>();
			try {
				var startNode:Tile = _astarData[startPoint.x][startPoint.y] as Tile;
				var endNode:Tile = _astarData[endPoint.x][endPoint.y] as Tile;
			} catch (e:Error){
				if(_map.getWidth() <= endPoint.x)
					throw new AstarError("Algorithm failed, endpoint Y coordinate is outside map at "+endPoint.x+' x '+endPoint.y);
				else if(_map.getHeight() <= endPoint.y)
					throw new AstarError("Algorithm failed, endpoint X coordinate is outside map at "+endPoint.x+' x '+endPoint.y);
				else
					throw new AstarError("Algorithm failed, propably endpoint coordinate is not astar map vaild coordinate at "+endPoint.x+' x '+endPoint.y);
			}
			if (endNode == null)
				throw new AstarError("Algorithm failed, endNode coordinate is null at "+endPoint.x+' x '+endPoint.y);
			
			startNode.open = true;
			startNode.visited = true;
			startNode.g = 0;
			startNode.h = distanceFunction(startNode, endNode);
			startNode.f = startNode.h;
			startNode.parent = null;
			_open.push(startNode);
			_visited.push(startNode);
 			while (_open.length > 0)
			{
				/**
				 * Totally changed open array management,
				 * changed Arrays to Vectors
				 * and added flags directly to Tile.
				 * 
				 * Progress faster by ~145 ms
				 * before time:  180-185 ms
				 * current time: 34 ms
				 */
				//get last from open list (with smallest f)
				smallestNode = _open.pop() as Tile;
				smallestNode.open = false;
				smallestF = smallestNode.f;
				
				/**
				 * End of algorithm
				 */
				if (smallestNode == endNode) {
					cleanVisitedProps();
					return createSolution(startNode, endNode).concat(startPoint.clone());
				}
				
				//add to closed
				_closed.push(smallestNode);
				smallestNode.closed = true;
				
				var neighborsArr:Array = neighbors(smallestNode);
				//for each neighbor check
				for each (neighbor in neighborsArr)
				{
					//if neighbor is already closed then check another neigbor.
					if (neighbor.closed)
						continue;
					
					//calculate new g for neigbor
					neigborG = smallestNode.g + neighbor.travelCost;
					needsUpdateOrFillWithData = false;
					if (!neighbor.open)
					{
						neighbor.parent = smallestNode;
						neighbor.g = neigborG;
						neighbor.h = distanceFunction(neighbor, endNode);
						neighbor.f = neighbor.g + neighbor.h;
						
						for (var i:int = _open.length; i > 0; i--) 
						{
							if (_open[i - 1].f >= neighbor.f )
								break;
							_open[i] = _open[i - 1];
						}
						
						//add to open
						_open[i] = neighbor;
						neighbor.open = true;
						
						//visit neighbor
						if (!neighbor.visited) {
							_visited.push(neighbor);
							neighbor.visited = true
						}
					}
					else if (neigborG < neighbor.g)
					{
						neighbor.parent = smallestNode;
						neighbor.g = neigborG;
						neighbor.h = distanceFunction(neighbor, endNode);
						neighbor.f = neighbor.g + neighbor.h;
					}
				}
			}
			cleanVisitedProps();
			return [];
		}
		
		private function cleanVisitedProps():void
		{
			if (_visited == null) return;
			for each (var node:Tile in _visited)
			{
				node.visited = false;
				node.open = false;
				node.closed = false;
			}
		}

		public static function distManhattan(t1:Tile, t2:Tile) : Number
		{
			var xDiff:int = t1.x - t2.x;
			var yDiff:int = t1.y - t2.y;
			
			/**
			 * Progress faster by 0-5 ms
			 * before time:  200-210 ms
			 * current time: 200-205 ms
			 */
			
			/* initial version * /
			return Math.abs(param1.x - param2.x) + Math.abs(param1.y - param2.y);
			//*/
			
			/* new version */
			return (xDiff < 0 ? -xDiff : xDiff) + (yDiff < 0 ? -yDiff : yDiff);
			//*/
		}

        public function get map():IAStarSearchable {
            return _map;
        }

        public function set map(value:IAStarSearchable):void {
            if(_map) {
                _map.resized.remove(onResize)
            }
            _map = value;
            _mapData = value.data;
            _width = value.getWidth();
            _height = value.getHeight();
            updateMap();
            _map.resized.add(onResize);
        }

    private function onResize(map:Tilemap):void {
        this.map = map
    }
    }
}
