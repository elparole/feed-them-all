﻿package com.blackmoondev.pathfind.astar {
import org.osflash.signals.Signal;

public interface IAStarSearchable {
    function getWidth():int;

    function getHeight():int;

    function get data():Array;

    function isWalkable(xPos:int, yPos:int):Boolean;

    function get resized():Signal;
}
}
