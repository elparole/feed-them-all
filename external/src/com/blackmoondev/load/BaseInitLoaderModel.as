/**
 * Created by IntelliJ IDEA.
 * User: wrobel221
 * Date: 11.05.11
 * Time: 13:13
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.load
{
import com.blackmoondev.load.signals.*;

import flash.utils.Dictionary;

import org.osflash.signals.ISignal;


/* PASTE IT TO YOUR APP CONTEXT
 signalCommandMap.mapSignalClass(com.blackmoondev.load.signals.LoaderFinishedSignal, LoaderFinishedCommand);
 injector.mapSingleton(com.blackmoondev.load.signals.LoaderFailedSignal);
 injector.mapSingleton(com.blackmoondev.load.signals.LoaderStepFailedSignal);
 injector.mapSingleton(com.blackmoondev.load.signals.LoaderStepFinishedSignal);
 */
public class BaseInitLoaderModel
{

	[Inject]
	public var loaderFinished:LoaderFinishedSignal;

	[Inject]
	public var loaderFailed:LoaderFailedSignal;

	[Inject]
	public var loaderStepFinished:LoaderStepFinishedSignal;

	[Inject]
	public var loaderStepFailed:LoaderStepFailedSignal;

	protected var loadSteps:Array = [];
	protected var loadStepParams:Dictionary = new Dictionary();

	/**
	 * METHODS
	 */

	public function addStep(step:*, params:Array = null):void
	{
		loadSteps.push(step);
		if (params)
			loadStepParams[step] = params;
	}

	public function startLoader():void
	{
		setSteps();
		setupStepListeners();
		nextStep()
	}

	private function setupStepListeners():void
	{
		loaderStepFinished.add(nextStep);
		loaderStepFailed.add(failLoader);
	}

	private function removeListeners():void
	{
		loaderStepFinished.remove(nextStep);
		loaderStepFailed.remove(failLoader);
	}

	protected function failLoader(msg:*):void
	{
		loadSteps = [];
		loaderStepFinished.remove(nextStep);
		loaderStepFailed.remove(failLoader);
		loaderFailed.dispatch(msg? msg.toString():"no msg");
		onFail();
	}

	protected function setSteps():void
	{
		//OVERRIDE HERE
	}

	private function nextStep():void
	{
		if (loadSteps.length == 0)
		{
			removeListeners();
			loaderFinished.dispatch();
			onFinish()
		} else
		{
//            try {
			//step is a function
			var step:* = loadSteps.shift();
			var params:Array = loadStepParams[step] || getLiveParams(step) || [];
			if (step is ISignal)
			{
				ISignal(step).dispatch.apply(step, params);
			} else
			{
				//step is a function
				Function(step).apply(null, params)
			}
			onStepDispatched()
//            } catch(e:Error) {
//                loaderFailed.dispatch(e.message);
//            }
		}
	}

	protected function onStepDispatched():void
	{

	}

	protected function onFinish():void
	{

	}

	protected function onFail():void
	{

	}

	protected function getLiveParams(step:*):Array
	{
		//OVERRIDE HERE
		return null
	}
}
}
