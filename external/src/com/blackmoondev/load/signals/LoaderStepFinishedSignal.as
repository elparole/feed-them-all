/*
 Copyright (c) 2010 Blackmoon, All Rights Reserved
 @author   Bartosz Indycki and Michal Wroblewski
 @contact  bartekindycki@gmail.com wrobel221@gmail.com
 @project  ItemPreview
 @internal
 */
package com.blackmoondev.load.signals
{
import org.osflash.signals.Signal;

public class LoaderStepFinishedSignal extends Signal
{
	public function LoaderStepFinishedSignal()
	{
		super();
	}
}
}