/*
 Copyright (c) 2010 Blackmoon, All Rights Reserved
 @author   Bartosz Indycki, Michal Wroblewski and Karol Furmann
 @contact  bartekindycki@gmail.com wrobel221@gmail.com karol.furmann@gmail.com
 @project  AgoraClient
 @internal
 */
package com.blackmoondev.load.signals
{
import org.osflash.signals.Signal;

public class LoaderStepFailedSignal extends Signal
{
	public function LoaderStepFailedSignal()
	{
		super(Object);
	}
}
}