package com.blackmoondev.geom  {

public class IntPoint extends Object {
	public var x:int;
	public var y:int;

	public function IntPoint(x:int = 0, y:int = 0) {
		this.x = x;
		this.y = y;
	}

	public function add(pt:IntPoint):IntPoint {
		x += pt.x;
		y += pt.y;
		return this
	}

	public function addNew(pt:IntPoint):IntPoint {
		return new IntPoint(x + pt.x, y + pt.y);
	}

	public function subtract(pt:IntPoint):IntPoint {
		x -= pt.x;
		y -= pt.y;
		return this;
	}

	public function substrNew(pt:IntPoint):IntPoint {
		return new IntPoint(x - pt.x, y - pt.y);
	}

	public function invert():IntPoint {
		x = -x;
		y = -y;
		return this
	}

	public function multiply(times:int):IntPoint {
		x *= times;
		y *= times;
		return this
	}

	public function multiplyNew(times:int):IntPoint {
		return new IntPoint(x * times, y * times)
	}

	public function toString():String {
		return "[" + x + "," + y + "]";
	}

	public function clone():IntPoint {
		return new IntPoint(x, y)
	}

	public function equal(pt:IntPoint):Boolean {
		return pt.x == x && pt.y == y;
	}
}
}
