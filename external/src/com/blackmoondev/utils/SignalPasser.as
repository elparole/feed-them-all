/**
 * Created by IntelliJ IDEA.
 * User: Elparole
 * Date: 27.01.12
 * Time: 16:37
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.utils {

import org.osflash.signals.Signal;

public class SignalPasser{
    
    private var signalA:Signal;
    
    private var signalB:Signal;


    public function SignalPasser(signalA:Signal, signal:Signal) {
        this.signalA = signalA;
        this.signalB = signal;
        
        
        switch(signalA.valueClasses.length){
            case 0: signalA.add(onPass0);
                break;
            case 1: signalA.add(onPass1);
                break;
            case 2: signalA.add(onPass2);
                break;
            case 3: signalA.add(onPass3);
                break;
            case 4: signalA.add(onPass4);
                break;
        }
    }

    private function onPass4(a1:*, a2:*, a3:*, a4:*):void {
        signalB.dispatch(a1, a2, a3, a4);
    }

    private function onPass3(a1:*, a2:*, a3:*):void {
        signalB.dispatch(a1, a2, a3);
    }

    private function onPass2(a1:*, a2:*):void {
        signalB.dispatch(a1,  a2);
    }

    private function onPass1(a1:*):void {
        signalB.dispatch(a1);
    }
    
    private function onPass0():void{
        signalB.dispatch();
    }

    public function destroy():void {
        switch(signalA.valueClasses.length){
            case 0: signalA.remove(onPass0);
                break;
            case 1: signalA.remove(onPass1);
                break;
            case 2: signalA.remove(onPass2);
                break;
            case 3: signalA.remove(onPass3);
                break;
            case 4: signalA.remove(onPass4);
                break;
        }
    }

}
}
