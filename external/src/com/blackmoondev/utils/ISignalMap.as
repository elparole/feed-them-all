/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 19:53
 */
package com.blackmoondev.utils
{
import org.osflash.signals.ISignal;

public interface ISignalMap
{
	function addToSignal(signal:ISignal, handler:Function):void;

	function addOnceToSignal(signal:ISignal, handler:Function):void;

	function removeFromSignal(signal:ISignal, handler:Function):void;

	function removeAll():void;

}
}
