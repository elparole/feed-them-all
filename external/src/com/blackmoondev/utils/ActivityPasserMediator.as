/**
 * Created by IntelliJ IDEA.
 * User: Elparole
 * Date: 27.01.12
 * Time: 15:46
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.utils {
import avmplus.extendsXml;

import flash.events.IEventDispatcher;

import flash.utils.Dictionary;

import org.osflash.signals.Signal;


public class ActivityPasserMediator extends SignalMediator{

    
    private var signalPasserMap:Dictionary;

    public function ActivityPasserMediator() {
        signalPasserMap = new Dictionary();

    }

    public function addEventSignalPasser(disp:IEventDispatcher, eventName:String,  signalB:Signal, args:Array = null):void{
        signalPasserMap[String(disp) + '_' + eventName] = new EventSignalPasser(disp,  eventName, signalB, args);
    }

    public function addSignalPasser(signalA:Signal, signalB:Signal):void{
        signalPasserMap[signalA] = new SignalPasser(signalA, signalB);        
    }

    public function removeEventSignalPasser(disp:IEventDispatcher, eventName:String, signalB:Signal):void{
        signalPasserMap[String(disp) + '_' + eventName].destroy();
        signalPasserMap[String(disp) + '_' + eventName] = null;
    }

    public function removeSignalPasser(signalA:Signal,  signalB:Signal):void{
        signalPasserMap[signalA].destroy();
        signalPasserMap[signalA] = null;
    }

    override public function destroy():void {
        for(var key:* in signalPasserMap) {
            signalPasserMap[key].destroy();
        }
        super.destroy();
    }
}
}
