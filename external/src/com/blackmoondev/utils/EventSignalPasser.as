/**
 * Created by IntelliJ IDEA.
 * User: Elparole
 * Date: 27.01.12
 * Time: 16:43
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.utils {
import flash.events.Event;
import flash.events.IEventDispatcher;

import org.casalib.core.IDestroyable;

import org.osflash.signals.Signal;

public class EventSignalPasser {

    private var disp:IEventDispatcher;

    private var eventName:String;

    private var signalB:Signal;
    
    private var args:Array;


    public function EventSignalPasser(disp:IEventDispatcher, eventName:String, signalB:Signal, args:Array = null) {
        this.disp = disp;
        this.eventName = eventName;
        this.signalB = signalB;
        this.args = args;
        
        disp.addEventListener(eventName, onEvent);
    }

    private function onEvent(..._):void {
        signalB.dispatch.apply(null,args);
    }

    public function destroy():void {
        disp.removeEventListener(eventName, onEvent);
    }
}
}
