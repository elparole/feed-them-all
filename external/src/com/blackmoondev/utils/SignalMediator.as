/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 19:51
 */
package com.blackmoondev.utils
{
import org.osflash.signals.ISignal;

import robotlegs.bender.bundles.mvcs.Mediator;

	public class SignalMediator extends Mediator
	{

		protected var _signalMap:ISignalMap;

		public function SignalMediator()
		{
			super();
		}

		override public function destroy():void {
			signalMap.removeAll();
			super.destroy();
		}

		protected function get signalMap():ISignalMap
		{
			return _signalMap ||= new SignalMap();
		}

		protected function addToSignal(signal:ISignal, handler:Function):void
		{
			signalMap.addToSignal(signal, handler);
		}

		protected function addOnceToSignal(signal:ISignal, handler:Function):void
		{
			signalMap.addOnceToSignal(signal, handler);
		}

	}
}
