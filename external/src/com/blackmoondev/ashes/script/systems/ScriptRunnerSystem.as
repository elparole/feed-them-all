/**
 * Created by IntelliJ IDEA.
 * User: Darkwing88
 * Date: 24.11.12
 * Time: 23:14
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.ashes.script.systems
{
import ash.core.NodeList;
import ash.core.System;

import com.blackmoondev.ashes.script.nodes.ScriptsNode;

public class ScriptRunnerSystem extends System
{
	[Inject(nodeType="com.blackmoondev.ashes.script.nodes.ScriptsNode")]
	public var scriptsList : NodeList;

	override public function update( time : Number ) : void {
		super.update( time );
		var node : ScriptsNode = scriptsList.head
		for ( node; node; node = node.next ) {
			node.scripts.runScripts( node.entity, time )
		}
	}

	[PostConstruct]
	public function setup() : void {
		scriptsList.nodeAdded.add( nodeAdded )
	}

	public function nodeAdded( argNode : ScriptsNode ) : void {
		argNode.scripts.resetAll()
	}
}
}
