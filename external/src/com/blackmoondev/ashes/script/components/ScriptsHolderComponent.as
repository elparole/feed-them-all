/**
 * Created by IntelliJ IDEA.
 * User: Darkwing88
 * Date: 24.11.12
 * Time: 23:14
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.ashes.script.components
{
import ash.core.Entity;

import com.blackmoondev.ashes.script.vo.IScript;
import com.blackmoondev.ashes.script.vo.ScriptList;

public class ScriptsHolderComponent
{

	private var allLists : Vector.<ScriptList> = new <ScriptList>[];

	public static function getComponent() : ScriptsHolderComponent {
		var comp : ScriptsHolderComponent = new ScriptsHolderComponent();
		return comp
	}

	public static function getSingleScriptListComponent( scriptList : ScriptList ) : ScriptsHolderComponent {
		return new ScriptsHolderComponent()
				.addConcurentScriptList( scriptList );
	}

	public static function getSingleScriptComponent( script : IScript, autoDisposeOnLast : Boolean ) : ScriptsHolderComponent {
		return new ScriptsHolderComponent()
				.addConcurentScriptList(
						ScriptList.getList( autoDisposeOnLast )
								.addScript( script )
				);
	}


	public function runScripts( entity : Entity, time : Number ) : void {
		//todo: iterate reversed so no need to i-- or other workarounds
		for ( var i : int = 0; i < allLists.length; i++ ) {
			allLists[i].run( entity, time );
			if ( allLists[i].autoDisposeOnLast && allLists[i].finished ) {
				allLists.splice( i, 1 );
				i -= 1;
			}
		}
	}

	public function finishCurrent():void{

	}

	public function resetAll() : void {
		for ( var i : int = 0; i < allLists.length; i++ ) {
			allLists[i].reset()
		}
	}

	public function removeAll( ):void {
		allLists.length = 0;
	}

	public function addConcurentScriptList( argList : ScriptList ) : ScriptsHolderComponent {
		allLists.push( argList );
		return this
	}
}
}
