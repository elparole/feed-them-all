/**
 * Created with IntelliJ IDEA.
 * User: wrobel221
 * Date: 04.02.13
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.ashes.script.impl
{
import ash.core.Entity;

public interface IEntityGuard
{
	function approve( e : Entity ) : Boolean
}
}
