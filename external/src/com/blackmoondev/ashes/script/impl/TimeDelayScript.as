/**
 * Created with IntelliJ IDEA.
 * User: wrobel221
 * Date: 05.02.13
 * Time: 02:20
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.ashes.script.impl
{
import ash.core.Entity;

import com.blackmoondev.ashes.script.vo.IScript;

public class TimeDelayScript implements IScript
{
	public function TimeDelayScript( time : Number ) {
		totalTime = timeLeft = time;
	}

	private var totalTime : Number;
	private var timeLeft : Number;

	public function run( e : Entity, time : Number ) : void {
		timeLeft -= time;
	}

	public function hasEnded() : Boolean {
		return timeLeft <= 0;
	}

	public function reset() : void {
		timeLeft = totalTime;
	}
}
}
