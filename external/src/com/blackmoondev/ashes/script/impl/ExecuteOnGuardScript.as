/**
 * Created with IntelliJ IDEA.
 * User: wrobel221
 * Date: 04.02.13
 * Time: 13:10
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.ashes.script.impl
{
import ash.core.Entity;

import com.blackmoondev.ashes.script.vo.IScript;

public class ExecuteOnGuardScript implements IScript
{
	public function ExecuteOnGuardScript( guard : IEntityGuard, functionOrCommand : * , runOnce:Boolean = false) {
		_guard = guard;
		_functionOrCommand = functionOrCommand;
		_runOnce = runOnce;
	}

	private var _runOnce : Boolean;
	private var _hasEnded : Boolean;
	private var _guard : IEntityGuard;
	private var _functionOrCommand : *;

	public function run( e : Entity, time : Number ) : void {
		if ( _guard.approve( e ) ) {
			execute( e, time )
		}
	}

	public function hasEnded() : Boolean {
		return _hasEnded;
	}

	public function reset() : void {
		_hasEnded = false;
	}

	private function execute( entity : Entity, time : Number ) : void {
		var fn : * = _functionOrCommand;
		if ( !(fn is Function) ) {
			fn = fn.execute;
		}
		if ( fn.length == 2 ) {
			( fn as Function )( entity, time );
		} else if ( fn.length == 1 ) {
			( fn as Function )( entity );
		} else {
			( fn as Function )();
		}
		if(_runOnce)
			_hasEnded = true;

	}
}
}
