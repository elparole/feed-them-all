/**
 * Created with IntelliJ IDEA.
 * User: wrobel221
 * Date: 04.02.13
 * Time: 13:22
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.ashes.script.vo
{
import ash.core.Entity;

public interface IScript
{
	function run( e : Entity, time : Number ) : void

	function hasEnded() : Boolean;

	function reset() : void
}
}
