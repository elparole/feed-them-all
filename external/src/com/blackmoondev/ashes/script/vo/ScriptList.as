/**
 * Created by IntelliJ IDEA.
 * User: Darkwing88
 * Date: 24.11.12
 * Time: 23:29
 * To change this template use File | Settings | File Templates.
 */
package com.blackmoondev.ashes.script.vo
{
import ash.core.Entity;

public class ScriptList
{
	public static function getList( argAutoDisposeOnLast : Boolean ) : ScriptList {
		var list : ScriptList = new ScriptList();
		list.autoDisposeOnLast = argAutoDisposeOnLast;
		return list
	}

	public var autoDisposeOnLast : Boolean;
	public var finished : Boolean;
	private var scripts : Vector.<IScript> = new <IScript>[];
	private var index : int;

	public function run( argEntity : Entity, time : Number ) : void {
		if ( index >= scripts.length ) {
			finished = true;
			return;
		}
		scripts[index].run( argEntity, time );
		if ( scripts[index].hasEnded() ) index++;
		if ( index >= scripts.length ) {
			finished = true
		}
	}

	public function addScript( argVO : IScript ) : ScriptList {
		scripts.push( argVO );
		return this
	}

	public function reset() : void {
		index = 0;
		for ( var i : int = 0; i < scripts.length; i++ ) {
			scripts[i].reset()
		}
	}
}
}
