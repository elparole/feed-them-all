/**
 * User: Elparole
 * Date: 25.03.13
 * Time: 18:34
 */
package com.elparole.feedThemAll.model.vo
{
public class ChildVO
{
	public var skinId:String;
	public var startTime:int;
	public var x:int;
	public var y:int;


	public function ChildVO(skinId:String, startTime:int, x:int, y:int) {
		this.skinId = skinId;
		this.startTime = startTime;
		this.x = x;
		this.y = y;
	}
}
}
