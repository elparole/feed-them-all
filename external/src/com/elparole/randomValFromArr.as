/**
 * User: Elparole
 * Date: 02.08.12
 * Time: 09:50
 */
package com.elparole
{
	public function randomValFromArr(arr:Array):* {
		var endIndex:int = arr.length;
		return arr[Math.floor(Math.random()*(endIndex - 0.000001))];
	}
}
