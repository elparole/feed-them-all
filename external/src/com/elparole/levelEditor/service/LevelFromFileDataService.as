package com.elparole.levelEditor.service
{

//import by.blooddy.crypto.serialization.JSON;

import com.elparole.feedThemAll.model.vo.ChildVO;
import com.elparole.levelEditor.model.vo.GameItemConfig;
import com.elparole.levelEditor.model.vo.LevelConfig;

import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.utils.Dictionary;

import org.casalib.load.DataLoad;
import org.casalib.math.geom.Point3d;
import org.robotlegs.oil.async.Promise;

public class LevelFromFileDataService implements ILevelService
	{
//		[Inject]
//		public var logger:ILogger;

		private var dataLoad:DataLoad;

		private var prom:Promise;
		private var sampleConfig:Object;
		private var _currentLevelName:String;
		public var list:Array;


		public function LevelFromFileDataService() {

		}


		public function hasLevelNR(nr:int):Boolean {
			return list.length>nr && (list[nr] != null);
		}

	public function loadLevelsList(path:String):Promise {
			prom = new Promise();
			trace('ensureProjFileExist');
			dataLoad = new DataLoad(path);
			dataLoad.addEventListener(IOErrorEvent.IO_ERROR, onReadLevelError,false,0,true);
			dataLoad.addEventListener(Event.COMPLETE, onReadLevelsListComplete,false,0,true);
			dataLoad.start();
			return prom;
		}


		public function loadConfigByNR(nr:int):Promise {
			return null;
		}

	public function loadConfig(name:String, path:String):Promise {
			prom = new Promise();
			this._currentLevelName = name;
			trace('ensureProjFileExist');
			dataLoad = new DataLoad(path);
			dataLoad.addEventListener(IOErrorEvent.IO_ERROR, onReadLevelError,false,0,true);
			dataLoad.addEventListener(Event.COMPLETE, onReadLevelComplete,false,0,true);
			dataLoad.start();
			return prom;
		}

		private function onReadLevelsListComplete(event:Event):void {
			var levStr:String = dataLoad.dataAsString;//.readUTFBytes(fileStream.bytesAvailable);
//			list = JSON.decode(levStr).levels;
			list = JSON.parse(levStr).levels;
			prom.handleResult({list:list});
		}
		private function onReadLevelComplete(event:Event):void {
			var levStr:String = dataLoad.dataAsString;//.readUTFBytes(fileStream.bytesAvailable);
//			var lc:LevelConfig = LevelConfig.fromObj(JSON.decode(levStr));
			var lc:LevelConfig = LevelConfig.fromObj(JSON.parse(levStr));
			var itemOnPosVos:Array = [];

			for each (var itemObj:Object in lc.floorItems) {
//			ensureItemVOExists(itemObj.skinId+'_'+itemObj.dirId, itemObj);
				itemOnPosVos.push(new GameItemConfig(
						itemObj.skinId,
						itemObj.animId,
						itemObj.dirId,
						new Point3d(itemObj.posX,itemObj.posY,itemObj.posZ)));
			}
			lc.floorItems = itemOnPosVos;

			itemOnPosVos = [];
			for each (itemObj in lc.items) {
				itemOnPosVos.push(new GameItemConfig(
						itemObj.skinId,
						itemObj.animId,
						itemObj.dirId,
						new Point3d(itemObj.posX,itemObj.posY,itemObj.posZ)));
			}
			lc.items = itemOnPosVos;

			itemOnPosVos = [];
			for each (itemObj in lc.childrenList) {
				itemOnPosVos.push(new ChildVO(
						itemObj.skinId,
						itemObj.startTime,
						itemObj.startX,
						itemObj.startY));
			}
			itemOnPosVos.sortOn('startTime',Array.NUMERIC);
			lc.childrenList = itemOnPosVos;
			prom.handleResult(lc);
		}

		private function onReadLevelError(event:IOErrorEvent):void {
//			if(fileStream.bytesAvailable<8){
//			resetDefaultProjFile();
//			}
			prom.handleResult({});

		}

	public function get currentLevelName():String {
		return _currentLevelName;
	}
}
}
