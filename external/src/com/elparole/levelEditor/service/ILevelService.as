package com.elparole.levelEditor.service
{

import org.robotlegs.oil.async.Promise;

public interface ILevelService
	{
		function loadConfig(name:String, path:String):Promise;

		function get currentLevelName():String;

		function loadLevelsList(s:String):Promise;

		function hasLevelNR(nr:int):Boolean;

		function loadConfigByNR(nr:int):Promise;
}
}
