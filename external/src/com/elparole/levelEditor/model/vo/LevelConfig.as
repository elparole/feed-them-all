/**
 * User: Elparole
 * Date: 08.03.13
 * Time: 14:13
 */
package com.elparole.levelEditor.model.vo
{
import com.blackmoondev.iso.isoModel.math.IsoMathUtils;

import flash.geom.Point;
import flash.utils.Dictionary;

public class LevelConfig
{
	public var items:Array = [];
	public var floorItems:Array = [];
	public var size:Point;
	public var name:String;
	public var index:int;
	public var obstacleMap:Dictionary;
	public var backupFloorItems:Array;
	public var backupItems:Array;
	public var childrenList:Array;
	public var sheepsNum:int = 0;


	public function LevelConfig(items:Array, floorItems:Array, childrenList:Array, size:Point,obstacleMap:Dictionary, name:String) {
		this.items = items;
		this.floorItems = floorItems;
		this.childrenList = childrenList;
		this.size = size;
		this.obstacleMap = obstacleMap;
		this.name = name;
	}

	public static function fromObj(levelObj:Object):LevelConfig {
		var obstacleDict:Dictionary;
		if(levelObj.obstacleMap){
			obstacleDict = IsoMathUtils.generateObstacleMapFromHexToDict(
					levelObj.obstacleMap,
					levelObj.size.isoHeight,
					levelObj.size.isoWidth)
		}
		obstacleDict ||= getDefaultObstacleMap(new Point(levelObj.size.isoWidth,levelObj.size.isoHeight));
		return new LevelConfig(
				levelObj.items? levelObj.items.concat():[],
				levelObj.floorItems? levelObj.floorItems.concat():[],
				levelObj.childrenList? levelObj.childrenList.concat():[],
				new Point(levelObj.size.isoWidth,levelObj.size.isoHeight),
				obstacleDict||getDefaultObstacleMap(new Point(levelObj.size.isoWidth,levelObj.size.isoHeight)),
				levelObj.name)
	}

	public static function getDefaultObstacleMap(size:Point):Dictionary {
		var dict:Dictionary  = new Dictionary();
		for (var ix:int = 0; ix < size.y; ix++) {
			for (var iy:int = 0; iy < size.x; iy++) {
				dict[ix+'_'+iy]= true;
			}
		}
		return dict;
	}
}
}
