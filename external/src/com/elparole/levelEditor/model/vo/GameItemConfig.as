/**
 * User: Elparole
 * Date: 08.03.13
 * Time: 14:25
 */
package com.elparole.levelEditor.model.vo
{

import flash.display.BitmapData;

import org.casalib.math.geom.Point3d;

public class GameItemConfig
{
	public var skinId:String;
	public var animId:String;
	public var dirId:String;
	public var pos:Point3d;
	public var bd:BitmapData


	public function GameItemConfig(skinId:String, animId:String, dirId:String, pos:Point3d) {
		this.skinId = skinId;
		this.animId = animId;
		this.dirId = dirId;
		this.pos = pos;
	}
	
	public function get sortId():int{
		return pos.x*5000+pos.z;
	}

//	public function get listId():String{
//		return id+'_'+dirId+'_'+totalFrames;
//		return itemVO.listId;
//	}
}
}
