/**
 * User: Elparole
 * Date: 02.08.12
 * Time: 09:50
 */
package com.elparole
{

/**
 *
 * @param startIndex
 * @param endIndex
 * @return random int >= startIndex and <= endIndex
 */
	public function randomIntInRange(startIndex:Number = 0, endIndex:Number = 0):int {
		return Math.floor(Math.random()*(endIndex - startIndex - 0.000001+1))+startIndex;
	}
}
