/**
 * User: Elparole
 * Date: 02.08.12
 * Time: 09:50
 */
package com.elparole
{
	public function cartesianToPolarCoords(dx:Number = 0, dy:Number = 0):Array {

		var l:Number = Math.sqrt((dx) * (dx) + (dy) * (dy));
		var angle:Number = Math.atan2(dy, dx);
		return [l, angle];
	}
}
