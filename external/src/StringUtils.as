/**
 * Created by IntelliJ IDEA.
 * User: Elparole
 * Date: 20.10.11
 * Time: 21:48
 * To change this template use File | Settings | File Templates.
 */
package {
public class StringUtils {
    public function StringUtils() {
    }

    private static var date:Date = new Date();

    public static function formatTime(time:Date, options:String):String{
        if(options == 's ms')
//        return String(time.getMilliseconds());
        return time? (time.getMinutes()>0? time.getMinutes().toString()+' ':'') +
                                    (time.getSeconds() < 10 ? "0":"" ) + time.getSeconds().toString()+" "+(time.getMilliseconds() < 100 ? "0":"" ) +
                                                        (time.getMilliseconds() < 10 ? "0":"" )+
                                                            (time.getMilliseconds() >=10? String(Math.floor(time.getMilliseconds()/10)):''):null;
//                                                    (time.getMilliseconds() < 100 ? "0":"" ) + (time.getMilliseconds() < 10 ? "0":"" )+
//                                                    (time.getMilliseconds()/10 >0? String(time.getMilliseconds()/10):''):null;

	    else if(options == 's,ms')
//        return String(time.getMilliseconds());
		    return time? (time.getMinutes()>0? time.getMinutes().toString():'') +
				    (time.getSeconds() < 10 ? "0":"" ) + time.getSeconds().toString()+","+(time.getMilliseconds() < 100 ? "0":"" ) +
				    (time.getMilliseconds() < 10 ? "0":"" )+
				    (time.getMilliseconds() >=10? '':'0')+
				    Math.floor(time.getMilliseconds()):null;

        return time? (time.getMinutes() < 10 ? "0":"" )+ time.getMinutes().toString() +
													":" + (time.getSeconds() < 10 ? "0":"" ) + time.getSeconds().toString()+":"+
                                                    (time.getMilliseconds() < 100 ? "0":"" ) + (time.getMilliseconds() < 10 ? "0":"" )+
                                                    String(time.getMilliseconds()):null;
    }

    public static function formatIntTime(time:int, options:String = null):String{
        date.setTime(time);
        return formatTime(date, options);
    }


	public static function renderZeroScore(places:int, value:Number):String {
//		trace('renderZeroScore', places, value, Math.ceil(places - Math.log(value)/Math.LN10)-1);
		var str:String = '';
		for (var i:int = 0; i < Math.min(Math.ceil(places - Math.log(value)/Math.LN10)-1,4); i++) {
			//trace(i)
			str+='0';
		}

		return str+(value>0? value:'');
	}
}
}
