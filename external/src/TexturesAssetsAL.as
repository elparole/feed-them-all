/**
 * User: Elparole
 * Date: 21.11.12
 * Time: 19:03
 */
package
{
import flash.display.Bitmap;
import flash.display.BitmapData;

public class TexturesAssetsAL
{


//	[Embed(source = "bmp1.png")]
//	public static const mc1:Class;
//	[Embed(source = "bmp2.png")]
//	public static const mc2:Class;
//	[Embed(source = "bmp3.png")]
//	public static const mc3:Class;
//	[Embed(source = "bmp4.png")]
//	public static const mc4:Class;
//	[Embed(source = "bmp5.png")]
//	public static const mc5:Class;

	[Embed(source="../../main-app/src/monster_walk_DL_48_71_frame_1.png")]
	public static const mc1:Class;
	[Embed(source="../../main-app/src/monster_walk_DL_48_71_frame_2.png")]
	public static const mc2:Class;
	[Embed(source="../../main-app/src/monster_walk_DL_48_71_frame_3.png")]
	public static const mc3:Class;
	[Embed(source="../../main-app/src/monster_walk_DL_48_71_frame_4.png")]
	public static const mc4:Class;
	[Embed(source="../../main-app/src/monster_walk_DL_48_71_frame_5.png")]
	public static const mc5:Class;

	public var bm1:Bitmap;
	public var bm2:Bitmap;
	public var bm3:Bitmap;
	public var bm4:Bitmap;
	public var bm5:Bitmap;

	public function TexturesAssetsAL() {
		var t1:Bitmap = new Bitmap(new BitmapData(128,128,true,0));
		t1.bitmapData.draw(new mc1());
		bm1 = t1;
		t1 = new Bitmap(new BitmapData(128,128,true,0));
		t1.bitmapData.draw(new mc2());
		bm2 = t1;
		t1 = new Bitmap(new BitmapData(128,128,true,0));
		t1.bitmapData.draw(new mc3());
		bm3 = t1;
		t1 = new Bitmap(new BitmapData(128,128,true,0));
		t1.bitmapData.draw(new mc4());
		bm4 = t1;
		t1 = new Bitmap(new BitmapData(128,128,true,0));
		t1.bitmapData.draw(new mc5());
		bm5 = t1;
	}
}
}
