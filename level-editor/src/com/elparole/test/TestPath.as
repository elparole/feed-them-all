/**
 * User: Elparole
 * Date: 17.12.12
 * Time: 17:52
 */
package com.elparole.test
{
import _247gamesonline55_fla.MainTimeline;

import by.blooddy.crypto.serialization.JSON;

import com.bit101.components.CheckBox;

import com.bit101.components.ComboBox;
import com.bit101.components.HSlider;
import com.bit101.components.Label;
import com.bit101.components.NumericStepper;
import com.bit101.components.PushButton;
import com.bit101.components.Text;

import flash.display.Sprite;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.filters.GlowFilter;
import flash.geom.Point;
import flash.ui.Keyboard;
import flash.utils.Timer;

import utils.type.getClassByName;


[SWF(width='940', height='700', frameRate='60', backgroundColor='#A29A77')]
public class TestPath extends Sprite
{

	private var addPlaneBt:PushButton;
	private var playBt:PushButton;
	private var pauseBt:PushButton;
	private var loadBt:PushButton;
	private var timeSlider:HSlider;
	private var clonePlaneBt:PushButton;
	private var planeTypeCB:ComboBox;
	private var saveBt:PushButton;
	private var savePosBt:PushButton;
	private var removePosBt:PushButton;
	private var timeInc:PushButton;
	private var timeDec:PushButton;
	private var nsx:NumericStepper;
	private var nsy:NumericStepper;
	private var pathPointsCB:ComboBox;
	private var nsBornTime:NumericStepper;
	private var removePlaneBt:PushButton;
	private var timeLabel:Label;
	private var saveText:Text;
	private var isCurrentTime:CheckBox;
	private var moveAll:CheckBox;
	private var nsCloneDelay:NumericStepper;
	private var loadLevelCB:ComboBox;

	private var planes:Array = [];
	private var cont:Sprite;
	private var time:Number = 0;
	private var selectedPlane:Object;
	private var bf:GlowFilter = new GlowFilter(0xffffff);
	private var timer:Timer = new Timer(20);
	private const MAX_TIME:Number = 45000;
	private var pathIndex:int = -1;
	private var levelDef:*;
	private var typesSpeeds:Array = [0,1,1,1.11,1.11,1.33, 1.556, 1.667, 1.778, 1.778, 1.778, 1.778];
	private var deselectBt:PushButton;
	private var offsetXNS:NumericStepper;
	private var offsetYNS:NumericStepper;
	private var saveOffsetBt:PushButton;
	private var flipHorizBt:PushButton;
	private var allPlanesSwitch:CheckBox;
	private var allTTypePlanesSwitch:CheckBox;
	private var deltaSlider:HSlider;
	private var shiftAllBt:PushButton;
	private var deltaLb:Label;
	private var cutOver8Bt:PushButton;

	public function TestPaths() {
//		trace(Enemy1,Enemy2,Enemy3,Enemy4,Enemy5,Enemy6,Enemy7,Enemy8,Enemy9);
		addEventListener(Event.ADDED_TO_STAGE, onInit);
	}

	private function onInit(event:Event):void {
		cont = new Sprite();
		cont.y = 100;
		addChild(cont);

		cont.graphics.lineStyle(1);
		cont.graphics.drawRect(0,0,640,520);
		cont.graphics.lineStyle(0);

		loadBt = new PushButton(this, 650,10,'load');
		saveBt = new PushButton(this, 770,10,'save', onSaveLevel);
		addPlaneBt = new PushButton(this, 650,40,'addPlane', onAddPlane);
		planeTypeCB = new ComboBox(this, 770,40,'1',[1,2,3,4,5,6,7,8]);
		planeTypeCB.selectedItem = 1;
		planeTypeCB.addEventListener('select', onTypeChange);

		allPlanesSwitch = new CheckBox(this,878,40,'all');
		allTTypePlanesSwitch = new CheckBox(this,878,55,'all that type');

		removePlaneBt = new PushButton(this, 650,70,'remove plane', onRemovePlane);
		clonePlaneBt = new PushButton(this, 754,70,'clone', onClone);
		nsCloneDelay = new NumericStepper(this, 878,70);
		nsCloneDelay.width = 70;
		isCurrentTime = new CheckBox(this,858,70);
		moveAll = new CheckBox(this,670,160);


		playBt = new PushButton(this, 650,100,'play', onPlay);
		playBt.width = 80;
		pauseBt = new PushButton(this, 754,100,'pause', onPause);
		pauseBt.width = 80;
		deselectBt = new PushButton(this,838, 100, 'desel', onDeselect);
		deselectBt.width = 80;

		timeSlider = new HSlider(this, 670,140,onShiftTime);
		timeSlider.width = 250;
		timeSlider.minimum = 0;
		timeSlider.maximum = 45000;
		timeSlider.tick = 100;

		timeInc = new PushButton(this, 920,140,'+',onTimeInc);
		timeDec = new PushButton(this, 650,140,'-',onTimeDec);

		timeInc.width = 20;
		timeInc.height = 20;

		timeDec.width = 20;
		timeDec.height = 20;

		nsx = new NumericStepper(this,650, 170, onChangeX);
		nsy = new NumericStepper(this,730, 170, onChangeY);
		nsBornTime = new NumericStepper(this,810, 170, onChangeTime);
		nsBornTime.minimum = 0;
		nsBornTime.maximum = 45000;
		nsBornTime.step = 50;

		pathPointsCB = new ComboBox(this, 650, 200, '', []);
		pathPointsCB.addEventListener('select', onSelectPoint);
		/**
		 * offset all points
		 */
		offsetXNS = new NumericStepper(this,760,200);
		offsetYNS = new NumericStepper(this,840,200);
		saveOffsetBt = new PushButton(this,760,230, 'save offset',onSaveOffset);
		flipHorizBt = new PushButton(this,850,230,'flip horiz', onFlipHoriz);
		flipHorizBt.width = 80;
		// modification options
		savePosBt = new PushButton(this, 650,260, 'save pos', onSavePos);
		removePosBt = new PushButton(this, 650,230, 'remove pos', onRemovePos);

//		levelDef = new LevelEmbedded();
//		levelDef.parse();

		var levelsIds:Array = [];
		for (var i:int = 1; i < 50; i++) {
			levelsIds[i]=i;
		}
		loadLevelCB = new ComboBox(this,770,260,'',levelsIds);
		loadLevelCB.addEventListener('select', onLevelChange);

		timeLabel = new Label(this, 650, 120, '0');

		saveText = new Text(this, 650,350,'');
		saveText.width = 250;
		saveText.height = 250;

		deltaLb = new Label(this,670,300);
		deltaSlider = new HSlider(this, 670,320, onShiftDeltas);
		deltaSlider.width = 120;
		deltaSlider.minimum = 0.1;
		deltaSlider.maximum = 2;
		deltaSlider.tick = 0.05;
		deltaSlider.value = 1;
		cutOver8Bt = new PushButton(this, 850,290,'cut > 8s', onCutWave);
		shiftAllBt = new PushButton(this, 850,310,'shift deltas', onApplyShiftDeltas);

		timer.addEventListener(TimerEvent.TIMER, onTick);
		stage.addEventListener(KeyboardEvent.KEY_UP, onKeyControl)
	}

	private function onCutWave(...args):void {
		var toCut:Array = [];
		for each (var plane:Object in planes) {
			if(plane.t>8000){
				toCut.push(plane);
			}
		}
		for each (var plane:Object in toCut) {
			planes.splice(planes.indexOf(plane),1);
		}
		renderPlanes();
	}

	private function onShiftDeltas(...args):void {
		deltaLb.text = String(deltaSlider.value);
	}

	private function onApplyShiftDeltas(...args):void {
		for each (var plane:Object in planes) {
			plane.t = deltaSlider.value*plane.t;
		}
		renderPlanes();
	}


	private function onKeyControl(event:KeyboardEvent):void {
		switch(event.keyCode){
			case Keyboard.D:
				onClone();
				break;
			case Keyboard.R:
				onRemovePlane();
				break;
			case Keyboard.A:
				onAddPlane();
				break;
			case Keyboard.P:
				if(timer.running)
					onPause();
				else
					onPlay();
				break;
			case Keyboard.O:
				onSavePos();
				break;
			case Keyboard.F:
				onSaveOffset();
				break;
		}
	}

	private function onFlipHoriz(...args):void {
		if(!selectedPlane)
			return;

		for (var i:int = 1; i < selectedPlane.pathPts.length; i++) {
			(selectedPlane.pathPts[i] as Point).x -= 2*((selectedPlane.pathPts[i] as Point).x-(selectedPlane.pathPts[0] as Point).x);
		}
		renderPlanes();
	}

	private function onSaveOffset(...args):void {
		for (var i:int = 0; i < selectedPlane.pathPts.length; i++) {
			(selectedPlane.pathPts[i] as Point).offset(offsetXNS.value,offsetYNS.value);
		}
		renderPlanes();
	}

	private function onTypeChange(...args):void {
		if(!selectedPlane && !allPlanesSwitch.selected)
			return;

		if(allPlanesSwitch.selected){
			for each (var plane:Object in planes) {
				switchPlaneType(plane);
			}
		}else if(allTTypePlanesSwitch.selected){
			var typeToSwap:int = selectedPlane.type;
			for each (var plane:Object in planes) {
				if(plane.type == typeToSwap)
					switchPlaneType(plane);
			}
		}else{
			switchPlaneType(selectedPlane);
		}
		renderPlanes();
	}

	private function switchPlaneType(plane:Object):void {
		plane.skin.parent && cont.removeChild(plane.skin);
		plane.type = planeTypeCB.selectedItem;
		plane.v = typesSpeeds[planeTypeCB.selectedItem] * 40;
		var eclazz:Class = getClassByName('Enemy' + plane.type);
		plane.skin = new eclazz();
		plane.skin.addEventListener(MouseEvent.MOUSE_DOWN, onPlaneStartDrag);
	}

	private function onDeselect(...args):void {
		if(selectedPlane)
			selectedPlane.skin.filters = [];

		selectedPlane = null;
	}

	private function onClone(...args):void {
		if(!selectedPlane)
			return;

		var type:Number = Number(selectedPlane.type);
		var eclazz:Class = getClassByName('Enemy'+type);
//		var planeVO:Object = {skin:new eclazz(), x:100, y:100, v:index*20};
		var planeVO:Object = {skin:new eclazz(), x:selectedPlane.x, y:selectedPlane.y, v:selectedPlane.v,
			t:isCurrentTime.selected? (selectedPlane.t+nsCloneDelay.value):time,
			pathPts:[], type:type};
		for (var i:int = 0; i < selectedPlane.pathPts.length; i++) {
			planeVO.pathPts.push((selectedPlane.pathPts[i] as Point).clone());
		}
		planes.push(planeVO);
		cont.addChild(planeVO.skin);
		planeVO.skin.x = planeVO.x;
		planeVO.skin.y = planeVO.y;
		planeVO.skin.addEventListener(MouseEvent.MOUSE_DOWN, onPlaneStartDrag);

	}

	private function onLevelChange(...args):void {
		while (cont.numChildren > 0) {
			cont.removeChildAt(0);
		}

		planes.length = 0;
		for each (var plane:Object in levelDef.config['config'+loadLevelCB.selectedItem].planes) {
//			planes.push({type:plane.type, creationTime:plane.t, velocity:plane.v, points:plane.pathPts})

			var eclazz:Class = getClassByName('Enemy'+plane.type);
//		var planeVO:Object = {skin:new eclazz(), x:100, y:100, v:index*20};
			var planeVO:Object = {skin:new eclazz(), x:plane.x, y:plane.y, v:plane.velocity, t:plane.creationTime, pathPts:[], type:plane.type};

			for each (var pt:Object in plane.points) {
				planeVO.pathPts.push(new Point(pt.x, pt.y));
			}

			planes.push(planeVO);

			planeVO.skin.addEventListener(MouseEvent.MOUSE_DOWN, onPlaneStartDrag);
		}

		renderPlanes();
	}

	private function onRemovePlane(...args):void {
		if(!selectedPlane)
			return;

		if(selectedPlane.skin.parent)
			cont.removeChild(selectedPlane.skin);

		planes.splice(planes.indexOf(selectedPlane),1);
		renderPlanes();
	}

	private function onSaveLevel(...args):void {
		var pl:Array = [];
		for each (var plane:Object in planes) {
			pl.push({type:plane.type, creationTime:plane.t, velocity:plane.v, points:[]});
			for each (var pt:Point in plane.pathPts) {
				pl[pl.length-1].points.push({x:pt.x, y:pt.y});
			}
		}

		trace(JSON.encode({planes:pl}));
		saveText.text = JSON.encode({planes:pl});
	}

	private function onSelectPoint(...args):void {
		if(pathPointsCB.selectedItem){
			pathIndex = selectedPlane.pathPts.indexOf(pathPointsCB.selectedItem);
			selectedPlane.skin.x = selectedPlane.pathPts[pathIndex].x;//-selectedPlane.skin.width/2;
			selectedPlane.skin.y = selectedPlane.pathPts[pathIndex].y;//-selectedPlane.skin.height/2;
		}
		else
			pathIndex = -1;
	}

	private function onChangeTime(...args):void {
		if(!selectedPlane)
			return;

		selectedPlane.t = nsBornTime.value;
		renderPlanes();
	}


	private function onChangeY(...args):void {
		if(!moveAll.selected)
			selectedPlane.skin.y = nsy.value;
		else{
			var lasty:int = selectedPlane.pathPts[0].y;
			for (var i:int = 0; i < selectedPlane.pathPts.length; i++) {
				selectedPlane.pathPts[i].y = nsy.value + selectedPlane.pathPts[i].y-lasty;
			}
			selectedPlane.pathPts[0].y = nsy.value;
			renderPlanes();
		}
	}

	private function onChangeX(...args):void {
		if(!moveAll.selected)
			selectedPlane.skin.x = nsx.value;
		else{
			var lastX:int = selectedPlane.pathPts[0].x;
			for (var i:int = 0; i < selectedPlane.pathPts.length; i++) {
				selectedPlane.pathPts[i].x = nsx.value + selectedPlane.pathPts[i].x-lastX;
			}
			selectedPlane.pathPts[0].x = nsx.value;
			renderPlanes();
		}
	}

	private function onTick(event:TimerEvent):void {
		time+=20;
		timeSlider.value = time;
		timeLabel.text = String(time);
		renderPlanes();
		if(time>MAX_TIME){
			timer.stop();
		}
	}

	private function onPause(...args):void {
		timer.stop();
	}

	private function onTimeDec(...args):void {
		time = Math.max(0, time-100);
		timeSlider.value = time;
		timeLabel.text = String(time);
		renderPlanes();
	}

	private function onTimeInc(...args):void {
		time = Math.min(45000, time+100);
		timeSlider.value = time;
		timeLabel.text = String(time);
		renderPlanes();
	}

	private function onRemovePos(...args):void {
		if(!selectedPlane)
			return;

		if(pathIndex <0 || pathIndex>selectedPlane.pathPts.length){

		}else{
			selectedPlane.pathPts.splice(pathIndex,1);
		}

		pathPointsCB.items = selectedPlane.pathPts;
		renderPlanes();
	}

	private function onSavePos(...args):void {
		if(!selectedPlane)
			return;

		if(pathIndex <0 || pathIndex>=selectedPlane.pathPts.length){
			selectedPlane.pathPts.push(new Point(selectedPlane.skin.x,  selectedPlane.skin.y));
		}else{
			selectedPlane.pathPts[pathIndex].x = selectedPlane.skin.x;
			selectedPlane.pathPts[pathIndex].y = selectedPlane.skin.y;
		}

		pathPointsCB.items = selectedPlane.pathPts;
		pathPointsCB.selectedIndex = -1;

		renderPlanes();
	}

	private function onShiftTime(...args):void {
		time = timeSlider.value;
		timeLabel.text = String(time);
		renderPlanes();
	}

	private function renderPlanes():void {
		while (cont.numChildren > 0) {
			cont.removeChildAt(0);
		}

		for each (var plane:Object in planes) {
			renderPlane(plane, time);
		}
		renderPaths();
	}

	private function renderPlane(plane:Object, time:Number):void {
		if(plane.t<=time){

			var st:Number = time - plane.t;
			var ptIndex:int = 0;

			cont.addChild(plane.skin);
			if(plane.pathPts.length<1){
				plane.pathPts.push(new Point(plane.x, plane.y));
			}

			plane.skin.alpha = 0.5;
			plane.skin.x = plane.pathPts[0].x;
			plane.skin.y = plane.pathPts[0].y;
			while(st>0){
				ptIndex++;
				if(ptIndex>=plane.pathPts.length)
					return;

				var dist:Number = GameUtils.distancePt(plane.pathPts[ptIndex], plane.pathPts[ptIndex-1]);
				var dt:Number = 100*dist/plane.v;
				st -= dt;
				plane.skin.x = plane.pathPts[ptIndex].x;
				plane.skin.y = plane.pathPts[ptIndex].y;

				if(st<0){
					var dx:Number = plane.pathPts[ptIndex].x - plane.pathPts[ptIndex-1].x;
					var dy:Number = plane.pathPts[ptIndex].y - plane.pathPts[ptIndex-1].y;

					plane.skin.x -= Math.round((-st)/dt*dx);
					plane.skin.y -= Math.round((-st)/dt*dy);
				}
			}
		}
	}

	private function renderPaths():void {
		cont.graphics.clear();
		cont.graphics.lineStyle(1);
		cont.graphics.drawRect(0,0,640,520);
		cont.graphics.lineStyle(0);

		for each (var pl:Object in planes) {
			if (pl.skin.parent) {
				cont.graphics.beginFill(pl == selectedPlane? 0xaa0000:0x00aa00);
				for each (var point:Point in pl.pathPts) {
					cont.graphics.drawRect(point.x+pl.skin.width/2, point.y+pl.skin.height/2, 5,5)
				}
				cont.graphics.endFill();
			}
		}
	}

	private function onAddPlane(...args):void {
		var type:Number = Number(planeTypeCB.selectedItem);
		var eclazz:Class = getClassByName('Enemy'+type);
//		var planeVO:Object = {skin:new eclazz(), x:100, y:100, v:index*20};
		var planeVO:Object = {skin:new eclazz(), x:320, y:-100, v:typesSpeeds[type]*40, t:time, pathPts:[], type:type};
		planes.push(planeVO);
		cont.addChild(planeVO.skin);
		planeVO.skin.x = planeVO.x;
		planeVO.skin.y = planeVO.y;
		planeVO.skin.addEventListener(MouseEvent.MOUSE_DOWN, onPlaneStartDrag);
//		planeVO.addEventListener(MouseEvent.CLICK, onPlaneSelected)

	}

	private function onPlaneStartDrag(event:MouseEvent):void {
		if(selectedPlane){
			selectedPlane.skin.filters = [];
			if(selectedPlane.pathPts.length ==0){
				selectedPlane.pathPts.push(new Point(selectedPlane.x,selectedPlane.y));
			}
		}
		var skin:Sprite = event.target as Sprite;
		for each (var object:Object in planes) {
			if(object.skin == skin){
				selectedPlane = object;
				selectedPlane.skin.filters = [bf];
			}
		}
		stage.addEventListener(MouseEvent.MOUSE_UP, onStopDragPlane);
		selectedPlane.skin.startDrag();
	}

	private function onStopDragPlane(event:MouseEvent):void {
		selectedPlane.skin.stopDrag();
		selectedPlane.x = selectedPlane.skin.x;
		selectedPlane.y = selectedPlane.skin.y;
		stage.removeEventListener(MouseEvent.MOUSE_UP, onStopDragPlane);

		if(!moveAll.selected){
			nsx.value = selectedPlane.skin.x;
			nsy.value = selectedPlane.skin.y;
		}else{
			nsx.value = selectedPlane.pathPts[0].x;
			nsy.value = selectedPlane.pathPts[0].y;
		}

		nsBornTime.value = selectedPlane.t;

		if(pathPointsCB.items !=selectedPlane.pathPts){
			pathPointsCB.items = selectedPlane.pathPts;
			pathPointsCB.selectedIndex = -1;
			pathIndex = -1;
		}

	}

//	private function onPlaneSelected(event:MouseEvent):void {

//	}

	private function onPlay(...args):void {
		timer.start();
	}
}
}
