/**
 * User: Elparole
 * Date: 05.03.13
 * Time: 20:54
 */
package com.elparole.levelEditor
{
import com.elparole.levelEditor.view.PPVLevelPreview;

import flash.display.Sprite;
import flash.events.Event;

[SWF(width = "800", height ="600")]
public class TestPPV extends Sprite
{
	public function TestPPV() {
		addEventListener(Event.ADDED_TO_STAGE, init)
	}

	private function init(event:Event):void {

//		this.graphics.beginFill(0x00ff00);
//		this.graphics.drawRect(0,0,640,480);
//		this.graphics.endFill();

		addChild(new PPVLevelPreview());

	}
}
}
