/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 19:12
 */
package com.elparole.levelEditor.model
{
import com.blackmoondev.load.signals.LoaderStepFailedSignal;
import com.blackmoondev.load.signals.LoaderStepFinishedSignal;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.model.vo.ProjConfigVO;
import com.elparole.levelEditor.signals.notifications.DragModeSelectedSignal;
import com.elparole.levelEditor.signals.notifications.FloorItemAddedSignal;
import com.elparole.levelEditor.signals.notifications.FloorItemRemovedSignal;
import com.elparole.levelEditor.signals.notifications.FloorModeSelectedSignal;
import com.elparole.levelEditor.signals.notifications.ItemsModeSelectedSignal;
import com.elparole.levelEditor.signals.notifications.ObstaclesModeSelectedSignal;
import com.elparole.levelEditor.signals.notifications.SnapToGridChangedSignal;

import org.robotlegs.oil.async.Promise;

public class MouseModeModel
{

	[Inject]
	public var floorModeSelected:FloorModeSelectedSignal;
	
	[Inject]
	public var itemsModeSelected:ItemsModeSelectedSignal;
	
	[Inject]
	public var obstaclesModeSelected:ObstaclesModeSelectedSignal;

	[Inject]
	public var dragModeSelected:DragModeSelectedSignal;

	[Inject]
	public var snapToGridChanged:SnapToGridChangedSignal;

	public static const FLOOR_MODE:String ='floorMode';
	public static const ITEMS_MODE:String ='itemsMode';
	public static const ADD_MODE:String ='addMode';
	public static const WAVE_EDIT_MODE:String ='waveEditMode';
	public static const ERASE_MODE:String ='eraseMode';
	public static const ENABLE_MODE:String ='enableMode';
	public static const DISABLE_MODE:String ='disableMode';
	public static const DRAG_MODE:String = 'dragMode';
	public static const OBSTACLE_MODE:String = 'obstacleMode';

	private var _floorMode:Boolean = false;
	private var _itemsMode:Boolean = false;
	private var _enableMode:Boolean = false;
	private var _disableMode:Boolean = false;
	private var _addMode:Boolean = true;
	private var _eraseMode:Boolean = false;
	private var _dragMode:Boolean = false;
	private var _snapToGrid:Boolean = false;
	private var _obstacleMapMode:Boolean = false;
	private var _waveEditMode:Boolean= false;


	public function get floorMode():Boolean {
		return _floorMode;
	}

	public function set floorMode(value:Boolean):void {
		_floorMode = value;
		floorModeSelected.dispatch(value);
	}

	public function get itemsMode():Boolean {
		return _itemsMode;
	}

	public function set itemsMode(value:Boolean):void {
		_itemsMode = value;
		itemsModeSelected.dispatch(value);
	}

	public function get addMode():Boolean {
		return _addMode;
	}

	public function set addMode(value:Boolean):void {
		_addMode = value;
	}

	public function get eraseMode():Boolean {
		return _eraseMode;
	}

	public function set eraseMode(value:Boolean):void {
		_eraseMode = value;
	}

	public function MouseModeModel() {

	}


	public function changeMode(propertyId:String, value:Boolean):void {
//		floorMode = false;
//		itemsMode = false;
//		dragMode = false;
//		enableMode = false;
//		disableMode = false;

		switch(propertyId){
			case FLOOR_MODE:
					itemsMode = !value;
					obstacleMapMode = !value;
					waveEditMode = !value;
					floorMode = value;
				break;
			case ITEMS_MODE:
					floorMode = !value;
					obstacleMapMode = !value;
					waveEditMode = value;
					itemsMode = value;
				break;
			case OBSTACLE_MODE:
					itemsMode = !value;
					floorMode = !value;
					waveEditMode = !value;
					obstacleMapMode = value;
				break;
			case WAVE_EDIT_MODE:
					itemsMode = !value;
					floorMode = !value;
					obstacleMapMode = !value;
					waveEditMode = value;
				break;
			case ADD_MODE:
					eraseMode = false;
					dragMode = false;
					addMode = value;
				break;
			case ERASE_MODE:
					addMode = false;
					dragMode = false;
					eraseMode = value;
				break;
			case DRAG_MODE:
					addMode = false;
					eraseMode = false;
					enableMode = !value;
					disableMode = !value;
					dragMode = value;
				break;
			case ENABLE_MODE:
					disableMode =!value;
					dragMode =!value;
					enableMode = value;
				break;
			case DISABLE_MODE:
					enableMode = !value;
					dragMode =!value;
					disableMode = value;

				break;
		}
	}

	public function set snapToGrid(value:Boolean):void {
		_snapToGrid = value;
		snapToGridChanged.dispatch(value);
	}

	public function get snapToGrid():Boolean {
		return _snapToGrid;
	}

	public function get dragMode():Boolean {
		return _dragMode;
	}

	public function set dragMode(value:Boolean):void {
		_dragMode = value;
		dragModeSelected.dispatch(value);
	}

	public function get enableMode():Boolean {
		return _enableMode;
	}

	public function set enableMode(value:Boolean):void {
		_enableMode = value;
	}

	public function get disableMode():Boolean {
		return _disableMode;
	}

	public function set disableMode(value:Boolean):void {
		_disableMode = value;
	}

	public function get obstacleMapMode():Boolean {
		return _obstacleMapMode;
	}

	public function set obstacleMapMode(value:Boolean):void {
		_obstacleMapMode = value;
		obstaclesModeSelected.dispatch(value);
	}

	public function get waveEditMode():Boolean {
		return _waveEditMode;
	}

	public function set waveEditMode(waveEditMode:Boolean):void {
		_waveEditMode = waveEditMode;
	}
}
}
