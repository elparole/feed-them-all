/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 19:12
 */
package com.elparole.levelEditor.model
{
import com.blackmoondev.load.signals.LoaderStepFailedSignal;
import com.blackmoondev.load.signals.LoaderStepFinishedSignal;
import com.elparole.levelEditor.model.vo.ProjConfigVO;

import org.robotlegs.oil.async.Promise;

public class MessegeModel
{
	[Inject]
	public var lsatep:LoaderStepFailedSignal;

	[Inject]
	public var lsistep:LoaderStepFinishedSignal;
	
	public function MessegeModel() {
	}

	public function onStepError(err:Object, prom:Promise):void {
		lsatep.dispatch(prom? prom.error:null);
	}

//	public function onStepComplete(err1:Object, err2:Object):void {
	public function onStepComplete(config:*, err:Object):* {
		lsistep.dispatch();
		return config;
	}

	public function onFileSystemError(err:Object, prom:Promise):void {
		trace('onFileSystemError');
	}
}
}
