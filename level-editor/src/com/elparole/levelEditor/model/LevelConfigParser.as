/**
 * User: Elparole
 * Date: 13.03.13
 * Time: 17:14
 */
package com.elparole.levelEditor.model
{
import com.elparole.levelEditor.model.vo.ItemOnPosVO;
import com.elparole.levelEditor.model.vo.LevelConfig;

import flash.geom.Point;

import flash.utils.Dictionary;

import org.casalib.math.geom.Point3d;

public class LevelConfigParser
{
	private var assetsIdsToVOs:Dictionary;
	public function LevelConfigParser() {
	}

	public function fillItems(config:LevelConfig, err:Object):LevelConfig {
	var itemOnPosVos:Array = [];

	for each (var itemObj:Object in config.floorItems) {
		itemOnPosVos.push(new ItemOnPosVO(
				assetsIdsToVOs[itemObj.skinId+'_'+itemObj.dirId],
				new Point(itemObj.posX,itemObj.posZ)));
	}
	config.floorItems = itemOnPosVos;

	itemOnPosVos = [];
	for each (itemObj in config.items) {
		itemOnPosVos.push(new ItemOnPosVO(
				assetsIdsToVOs[itemObj.skinId+'_'+itemObj.dirId],
				new Point(itemObj.posX,itemObj.posZ)));
	}
	config.items = itemOnPosVos;
//	levelSize = config.size.clone()


	return config;
}
}
}
