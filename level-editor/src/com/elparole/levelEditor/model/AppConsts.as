/**
 * User: Elparole
 * Date: 08.03.13
 * Time: 17:19
 */
package com.elparole.levelEditor.model
{
public class AppConsts
{
	public static const ENABLED_ALPHA:Number = 0.9;
	public static const DISABLED_ALPHA:Number = 0.5;

	public function AppConsts() {
	}
}
}
