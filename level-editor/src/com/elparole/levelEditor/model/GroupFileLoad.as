/**
 * User: Elparole
 * Date: 04.04.13
 * Time: 11:49
 */
package com.elparole.levelEditor.model
{
import com.elparole.levelEditor.model.vo.BitmapFileLoad;

import flash.display.Loader;

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IOErrorEvent;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;

public class GroupFileLoad extends EventDispatcher{
	
	public var loads:Array = [];
	public var loadedLoads:Array = [];
	public var failedLoads:Array = [];
	private var file:File;
	private var fileStream:FileStream;
	private var loadIndex:int;
	
	public function GroupFileLoad() {
		
	}
	
	public function addLoad(load:BitmapFileLoad):void{
		loads.push(load);
	}
	
	public function start():void{
		file = new File();
		loadIndex = 0;
		file.resolvePath(loads[loadIndex].path);
		if(fileStream)
			fileStream.close();
		fileStream = new FileStream();
		fileStream.openAsync(file, FileMode.READ);
		fileStream.addEventListener(IOErrorEvent.IO_ERROR, onReadError);
		fileStream.addEventListener(Event.COMPLETE, onReadComplete);
		fileStream.position = 0;
	}

	private function onReadComplete(event:Event):void {
		var loader:Loader = new Loader();
		loader.loadBytes(file.data);
		loads[loadIndex].bitmap = file.data
	}

	private function onReadError(event:IOErrorEvent):void {

	}
}
}
