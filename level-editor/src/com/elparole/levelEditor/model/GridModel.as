/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 19:12
 */
package com.elparole.levelEditor.model
{
import com.elparole.feedThemAll.model.GameUtils;

import flash.geom.Point;

import org.casalib.math.geom.Point3d;

public class GridModel
{
	public var xGrid:int = 50;//72;//32;
	public var zGrid:int = 50;//72;//32;
	
	public function GridModel() {
		
	}

	public function snap(coords:Point):void {
		coords.x = Math.round((coords.x)/50)*50;
		coords.y = Math.round((coords.y)/50)*50;
	}


}
}
