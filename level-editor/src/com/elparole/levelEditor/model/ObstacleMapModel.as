/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 19:12
 */
package com.elparole.levelEditor.model
{

import com.elparole.components.DisplayBlis;
import com.elparole.feedThemAll.AppConsts;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.ISolvable;
import com.elparole.feedThemAll.model.ItemVO;
import com.elparole.feedThemAll.model.MapModel;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.levelEditor.model.vo.MouseRequestVO;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.model.vo.ItemOnPosVO;
import com.elparole.levelEditor.model.vo.LevelConfig;
import com.elparole.levelEditor.model.vo.LevelsConfigVO;
import com.elparole.levelEditor.signals.notifications.FloorItemAddedSignal;
import com.elparole.levelEditor.signals.notifications.FloorItemRemovedSignal;
import com.elparole.levelEditor.signals.notifications.ItemAddedSignal;
import com.elparole.levelEditor.signals.notifications.ItemRemovedSignal;
import com.elparole.levelEditor.signals.notifications.MapSizeChangedSignal;
import com.elparole.levelEditor.signals.notifications.ObstacleChangedSignal;
import com.elparole.levelEditor.signals.notifications.RefreshPreviewSignal;
import com.elparole.levelEditor.signals.notifications.SetupTabSignal;

import flash.geom.Point;
import flash.geom.Rectangle;

import flash.utils.Dictionary;

import org.casalib.math.geom.Point3d;
import org.papervision3d.objects.primitives.Plane;
import org.robotlegs.oil.async.Promise;

public class ObstacleMapModel  implements ISolvable
{
	[Inject]
	public var obstacleChanged:ObstacleChangedSignal;

	[Inject]
	public var itemAdded:ItemAddedSignal;

	[Inject]
	public var itemRemoved:ItemRemovedSignal;

	[Inject]
	public var gameUtils:GameUtils;
	
	[Inject]
	public var gridModel:GridModel;

	[Inject]
	public var refresh:RefreshPreviewSignal;

	[Inject]
	public var mapSizeChanged:MapSizeChangedSignal;


//	[Inject]
//	public var lsatep:LoaderStepFailedSignal;

//	[Inject]
//	public var lsistep:LoaderStepFinishedSignal;

	public var obstacleItems:Dictionary = new Dictionary();

	public var obstacleMap:Dictionary = new Dictionary();
	
	public var mapModel:MapModel = new MapModel();

	
	public function ObstacleMapModel() {

	}

	public function clear():void{
		for each (var planeItem:IsoEditItem in obstacleItems) {
			itemRemoved.dispatch(planeItem,false);
		}
		obstacleItems = new Dictionary();
		refresh.dispatch();
	}

	public function setupDefaultObstacleMap(size:Point):void{

		for (var ix:int = 0; ix < size.y; ix++) {
			for (var iz:int = 0; iz < size.x; iz++) {
				obstacleMap[ix+'_'+iz] = true;
			}
		}
	}

	public function showObstacleMap(size:Point):void{
		for (var ix:int = 0; ix < size.y; ix++) {
			for (var iz:int = 0; iz < size.x; iz++) {
				obstacleItems[ix+'_'+iz] = getNewTile(obstacleMap[ix+'_'+iz]);
				obstacleItems[ix+'_'+iz].position = new Point3d(
						ix*gridModel.xGrid/* + (900)/17.14*/,
						-1,
						iz*gridModel.zGrid/* + (900)/17.14*/);
				itemAdded.dispatch(obstacleItems[ix+'_'+iz],false);
			}
		}
		refresh.dispatch();
	}

	private function getNewTile(walkable:Boolean):IsoEditItem {
		var disp:DisplayBlis = new DisplayBlis(walkable? AnimsIDs.TILE_GREEN:AnimsIDs.TILE_RED, AnimsIDs.IDLE, AnimsIDs.DR,
				gameUtils.getSkinByID(walkable? AnimsIDs.TILE_GREEN:AnimsIDs.TILE_RED, AnimsIDs.IDLE, AnimsIDs.DR, 0),
				gameUtils.getSkinOffset(walkable? AnimsIDs.TILE_GREEN:AnimsIDs.TILE_RED, AnimsIDs.IDLE, AnimsIDs.DR, 0));
		var fi:IsoEditItem = new IsoEditItem(disp);
		return fi;
	}

	public function enableTile(request:MouseRequestVO):void {
		var gx:int = Math.round((request.coordsIso.x/*-(900)/17.14*/)/gridModel.xGrid);
		var gz:int = Math.round((request.coordsIso.y/*-(900)/17.14*/)/gridModel.zGrid);
		if(!obstacleItems[gx+'_'+gz])
			return;
		itemRemoved.dispatch(obstacleItems[gx+'_'+gz],true);

		obstacleMap[gx+'_'+gz] = true;

		obstacleItems[gx+'_'+gz] = getNewTile(true);
		obstacleItems[gx+'_'+gz].position = new Point3d(
				gx*gridModel.xGrid /*+ (900)/17.14*/,
				-1,
				gz*gridModel.zGrid/* + (900)/17.14*/);
		itemAdded.dispatch(obstacleItems[gx+'_'+gz],true);
		mapModel.setWalkable(gx, gz, true);
	}

	public function disableTile(request:MouseRequestVO):void {
		var gx:int = Math.round((request.coordsIso.x/*-(900)/17.14*/)/gridModel.xGrid);
		var gz:int = Math.round((request.coordsIso.y/*-(900)/17.14*/)/gridModel.zGrid);
		if(!obstacleItems[gx+'_'+gz])
			return;
		itemRemoved.dispatch(obstacleItems[gx+'_'+gz],true);
		obstacleMap[gx+'_'+gz] = false;
		obstacleItems[gx+'_'+gz] = getNewTile(false);
		obstacleItems[gx+'_'+gz].position = new Point3d(
				gx*gridModel.xGrid /*+ (900)/17.14*/,
				-1,
				gz*gridModel.zGrid /*+ (900)/17.14*/);
		itemAdded.dispatch(obstacleItems[gx+'_'+gz],true);
		mapModel.setWalkable(gx, gz, false);
	}

	public function setObstacleMap(config:LevelConfig, err:Object):*{
		if(!config)
			return config;

		obstacleMap = config.obstacleMap;
		mapModel.parseObstacleMap(config,null);
		mapSizeChanged.dispatch(config.size);
		return config;
	}


	public function solve(ex:int, ez:int, sx:int, sz:int):Array {
		return mapModel.solve(ex,ez, sx, sz);
	}

//	public function addItemVoToPos(item:ItemVO = null,pos:Point3d = null):void {
//		addFloorItem(null,pos);
//	}
//
//	public function addFloorItem(item:PlaneItem = null,pos:Point3d = null):void {
//		item ||= createItemFrom(selectedItem);
//		item.position = pos.clone();
//		items.push(item);
//		floorItemAdded.dispatch(item);
//	}
//
//	private function createItemFrom(item:ItemVO):PlaneItem {
//		var disp:Display = new Display(item.id, item.animId, item.dirId,
//				gameUtils.getSkinByID(item.id, item.animId, item.dirId, 0),
//				gameUtils.getSkinOffset(item.id, item.animId, item.dirId, 0));
//		var fi:PlaneItem = new PlaneItem(disp);
//		return fi;
//	}
//
//	public function removeFloorItem(item:PlaneItem):void {
//		items.splice(items.indexOf(item),1);
//		floorItemRemoved.dispatch(item);
//	}
//
//	public function onFileSystemError(err:Object, prom:Promise):void {
//		trace('onFileSystemError');
//	}
//
//	public function removeFloorItemByPlane(plane:Plane):void {
//		for each (var floorItem:PlaneItem in items) {
//			if(floorItem.display.plane == plane)
//				return removeFloorItem(floorItem);
//		}
//	}
//
//	public function clear(config:Object = null, err:Object = null):Object {
//		while(items.length>0){
//			removeFloorItem(items[0]);
//		}
//		return config;
//	}
//
//	public function addLevelFloors(config:LevelConfig, err:Object):*{
//		for each (var itemOnPosVO:ItemOnPosVO in config.floorItems) {
//			addItemVoToPos(itemOnPosVO.itemVO,itemOnPosVO.pos);
//		}
//		return config;
//	}
//
//	public function setItemsAlpha(value:Number):void {
//		for each (var planeItem:PlaneItem in items) {
//			planeItem.display.plane.alpha = value;
//		}
//	}
	public function fitToRect(coordsIso:Point):Boolean {
		return mapModel.bounds.contains(
				coordsIso.x/com.elparole.feedThemAll.AppConsts.xGrid,
				coordsIso.y/com.elparole.feedThemAll.AppConsts.zGrid);
	}
}
}
