/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 19:12
 */
package com.elparole.levelEditor.model
{

import com.elparole.components.DisplayBlis;
import com.elparole.feedThemAll.components.PathComponent;
import com.elparole.feedThemAll.controller.PathManager;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.test.IsoAnimItem;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.model.vo.ChildVO;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.model.vo.LevelConfig;
import com.elparole.levelEditor.model.vo.PathFollowingIsoItem;
import com.elparole.levelEditor.signals.notifications.ChildSelectionChangedSignal;
import com.elparole.levelEditor.signals.notifications.ItemAddedSignal;
import com.elparole.levelEditor.signals.notifications.ItemRemovedSignal;
import com.elparole.levelEditor.signals.notifications.RefreshPreviewSignal;
import com.elparole.levelEditor.signals.notifications.SourceSelectChangedSignal;
import com.elparole.levelEditor.signals.notifications.TickSignal;
import com.elparole.randomIntInRange;

import flash.events.TimerEvent;
import flash.utils.Dictionary;
import flash.utils.Timer;

import org.casalib.math.geom.Point3d;

import spark.effects.supportClasses.AnimateInstance;

import utils.object.isSimple;

public class WavesModel
{
	[Inject]
	public var itemAdded:ItemAddedSignal;
	
	[Inject]
	public var itemRemoved:ItemRemovedSignal;

	[Inject]
	public var gameUtils:GameUtils;

	[Inject]
	public var sourceSelectedChange:SourceSelectChangedSignal;

	[Inject]
	public var childSelectionChanged:ChildSelectionChangedSignal;

	[Inject]
	public var refresh:RefreshPreviewSignal;
	
	[Inject]
	public var obstacleMap:ObstacleMapModel;

	[Inject]
	public var tick:TickSignal;
	private var destId:int;

//	[Inject]
//	public var lsatep:LoaderStepFailedSignal;

//	[Inject]
//	public var lsistep:LoaderStepFinishedSignal;

	public var displayIdsToPaths:Dictionary = new Dictionary();
	public var sourceChildren:Dictionary = new Dictionary();
	public var sources:Dictionary = new Dictionary()
	public var selectedSource:IsoEditItem;
	public var selectedChild:IsoEditItem;
	private var targets:Array=  [];
	private var pathManager:PathManager = new PathManager();
	private var timer:Timer = new Timer(1000/20);

	
	public function WavesModel() {

	}

	public function addSourceChild(skinId:String, sourceId:int, time:int, addToContainer:Boolean = true):void{

		if(sourceId<0){
			sourceId = selectedSource.display.id;
		}else if(skinId == AnimsIDs.MONSTER)
			skinId = selectedChild.display.skinId;

		var disp:DisplayBlis = new DisplayBlis(skinId, AnimsIDs.IDLE, 'DR',
				gameUtils.getSkinByID(skinId, AnimsIDs.IDLE, 'DR', 0),
				gameUtils.getSkinOffset(skinId, AnimsIDs.IDLE, 'DR', 0));
//		var fi:IsoEditItem = new IsoEditItem(disp);



		var pc:PathComponent = getPathWithTarget(sourceId);
		var editItem:IsoEditItem = new IsoEditItem(disp,new Point3d(pc.startPt.x, 0,pc.startPt.y));
		var child:PathFollowingIsoItem = new PathFollowingIsoItem(pc,editItem,
				destId? (targets[destId] as IsoEditItem).display.id:-1);
		
		child.startTime = time;
		sourceChildren[sourceId] ||=[];
		sourceChildren[sourceId].push(child);
		displayIdsToPaths[child.editItem.display.id] = child;

		addToContainer && itemAdded.dispatch(editItem,true);
		addToContainer && editItem.display.renderItem.drawSelectedBD();
	}
	
	public function setPositionsInTime(time:int):void{
		for each (var array:Array in sourceChildren) {
			for each (var pathFollowingIsoItem:PathFollowingIsoItem in array) {
				(pathFollowingIsoItem.editItem.display.renderItem as IsoAnimItem).visible =
						time>pathFollowingIsoItem.startTime;
				if((pathFollowingIsoItem.editItem.display.renderItem as IsoAnimItem).visible){
					pathFollowingIsoItem.path.timeElapsed = (time-pathFollowingIsoItem.startTime)/1000;
					pathFollowingIsoItem.path.currentIndex = 0;
					pathManager.solvePosition(pathFollowingIsoItem.path);

					var item:IsoAnimItem = (pathFollowingIsoItem.editItem.display.renderItem as IsoAnimItem);
					item.isoX = -pathManager.px;
					item.isoY = -pathManager.pz;
					pathFollowingIsoItem.editItem.display.dirId = pathManager.dir;
	//				(pathFollowingIsoItem.editItem.display.renderItem
					pathFollowingIsoItem.editItem.display.setBitmap(gameUtils.getSkinByID(
							pathFollowingIsoItem.editItem.display.skinId,
							pathFollowingIsoItem.editItem.display.animId,
							pathFollowingIsoItem.editItem.display.dirId,0));

					if(pathFollowingIsoItem.path.pathPts[pathFollowingIsoItem.path.pathPts.length - 1].distanceFromStart <
							pathFollowingIsoItem.path.timeElapsed * pathFollowingIsoItem.path.velocity)
						(pathFollowingIsoItem.editItem.display.renderItem as IsoAnimItem).visible = false;
				}
//						-pathFollowingIsoItem.path.startPt.x+(time-pathFollowingIsoItem.startTime)/100;
			}
		}
		refresh.dispatch();
	}
	
	public function addSource(item:IsoEditItem):void {
		item.display.renderItem.drawSelectedBD();
		sources[item.display.id]=item;
	}

	public function removeSource(item:IsoEditItem):void {
		if(item && AnimsIDs.isMonsterCave(item.display.skinId)){
			delete sources[item.display.id];
			delete sourceChildren[item.display.id];
		}
//		for each (var isoItem:PathFollowingIsoItem in sourceChildren[item.display.id]) {
//			remov
//		}
//		sources.splice(sources.indexOf(item),1);
	}

	public function removeTarget(item:IsoEditItem):void {
		if(item && item.display.skinId !=AnimsIDs.SHEEP)
			return;

		targets.splice(targets.indexOf(item),1);
		for (var sourceId:String in sourceChildren) {
			for each (var pathFollowingIsoItem:PathFollowingIsoItem in sourceChildren[sourceId]) {
				if(pathFollowingIsoItem.sheepDestId<0 || !isValidTarget(pathFollowingIsoItem)){
					pathFollowingIsoItem.path = getPathWithTarget(int(sourceId));
				}
			}
		}
	}

	public function clear(config:Object = null, err:Object = null):Object {
		for (var key:String in sources) {
			removeSource(sources[key]);
		}
		targets.length = 0;
		sourceChildren = new Dictionary();

		return config;
	}

	public function addLevelSrcsAndTrgts(config:LevelConfig, err:Object):*{
		if(!config)
			return config;

		var sourcesNum:int = 0;
		for each (var item:IsoEditItem in config.items) {
			if(AnimsIDs.isMonsterCave(item.display.skinId)){
				addSource(item);
				sourcesNum++;
			}else if(item.display.skinId==AnimsIDs.SHEEP){
				addTarget(item);
			}
		}

		if(sourcesNum>0){
			for each (var object:Object in config.childrenList) {
				addSourceChild(
						object.skinId,
						getSourceOnPos(object.startX, object.startY),
						object.startTime,
						false);
			}
		}

		return config;
	}

	private function getSourceOnPos(startX:int, startY:int):int {
		for (var key:String in sources) {
			if((sources[key] as IsoEditItem).position.x == startX &&
					(sources[key] as IsoEditItem).position.z ==startY){
				return (sources[key] as IsoEditItem).display.id;
			}
		}
		return -1;
	}

	public function addTarget(item:IsoEditItem):void {
		item.display.renderItem.drawSelectedBD();
		targets.push(item);
		fillDestinations();
	}

	public function fillDestinations(force:Boolean = false):void {
		for (var sourceId:String in sourceChildren) {
			for each (var pathFollowingIsoItem:PathFollowingIsoItem in sourceChildren[sourceId]) {
				if (force || (pathFollowingIsoItem.sheepDestId < 0 || !isValidTarget(pathFollowingIsoItem))) {
					pathFollowingIsoItem.path = getPathWithTarget(int(sourceId));
				}
			}
		}
	}

	private function getPathWithTarget( sourceId:int):PathComponent {
		var pc:PathComponent;

		if(targets.length>0){
				destId = randomIntInRange(0,targets.length-1);
			pc = gameUtils.getPathComponent(
					(sources[sourceId] as IsoEditItem).position.x,
					(sources[sourceId] as IsoEditItem).position.z,
					(targets[destId] as IsoEditItem).position.x,
					(targets[destId] as IsoEditItem).position.z,
					obstacleMap);
		}else{
			pc = gameUtils.getPathComponent(
					(sources[sourceId] as IsoEditItem).position.x,
					(sources[sourceId] as IsoEditItem).position.z,
					(sources[sourceId] as IsoEditItem).position.x,
					(sources[sourceId] as IsoEditItem).position.z,
					obstacleMap);
		}
		
		return pc;
	}

	private function isValidTarget(pathFollowingIsoItem:PathFollowingIsoItem):Boolean {
		var isvalid:Boolean = false;
		for each (var isoEditItem:IsoEditItem in targets) {
			if(isoEditItem.display.id == pathFollowingIsoItem.sheepDestId){
				return true;
			}
		}
		return false;
	}

	public function getPFIsoItemFromIsoAnim(plane:IsoAnimItem,sourceIdMode:Boolean = false):*{
		for (var key:String in sourceChildren) {
			for each (var pfi:PathFollowingIsoItem in sourceChildren[key]) {
				if(pfi.editItem.display.renderItem == plane)
					return sourceIdMode? int(key):pfi;
			}
		}
		
		return sourceIdMode? -1:pfi;
	}
	
	public function getSourceChildFromIsoAnim(plane:IsoAnimItem):IsoEditItem{
		var item:PathFollowingIsoItem = getPFIsoItemFromIsoAnim(plane);
		return item? item.editItem:null;
	}
	
	public function getIsoEditFromIsoAnim(plane:IsoAnimItem):IsoEditItem{
		for each (var floorItem:IsoEditItem in sources) {
			if(floorItem.display.renderItem == plane)
				return floorItem;
		}
		return null;
	}

	public function setSelectedItem(objectUnderMouse:IsoAnimItem):void {
		var isSource:Boolean = false;
		var isoEditItem:IsoEditItem = getIsoEditFromIsoAnim(objectUnderMouse);
		if(!isoEditItem){
			isoEditItem = getSourceChildFromIsoAnim(objectUnderMouse);	
		}else{
			isSource = true;
		}
		
		if(!isoEditItem || AnimsIDs.isBadCreature(isoEditItem.display.skinId) ||
				AnimsIDs.isMonsterCave(isoEditItem.display.skinId)/*&& displayIdsToPaths[isoEditItem.display.id]*/){

			if(isSource || !isoEditItem){
				if(selectedSource)
					selectedSource.display.renderItem.selected = false;
				
				selectedSource = isoEditItem/*? displayIdsToPaths[isoEditItem.display.id]:null;*/
	
				if(selectedSource)
					selectedSource.display.renderItem.selected = true;
	
				sourceSelectedChange.dispatch(selectedSource);
			}

			if(!isSource){
				if(selectedChild)
					selectedChild.display.selected = false;

				selectedChild = isoEditItem;

				if(selectedChild)
					selectedChild.display.selected = true;

				childSelectionChanged.dispatch(selectedChild)
			}
			refresh.dispatch();
		}
	}

	public function play():void {
		timer.addEventListener(TimerEvent.TIMER, onTick);
		timer.start();
	}

	private function onTick(event:TimerEvent):void {
		tick.dispatch(50);
	}

	public function pause():void {
		timer.stop();
	}

	public function clearSourcesChildren():void {


		for (var sourceId:String in sourceChildren) {
			for each (var pathFollowingIsoItem:PathFollowingIsoItem in sourceChildren[sourceId]) {

				itemRemoved.dispatch(pathFollowingIsoItem.editItem,false);
			}
		}

		refresh.dispatch();
	}

	public function addSourcesChildren():void {

		for (var sourceId:String in sourceChildren) {
			for each (var pathFollowingIsoItem:PathFollowingIsoItem in sourceChildren[sourceId]) {
				itemAdded.dispatch(pathFollowingIsoItem.editItem,false);
			}
		}

		refresh.dispatch();
	}

	public function getAllChildrenList():Array {
		var children:Array = [];

		for (var sourceId:String in sourceChildren) {
			for each (var pathFollowingIsoItem:PathFollowingIsoItem in sourceChildren[sourceId]) {
				children.push(new ChildVO(
						pathFollowingIsoItem.editItem.display.skinId,
						pathFollowingIsoItem.startTime,
						pathFollowingIsoItem.path.startPt.x,
						pathFollowingIsoItem.path.startPt.y));
			}
		}

		return children;
	}

	public function removeSelectedChild():void {
		if(!selectedChild)
			return;
		
		for each (var itemsArr:Array in sourceChildren) {
			for each (var pfi:PathFollowingIsoItem in itemsArr) {
				if(pfi.editItem.display.id == selectedChild.display.id){
					itemsArr.splice(itemsArr.indexOf(pfi),1);
					break;
				}
			}
		}
		selectedChild.display.selected = false;
		itemRemoved.dispatch(selectedChild,true);
		selectedChild = null;
		
	}
}
}
