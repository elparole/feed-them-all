/*
 Copyright (c) 2012 Blackmoon, All Rights Reserved
 @author   Karol Furmann
 @contact  karol.furmann@gmail.com
 @project  ZywiecTrail
 @internal
 */
package com.elparole.levelEditor.model
{
import com.blackmoondev.load.BaseInitLoaderModel;
import com.elparole.levelEditor.model.vo.LevelVO;
import com.elparole.levelEditor.signals.EnsureProjFileSignal;
import com.elparole.levelEditor.signals.LoadAssetsSignal;
import com.elparole.levelEditor.signals.LoadLastLevelSignal;
import com.elparole.levelEditor.signals.SetPageSignal;
import com.elparole.levelEditor.signals.LoadLevelSignal;
import com.elparole.levelEditor.signals.LoadLevelsListSignal;
import com.elparole.levelEditor.view.MainTabView;

import flash.filesystem.File;

import mx.managers.CursorManager;


public class InitLoaderModel extends BaseInitLoaderModel {

	[Inject]
	public var ensureProjFile:EnsureProjFileSignal;

	[Inject]
	public var loadLevel:LoadLevelSignal;

	[Inject]
	public var loadLevelsList:LoadLevelsListSignal;

	[Inject]
	public var loadAssets:LoadAssetsSignal;
	
	[Inject]
	public var setPage:SetPageSignal;
	
	[Inject]
	public var configModel:ConfigModel;

	override protected function setSteps():void
	{
		addStep(ensureProjFile);
		addStep(loadAssets);
		addStep(loadLevelsList);
		addStep(loadLevel);
//		addStep(setPage);
	}

	override protected function onStepDispatched():void
	{
//      CreatorToolPreloader.nextPart();
	}

	override protected function getLiveParams(step:*):Array
	{
        switch (step)
        {
            case setPage:
                return [MainTabView];
                break;
            case loadLevel:
		        if(configModel.recentLevelsPaths && configModel.recentLevelsPaths.length>0)
                    return [new LevelVO(configModel.recentLevelsPaths[configModel.recentLevelsPaths.length-1],
		                    configModel.levelTargetDir+File.separator+configModel.recentLevelsPaths[configModel.recentLevelsPaths.length-1]+'.lev')];
		        else
			        return [null];
                break;
//                case loadSpaceConfig:
//                    return [mapModel.configToken];
//                    break;n
//                case loadSpaceBackground:
//                    return [mapModel.zipToken];
//                    break;
        }
		return [];
	}

	override public function startLoader():void	{
		super.startLoader();
	}

	override protected function onFail():void {
		setPage.dispatch(MainTabView);
		CursorManager.removeBusyCursor();
	}

	override protected function onFinish():void {
		setPage.dispatch(MainTabView);
		CursorManager.removeBusyCursor();
		TestAIR1.L.info('init loader model onFinish removeBusyCursor');
//        CreatorToolPreloader.nextPart();
//            progressComplete.dispatch(new com.blackmoondev.geom.IntPoint(0, 0));
//            signalCommandMap.unmapSignalClass(LoaderFinishedSignal, LoaderFinishedCommand);
//            signalCommandMap.unmapSignalClass(LoaderFailedSignal, LoaderFailedCommand);
	}
}
}