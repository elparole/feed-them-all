/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 19:09
 */
package com.elparole.levelEditor.model
{
import com.elparole.feedThemAll.model.FilesListConfigParser;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.ItemVO;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.levelEditor.model.vo.AssetsListVO;
import com.elparole.levelEditor.model.vo.ItemOnPosVO;
import com.elparole.levelEditor.model.vo.LevelConfig;
import com.elparole.levelEditor.model.vo.LevelsConfigVO;
import com.elparole.levelEditor.model.vo.ProjConfigVO;
import com.elparole.levelEditor.signals.LoadAssetsSignal;
import com.elparole.levelEditor.signals.notifications.AssetsDirChangedSignal;
import com.elparole.levelEditor.signals.notifications.LevelNameChangedSignal;
import com.elparole.levelEditor.signals.notifications.LevelSizeChangedSignal;
import com.elparole.levelEditor.signals.notifications.LevelsListChangedSignal;

import flash.geom.Point;
import flash.utils.Dictionary;

import mx.collections.ArrayCollection;

import org.casalib.math.geom.Point3d;

public class ConfigModel
{
	[Inject]
	public var levelsChanged:LevelsListChangedSignal;

	[Inject]
	public var assetsDirChanged:AssetsDirChangedSignal;

	[Inject]
	public var levelNameChanged:LevelNameChangedSignal;

	[Inject]
	public var levelSizeChanged:LevelSizeChangedSignal;

	[Inject]
	public var loadAssets:LoadAssetsSignal;

	[Inject]
	public var gameUtils:GameUtils;

	public var recentLevelsPaths:Array;
	public var assetsPath:String;
	public var levelTargetDir:String;
	public var levels:ArrayCollection;
	public var assetsListVO:AssetsListVO;
	public var assetsIdsToVOs:Dictionary;
	private var _levelName:String;
	private var _levelSize:Point;

	public function ConfigModel() {

	}

	public function setConfig(config:ProjConfigVO, err:Object):ProjConfigVO {
		recentLevelsPaths = config.recentLevelsPaths? config.recentLevelsPaths.concat():[];
		assetsPath = config.assetsPath;
		assetsDirChanged.dispatch(new AssetsListVO(assetsPath,null));
		levelTargetDir = config.levelTargetDir;
		levelsChanged.dispatch(new LevelsConfigVO(config.levelTargetDir,null));
		return config;
	}
	
	public function setAssetsVO(assetsListVO:AssetsListVO, param2:Object):String {
//		TestAIR1.L.info('setAssetsVO '+assetsListVO.assets+' path '+assetsListVO.path);
		this.assetsPath = assetsListVO.path;
		this.assetsListVO = parseFilesToItemsDirs(assetsListVO);
		assetsIdsToVOs = new Dictionary();
		for each (var itemVO:ItemVO in assetsListVO.assets) {
			assetsIdsToVOs[itemVO.listId] = itemVO;
		}
		assetsDirChanged.dispatch(assetsListVO);
		if(!assetsListVO.assets || assetsListVO.assets.length==0){
			loadAssets.dispatch();
		}
//		TestAIR1.L.info('setAssetsVO path '+assetsListVO.path);
		return assetsListVO.path;
	}

	private function parseFilesToItemsDirs(assetsListVO:AssetsListVO):AssetsListVO {
		(gameUtils.parser as FilesListConfigParser).filesList = assetsListVO.assets;
		gameUtils.config = (gameUtils.parser as FilesListConfigParser).parseNamesToConfig(gameUtils.config);
		
		var newFilesArr:Array = [];
		var filesIds:Dictionary = new Dictionary();

		for (var string:String in gameUtils.config) {
			if(!filesIds[string.split('_')[0]+'_'+string.split('_')[2]]){
				filesIds[string.split('_')[0]+'_'+string.split('_')[2]] = true;
				gameUtils.config[string].item.totalFrames = gameUtils.config[string].totalFrames; 
				newFilesArr.push(gameUtils.config[string].item);
			}
		}

		newFilesArr = newFilesArr.sortOn('listId',Array.DESCENDING);
		assetsListVO.assets = new ArrayCollection(newFilesArr);
		return assetsListVO;
	}

	public function setLevelsDir(levelsConfig:LevelsConfigVO, err:Object):String {
		this.levelTargetDir = levelsConfig.dirPath;
		this.levels = levelsConfig.levels;
		levelsChanged.dispatch(levelsConfig);
		return levelTargetDir;
	}

	public function fillItems(config:LevelConfig, err:Object):LevelConfig {
		if(!config)
			return config;

		var itemOnPosVos:Array = [];

		for each (var itemObj:Object in config.floorItems) {
//			ensureItemVOExists(itemObj.skinId+'_'+itemObj.dirId, itemObj);
			itemOnPosVos.push(new ItemOnPosVO(
					assetsIdsToVOs[itemObj.skinId+'_'+itemObj.dirId],
					new Point(
							itemObj.posX,
							itemObj.posZ)));
//					new Point(
//							itemObj.posZ*0.706,
//							itemObj.posX*0.706)));
		}
		config.floorItems = itemOnPosVos;

		itemOnPosVos = [];
		for each (itemObj in config.items) {
			itemOnPosVos.push(new ItemOnPosVO(
					assetsIdsToVOs[itemObj.skinId+'_'+itemObj.dirId],
					new Point(
							itemObj.posX,
							itemObj.posZ)));
		}
		config.items = itemOnPosVos;
		levelSize = config.size.clone();
		return config;
	}

//	private function ensureItemVOExists(id:String, itemObj:Object):void {
//		if(!assetsIdsToVOs[id]){
//			var ivo:ItemVO = new ItemVO(itemObj.name,itemObj.path, null);
//			ivo.bitmap = gameUtils.getSkinByID(ivo.id,ivo.animId,ivo.dirId,0);
//			ivo.offset = gameUtils.getSkinOffset(ivo.id,ivo.animId,ivo.dirId,0);
//			assetsIdsToVOs[id] = ivo;
//		}
//	}

	public function get levelName():String {
		return _levelName;
	}

	public function set levelName(value:String):void {
		_levelName = value;
		levelNameChanged.dispatch(value);
	}

	public function get levelSize():Point {
		return _levelSize;
	}

	public function set levelSize(value:Point):void {
		_levelSize = value;
		levelSizeChanged.dispatch(value);
	}

	public function setLevelSize(config:LevelConfig, err:Object):*{
		if(!config)
			return config;

		levelSize = config.size;
		return config;
	}
}
}
