/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 19:12
 */
package com.elparole.levelEditor.model
{

import com.elparole.components.DisplayBlis;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.ItemVO;
import com.elparole.feedThemAll.test.IsoAnimItem;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.levelEditor.model.vo.ItemOnPosVO;
import com.elparole.levelEditor.model.vo.LevelConfig;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.signals.notifications.ItemAddedSignal;
import com.elparole.levelEditor.signals.notifications.ItemRemovedSignal;

import flash.geom.Point;

import org.casalib.math.geom.Point3d;
import org.robotlegs.oil.async.Promise;

public class ItemsModel
{
	[Inject]
	public var itemAdded:ItemAddedSignal;
	
	[Inject]
	public var itemRemoved:ItemRemovedSignal;

	[Inject]
	public var gameUtils:GameUtils;

//	[Inject]
//	public var lsatep:LoaderStepFailedSignal;

//	[Inject]
//	public var lsistep:LoaderStepFinishedSignal;

	public var items:Array = [];
	public var selectedItem:ItemVO;
	
	public function ItemsModel() {

	}

	public function addItemVoToPos(item:ItemVO = null,pos:Point = null):void {
		selectedItem = item;
		addItem(null,pos);
	}
	
	public function addItem(item:IsoEditItem = null,pos:Point = null):IsoEditItem {
		item ||= createItemFrom(selectedItem);
		item.position = new Point3d(pos.x, 0,pos.y);
		items.push(item);
		itemAdded.dispatch(item,true);
		return item
	}

	private function createItemFrom(item:ItemVO):IsoEditItem {
		var disp:DisplayBlis = new DisplayBlis(item.id, item.animId, item.dirId,
				gameUtils.getSkinByID(item.id, item.animId, item.dirId, 0),
				gameUtils.getSkinOffset(item.id, item.animId, item.dirId, 0));
		var fi:IsoEditItem = new IsoEditItem(disp);
		return fi;
	}

	public function removeFloorItem(item:IsoEditItem):void {
		items.splice(items.indexOf(item),1);
		itemRemoved.dispatch(item,true);
	}

	public function onFileSystemError(err:Object, prom:Promise):void {
		trace('onFileSystemError');
	}

	public function getIsoEditFromIsoAnim(plane:IsoAnimItem):IsoEditItem{
		for each (var floorItem:IsoEditItem in items) {
			if(floorItem.display.renderItem == plane)
				return floorItem;
		}
		
		return null;
	}

	public function removePlaneItemByPlane(plane:*):void {
		for each (var item:IsoEditItem in items) {
			if(item.display.renderItem == plane)
				return removeFloorItem(item);
		}
	}

	public function clear(config:Object = null, err:Object = null):Object {
		while(items.length>0){
			removeFloorItem(items[0]);
		}
		return config;
	}

	public function addLevelItems(config:LevelConfig, err:Object):*{
		if(!config)
			return config;

		for each (var itemOnPosVO:ItemOnPosVO in config.items) {
			if(!AnimsIDs.isBadCreature(itemOnPosVO.itemVO.id))
				addItemVoToPos(itemOnPosVO.itemVO,itemOnPosVO.pos);
		}
		config.items = items;
		return config;
	}

	public function setItemsAlpha(number:Number):void {
		for each (var planeItem:IsoEditItem in items) {
			planeItem.display.renderItem.alpha = number;
		}
	}

	public function hasToBeOnObstacleMap(selectedItem:ItemVO):Boolean {
		return AnimsIDs.isBadCreature(selectedItem.id) || AnimsIDs.isMonsterCave(selectedItem.id) || selectedItem.id == AnimsIDs.SHEEP;
	}
}
}
