/**
 * User: Elparole
 * Date: 04.04.13
 * Time: 11:55
 */
package com.elparole.levelEditor.model.vo
{
import flash.display.Bitmap;

public class BitmapFileLoad
{
	public var path:String;
	public var data:Bitmap;


	public function BitmapFileLoad(path:String, data:Bitmap) {
		this.path = path;
		this.data = data;
	}
}
}
