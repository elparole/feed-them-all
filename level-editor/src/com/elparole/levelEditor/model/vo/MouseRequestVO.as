/**
 * User: Elparole
 * Date: 05.03.13
 * Time: 19:57
 */
package com.elparole.levelEditor.model.vo
{
import flash.geom.Point;

import org.casalib.math.geom.Point3d;

public class MouseRequestVO
{
	public var mousePos:Point;
	public var coordsIso:Point;
	public var objectsUnderMouse:Array;
	public var selectedItem:IsoEditItem;
	public var type:String;
	public static const CLICK:String ='click';
	public static const MOUSE_UP:String = 'mouseUp';
	public static const MOUSE_DOWN:String = 'mouseDown';
	public static const MOUSE_MOVE:String = 'mouseMove';


	public function MouseRequestVO(mousePos:Point, coordsIso:Point, objectsUnderMouse:Array, type:String = 'click') {
		this.mousePos = mousePos;
		this.coordsIso = coordsIso;
		this.objectsUnderMouse = objectsUnderMouse;
		this.type = type;
	}

}
}
