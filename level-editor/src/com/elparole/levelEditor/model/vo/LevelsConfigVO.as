/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 20:55
 */
package com.elparole.levelEditor.model.vo
{
import mx.collections.ArrayCollection;

public class LevelsConfigVO
{
	public var dirPath:String;
	public var levels:ArrayCollection;


	public function LevelsConfigVO(dirPath:String, levelsNames:ArrayCollection) {
		this.dirPath = dirPath;
		this.levels = levelsNames;
	}
}
}
