/**
 * User: Elparole
 * Date: 24.03.13
 * Time: 14:52
 */
package com.elparole.levelEditor.model.vo
{
import com.elparole.feedThemAll.components.PathComponent;
import com.elparole.feedThemAll.components.PathComponent;
import com.elparole.feedThemAll.test.IsoAnimItem;

import flash.geom.Point;

import org.casalib.math.geom.Point3d;

public class PathFollowingIsoItem
{
//	public var position:Point3d;
	public var path:PathComponent;
	public var editItem:IsoEditItem;
	public var startTime:int;
	public var sheepDestId:int;


	public function PathFollowingIsoItem(path:PathComponent,editItem:IsoEditItem, sheepDestId:int = -1) {
		this.path = path;
		this.editItem = editItem;
		this.sheepDestId =sheepDestId;
	}
}
}
