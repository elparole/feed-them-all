/**
 * User: Elparole
 * Date: 08.03.13
 * Time: 14:25
 */
package com.elparole.levelEditor.model.vo
{
import com.elparole.feedThemAll.model.ItemVO;

import flash.geom.Point;

import org.casalib.math.geom.Point3d;

public class ItemOnPosVO
{
	public var itemVO:ItemVO;
	public var pos:Point;


	public function ItemOnPosVO(itemVO:ItemVO, pos:Point) {
		this.itemVO = itemVO;
		this.pos = pos;
	}

	public function get listId():String{
//		return id+'_'+dirId+'_'+totalFrames;
		return itemVO.listId;
	}
}
}
