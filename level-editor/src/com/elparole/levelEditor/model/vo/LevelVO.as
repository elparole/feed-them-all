/**
 * User: Elparole
 * Date: 27.02.13
 * Time: 15:39
 */
package com.elparole.levelEditor.model.vo
{
public class LevelVO
{
	public var name:String;
	public var path:String;


	public function LevelVO(name:String, path:String) {
		this.name = name;
		this.path = path;
	}


	public function toString():String {
		return name;
	}
}
}
