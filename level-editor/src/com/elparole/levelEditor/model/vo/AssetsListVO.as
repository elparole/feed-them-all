/**
 * User: Elparole
 * Date: 04.03.13
 * Time: 17:25
 */
package com.elparole.levelEditor.model.vo
{
import mx.collections.ArrayCollection;

public class AssetsListVO
{
	public var assets:ArrayCollection
	public var path:String;


	public function AssetsListVO(path:String, assets:ArrayCollection) {
		this.assets = assets;
		this.path = path;
	}
}
}
