/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 18:58
 */
package com.elparole.levelEditor.model.vo
{
import com.elparole.levelEditor.model.*;

public class ProjConfigVO
{
	public var recentLevelsPaths:Array;
	public var assetsPath:String;
	public var levelTargetDir:String;


	public function ProjConfigVO(recentLevelsPaths:Array, assetsPath:String, levelTargetDir:String,lastLevelPath:String) {
		this.recentLevelsPaths = recentLevelsPaths;
		this.assetsPath = assetsPath;
		this.levelTargetDir = levelTargetDir;
	}

	public static function fromJSON(s:String):ProjConfigVO {
		var raw:Object = JSON.parse(s);
		return new ProjConfigVO(raw.recentLevelsPaths,raw.assetsPath,raw.levelTargetDir,raw.lastLevelPath);
	}

	public static function fromObj(raw:Object):ProjConfigVO {
		return new ProjConfigVO(raw.recentLevelsPaths,raw.assetsPath,raw.levelTargetDir,raw.lastLevelPath);
	}
}
}
