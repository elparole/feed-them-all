/**
 * User: Elparole
 * Date: 05.03.13
 * Time: 21:51
 */
package com.elparole.levelEditor.model.vo
{
import com.elparole.components.DisplayBlis;

import org.casalib.math.geom.Point3d;

import org.papervision3d.core.math.Number3D;

public class IsoEditItem
{
	public var display:DisplayBlis;
	public var position:Point3d;
//	private var num3d:Number3D;


	public function IsoEditItem(display:DisplayBlis, position:Point3d = null) {
		this.display = display;
		this.position = position || new Point3d();
	}

//	public function getPositionN3D():Number3D {
//		num3d ||= new Number3D();
//		num3d.x = position.x;
//		num3d.y = position.y;
//		num3d.z = position.z;
//		return num3d;
//	}
}
}
