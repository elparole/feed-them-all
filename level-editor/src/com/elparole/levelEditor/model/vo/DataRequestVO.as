/**
 * User: Elparole
 * Date: 04.03.13
 * Time: 17:48
 */
package com.elparole.levelEditor.model.vo
{
public class DataRequestVO
{
	public var viewClass:Class;
	public var data:Object;


	public function DataRequestVO(viewClass:Class) {
		this.viewClass = viewClass;
	}
}
}
