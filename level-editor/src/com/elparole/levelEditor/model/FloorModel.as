/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 19:12
 */
package com.elparole.levelEditor.model
{

import com.elparole.components.DisplayBlis;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.model.ItemVO;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.model.vo.ItemOnPosVO;
import com.elparole.levelEditor.model.vo.LevelConfig;
import com.elparole.levelEditor.model.vo.LevelsConfigVO;
import com.elparole.levelEditor.signals.notifications.FloorItemAddedSignal;
import com.elparole.levelEditor.signals.notifications.FloorItemRemovedSignal;

import flash.geom.Point;

import org.casalib.math.geom.Point3d;

import org.casalib.math.geom.Point3d;
import org.papervision3d.objects.primitives.Plane;
import org.robotlegs.oil.async.Promise;

public class FloorModel
{
	[Inject]
	public var floorItemAdded:FloorItemAddedSignal;
	
	[Inject]
	public var floorItemRemoved:FloorItemRemovedSignal;

	[Inject]
	public var gameUtils:GameUtils;

//	[Inject]
//	public var lsatep:LoaderStepFailedSignal;

//	[Inject]
//	public var lsistep:LoaderStepFinishedSignal;

	public var items:Array = [];
	public var selectedItem:ItemVO;
	
	public function FloorModel() {

	}

	public function addItemVoToPos(item:ItemVO = null,pos:Point = null):void {
		selectedItem = item;
		addFloorItem(null,pos);
	}
	
	public function addFloorItem(item:IsoEditItem = null,pos:Point = null):void {
		for each (var isoEditItem:IsoEditItem in items) {
			if(isoEditItem.position.x == pos.x &&
					isoEditItem.position.z == pos.y)
			return;
		}
		item ||= createItemFrom(selectedItem);
		item.position = new Point3d(pos.x, -1,pos.y);
		items.push(item);
		floorItemAdded.dispatch(item,true);
	}

	private function createItemFrom(item:ItemVO):IsoEditItem {
		var disp:DisplayBlis = new DisplayBlis(item.id, item.animId, item.dirId,
				gameUtils.getSkinByID(item.id, item.animId, item.dirId, 0),
				gameUtils.getSkinOffset(item.id, item.animId, item.dirId, 0));
		var fi:IsoEditItem = new IsoEditItem(disp);
		return fi;
	}

	public function removeFloorItem(item:IsoEditItem):void {
		items.splice(items.indexOf(item),1);
		floorItemRemoved.dispatch(item,true);
	}

	public function onFileSystemError(err:Object, prom:Promise):void {
		trace('onFileSystemError');
	}

	public function removeFloorItemByPlane(plane:*):void {
		for each (var floorItem:IsoEditItem in items) {
			if(floorItem.display.renderItem == plane)
				return removeFloorItem(floorItem);
		}
	}

	public function clear(config:Object = null, err:Object = null):Object {
		while(items.length>0){
			removeFloorItem(items[0]);
		}
		return config;
	}

	public function addLevelFloors(config:LevelConfig, err:Object):*{
		if(!config)
			return config;

		for each (var itemOnPosVO:ItemOnPosVO in config.floorItems) {
			addItemVoToPos(itemOnPosVO.itemVO,itemOnPosVO.pos);
		}
		return config;
	}

	public function setItemsAlpha(value:Number):void {
		for each (var planeItem:IsoEditItem in items) {
			planeItem.display.renderItem.alpha = value;
		}
	}
}
}
