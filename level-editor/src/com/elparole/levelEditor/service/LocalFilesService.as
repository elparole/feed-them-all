package com.elparole.levelEditor.service
{

import com.blackmoondev.iso.isoModel.math.IsoMathUtils;
import com.elparole.feedThemAll.model.ItemVO;
import com.elparole.levelEditor.EditorConstants;
import com.elparole.levelEditor.model.vo.AssetsListVO;
import com.elparole.feedThemAll.model.vo.ChildVO;
import com.elparole.levelEditor.model.vo.LevelConfig;
import com.elparole.levelEditor.model.vo.LevelVO;
import com.elparole.levelEditor.model.vo.LevelsConfigVO;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.model.vo.ProjConfigVO;

import flash.display.BitmapData;

import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IOErrorEvent;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.geom.Point;
import flash.utils.Dictionary;

import mx.collections.ArrayCollection;

import org.casalib.events.LoadEvent;
import org.casalib.load.GroupLoad;
import org.casalib.load.ImageLoad;
import org.robotlegs.oil.async.Promise;

import robotlegs.bender.framework.api.ILogger;

public class LocalFilesService implements ILocalFilesService
	{
		[Inject]
		public var logger:ILogger;

		private var appDir:String;
		private var fileStream:FileStream;

		private var file:File;
		private var prom:Promise;
		private var sampleConfig:Object;
		private var newPath:String;
		private var configModifier:Function;
		private var bitmapsProm:Promise;
		private var bitmapsLoad:GroupLoad;
		private var assetsPathsToItems:Dictionary;
		private var assetsVO:AssetsListVO;
		private var bitmapsAdded:Dictionary;
		private const levelsListName:String = 'levelsList.txt';

		public function LocalFilesService() {

		}

		public function changeConfig(pathId:String):Promise {
			switch(pathId){
				case 'assetsPath':
					configModifier = changeAssetsPath;
					break;
				case 'changeConfigDestDir':
					configModifier = changeLevelsPath;
					break;
			}
			if(fileStream)
				fileStream.close();
			prom = new Promise();
			file = new File();
			file.addEventListener(Event.SELECT, onPathSelected);
			file.browseForDirectory("Please select a directory...");

			return prom;
		}


		public function saveProjConfig(recentLevelsPaths:Array, assetsPath:String, levelTargetDir:String):void {
			prom = new Promise();
			trace('ensureProjFileExist');
//			var target:File = File.documentsDirectory.resolvePath("AIR Test/test.txt");
//			var targetParent:File = target.parent;
//			targetParent.createDirectory();
			file = File.documentsDirectory;
			file = file.resolvePath(EditorConstants.APP_DIR_NAME+File.separator+EditorConstants.DEFAULT_PROJECT_NAME);
			
			sampleConfig = {recentLevelsPaths:recentLevelsPaths, assetsPath:assetsPath, levelTargetDir:levelTargetDir};

			if(fileStream){
				try { fileStream.close();} catch (e:Error) { }
			}

			fileStream = new FileStream();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeUTFBytes(JSON.stringify(sampleConfig));
			fileStream.close();
		}

	public function ensureProjFileExist():Promise {
			prom = new Promise(); 
			trace('ensureProjFileExist');
//			var target:File = File.documentsDirectory.resolvePath("AIR Test/test.txt");
//			var targetParent:File = target.parent;
//			targetParent.createDirectory();
			file = File.documentsDirectory;
			file = file.resolvePath(EditorConstants.APP_DIR_NAME+File.separator+EditorConstants.DEFAULT_PROJECT_NAME);
			fileStream = new FileStream();
			fileStream.openAsync(file, FileMode.READ);
			fileStream.addEventListener(IOErrorEvent.IO_ERROR, onReadError);
			fileStream.addEventListener(Event.COMPLETE, onReadComplete);

			fileStream.position = 0;
			return prom;
//			fileStream.close();
//			appDir = File.applicationStorageDirectory
		}

		public function createNewLevel(levelName:String, levelTargetDir:String, levelSize:Point):String {
			file = new File(levelTargetDir);
			var levels:Array = file.getDirectoryListing();
			for (var i:int = 0; i < levels.length; i++) {
				var file1:File = levels[i];
				trace(file1.nativePath+File.separator+file1.name);
				if(file1.nativePath.split('.').pop()!='lev'){
					levels.splice(i, 1);
					i=0;
				}
			}
			levelName+='.lev';

			var isUnique:Boolean = true;
			var levelNameIndex:int = 1;

			do{
				if(!isUnique){
					levelName = 'MyLevel'+String(levelNameIndex)+'.lev';
					levelNameIndex++;
					isUnique = true;
				}
				for (i = 0; i < levels.length; i++) {
					if(levels[i].nativePath.split('.').pop().indexOf(levelName)>=0){
						isUnique = false;
					}
				}
			}while(!isUnique);
			file = new File(levelTargetDir+File.separator+levelName);

//			file = File.documentsDirectory;
//			file = file.resolvePath(EditorConstants.APP_DIR_NAME+File.separator+EditorConstants.DEFAULT_PROJECT_NAME);
			fileStream = new FileStream();
			try {
				fileStream.open(file, FileMode.WRITE);
			} catch (e:Error) {

			}
			var levelToWrite:Object = {size:{isoWidth:levelSize.x,isoHeight:levelSize.y}, name:levelName.split('.')[0], floorItems:[], items:[]}
			for (var ix:int = 0; ix < levelSize.y; ix++) {
				for (var iy:int = 0; iy < levelSize.x; iy++) {
					levelToWrite.floorItems.push({posY:-1,posX:ix*50,animId:"idle",posZ:iy*50,dirId:"DR",skinId:"terrainPlain"})
				}
			}
			fileStream.writeUTFBytes(JSON.stringify(levelToWrite));
			fileStream.close();

			return levelName;

//			fileStream.openAsync(file, FileMode.READ);
//			fileStream.addEventListener(IOErrorEvent.IO_ERROR, onReadError);
		}

		public function loadBitmaps(assetsVO:AssetsListVO):Promise {
			logger.info('loadBitmaps');
			bitmapsProm = new Promise();
			this.assetsPathsToItems = new Dictionary();
			this.assetsVO = assetsVO;
			bitmapsLoad = new GroupLoad();
			bitmapsAdded = new Dictionary();

			for each (var itemVO:ItemVO in assetsVO.assets) {
				itemVO.id = itemVO.name.split('_')[0];
				itemVO.dirId = itemVO.name.split('_')[2];
				if(!bitmapsAdded[itemVO.id+'_'+itemVO.dirId]){
					file.resolvePath(itemVO.path+File.separator+itemVO.name);
					logger.info('itemVO name: '+itemVO.name+', path:'+itemVO.path);
					logger.info('url: '+file.url+File.separator+itemVO.name);
					bitmapsLoad.addLoad(
							new ImageLoad(
									file.url+File.separator+itemVO.name+'.'+itemVO.path.split('.').pop()));
	//				trace(itemVO.path);
					bitmapsAdded[itemVO.id+'_'+itemVO.dirId] = itemVO;
				}
				assetsPathsToItems[file.url+File.separator+itemVO.name+'.'+itemVO.path.split('.').pop()] = itemVO;
			}

			bitmapsLoad.addEventListener(LoadEvent.COMPLETE, onBitmapsLoaded);
			bitmapsLoad.addEventListener(LoadEvent.PROGRESS, onBitmapsProgress);
			bitmapsLoad.addEventListener(IOErrorEvent.IO_ERROR, onBitmapsError);
			bitmapsLoad.addEventListener(HTTPStatusEvent.HTTP_STATUS, onBitmapsError);
			bitmapsLoad.start();

			return bitmapsProm;
		}

		private function onBitmapsError(event:Event):void {
			logger.info('onBitmapsError '+event.type);
		}

		private function onBitmapsProgress(event:Event):void {
			logger.info('onBitmapsProgress');
		}

		private function onBitmapsLoaded(event:LoadEvent):void {
			logger.info('onBitmapsLoaded');
			bitmapsLoad.removeEventListener(LoadEvent.COMPLETE, onBitmapsLoaded)
			for each (var imageLoad:ImageLoad in bitmapsLoad.completedLoads) {
				assetsPathsToItems[imageLoad.url].bitmap = imageLoad.contentAsBitmapData;
			}

			for each (var itemVO:ItemVO in assetsPathsToItems) {
				itemVO.bitmap ||= bitmapsAdded[itemVO.id+'_'+itemVO.dirId].bitmap;//new BitmapData(128,128,0,0);
			}

			bitmapsProm.handleResult(this.assetsVO);
		}

		public function updateAssets(path:String):AssetsListVO {
			file = new File(path);
			var levels:Array = file.getDirectoryListing();
//			for (var i:int = 0; i < levels.length; i++) {
//				var file1:File = levels[i];
//				trace(file1.nativePath+File.separator+file1.name);
//				if(file1.nativePath.split('.').pop()!='lev'){
//					levels.splice(i, 1);
//					i=0;
//				}
//			}
			var levelsVOs:Array = [];
			for each (var file2:File in levels) {
				if(file2.name.split('.').length>1 && file2.name.split('.').pop()=='png')
					levelsVOs.push(new ItemVO(
							file2.nativePath.split(File.separator).pop().split('.')[0],
							file2.nativePath,null));
			}
			return new AssetsListVO(path, new ArrayCollection(levelsVOs));
		}

		public function updateConfig(path:String):LevelsConfigVO {
			file = new File(path);
			var levels:Array = file.getDirectoryListing();
			for (var i:int = 0; i < levels.length; i++) {
				var file1:File = levels[i];
				trace(file1.nativePath+File.separator+file1.name);
				if(file1.nativePath.split('.').pop()!='lev'){
					levels.splice(i, 1);
					i=0;
				}
			}
			var levelsVOs:Array = [];
			for each (var file2:File in levels) {
				levelsVOs.push(new LevelVO(file2.nativePath.split(File.separator).pop().split('.')[0],file2.nativePath));
			}

			var listPath:String = path.split(File.separator+'levels').join('');
			saveLevelsList(listPath,levels);

			return new LevelsConfigVO(path,new ArrayCollection(levelsVOs));

		}

		private function onReadComplete(event:Event):void {
			var projConfig:ProjConfigVO;
			if(fileStream.bytesAvailable<8){
				resetDefaultProjFile();
				projConfig = ProjConfigVO.fromObj(sampleConfig);
			}
			else
				projConfig = ProjConfigVO.fromJSON(fileStream.readUTFBytes(fileStream.bytesAvailable));
			fileStream.close();

			prom.handleResult(projConfig);
		}

		private function onReadError(event:IOErrorEvent):void {
//			if(fileStream.bytesAvailable<8){
			resetDefaultProjFile();
			fileStream.close();
//			}
			prom.handleResult(ProjConfigVO.fromObj(sampleConfig));

		}

		private function resetDefaultProjFile():void {
			sampleConfig = {recentLevelsPaths:[], assetsPath:'', levelTargetDir:""};

			fileStream.close();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeUTFBytes(JSON.stringify(sampleConfig));
			fileStream.close();
		}

		private function changeLevelsPath(path:String):void {
			sampleConfig = JSON.parse(fileStream.readUTFBytes(fileStream.bytesAvailable));
			sampleConfig.levelTargetDir = path;
		}

		private function changeAssetsPath(path:String):void {
			sampleConfig = JSON.parse(fileStream.readUTFBytes(fileStream.bytesAvailable));
			sampleConfig.assetsPath = path;
		}

		private function onPathSelected(event:Event):void {
			newPath = file.nativePath;
			file = File.documentsDirectory;
			file = file.resolvePath(EditorConstants.APP_DIR_NAME+File.separator+EditorConstants.DEFAULT_PROJECT_NAME);
			fileStream = new FileStream();
			fileStream.openAsync(file, FileMode.READ);
			fileStream.addEventListener(IOErrorEvent.IO_ERROR, onReadError);
			fileStream.addEventListener(Event.COMPLETE, onOverwritePath);
		}

		private function onOverwritePath(event:Event):void {

			configModifier.call(this,newPath);

			file.cancel();
			fileStream.close();
			fileStream.open(file, FileMode.WRITE);
			fileStream.writeUTFBytes(JSON.stringify(sampleConfig));
			fileStream.close();
//			}


			if(configModifier == changeAssetsPath)
				prom.handleResult(new AssetsListVO(newPath,null));
			else{
				file = new File(newPath);
				var levels:Array = file.getDirectoryListing();
				for (var i:int = 0; i < levels.length; i++) {
					var file1:File = levels[i];
					trace(file1.nativePath+File.separator+file1.name);
					if(file1.nativePath.split('.').pop()!='lev'){
						levels.splice(i, 1);
						i=0;
					}
				}
				var listPath:String = newPath.split(File.separator+'levels').join('');
				saveLevelsList(listPath,levels);
				prom.handleResult(new LevelsConfigVO(newPath,new ArrayCollection(levels)));
			}
		}

		private function saveLevelsList(listPath:String, levels:Array):void {
			file = new File(listPath+File.separator+levelsListName);
			var levelsNames:Array = []
			for each (var file1:* in levels) {
				levelsNames.push(file1.name + (file1.name.indexOf('.lev')>=0? '':'.lev'));
			}

//			file = File.documentsDirectory;
//			file = file.resolvePath(EditorConstants.APP_DIR_NAME+File.separator+EditorConstants.DEFAULT_PROJECT_NAME);
			fileStream = new FileStream();
			try {
				fileStream.open(file, FileMode.WRITE);
			} catch (e:Error) {

			}
			fileStream.writeUTFBytes(JSON.stringify({levels:levelsNames}));
			fileStream.close();
		}

		public function saveConfig(
				floorItems:Array,
				items:Array,
				childrenList:Array,
				obstacleMap:Dictionary,
				size:Point, 
				name:String,
				path:String):Promise {
			if(file)
				file.cancel();
			if(fileStream)
				fileStream.close();

			var savePath:String = name.split('.').pop() == 'lev'? path+File.separator+name:path+File.separator+name+'.lev';
			file = new File(savePath);
			file.resolvePath(savePath);

			fileStream = new FileStream();
			try {
				fileStream.open(file, FileMode.WRITE);
			} catch (e:Error) {

			}

			var levelObj:Object = {
				size:{isoWidth:size.x,isoHeight:size.y},
				name:name.split('.')[0], 
				floorItems:[], 
				items:[],
				childrenList:[],
				obstacleMap:IsoMathUtils.generateHexObstacleMapFromDict(obstacleMap)
			};

			for each (var item:IsoEditItem in floorItems) {
				levelObj.floorItems.push({
					skinId:item.display.skinId,
					animId:item.display.animId,
					dirId:item.display.dirId,
					posX:item.position.x,
					posY:item.position.y,
					posZ:item.position.z
				});
			}
			for each (item in items) {
				levelObj.items.push({
					skinId:item.display.skinId,
					animId:item.display.animId,
					dirId:item.display.dirId,
					posX:item.position.x,
					posY:item.position.y,
					posZ:item.position.z
				});
			}
			for each (var childVO:ChildVO in childrenList) {
				levelObj.childrenList.push({
					skinId:childVO.skinId,
					startTime:childVO.startTime,
					startX:childVO.x,
					startY:childVO.y
				});
			}

			fileStream.writeUTFBytes(JSON.stringify(levelObj));
			fileStream.close();

			return null;
		}
}
}
