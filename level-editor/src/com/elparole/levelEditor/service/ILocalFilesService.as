package com.elparole.levelEditor.service
{
import com.elparole.levelEditor.model.vo.AssetsListVO;
import com.elparole.levelEditor.model.vo.LevelsConfigVO;

import flash.geom.Point;
import flash.utils.Dictionary;

import org.robotlegs.oil.async.Promise;

public interface ILocalFilesService
	{
		function ensureProjFileExist():Promise;

		function changeConfig(pathId:String):Promise;

		function createNewLevel(levelName:String, levelTargetDir:String, levelSize:Point):String;

		function updateConfig(s:String):LevelsConfigVO;

		function updateAssets(assetsPath:String):AssetsListVO;

		function loadBitmaps(assetsVO:AssetsListVO):Promise;

		function saveConfig(
				floorItems:Array,
				items:Array,
				childrenList:Array,
				obstacleMap:Dictionary,
				size:Point,
				name:String,
				path:String):Promise;

		function saveProjConfig(recentLevelsPaths:Array, assetsPath:String, levelTargetDir:String):void;
}
}
