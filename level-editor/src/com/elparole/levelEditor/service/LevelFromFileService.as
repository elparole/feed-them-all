package com.elparole.levelEditor.service
{

import com.blackmoondev.iso.isoModel.math.IsoMathUtils;
import com.elparole.feedThemAll.model.ItemVO;
import com.elparole.levelEditor.EditorConstants;
import com.elparole.levelEditor.model.LevelConfigParser;
import com.elparole.levelEditor.model.vo.AssetsListVO;
import com.elparole.levelEditor.model.vo.LevelConfig;
import com.elparole.levelEditor.model.vo.LevelVO;
import com.elparole.levelEditor.model.vo.LevelsConfigVO;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.model.vo.ProjConfigVO;

import flash.events.Event;
import flash.events.IOErrorEvent;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.geom.Point;
import flash.utils.Dictionary;

import mx.collections.ArrayCollection;

import org.casalib.events.LoadEvent;
import org.casalib.load.GroupLoad;
import org.casalib.load.ImageLoad;
import org.robotlegs.oil.async.Promise;

import robotlegs.bender.framework.api.ILogger;

public class LevelFromFileService implements ILevelService
	{
		[Inject]
		public var logger:ILogger;

		[Inject]
		public var levelParser:LevelConfigParser;

		private var fileStream:FileStream;

		private var file:File;
		private var prom:Promise;
		private var sampleConfig:Object;


		public function LevelFromFileService() {

		}


	public function hasLevelNR(nr:int):Boolean {
		return true;
	}

	public function loadConfigByNR(nr:int):Promise {
		return null;
	}

	public function loadLevelsList(path:String):Promise {
		prom = new Promise();
		return prom;
	}

	public function get currentLevelName():String {
		return "";
	}

	public function loadConfig(name:String, path:String):Promise {

			prom = new Promise();
			trace('ensureProjFileExist');
//			var target:File = File.documentsDirectory.resolvePath("AIR Test/test.txt");
//			var targetParent:File = target.parent;
//			targetParent.createDirectory();
			file = File.documentsDirectory;
//			file = file.resolvePath(path+File.separator+name+'.lev');
			file = file.resolvePath(path);
			fileStream = new FileStream();
			fileStream.openAsync(file, FileMode.READ);
			fileStream.addEventListener(IOErrorEvent.IO_ERROR, onReadLevelError,false,0,true);
			fileStream.addEventListener(Event.COMPLETE, onReadLevelComplete,false,0,true);

			fileStream.position = 0;
			return prom;
		}

		private function onReadLevelComplete(event:Event):void {
//			trace('onReadLevelComplete', fileStream.readUTFBytes(fileStream.bytesAvailable));
//			fileStream.position = 0;
			var levStr:String = fileStream.readUTFBytes(fileStream.bytesAvailable);
//			levStr = levStr.split('monster').join('monster1');
			var lc:LevelConfig = LevelConfig.fromObj(JSON.parse(levStr));
//			var lc:LevelConfig = LevelConfig.fromObj(JSON.parse(
//					'{"name":"lev3","size":{"isoWidth":20,"isoHeight":15},"floorItems":[{"dirId":"DR","posX":560,"posY":-600,"skinId":"terrainRocks1","animId":"idle","posZ":840},{"dirId":"DR","posX":560,"posY":-600,"skinId":"terrainRocks1","animId":"idle","posZ":700},{"dirId":"DR","posX":630,"posY":-600,"skinId":"terrainRocks1","animId":"idle","posZ":770},{"dirId":"DR","posX":700,"posY":-600,"skinId":"terrainRocks1","animId":"idle","posZ":840},{"dirId":"DR","posX":700,"posY":-600,"skinId":"terrainRocks1","animId":"idle","posZ":700}]}')
//			);
//			var lc:LevelConfig = LevelConfig.fromObj(JSON.parse('{"dataName":"dataValue"}'));
			fileStream.close();
			prom.handleResult(lc);
		}

		private function onReadLevelError(event:IOErrorEvent):void {
//			if(fileStream.bytesAvailable<8){
//			resetDefaultProjFile();
//			}
			prom.handleResult({});

		}
}
}
