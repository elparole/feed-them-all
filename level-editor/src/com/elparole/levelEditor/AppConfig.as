package com.elparole.levelEditor {

import com.blackmoondev.load.signals.LoaderFailedSignal;
import com.blackmoondev.load.signals.LoaderFinishedSignal;
import com.blackmoondev.load.signals.LoaderStepFailedSignal;
import com.blackmoondev.load.signals.LoaderStepFinishedSignal;
import com.elparole.feedThemAll.model.FilesListConfigParser;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.levelEditor.controller.*;
import com.elparole.levelEditor.model.*;
import com.elparole.levelEditor.service.ILevelService;
import com.elparole.levelEditor.service.LevelFromFileService;
import com.elparole.levelEditor.service.LocalFilesService;
import com.elparole.levelEditor.service.ILocalFilesService;
import com.elparole.levelEditor.signals.ChangeAssetsDirSignal;
import com.elparole.levelEditor.signals.ChangeConfigDestDirSignal;
import com.elparole.levelEditor.signals.EnsureProjFileSignal;
import com.elparole.levelEditor.signals.LoadAssetsSignal;
import com.elparole.levelEditor.signals.LoadLastLevelSignal;
import com.elparole.levelEditor.signals.LoadLevelSignal;
import com.elparole.levelEditor.signals.LoadLevelsListSignal;
import com.elparole.levelEditor.signals.MapClickRequestSignal;
import com.elparole.levelEditor.signals.PauseSignal;
import com.elparole.levelEditor.signals.PlaySignal;
import com.elparole.levelEditor.signals.SaveLevelSignal;
import com.elparole.levelEditor.signals.StartupSignal;
import com.elparole.levelEditor.signals.notifications.*;
import com.elparole.levelEditor.signals.requests.*;
import com.elparole.levelEditor.signals.SetPageSignal;
import com.elparole.levelEditor.view.*;
import com.elparole.levelEditor.view.MainTabMediator;
import com.elparole.levelEditor.view.MainTabView;


import flash.events.IEventDispatcher;

//import flexunit.framework.AssertStringFormats;


import org.swiftsuspenders.Injector;

	import robotlegs.bender.extensions.contextView.ContextView;
	import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IContext;
	import robotlegs.bender.framework.api.ILogger;
	import robotlegs.bender.framework.api.LogLevel;
import robotlegs.bender.framework.impl.ConfigManager;

public class AppConfig implements IConfig
	{
		[Inject]
		public var context:IContext;

		[Inject]
		public var commandMap:ISignalCommandMap;

		[Inject]
		public var mediatorMap:IMediatorMap;

		[Inject]
		public var injector:Injector;

		[Inject]
		public var logger:ILogger;

		[Inject]
		public var contextView:ContextView;

		[Inject]
		public var dispatcher:IEventDispatcher;


		public function configure():void {

			// Configure logger.
			context.logLevel = LogLevel.DEBUG;
			logger.info( "configuring application" );

			// Map commands.
			commandMap.map( StartupSignal ).toCommand( StartupCommand );
			commandMap.map( ResponseDataSignal ).toCommand( ResponseDataCommand );
			commandMap.map( CreateNewLevelSignal ).toCommand( CreateNewLevelCommand );
			commandMap.map( EnsureProjFileSignal ).toCommand( EnsureProjFileCommand );
			commandMap.map( LoadAssetsSignal ).toCommand( LoadAssetsCommand );
			commandMap.map( LoadLastLevelSignal ).toCommand( LoadLastLevelCommand );
			commandMap.map( LoadLevelsListSignal ).toCommand( LoadLevelsListCommand );
			commandMap.map( ChangeAssetsDirSignal ).toCommand( ChangeAssetsDirCommand );
			commandMap.map( ChangeConfigDestDirSignal ).toCommand( ChangeConfigDestDirCommand );
			commandMap.map( MapClickRequestSignal ).toCommand( MapClickRequestCommand );
			commandMap.map( SelectItemSignal).toCommand( SelectItemCommand );
			commandMap.map( ChangeMouseModeSignal).toCommand( ChangeMouseModeCommand );
			commandMap.map( ChangeSnapToGridSignal).toCommand( ChangeSnapToGridCommand );
			commandMap.map( SaveLevelSignal).toCommand( SaveLevelCommand );
			commandMap.map( LoadLevelSignal).toCommand( LoadLevelCommand );
			commandMap.map( SetPageSignal ).toCommand( SetPageCommand );
			commandMap.map( ExportFloorSignal ).toCommand( ExportFloorCommand );
			commandMap.map( CreateMonsterInWaveSignal ).toCommand( CreateMonsterInWaveCommand );
			commandMap.map( SetPositionsInTimeSignal ).toCommand( SetPositionsInTimeCommand );
			commandMap.map( PlaySignal ).toCommand( PlayCommand );
			commandMap.map( PauseSignal ).toCommand( PauseCommand );
			commandMap.map( RemoveChildSignal ).toCommand( RemoveChildCommand );

			// Map independent notification signals.
			injector.map( MapSizeChangedSignal ).asSingleton();
			injector.map( SetupTabSignal ).asSingleton();
			injector.map( ChildSelectionChangedSignal ).asSingleton();
			injector.map( ObstacleChangedSignal ).asSingleton();
			injector.map( LevelNameChangedSignal ).asSingleton();
			injector.map( LevelsListChangedSignal ).asSingleton();
			injector.map( AssetsDirChangedSignal ).asSingleton();
			injector.map( NotifyImageSelectedSignal ).asSingleton();
			injector.map( LoaderFinishedSignal ).asSingleton();
			injector.map( LoaderFailedSignal ).asSingleton();
			injector.map( LoaderStepFinishedSignal ).asSingleton();
			injector.map( LoaderStepFailedSignal ).asSingleton();
			injector.map( FloorItemsListChangedSignal ).asSingleton();
			injector.map( ItemAddedSignal ).asSingleton();
			injector.map( ItemRemovedSignal ).asSingleton();
			injector.map( FloorItemAddedSignal ).asSingleton();
			injector.map( FloorItemRemovedSignal ).asSingleton();
			injector.map( FloorModeSelectedSignal ).asSingleton();
			injector.map( ItemsModeSelectedSignal ).asSingleton();
			injector.map( TickSignal ).asSingleton();
			injector.map( ObstaclesModeSelectedSignal ).asSingleton();
			injector.map( SnapToGridChangedSignal ).asSingleton();
			injector.map( DragModeSelectedSignal ).asSingleton();
			injector.map( GameUtilsManager ).asSingleton();
			injector.map( LevelSizeChangedSignal ).asSingleton();
			injector.map( SourceSelectChangedSignal ).asSingleton();
			injector.map( RefreshPreviewSignal ).asSingleton();

			var gameUtils:GameUtils = new GameUtils();
			gameUtils.parser = new FilesListConfigParser();

			injector.map( GameUtils ).toValue(gameUtils);
			// Map views.
			mediatorMap.map( WaveEditView ).toMediator( WaveEditMediator );
			mediatorMap.map( MainTabNavigator ).toMediator( MainTabNavigatorMediator );
			mediatorMap.map( FloorTabView ).toMediator( FloorTabMediator );
			mediatorMap.map( ItemsTabView ).toMediator( ItemsTabMediator );
			mediatorMap.map( MainTabView ).toMediator( MainTabMediator );
			mediatorMap.map( BlisLevelPreview ).toMediator( LevelPreviewMediator );
			mediatorMap.map( ObstacleMapTabView ).toMediator( ObstacleMapTabMediator );

			// Map models.
			injector.map( WavesModel ).asSingleton();
			injector.map( ObstacleMapModel ).asSingleton();
			injector.map( ItemsModel ).asSingleton();
			injector.map( GridModel ).asSingleton();
			injector.map( MouseModeModel ).asSingleton();
			injector.map( InitLoaderModel ).asSingleton();
			injector.map( ConfigModel ).asSingleton();
			injector.map( MessegeModel ).asSingleton();
			injector.map( FloorModel ).asSingleton();
			injector.map( LevelConfigParser ).asSingleton();

			// Map services.
			injector.map( ILocalFilesService ).toSingleton( LocalFilesService );
			injector.map( ILevelService ).toSingleton( LevelFromFileService );

			// Start.
			context.afterInitializing( init );

		}

		private function init():void {

			logger.info( "application ready" );

		}
	}
}
