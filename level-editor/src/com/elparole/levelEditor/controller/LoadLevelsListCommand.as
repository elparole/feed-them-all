package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.MessegeModel;
import com.elparole.levelEditor.service.ILocalFilesService;

import robotlegs.bender.framework.api.ILogger;

	public class LoadLevelsListCommand
	{
		[Inject]
		public var initLoaderModel:InitLoaderModel;
		
		[Inject]
		public var localFileService:ILocalFilesService;
		
		[Inject]
		public var configModel:ConfigModel;

		[Inject]
		public var messegeModel:MessegeModel;

		[Inject]
		public var logger:ILogger;

		public function execute() {

			logger.info( "triggering LoadLevelsListCommand" );
			if(!configModel.levelTargetDir || configModel.levelTargetDir.length==0){
				messegeModel.onStepError(null,null);
				return;
			}


			configModel.setLevelsDir(localFileService.updateConfig(configModel.levelTargetDir),null);
			messegeModel.onStepComplete(null,null);
//			initLoaderModel.startLoader();

		}
	}
}
