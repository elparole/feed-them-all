package com.elparole.levelEditor.controller
{

import com.elparole.feedThemAll.model.ItemVO;
import com.elparole.levelEditor.model.FloorModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.ItemsModel;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.model.vo.FloorItemVO;
import com.elparole.levelEditor.service.ILocalFilesService;

import robotlegs.bender.framework.api.ILogger;

	public class SelectItemCommand
	{

		[Inject]
		public var item:ItemVO;

		[Inject]
		public var floorModel:FloorModel;

		[Inject]
		public var itemsModel:ItemsModel;
		
		[Inject]
		public var logger:ILogger;

		public function execute() {

			logger.info( "triggering SetPageCommand" );

			floorModel.selectedItem = item;
			itemsModel.selectedItem = item;
		}
	}
}
