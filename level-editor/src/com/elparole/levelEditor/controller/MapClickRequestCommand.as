package com.elparole.levelEditor.controller
{

import com.elparole.feedThemAll.model.ItemVO;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.FloorModel;
import com.elparole.levelEditor.model.GridModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.ItemsModel;
import com.elparole.levelEditor.model.MessegeModel;
import com.elparole.levelEditor.model.MouseModeModel;
import com.elparole.levelEditor.model.ObstacleMapModel;
import com.elparole.levelEditor.model.WavesModel;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.model.vo.MouseRequestVO;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.service.ILocalFilesService;

import flash.geom.Point;

import flash.utils.setInterval;

import robotlegs.bender.framework.api.ILogger;

import utils.mvc.IModel;

public class MapClickRequestCommand
	{
		[Inject]
		public var request:MouseRequestVO;

		[Inject]
		public var localFileService:ILocalFilesService;

		[Inject]
		public var logger:ILogger;

		[Inject]
		public var mouseModeModel:MouseModeModel;
		
		[Inject]
		public var floorModel:FloorModel;

		[Inject]
		public var itemsModel:ItemsModel;

		[Inject]
		public var wavesModel:WavesModel;

		[Inject]
		public var gridModel:GridModel;

		[Inject]
		public var configModel:ConfigModel;

		[Inject]
		public var messegeModel:MessegeModel;

		[Inject]
		public var obstacleMapModel:ObstacleMapModel;

		public function execute() {

			logger.info( "triggering MapClickRequestCommand" );
					
			if(request.type == MouseRequestVO.CLICK){
				if(mouseModeModel.snapToGrid){
					gridModel.snap(request.coordsIso);
				}
	
//				if(request.objectsUnderMouse.length==0){
				mouseModeModel.addMode &&
						mouseModeModel.floorMode &&
							floorModel.addFloorItem(null,request.coordsIso);


				if(mouseModeModel.addMode &&
					mouseModeModel.itemsMode &&
						itemsModel.selectedItem &&
						(!itemsModel.hasToBeOnObstacleMap(itemsModel.selectedItem)
							||obstacleMapModel.fitToRect(request.coordsIso))){

					var addedItem:IsoEditItem = itemsModel.addItem(null,request.coordsIso);
					if(AnimsIDs.isMonsterCave(addedItem.display.skinId)){
						wavesModel.addSource(addedItem);
					}else if(addedItem.display.skinId==AnimsIDs.SHEEP){
						wavesModel.addTarget(addedItem);
					}
				}
//				setInterval(onAdd,500);
//				}
//				else{
				if(mouseModeModel.eraseMode && mouseModeModel.floorMode)
							floorModel.removeFloorItemByPlane(request.objectsUnderMouse[0]);

				if(mouseModeModel.eraseMode && mouseModeModel.itemsMode){
					wavesModel.removeTarget(itemsModel.getIsoEditFromIsoAnim(request.objectsUnderMouse[0]));
					wavesModel.removeSource(itemsModel.getIsoEditFromIsoAnim(request.objectsUnderMouse[0]));
					itemsModel.removePlaneItemByPlane(request.objectsUnderMouse[0]);
				}

				mouseModeModel.enableMode &&
						mouseModeModel.obstacleMapMode &&
							obstacleMapModel.enableTile(request);

				mouseModeModel.disableMode &&
						mouseModeModel.obstacleMapMode &&
							obstacleMapModel.disableTile(request);
				
				mouseModeModel.waveEditMode &&
						wavesModel.setSelectedItem(request.objectsUnderMouse[0]);
//				}
			}
//			else if(request.type == MouseRequestVO.MOUSE_DOWN){
//
//			}else if(request.type == MouseRequestVO.MOUSE_UP){
//
//			}

//			initLoaderModel.startLoader();

		}

		private function onAdd():void {
			request.coordsIso.x+=Math.random()*100-50;
			request.coordsIso.y+=Math.random()*100-50;

			floorModel.addFloorItem(null,request.coordsIso);
		}
	}
}
