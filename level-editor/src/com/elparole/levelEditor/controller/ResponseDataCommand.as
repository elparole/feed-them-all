package com.elparole.levelEditor.controller
{

import com.elparole.feedThemAll.model.MapModel;
import com.elparole.levelEditor.model.ObstacleMapModel;
import com.elparole.levelEditor.model.vo.AssetsListVO;
import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.vo.DataRequestVO;
import com.elparole.levelEditor.service.ILocalFilesService;
import com.elparole.levelEditor.view.BlisLevelPreview;
import com.elparole.levelEditor.view.FloorTabView;

import flash.geom.Point;

import mx.collections.ArrayCollection;

import robotlegs.bender.framework.api.ILogger;

	public class ResponseDataCommand
	{
		[Inject]
		public var dataRequestVO:DataRequestVO;

		[Inject]
		public var initLoaderModel:InitLoaderModel;

		[Inject]
		public var configModel:ConfigModel;

		[Inject]
		public var obstacleMap:ObstacleMapModel;

		[Inject]
		public var localFileService:ILocalFilesService;

		[Inject]
		public var logger:ILogger;

		public function execute() {

			logger.info( "triggering ResponseDataCommand" );
			switch(dataRequestVO.viewClass){
				case FloorTabView:
					dataRequestVO.data = configModel.assetsListVO;
					break;
				case BlisLevelPreview:
					dataRequestVO.data = new Point(obstacleMap.mapModel.bounds.width, obstacleMap.mapModel.bounds.height);
					break;
			}
		}
	}
}
