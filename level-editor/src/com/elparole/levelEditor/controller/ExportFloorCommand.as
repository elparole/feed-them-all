package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.AppConsts;
import com.elparole.levelEditor.model.FloorModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.ItemsModel;
import com.elparole.levelEditor.model.MouseModeModel;
import com.elparole.levelEditor.service.ILocalFilesService;
import com.elparole.levelEditor.view.FloorTabView;
import com.elparole.levelEditor.view.ItemsTabView;

import robotlegs.bender.framework.api.ILogger;

	public class ExportFloorCommand
	{
		[Inject]
		public var mouseMode:MouseModeModel;

		[Inject]
		public var floorModel:FloorModel;

		[Inject]
		public var logger:ILogger;

		public function execute() {

			logger.info( "triggering ExportFloorCommand" );


		}
	}
}
