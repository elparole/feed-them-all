package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.AppConsts;
import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.FloorModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.ItemsModel;
import com.elparole.levelEditor.model.MouseModeModel;
import com.elparole.levelEditor.model.ObstacleMapModel;
import com.elparole.levelEditor.model.WavesModel;
import com.elparole.levelEditor.service.ILocalFilesService;
import com.elparole.levelEditor.signals.notifications.SetupTabSignal;
import com.elparole.levelEditor.view.FloorTabView;
import com.elparole.levelEditor.view.ItemsTabView;
import com.elparole.levelEditor.view.ObstacleMapTabView;
import com.elparole.levelEditor.view.WaveEditView;

import robotlegs.bender.framework.api.ILogger;

import utils.load.calculateMillisecondsUntilBuffered;

public class SetPageCommand
	{
		[Inject]
		public var pageClass:Class;

		[Inject]
		public var mouseMode:MouseModeModel;

		[Inject]
		public var floorModel:FloorModel;

		[Inject]
		public var itemsModel:ItemsModel;

		[Inject]
		public var obstacleMapModel:ObstacleMapModel;

		[Inject]
		public var configModel:ConfigModel;

		[Inject]
		public var setupTab:SetupTabSignal;

		[Inject]
		public var wavesModel:WavesModel;

		[Inject]
		public var logger:ILogger;

		public function execute() {

			logger.info( "triggering SetPageCommand" );

			mouseMode.floorMode = pageClass == FloorTabView;
			floorModel.setItemsAlpha(pageClass == FloorTabView? AppConsts.ENABLED_ALPHA:AppConsts.DISABLED_ALPHA);

			mouseMode.itemsMode = pageClass == ItemsTabView;
			itemsModel.setItemsAlpha(pageClass == ItemsTabView? AppConsts.ENABLED_ALPHA:AppConsts.DISABLED_ALPHA);

			mouseMode.obstacleMapMode = pageClass == ObstacleMapTabView;

			mouseMode.waveEditMode = pageClass == WaveEditView;

			if(pageClass != ObstacleMapTabView)
				obstacleMapModel.clear();
			else
				obstacleMapModel.showObstacleMap(configModel.levelSize);

			if(pageClass == FloorTabView || pageClass == ItemsTabView){
				mouseMode.changeMode(MouseModeModel.ADD_MODE, true);
				mouseMode.snapToGrid = true;
				setupTab.dispatch();
			}
			if(pageClass == ObstacleMapTabView || pageClass == WaveEditView ) {
				mouseMode.changeMode(MouseModeModel.ENABLE_MODE, true);
				itemsModel.selectedItem = null;
				floorModel.selectedItem = null;
				setupTab.dispatch();
			}
			if(pageClass!=WaveEditView){
				wavesModel.setSelectedItem(null);
				wavesModel.pause();
				wavesModel.clearSourcesChildren();
			}else{
				wavesModel.fillDestinations(true);
				wavesModel.addSourcesChildren();
			}
		}
	}
}
