package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.MouseModeModel;
import com.elparole.levelEditor.service.ILocalFilesService;

import robotlegs.bender.framework.api.ILogger;

	public class ChangeMouseModeCommand
	{
		[Inject]
		public var propertyId:String;

		[Inject]
		public var value:Boolean;

		[Inject]
		public var mouseMode:MouseModeModel;

		[Inject]
		public var logger:ILogger;

		public function execute() {

			logger.info( "triggering ChangeMouseModeCommand" );

			mouseMode.changeMode(propertyId,value);

		}
	}
}
