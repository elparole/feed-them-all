package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.service.ILocalFilesService;

import robotlegs.bender.framework.api.ILogger;

	public class StartupCommand
	{
		[Inject]
		public var initLoaderModel:InitLoaderModel;

		[Inject]
		public var logger:ILogger;

		public function execute() {

			logger.info( "triggering StartupCommand" );

			initLoaderModel.startLoader();

		}
	}
}
