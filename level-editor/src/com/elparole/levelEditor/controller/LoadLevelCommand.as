package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.FloorModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.ItemsModel;
import com.elparole.levelEditor.model.MessegeModel;
import com.elparole.levelEditor.model.ObstacleMapModel;
import com.elparole.levelEditor.model.WavesModel;
import com.elparole.levelEditor.model.vo.LevelConfig;
import com.elparole.levelEditor.model.vo.LevelVO;
import com.elparole.levelEditor.service.ILevelService;
import com.elparole.levelEditor.service.ILocalFilesService;

import robotlegs.bender.framework.api.ILogger;

	public class LoadLevelCommand
	{
		[Inject]
		public var levelVO:LevelVO

		[Inject]
		public var levelLoadService:ILevelService;

		[Inject]
		public var localFileService:ILocalFilesService;

		[Inject]
		public var logger:ILogger;

		[Inject]
		public var configModel:ConfigModel;

		[Inject]
		public var floorsModel:FloorModel;
		
		[Inject]
		public var itemsModel:ItemsModel;

		[Inject]
		public var obstacleMap:ObstacleMapModel;

		[Inject]
		public var messegeModel:MessegeModel;

		[Inject]
		public var wavesModel:WavesModel;

		public function execute() {

			logger.info( "triggering LoadLevelCommand" );
			if(!levelVO){
//				messegeModel.onStepError({},null);
				messegeModel.onStepComplete({},null);
				return;
			}
			
			configModel.levelName = levelVO.name;
			levelLoadService.loadConfig(levelVO.name, levelVO.path)
					.addResultProcessor(configModel.fillItems)
					.addResultProcessor(floorsModel.clear)
					.addResultProcessor(itemsModel.clear)
					.addResultProcessor(wavesModel.clear)
					.addResultProcessor(floorsModel.addLevelFloors)
					.addResultProcessor(itemsModel.addLevelItems)
					.addResultProcessor(configModel.setLevelSize)
					.addResultProcessor(obstacleMap.setObstacleMap)
					.addResultProcessor(saveLevelToRecent)
					.addResultProcessor(wavesModel.addLevelSrcsAndTrgts)
					.addResultProcessor(messegeModel.onStepComplete);
//					.addResultProcessor(configModel.setAssetsVO)
//					.addResultProcessor(messegeModel.onStepComplete)
//					.addErrorHandler(messegeModel.onFileSystemError);
//			initLoaderModel.startLoader();

		}

		private function saveLevelToRecent(config:LevelConfig, err:Object):*{
			if(!config)
				return config;

			configModel.recentLevelsPaths.push(config.name);
			while(configModel.recentLevelsPaths.length>10){
				configModel.recentLevelsPaths.shift();
			}
			localFileService.saveProjConfig(configModel.recentLevelsPaths,configModel.assetsPath, configModel.levelTargetDir);
			
			return config;
		}
	}
}
