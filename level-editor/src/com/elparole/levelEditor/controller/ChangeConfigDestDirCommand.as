package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.MessegeModel;
import com.elparole.levelEditor.service.ILocalFilesService;
import com.elparole.levelEditor.signals.LoadLevelsListSignal;

import robotlegs.bender.framework.api.ILogger;

	public class ChangeConfigDestDirCommand
	{
		[Inject]
		public var localFileService:ILocalFilesService;

		[Inject]
		public var logger:ILogger;

		[Inject]
		public var configModel:ConfigModel;
		
		[Inject]
		public var loadLevelsList:LoadLevelsListSignal;

		[Inject]
		public var messegeModel:MessegeModel;

		public function execute() {

			logger.info( "triggering ChangeAssetsDirCommand" );
			
			localFileService.changeConfig('changeConfigDestDir')
					.addResultProcessor(configModel.setLevelsDir)
					.addResultProcessor(onLoadLevelsList)
//					.addResultProcessor(messegeModel.onStepComplete)
					.addErrorHandler(messegeModel.onFileSystemError);
//			initLoaderModel.startLoader();

		}

		private function onLoadLevelsList(str:String, err:Object):String {
			loadLevelsList.dispatch();
			return str;
		}
	}
}
