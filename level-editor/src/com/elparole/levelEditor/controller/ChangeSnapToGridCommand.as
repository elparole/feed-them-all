package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.FloorModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.MouseModeModel;
import com.elparole.levelEditor.service.ILocalFilesService;
import com.elparole.levelEditor.view.FloorTabView;
import com.elparole.levelEditor.view.ItemsTabView;

import robotlegs.bender.framework.api.ILogger;

	public class ChangeSnapToGridCommand
	{
		[Inject]
		public var value:Boolean;

		[Inject]
		public var mouseMode:MouseModeModel;

		[Inject]
		public var logger:ILogger;

		public function execute() {

			logger.info( "triggering SetPageCommand" );

			mouseMode.snapToGrid = value;
		}
	}
}
