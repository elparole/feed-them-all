package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.MessegeModel;
import com.elparole.levelEditor.model.WavesModel;
import com.elparole.levelEditor.service.ILocalFilesService;

import robotlegs.bender.framework.api.ILogger;

	public class RemoveChildCommand
	{

		[Inject]
		public var wavesModel:WavesModel;

		[Inject]
		public var logger:ILogger;


		public function execute() {

			logger.info( "triggering ChangeAssetsDirCommand" );
			wavesModel.removeSelectedChild();
//			initLoaderModel.startLoader();

		}
	}
}
