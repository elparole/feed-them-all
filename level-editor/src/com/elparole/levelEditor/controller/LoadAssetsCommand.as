package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.MessegeModel;
import com.elparole.levelEditor.model.vo.AssetsListVO;
import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.service.ILocalFilesService;

import mx.collections.ArrayCollection;

import robotlegs.bender.framework.api.ILogger;

	public class LoadAssetsCommand
	{
		[Inject]
		public var initLoaderModel:InitLoaderModel;

		[Inject]
		public var configModel:ConfigModel;

		[Inject]
		public var localFileService:ILocalFilesService;

		[Inject]
		public var messegeModel:MessegeModel;

		[Inject]
		public var logger:ILogger;

		public function execute() {

			logger.info( "triggering LoadAssetsCommand" );

			if(!configModel.assetsPath || configModel.assetsPath.length==0){
				messegeModel.onStepError(null,null);
				return;
			}

			var itemsList:AssetsListVO = localFileService.updateAssets(configModel.assetsPath);

			logger.info('itemsList '+itemsList.assets+' configModel.assetsPath:'+configModel.assetsPath)
			localFileService.loadBitmaps(itemsList)
						.addResultProcessor(configModel.setAssetsVO)
						.addResultProcessor(messegeModel.onStepComplete)
						.addErrorHandler(messegeModel.onStepError);


		}
	}
}
