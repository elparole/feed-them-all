package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.MessegeModel;
import com.elparole.levelEditor.service.ILocalFilesService;

import robotlegs.bender.framework.api.ILogger;

	public class ChangeAssetsDirCommand
	{
		[Inject]
		public var localFileService:ILocalFilesService;

		[Inject]
		public var logger:ILogger;

		[Inject]
		public var configModel:ConfigModel;

		[Inject]
		public var messegeModel:MessegeModel;

		public function execute() {

			logger.info( "triggering ChangeAssetsDirCommand" );
			
			localFileService.changeConfig('assetsPath')
					.addResultProcessor(configModel.setAssetsVO)
//					.addResultProcessor(messegeModel.onStepComplete)
					.addErrorHandler(messegeModel.onFileSystemError);
//			initLoaderModel.startLoader();

		}
	}
}
