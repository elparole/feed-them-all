package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.MessegeModel;
import com.elparole.levelEditor.model.WavesModel;
import com.elparole.levelEditor.service.ILocalFilesService;
import robotlegs.bender.framework.api.ILogger;

	public class SetPositionsInTimeCommand
	{
		[Inject]
		public var time:int

		[Inject]
		public var logger:ILogger;

		[Inject]
		public var wavesModel:WavesModel;

		public function execute() {

//			logger.info( "triggering SetPositionsInTimeCommand" );

			wavesModel.setPositionsInTime(time);
//			initLoaderModel.startLoader();

		}
	}
}
