package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.MessegeModel;
import com.elparole.levelEditor.service.ILocalFilesService;

import flash.geom.Point;

import robotlegs.bender.framework.api.ILogger;

	public class CreateNewLevelCommand
	{
		[Inject]
		public var levelName:String;

		[Inject]
		public var levelSize:Point;

		[Inject]
		public var localFileService:ILocalFilesService;

		[Inject]
		public var logger:ILogger;

		[Inject]
		public var configModel:ConfigModel;

		[Inject]
		public var messegeModel:MessegeModel;

		public function execute() {

			logger.info( "triggering CreateNewLevelCommand" );
			
			levelName = localFileService.createNewLevel(levelName, configModel.levelTargetDir, levelSize)
			configModel.levelName = levelName;
			configModel.levelSize = levelSize;
			configModel.setLevelsDir(localFileService.updateConfig(configModel.levelTargetDir),null);
//			initLoaderModel.startLoader();

		}
	}
}
