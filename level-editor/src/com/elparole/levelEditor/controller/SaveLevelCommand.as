package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.FloorModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.ItemsModel;
import com.elparole.levelEditor.model.MessegeModel;
import com.elparole.levelEditor.model.ObstacleMapModel;
import com.elparole.levelEditor.model.WavesModel;
import com.elparole.levelEditor.service.ILocalFilesService;

import flash.utils.Dictionary;

import robotlegs.bender.framework.api.ILogger;

	public class SaveLevelCommand
	{
		[Inject]
		public var localFileService:ILocalFilesService;

		[Inject]
		public var logger:ILogger;

		[Inject]
		public var configModel:ConfigModel;

		[Inject]
		public var floorsModel:FloorModel;
		
		[Inject]
		public var itemsModel:ItemsModel;

		[Inject]
		public var obstacleMap:ObstacleMapModel;

		[Inject]
		public var wavesModel:WavesModel;

		[Inject]
		public var messegeModel:MessegeModel;

		public function execute() {

			logger.info( "triggering SaveLevelCommand" );
			
			localFileService.saveConfig(
					floorsModel.items,
					itemsModel.items,
					wavesModel.getAllChildrenList(),
					obstacleMap.obstacleMap,
					configModel.levelSize,
					configModel.levelName,
					configModel.levelTargetDir)
//					.addResultProcessor(configModel.setAssetsVO)
//					.addResultProcessor(messegeModel.onStepComplete)
//					.addErrorHandler(messegeModel.onFileSystemError);
//			initLoaderModel.startLoader();

		}
	}
}
