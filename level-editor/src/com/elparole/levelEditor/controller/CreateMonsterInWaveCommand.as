package com.elparole.levelEditor.controller
{

import com.elparole.feedThemAll.view.AnimsIDs;
import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.MessegeModel;
import com.elparole.levelEditor.model.WavesModel;
import com.elparole.levelEditor.service.ILocalFilesService;

import robotlegs.bender.framework.api.ILogger;

	public class CreateMonsterInWaveCommand
	{
		[Inject]
		public var startTime:int;

		[Inject]
		public var type:String;
		
		[Inject]
		public var cloneMode:Boolean;

		[Inject]
		public var logger:ILogger;

		[Inject]
		public var messegeModel:MessegeModel;

		[Inject]
		public var wavesModel:WavesModel;

		public function execute() {

			logger.info( "triggering CreateMonsterInWaveCommand" );
			var sourceId:int;
			if(cloneMode){
				sourceId = wavesModel.getPFIsoItemFromIsoAnim(wavesModel.selectedChild.display.renderItem,true);
				startTime = wavesModel.getPFIsoItemFromIsoAnim(wavesModel.selectedChild.display.renderItem,false).startTime+startTime;
			}
			
//			wavesModel.selectedChild
			wavesModel.addSourceChild(AnimsIDs.MONSTER+type,cloneMode? sourceId:-1,startTime,true);
//			initLoaderModel.startLoader();

		}
	}
}
