package com.elparole.levelEditor.controller
{

import com.elparole.levelEditor.model.ConfigModel;
import com.elparole.levelEditor.model.InitLoaderModel;
import com.elparole.levelEditor.model.MessegeModel;
import com.elparole.levelEditor.service.ILocalFilesService;

import robotlegs.bender.framework.api.ILogger;

	public class EnsureProjFileCommand
	{
		[Inject]
		public var initLoaderModel:InitLoaderModel;
		
		[Inject]
		public var localFileService:ILocalFilesService;
		
		[Inject]
		public var configModel:ConfigModel;

		[Inject]
		public var messegeModel:MessegeModel;

		[Inject]
		public var logger:ILogger;

		public function execute() {

			logger.info( "triggering EnsureProjFileCommand" );

			localFileService.ensureProjFileExist()
					.addResultProcessor(configModel.setConfig)
					.addResultProcessor(messegeModel.onStepComplete)
					.addErrorHandler(messegeModel.onStepError);
//			initLoaderModel.startLoader();

		}
	}
}
