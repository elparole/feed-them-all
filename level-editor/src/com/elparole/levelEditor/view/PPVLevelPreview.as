/**
 * User: Elparole
 * Date: 05.03.13
 * Time: 15:53
 */
package com.elparole.levelEditor.view
{
import com.elparole.components.DisplayPPV;
import com.elparole.feedThemAll.view.PPVContainer;
import com.elparole.levelEditor.model.AppConsts;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.levelEditor.model.vo.MouseRequestVO;

import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;

import mx.core.UIComponent;

import net.hires.debug.Stats;

import org.casalib.math.geom.Point3d;
import org.osflash.signals.Signal;
import org.papervision3d.core.math.Number3D;
import org.papervision3d.core.render.data.RenderHitData;
import org.papervision3d.events.InteractiveScene3DEvent;
import org.papervision3d.objects.primitives.Plane;

//public class PPVLevelPreview extends UIComponent
public class PPVLevelPreview extends UIComponent implements ILevelContainerPreview
{
	public var ppvCont:PPVContainer;
	
	private var _contClicked:Signal = new Signal(MouseRequestVO);

	public var clickSurfaceY:int = -600;

	private var disp:DisplayPPV;
	private var dragMode:Boolean = false;
	private var startDrag3DPos:Point3d;
	private var startDragCameraPos:Number3D;
	private var stats:DisplayObject;

	public function PPVLevelPreview() {
		this.ppvCont = new PPVContainer(640,600);
		ppvCont.graphics.beginFill(0x555555,0.1);
		ppvCont.graphics.drawRect(0,0,640,600);
		ppvCont.graphics.endFill();
		addChildAt(ppvCont,0);
		ppvCont.init(true);
		
//		var disp:Display = new Display('t1','a1','d1',new BitmapData(128,128, true, 0xffff0000), new Point(0,0));
//		disp = new Display('t1', 'a1', 'd1', new BitmapData(128, 128, true, 0), new Point(0, 0));
//		disp.bitmap.fillRect(new Rectangle(32,32,64,64),0xffff0000);

//		ppvCont.add3DBitmap(disp,true);
//		ppvCont.movePlaneTo(disp.plane,new Number3D(935,-600,935));
//		disp.plane.addEventListener(InteractiveScene3DEvent.OBJECT_CLICK, onClickA);
//		disp.material.addEventListener(InteractiveScene3DEvent.OBJECT_PRESS, onClickA);


//		disp = new Display('t1','a1','d1',new BitmapData(128,128, true, 0xff0ff000), new Point(0,0));
//		ppvCont.add3DBitmap(disp, true);
//		ppvCont.movePlaneTo(disp.plane,new Number3D(735,-600,735));
//		disp.plane.addEventListener(MouseEvent.CLICK, onClickA);
//		disp.material.addEventListener(InteractiveScene3DEvent.OBJECT_PRESS, onClickA);

		ppvCont.render();
//		disp.plane.container.addEventListener(MouseEvent.CLICK, onClickA);
//		disp.plane.addEventListener(InteractiveScene3DEvent.OBJECT_CLICK, onClickA);
//		disp.plane.addEventListener(InteractiveScene3DEvent.OBJECT_PRESS, onClickA);
		ppvCont.addEventListener(MouseEvent.CLICK, onClick,false,0, true);
		ppvCont.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown,false,0, true);		
	}

	private function onMouseDown(event:MouseEvent):void {
		event.stopImmediatePropagation();
		event.stopPropagation();
		event.preventDefault();

		if(dragMode){
			startDrag3DPos = ppvCont.getScreenTo3DCoords(this.mouseX, this.mouseY, clickSurfaceY);
			startDragCameraPos = ppvCont.camera.position.clone();
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMove,false,0, true);
			stage.addEventListener(MouseEvent.MOUSE_UP, onStopDrag,false,0, true);
		}
	}

	private function onMove(event:MouseEvent):void {

		var mpos:Point3d;
		mpos = ppvCont.getScreenTo3DCoords(this.mouseX, this.mouseY, clickSurfaceY);

		ppvCont.camera.z = startDragCameraPos.z - (mpos.z-startDrag3DPos.z);
		ppvCont.camera.x = startDragCameraPos.x - (mpos.x-startDrag3DPos.x);
//		ppvCont.camera.y = startDragCameraPos.y - (mpos.y-startDrag3DPos.y);
		ppvCont.render();

		startDrag3DPos = ppvCont.getScreenTo3DCoords(this.mouseX, this.mouseY, clickSurfaceY);
		startDragCameraPos = ppvCont.camera.position.clone();

	}

	private function onStopDrag(event:MouseEvent):void {
		stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMove);
		stage.removeEventListener(MouseEvent.MOUSE_UP, onStopDrag);

		var mpos:Point3d;
		mpos = ppvCont.getScreenTo3DCoords(this.mouseX, this.mouseY, clickSurfaceY);


		ppvCont.camera.z = startDragCameraPos.z - (mpos.z-startDrag3DPos.z);
		ppvCont.camera.x = startDragCameraPos.x - (mpos.x-startDrag3DPos.x);
//		ppvCont.camera.y = startDragCameraPos.y - (mpos.y-startDrag3DPos.y);
		ppvCont.render();
	}

	private function onClickA(event:InteractiveScene3DEvent):void {
//		if(event.renderHitData.material.bitmap.getPixel(event.x, event.y)){
			
//		}
		if((event.renderHitData.material.bitmap.getPixel32(event.x, event.y)/0x1000000)<=0){
			return;
		}
//			event.renderHitData.)
		trace('click object');
		contClicked.dispatch(new MouseRequestVO(
				new Point(this.mouseX,this.mouseY),
				ppvCont.getScreenTo3DCoords(this.mouseX, this.mouseY, clickSurfaceY),
				[event.displayObject3D])
		);
	}

	private function onClick(event:Event):void {
//		event.stopImmediatePropagation();
//		event.stopPropagation();
//		event.preventDefault();

		trace('click viewport');
		var mp:Point = new Point(this.mouseX-384+64,this.mouseY-330+64-33);
		var objectsHit:Array = [];
		var nearestIndex:int = 0;
		var maxScreenZ:Number = 1000000;

		ppvCont.viewport.interactive = true;
		for (var i:int = 0; i < ppvCont.scene.numChildren; i++) {
			var diso3D:Plane =(ppvCont.scene.objects[i] as Plane);
			diso3D.material.interactive = true;
			var rhd:RenderHitData = ppvCont.viewport.hitTestPointObject(mp,diso3D);
//					var rhd:RenderHitData = ppvCont.viewport.hitTestPoint2D(mp);
			if(rhd.hasHit && diso3D.alpha >= AppConsts.ENABLED_ALPHA){
				diso3D.calculateScreenCoords(ppvCont.camera);
				if(rhd.material &&((rhd.material.bitmap.getPixel32(mp.x-diso3D.screen.x+64, 64+mp.y-diso3D.screen.y)/0x1000000)>0)){
					objectsHit.push(diso3D);
					if(diso3D.screenZ<maxScreenZ){
						nearestIndex = objectsHit.length-1;
						maxScreenZ =diso3D.screenZ;
					}
				}
			}

			diso3D.material.interactive = false;
		}
		ppvCont.viewport.interactive = false;
		trace('objects hit',objectsHit.length);

//		onFloorItemAdded(null);

		contClicked.dispatch(new MouseRequestVO(
				mp,
				ppvCont.getScreenTo3DCoords(this.mouseX, this.mouseY, clickSurfaceY),
				objectsHit.length>0? [objectsHit[nearestIndex]]:[])
		);
	}

	public function onItemRemoved(item:IsoEditItem, refresh:Boolean):void {
		ppvCont.remove3DBitmap(item.display.plane);
//		item.display.plane.removeEventListener(InteractiveScene3DEvent.OBJECT_CLICK, onClickA);
		ppvCont.render();
	}

	public function onItemAdded(item:IsoEditItem, refresh:Boolean):void {
		if(!stats){
			stats = new Stats();
			stage.addChild(stats);
		}
			
//		item.display.bitmap.fillRect(new Rectangle(0,0,120,120),0xffff0000);

		ppvCont.add3DBitmap(item.display,true);
		ppvCont.movePlaneToPoint(item.display.plane,item.position);
		ppvCont.render();

//		item.display.plane.addEventListener(InteractiveScene3DEvent.OBJECT_CLICK, onClickA);
//		ppvCont.movePlaneTo(item.display.plane,new Number3D(935+Math.random()*200-100,-600,935+Math.random()*200-100));

//		var disp:Display = new Display('t1','a1','d1',new BitmapData(128,128, true, 0xffff0000), new Point(0,0));
//		ppvCont.add3DBitmap(disp,true);
//		ppvCont.movePlaneToPoint(disp.plane,ppvCont.getScreenTo3DCoords(this.mouseX, this.mouseY, clickSurfaceY));
//		var pt:Point3d = new Point3d(Math.random()*2000-1000, Math.random()*2000-1000, Math.random()*2000-1000);
//		var pt:Point3d = new Point3d(Math.random()*200-100, -65, Math.random()*200-100);
//		var pt:Point3d = ppvCont.getScreenTo3DCoords(this.mouseX, this.mouseY, -65);
//				562 -876 217
//				144 204 -132
//				-386 -65 -58
//		trace(pt, pt.x, pt.y, pt.z)
//		ppvCont.movePlaneToPoint(disp.plane,pt);
//		ppvCont.render();

//		trace(item.getPositionN3D())
//		ppvCont.movePlaneTo(item.display.plane,item.getPositionN3D());
//		ppvCont.movePlaneTo(disp.plane,item.getPositionN3D());
	}

	public function onItemsModeSelected(value:Boolean):void {
		if(value)
			clickSurfaceY = 0;
//		clickSurfaceY = 0;
	}

	public function onObstacleModeSelected(value:Boolean):void {
		if(value)
			clickSurfaceY = -300;
	}

	public function onFloorModeSelected(value:Boolean):void {
		if(value)
			clickSurfaceY = -600;
	}

	public function onDragModeSelected(value:Boolean):void {
		dragMode = value;
	}


	public function get contClicked():Signal {
		return _contClicked;
	}
}
}
