/**
 * User: Elparole
 * Date: 05.03.13
 * Time: 15:53
 */
package com.elparole.levelEditor.view
{
import com.elparole.components.DisplayBlis;
import com.elparole.feedThemAll.model.GameUtils;
import com.elparole.feedThemAll.test.IsoAnimItem;
import com.elparole.feedThemAll.view.BlisContainer;
import com.elparole.levelEditor.AppConfig;
import com.elparole.levelEditor.model.AppConsts;
import com.elparole.levelEditor.model.vo.MouseRequestVO;
import com.elparole.levelEditor.model.vo.IsoEditItem;
import com.elparole.randomIntInRange;

import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;

import mx.core.UIComponent;

import net.hires.debug.Stats;

import org.casalib.math.geom.Point3d;

import org.osflash.signals.Signal;

import org.papervision3d.core.math.Number3D;
import org.papervision3d.core.render.data.RenderHitData;
import org.papervision3d.events.InteractiveScene3DEvent;
import org.papervision3d.objects.DisplayObject3D;
import org.papervision3d.objects.primitives.Plane;

//public class PPVLevelPreview extends UIComponent
public class BlisLevelPreview extends UIComponent implements ILevelContainerPreview
{
	public var blisCont:BlisContainer;
	
	private var _contClicked:Signal = new Signal(MouseRequestVO);

	public var clickSurfaceY:int = -600;

	private var dragMode:Boolean = false;
	private var startDrag3DPos:Point;
	private var startDragCameraPos:Point = new Point(-150,-150);
	private var stats:DisplayObject;
	private var objectUnderPoint:IsoAnimItem;
	public var setPositionsInTime:Signal;
	private var mouseStartPt:Point;

	public function BlisLevelPreview() {
		this.blisCont = new BlisContainer(640,600);
//		blisCont.graphics.beginFill(0x555555,0.1);
//		blisCont.graphics.drawRect(0,0,640,600);
//		blisCont.graphics.endFill();
		addChildAt(blisCont,0);
		blisCont.init(true,true);
		
//		var disp:Display = new Display('t1','a1','d1',new BitmapData(128,128, true, 0xffff0000), new Point(0,0));
//		disp = new Display('t1', 'a1', 'd1', new BitmapData(128, 128, true, 0), new Point(0, 0));
//		disp.bitmap.fillRect(new Rectangle(32,32,64,64),0xffff0000);

//		ppvCont.add3DBitmap(disp,true);
//		ppvCont.movePlaneTo(disp.plane,new Number3D(935,-600,935));
//		disp.plane.addEventListener(InteractiveScene3DEvent.OBJECT_CLICK, onClickA);
//		disp.material.addEventListener(InteractiveScene3DEvent.OBJECT_PRESS, onClickA);


//		disp = new Display('t1','a1','d1',new BitmapData(128,128, true, 0xff0ff000), new Point(0,0));
//		ppvCont.add3DBitmap(disp, true);
//		ppvCont.movePlaneTo(disp.plane,new Number3D(735,-600,735));
//		disp.plane.addEventListener(MouseEvent.CLICK, onClickA);
//		disp.material.addEventListener(InteractiveScene3DEvent.OBJECT_PRESS, onClickA);

		blisCont.render(0.33);
//		disp.plane.container.addEventListener(MouseEvent.CLICK, onClickA);
//		disp.plane.addEventListener(InteractiveScene3DEvent.OBJECT_CLICK, onClickA);
//		disp.plane.addEventListener(InteractiveScene3DEvent.OBJECT_PRESS, onClickA);
		blisCont.addEventListener(MouseEvent.CLICK, onClick,false,0, true);
		blisCont.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown,false,0, true);
		blisCont.moveCameraTo(startDragCameraPos.x,startDragCameraPos.y);
	}

	private function onMouseDown(event:MouseEvent):void {
//		event.stopImmediatePropagation();
//		event.stopPropagation();
//		event.preventDefault();

		dragMode = false;
		mouseStartPt = null;
		if(event.ctrlKey){
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMove,false,0, true);
			stage.addEventListener(MouseEvent.MOUSE_UP, onStopDrag,false,0, true);
		}else{
			startDrag3DPos = blisCont.getScreenTo3DCoords(this.mouseX, -this.mouseY, clickSurfaceY);
			mouseStartPt = new Point(this.mouseX, this.mouseY);
//			startDragCameraPos = blisCont.getCameraPosIso();
			trace('startDrag3DPos',startDrag3DPos.x, startDrag3DPos.y);
			trace('startDragCameraPos',startDragCameraPos.x, startDragCameraPos.y);
			trace('cameraX',blisCont.getCameraPos().x);
			trace('caneraY',blisCont.getCameraPos().y);

			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMove,false,0, true);
			stage.addEventListener(MouseEvent.MOUSE_UP, onStopDrag,false,0, true);
		}
	}

	private function onMove(event:MouseEvent):void {


		if(dragMode||(mouseStartPt&&
				GameUtils.distanceSquaredStat(mouseStartPt.x, mouseStartPt.y, this.mouseX, this.mouseY)>25)){
			dragMode = true;
			var mpos:Point;
			mpos = blisCont.getScreenTo3DCoords(this.mouseX, -this.mouseY, clickSurfaceY);

			trace('mpos',mpos.x, mpos.y);

			startDragCameraPos.x += mpos.x - startDrag3DPos.x;
			startDragCameraPos.y += mpos.y - startDrag3DPos.y;

			blisCont.moveCameraTo(startDragCameraPos.x/* + (mpos.x-startDrag3DPos.x)*/,
					startDragCameraPos.y/* + (mpos.y-startDrag3DPos.y)*/);

			blisCont.render(0.33);

			startDrag3DPos = blisCont.getScreenTo3DCoords(this.mouseX, -this.mouseY, clickSurfaceY);
		}

		if(event.ctrlKey){
//			trace('click viewport');
			var mp:Point = new Point(this.mouseX-384+64,this.mouseY-330+64-33);
			blisCont.render(0.33);
			objectUnderPoint = blisCont.getObjectUnderMouse();
//			trace('objectUnderPoint',objectUnderPoint);


			var pt:Point = blisCont.getScreenTo3DCoords(this.mouseX, this.mouseY, clickSurfaceY);
			pt.x = -pt.x;
			pt.y = -pt.y;
			contClicked.dispatch(new MouseRequestVO(
					mp,
					pt,
					objectUnderPoint? [objectUnderPoint]:[])
			);
			blisCont.render(0.33);
		}
//		startDragCameraPos = blisCont.getCameraPos();

	}

	private function onStopDrag(event:MouseEvent):void {
		stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMove);
		stage.removeEventListener(MouseEvent.MOUSE_UP, onStopDrag);

		if(dragMode){
			var mpos:Point;
			mpos = blisCont.getScreenTo3DCoords(this.mouseX, -this.mouseY, clickSurfaceY);

			trace('mpos',mpos.x, mpos.y);

			startDragCameraPos.x += mpos.x - startDrag3DPos.x;
			startDragCameraPos.y += mpos.y - startDrag3DPos.y;

			blisCont.moveCameraTo(startDragCameraPos.x/* + (mpos.x-startDrag3DPos.x)*/,
					startDragCameraPos.y/* + (mpos.y-startDrag3DPos.y)*/);
	//		ppvCont.camera.y = startDragCameraPos.y - (mpos.y-startDrag3DPos.y);
			blisCont.render(0.33);
		}
	}

	private function onClickA(event:InteractiveScene3DEvent):void {
//		if(event.renderHitData.material.bitmap.getPixel(event.x, event.y)){
			
//		}
		if((event.renderHitData.material.bitmap.getPixel32(event.x, event.y)/0x1000000)<=0){
			return;
		}
//			event.renderHitData.)
		trace('click object');
		contClicked.dispatch(new MouseRequestVO(
				new Point(this.mouseX,this.mouseY),
				blisCont.getScreenTo3DCoords(this.mouseX, this.mouseY, clickSurfaceY),
				[event.displayObject3D])
		);
	}

	private function onClick(event:Event):void {
		if(dragMode)
			return;
		trace('click viewport');
		var mp:Point = new Point(this.mouseX-384+64,this.mouseY-330+64-33);
		blisCont.render(0.33);
		objectUnderPoint = blisCont.getObjectUnderMouse();
		trace('objectUnderPoint',objectUnderPoint);

		var pt:Point = blisCont.getScreenTo3DCoords(this.mouseX, this.mouseY, clickSurfaceY);
		pt.x = -pt.x;
		pt.y = -pt.y;
		contClicked.dispatch(new MouseRequestVO(
				mp,
				pt,
				objectUnderPoint? [objectUnderPoint]:[])
		);
		blisCont.render(0.33);
	}

	public function onItemRemoved(item:IsoEditItem,refresh:Boolean):void {
		if(item.display.renderItem)
			blisCont.remove3DBitmap(item.display.renderItem);
//		item.display.plane.removeEventListener(InteractiveScene3DEvent.OBJECT_CLICK, onClickA);,
		if(refresh){
			blisCont.render(0.33);
			blisCont.render(0.33);
		}
	}

	public function onItemAdded(item:IsoEditItem,refresh:Boolean):void {
//		if(!stats){
//			stats = new Stats();
//			stage.addChild(stats);
//		}
			
//		item.display.bitmap.fillRect(new Rectangle(0,0,120,120),0xffff0000);

		blisCont.add3DBitmap(item.display,item.position.y);
		blisCont.movePlaneToPoint(item.display.renderItem,item.position);
		if(refresh){
			blisCont.render(0.33);
			blisCont.render(0.33);
		}

//		item.display.plane.addEventListener(InteractiveScene3DEvent.OBJECT_CLICK, onClickA);
//		ppvCont.movePlaneTo(item.display.plane,new Number3D(935+Math.random()*200-100,-600,935+Math.random()*200-100));

//		var disp:Display = new Display('t1','a1','d1',new BitmapData(128,128, true, 0xffff0000), new Point(0,0));
//		ppvCont.add3DBitmap(disp,true);
//		ppvCont.movePlaneToPoint(disp.plane,ppvCont.getScreenTo3DCoords(this.mouseX, this.mouseY, clickSurfaceY));
//		var pt:Point3d = new Point3d(Math.random()*2000-1000, Math.random()*2000-1000, Math.random()*2000-1000);
//		var pt:Point3d = new Point3d(Math.random()*200-100, -65, Math.random()*200-100);
//		var pt:Point3d = ppvCont.getScreenTo3DCoords(this.mouseX, this.mouseY, -65);
//				562 -876 217
//				144 204 -132
//				-386 -65 -58
//		trace(pt, pt.x, pt.y, pt.z)
//		ppvCont.movePlaneToPoint(disp.plane,pt);
//		ppvCont.render();

//		trace(item.getPositionN3D())
//		ppvCont.movePlaneTo(item.display.plane,item.getPositionN3D());
//		ppvCont.movePlaneTo(disp.plane,item.getPositionN3D());
	}

	public function onItemsModeSelected(value:Boolean):void {
		if(value)
			clickSurfaceY = 0;
//		clickSurfaceY = 0;
	}

	public function onObstacleModeSelected(value:Boolean):void {
		if(value)
			clickSurfaceY = -300;
	}

	public function onFloorModeSelected(value:Boolean):void {
		if(value)
			clickSurfaceY = -600;
	}

	public function onDragModeSelected(value:Boolean):void {
		dragMode = value;
	}

	public function get contClicked():Signal {
		return _contClicked;
	}

	public function onRefresh(...args):void {
		blisCont.render(0.33);
		blisCont.render(0.33);
	}

	public function renderGrid(size:Point):void {
		blisCont.renderGrid(size.x,size.y);
	}
}
}
