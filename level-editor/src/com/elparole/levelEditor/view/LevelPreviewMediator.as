package com.elparole.levelEditor.view
{
import com.blackmoondev.utils.ActivityPasserMediator;
import com.elparole.feedThemAll.view.BlisContainer;
import com.elparole.levelEditor.signals.MapClickRequestSignal;
import com.elparole.levelEditor.model.vo.AssetsListVO;
import com.elparole.levelEditor.model.vo.DataRequestVO;
import com.elparole.levelEditor.model.vo.FloorItemVO;
import com.elparole.levelEditor.signals.notifications.AssetsDirChangedSignal;
import com.elparole.levelEditor.signals.notifications.DragModeSelectedSignal;
import com.elparole.levelEditor.signals.notifications.FloorItemAddedSignal;
import com.elparole.levelEditor.signals.notifications.FloorItemRemovedSignal;
import com.elparole.levelEditor.signals.notifications.FloorItemsListChangedSignal;
import com.elparole.levelEditor.signals.notifications.FloorModeSelectedSignal;
import com.elparole.levelEditor.signals.notifications.ItemAddedSignal;
import com.elparole.levelEditor.signals.notifications.ItemRemovedSignal;
import com.elparole.levelEditor.signals.notifications.ItemsModeSelectedSignal;
import com.elparole.levelEditor.signals.notifications.MapSizeChangedSignal;
import com.elparole.levelEditor.signals.notifications.NotifyImageSelectedSignal;
import com.elparole.levelEditor.signals.notifications.ObstaclesModeSelectedSignal;
import com.elparole.levelEditor.signals.notifications.RefreshPreviewSignal;
import com.elparole.levelEditor.signals.notifications.TickSignal;
import com.elparole.levelEditor.signals.requests.ResponseDataSignal;
import com.elparole.levelEditor.signals.requests.SetPositionsInTimeSignal;
import com.elparole.levelEditor.view.MainTabNavigator;

import flash.geom.Point;

import org.osflash.signals.Signal;

import robotlegs.bender.bundles.mvcs.Mediator;

import robotlegs.bender.framework.api.ILogger;

	public class LevelPreviewMediator extends ActivityPasserMediator
	{
		[Inject]
		public var logger:ILogger;

		[Inject]
		public var view:BlisLevelPreview;
		[Inject]
		public var floorItemAdded:FloorItemAddedSignal;

		[Inject]
		public var floorItemRemoved:FloorItemRemovedSignal;

		[Inject]
		public var itemAdded:ItemAddedSignal;

		[Inject]
		public var itemRemoved:ItemRemovedSignal;

		[Inject]
		public var mapClickRequest:MapClickRequestSignal;

		[Inject]
		public var floorModeSelected:FloorModeSelectedSignal;

		[Inject]
		public var itemsModeSelected:ItemsModeSelectedSignal;

		[Inject]
		public var obstaclesModeSelected:ObstaclesModeSelectedSignal;
		
		[Inject]
		public var dragModeSelected:DragModeSelectedSignal;

		[Inject]
		public var setPositionsInTime:SetPositionsInTimeSignal;
		
		[Inject]
		public var requestData:ResponseDataSignal;

		[Inject]
		public var mapSizeChanged:MapSizeChangedSignal;

		[Inject]
		public var tick:TickSignal;
		
		[Inject]
		public var refreshPreview:RefreshPreviewSignal;


		override public function initialize():void {

//			logger.info( "initialized" );
			addSignalPasser(view.contClicked, mapClickRequest);
			addToSignal(itemAdded, view.onItemAdded);
			addToSignal(itemRemoved, view.onItemRemoved);
			addToSignal(floorItemAdded, view.onItemAdded);
			addToSignal(floorItemRemoved, view.onItemRemoved);
			addToSignal(floorModeSelected, view.onFloorModeSelected);
			addToSignal(itemsModeSelected, view.onItemsModeSelected);
			addToSignal(obstaclesModeSelected, view.onObstacleModeSelected);
			addToSignal(dragModeSelected, view.onDragModeSelected);
			addToSignal(refreshPreview, view.onRefresh);
			addToSignal(mapSizeChanged, view.renderGrid);
			addToSignal(tick, view.onRefresh);

			var dr:DataRequestVO = new DataRequestVO(BlisLevelPreview);
			requestData.dispatch(dr);
			view.renderGrid(dr.data as Point);
			

			// From app.

		}
	}
}
