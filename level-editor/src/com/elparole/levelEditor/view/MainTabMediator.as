package com.elparole.levelEditor.view
{
import com.blackmoondev.utils.ActivityPasserMediator;
import com.elparole.levelEditor.model.vo.FloorItemVO;
import com.elparole.levelEditor.signals.ChangeAssetsDirSignal;
import com.elparole.levelEditor.signals.ChangeConfigDestDirSignal;
import com.elparole.levelEditor.signals.LoadLevelSignal;
import com.elparole.levelEditor.signals.SaveLevelSignal;
import com.elparole.levelEditor.signals.notifications.AssetsDirChangedSignal;
import com.elparole.levelEditor.signals.notifications.LevelNameChangedSignal;
import com.elparole.levelEditor.signals.notifications.LevelSizeChangedSignal;
import com.elparole.levelEditor.signals.notifications.LevelsListChangedSignal;
import com.elparole.levelEditor.signals.notifications.NotifyImageSelectedSignal;
import com.elparole.levelEditor.signals.requests.CreateNewLevelSignal;
import com.elparole.levelEditor.signals.requests.ExportFloorSignal;
import com.elparole.levelEditor.view.MainTabNavigator;

import flash.events.MouseEvent;

import org.osflash.signals.Signal;

import robotlegs.bender.bundles.mvcs.Mediator;

import robotlegs.bender.framework.api.ILogger;

	public class MainTabMediator extends ActivityPasserMediator
	{
		[Inject]
		public var logger:ILogger;

		[Inject]
		public var view:MainTabView;

		[Inject]
		public var changeAssetsDir:ChangeAssetsDirSignal;

		[Inject]
		public var changeTargetDir:ChangeConfigDestDirSignal;

		[Inject]
		public var createNewLevel:CreateNewLevelSignal;

		[Inject]
		public var levelsListChanged:LevelsListChangedSignal;

		[Inject]
		public var assetsDirChanged:AssetsDirChangedSignal;

		[Inject]
		public var saveLevel:SaveLevelSignal;

		[Inject]
		public var exportFloor:ExportFloorSignal;

		[Inject]
		public var loadLevel:LoadLevelSignal;

		[Inject]
		public var levelNameChanged:LevelNameChangedSignal;
		
		[Inject]
		public var levelSizeChanged:LevelSizeChangedSignal;

		override public function initialize():void {

			logger.info( "initialized" );

			addEventSignalPasser(view.changeAssetsBt,MouseEvent.CLICK,changeAssetsDir);
			addEventSignalPasser(view.changeConfigDestBt,MouseEvent.CLICK,changeTargetDir);
			addEventSignalPasser(view.saveLevelBt,MouseEvent.CLICK,saveLevel);
			addEventSignalPasser(view.exportFloorBt,MouseEvent.CLICK,saveLevel);
			addSignalPasser(view.newLevel, createNewLevel);
			addSignalPasser(view.loadLevel, loadLevel);
			addToSignal(assetsDirChanged, view.setAssetsDir);
			addToSignal(levelsListChanged, view.setLevels);
			addToSignal(levelNameChanged, view.setLevelName);
			addToSignal(levelSizeChanged, view.setLevelSize);
			// From app.
		}
	}
}
