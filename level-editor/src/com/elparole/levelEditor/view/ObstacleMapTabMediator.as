package com.elparole.levelEditor.view
{
import com.blackmoondev.utils.ActivityPasserMediator;
import com.elparole.levelEditor.model.vo.AssetsListVO;
import com.elparole.levelEditor.model.vo.DataRequestVO;
import com.elparole.levelEditor.model.vo.FloorItemVO;
import com.elparole.levelEditor.signals.notifications.AssetsDirChangedSignal;
import com.elparole.levelEditor.signals.notifications.NotifyImageSelectedSignal;
import com.elparole.levelEditor.signals.notifications.SetupTabSignal;
import com.elparole.levelEditor.signals.requests.ChangeMouseModeSignal;
import com.elparole.levelEditor.signals.requests.ChangeSnapToGridSignal;
import com.elparole.levelEditor.signals.requests.ResponseDataSignal;
import com.elparole.levelEditor.signals.requests.SelectItemSignal;
import com.elparole.levelEditor.view.MainTabNavigator;

import robotlegs.bender.bundles.mvcs.Mediator;

import robotlegs.bender.framework.api.ILogger;

	public class ObstacleMapTabMediator extends ActivityPasserMediator
	{
		[Inject]
		public var logger:ILogger;

		[Inject]
		public var view:ObstacleMapTabView;

		[Inject]
		public var assetsDirChanged:AssetsDirChangedSignal;

		[Inject]
		public var requestData:ResponseDataSignal;

		[Inject]
		public var selectItem:SelectItemSignal;

		[Inject]
		public var changeMouseMode:ChangeMouseModeSignal;

		[Inject]
		public var changeSnapToGrid:ChangeSnapToGridSignal;

		[Inject]
		public var setupTab:SetupTabSignal;


		override public function initialize():void {

			trace('floor tab mediator initialize');
			logger.info( "initialized" );

			addSignalPasser(view.mouseModeChange, changeMouseMode);
			addToSignal(setupTab, view.setupView);

			// From app.

		}
	}
}
