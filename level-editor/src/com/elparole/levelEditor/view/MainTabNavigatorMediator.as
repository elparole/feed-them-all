package com.elparole.levelEditor.view
{
import com.blackmoondev.utils.ActivityPasserMediator;
import com.elparole.levelEditor.model.vo.FloorItemVO;
import com.elparole.levelEditor.signals.SetPageSignal;
import com.elparole.levelEditor.signals.notifications.NotifyImageSelectedSignal;
import com.elparole.levelEditor.signals.StartupSignal;
import com.elparole.levelEditor.view.MainTabNavigator;

import flash.ui.Mouse;

import flash.ui.MouseCursor;

import mx.managers.CursorManager;

import robotlegs.bender.bundles.mvcs.Mediator;

import robotlegs.bender.framework.api.ILogger;

	public class MainTabNavigatorMediator extends ActivityPasserMediator
	{
		[Inject]
		public var logger:ILogger;

		[Inject]
		public var view:MainTabNavigator;

		[Inject]
		public var notifyImageSelectedSignal:NotifyImageSelectedSignal;

		[Inject]
		public var startSignal:StartupSignal;

		[Inject]
		public var setPage:SetPageSignal;

		override public function initialize():void {

			logger.info( "initialized" );
			startSignal.dispatch();

			CursorManager.setBusyCursor();
			addSignalPasser(view.changeViewState, setPage);



			// From app.

		}
	}
}
