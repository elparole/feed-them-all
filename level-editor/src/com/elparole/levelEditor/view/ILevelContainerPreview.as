/**
 * User: Elparole
 * Date: 22.03.13
 * Time: 13:58
 */
package com.elparole.levelEditor.view
{
import com.elparole.levelEditor.model.vo.IsoEditItem;

import org.osflash.signals.Signal;

public interface ILevelContainerPreview
{
	function get contClicked():Signal;

	function onItemAdded(item:IsoEditItem, refresh:Boolean):void;

	function onItemRemoved(item:IsoEditItem, refresh:Boolean):void;

	function onFloorModeSelected(value:Boolean):void;

	function onItemsModeSelected(value:Boolean):void;

	function onObstacleModeSelected(value:Boolean):void;

	function onDragModeSelected(value:Boolean):void;
}
}
