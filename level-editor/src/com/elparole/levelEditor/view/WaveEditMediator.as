package com.elparole.levelEditor.view
{
import com.blackmoondev.utils.ActivityPasserMediator;
import com.elparole.levelEditor.model.vo.AssetsListVO;
import com.elparole.levelEditor.model.vo.DataRequestVO;
import com.elparole.levelEditor.model.vo.FloorItemVO;
import com.elparole.levelEditor.signals.notifications.AssetsDirChangedSignal;
import com.elparole.levelEditor.signals.notifications.ChildSelectionChangedSignal;
import com.elparole.levelEditor.signals.notifications.NotifyImageSelectedSignal;
import com.elparole.levelEditor.signals.notifications.SetupTabSignal;
import com.elparole.levelEditor.signals.notifications.SourceSelectChangedSignal;
import com.elparole.levelEditor.signals.notifications.TickSignal;
import com.elparole.levelEditor.signals.requests.ChangeMouseModeSignal;
import com.elparole.levelEditor.signals.requests.ChangeSnapToGridSignal;
import com.elparole.levelEditor.signals.requests.CreateMonsterInWaveSignal;
import com.elparole.levelEditor.signals.PauseSignal;
import com.elparole.levelEditor.signals.PlaySignal;
import com.elparole.levelEditor.signals.requests.RemoveChildSignal;
import com.elparole.levelEditor.signals.requests.ResponseDataSignal;
import com.elparole.levelEditor.signals.requests.SelectItemSignal;
import com.elparole.levelEditor.signals.requests.SetPositionsInTimeSignal;
import com.elparole.levelEditor.view.MainTabNavigator;

import flash.events.MouseEvent;

import org.osflash.signals.Signal;

import org.swiftsuspenders.mapping.MappingEvent;

import robotlegs.bender.bundles.mvcs.Mediator;

import robotlegs.bender.framework.api.ILogger;

	public class WaveEditMediator extends ActivityPasserMediator
	{
		[Inject]
		public var logger:ILogger;

		[Inject]
		public var view:WaveEditView;

		[Inject]
		public var assetsDirChanged:AssetsDirChangedSignal;

		[Inject]
		public var requestData:ResponseDataSignal;

		[Inject]
		public var selectItem:SelectItemSignal;

		[Inject]
		public var changeMouseMode:ChangeMouseModeSignal;

		[Inject]
		public var changeSnapToGrid:ChangeSnapToGridSignal;

		[Inject]
		public var setupTab:SetupTabSignal;

		[Inject]
		public var selectedSourceChanged:SourceSelectChangedSignal;

		[Inject]
		public var setPositionsInTime:SetPositionsInTimeSignal;

		[Inject]
		public var createMonster:CreateMonsterInWaveSignal;

		[Inject]
		public var childSelChanged:ChildSelectionChangedSignal;

		[Inject]
		public var removeChild:RemoveChildSignal;
		
		[Inject]
		public var play:PlaySignal;

		[Inject]
		public var pause:PauseSignal;

		[Inject]
		public var tick:TickSignal;


		override public function initialize():void {

			trace('floor tab mediator initialize');
			logger.info( "initialized" );

			addSignalPasser(view.createMonster, createMonster);
			addSignalPasser(view.mouseModeChange, changeMouseMode);
			addToSignal(setupTab, view.setupView);
			addToSignal(tick, view.onTick);
			addToSignal(selectedSourceChanged, view.onSetupFromSource);
			addToSignal(childSelChanged, view.onSetupFromChild);
			addEventSignalPasser(view.playBt,MouseEvent.CLICK,play);
			addEventSignalPasser(view.pauseBt,MouseEvent.CLICK,pause);
			addEventSignalPasser(view.removeCreatureBt,MouseEvent.CLICK,removeChild);

			addSignalPasser(view.setPositionsInTime, setPositionsInTime);
			view.setupView();

			// From app.

		}
	}
}
