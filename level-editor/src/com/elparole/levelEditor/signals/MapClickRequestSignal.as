/**
 * User: Elparole
 * Date: 05.03.13
 * Time: 20:07
 */
package com.elparole.levelEditor.signals
{
import com.elparole.levelEditor.model.vo.MouseRequestVO;

import org.osflash.signals.Signal;

public class MapClickRequestSignal extends Signal
{
	public function MapClickRequestSignal() {
		super(MouseRequestVO)
	}
}
}
