package com.elparole.levelEditor.signals.requests
{

	import org.osflash.signals.Signal;

	public class SetPositionsInTimeSignal extends Signal
	{
		public function SetPositionsInTimeSignal() {

			super(int);

		}
	}
}
