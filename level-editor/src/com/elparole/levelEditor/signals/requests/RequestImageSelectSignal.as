package com.elparole.levelEditor.signals.requests
{

import com.elparole.levelEditor.model.vo.FloorItemVO;


	import org.osflash.signals.Signal;

	public class RequestImageSelectSignal extends Signal
	{
		public function RequestImageSelectSignal() {
			super( FloorItemVO );
		}
	}
}
