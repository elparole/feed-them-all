package com.elparole.levelEditor.signals.requests
{

	import org.osflash.signals.Signal;

	public class ChangeMouseModeSignal extends Signal
	{
		public function ChangeMouseModeSignal() {

			super(String, Boolean);

		}
	}
}
