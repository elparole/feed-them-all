package com.elparole.levelEditor.signals.requests
{

import flash.geom.Point;

import org.osflash.signals.Signal;

	public class CreateNewLevelSignal extends Signal
	{
		public function CreateNewLevelSignal() {

			super(String, Point);

		}
	}
}
