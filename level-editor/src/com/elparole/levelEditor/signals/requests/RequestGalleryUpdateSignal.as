package com.elparole.levelEditor.signals.requests
{

import com.elparole.levelEditor.model.vo.FloorItemVO;


	import org.osflash.signals.Signal;

	public class RequestGalleryUpdateSignal extends Signal
	{
		public function RequestGalleryUpdateSignal() {

			super( Vector.<FloorItemVO> );

		}
	}
}
