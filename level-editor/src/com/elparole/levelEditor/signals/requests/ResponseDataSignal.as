/**
 * User: Elparole
 * Date: 04.03.13
 * Time: 17:52
 */
package com.elparole.levelEditor.signals.requests
{
import com.elparole.levelEditor.model.vo.DataRequestVO;

import org.osflash.signals.Signal;

public class ResponseDataSignal extends Signal
{
	public function ResponseDataSignal() {
		super(DataRequestVO)
	}
}
}
