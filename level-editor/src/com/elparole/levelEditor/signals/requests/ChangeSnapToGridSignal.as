package com.elparole.levelEditor.signals.requests
{

	import org.osflash.signals.Signal;

	public class ChangeSnapToGridSignal extends Signal
	{
		public function ChangeSnapToGridSignal() {

			super(Boolean);

		}
	}
}
