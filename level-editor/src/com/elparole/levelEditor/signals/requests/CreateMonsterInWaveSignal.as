package com.elparole.levelEditor.signals.requests
{

import com.elparole.feedThemAll.test.IsoAnimItem;

import org.osflash.signals.Signal;

	public class CreateMonsterInWaveSignal extends Signal
	{
		public function CreateMonsterInWaveSignal() {

			super(int,String, Boolean);

		}
	}
}
