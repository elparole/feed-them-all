/**
 * User: Elparole
 * Date: 04.03.13
 * Time: 17:52
 */
package com.elparole.levelEditor.signals.requests
{
import com.elparole.feedThemAll.model.ItemVO;
import com.elparole.levelEditor.model.vo.DataRequestVO;
import com.elparole.levelEditor.model.vo.FloorItemVO;

import org.osflash.signals.Signal;

public class SelectItemSignal extends Signal
{
	public function SelectItemSignal() {
		super(ItemVO)
	}
}
}
