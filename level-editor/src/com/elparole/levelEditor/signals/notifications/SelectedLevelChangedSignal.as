/**
 * User: Elparole
 * Date: 27.02.13
 * Time: 13:11
 */
package com.elparole.levelEditor.signals.notifications
{
import mx.collections.ArrayCollection;

import org.osflash.signals.Signal;

public class SelectedLevelChangedSignal extends Signal
{
	public function SelectedLevelChangedSignal() {
		super(String);
	}
}
}
