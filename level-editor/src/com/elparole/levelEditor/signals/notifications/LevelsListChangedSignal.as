/**
 * User: Elparole
 * Date: 27.02.13
 * Time: 13:11
 */
package com.elparole.levelEditor.signals.notifications
{
import com.elparole.levelEditor.model.vo.LevelsConfigVO;

import mx.collections.ArrayCollection;

import org.osflash.signals.Signal;

public class LevelsListChangedSignal extends Signal
{
	public function LevelsListChangedSignal() {
		super(LevelsConfigVO);
	}
}
}
