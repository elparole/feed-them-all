/**
 * User: Elparole
 * Date: 27.02.13
 * Time: 13:11
 */
package com.elparole.levelEditor.signals.notifications
{
import com.elparole.levelEditor.model.vo.AssetsListVO;

import mx.collections.ArrayCollection;

import org.osflash.signals.Signal;

public class ItemsModeSelectedSignal extends Signal
{
	public function ItemsModeSelectedSignal() {
		super(Boolean);
	}
}
}
