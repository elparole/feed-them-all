/**
 * User: Elparole
 * Date: 27.02.13
 * Time: 13:11
 */
package com.elparole.levelEditor.signals.notifications
{
import com.elparole.levelEditor.model.vo.AssetsListVO;
import com.elparole.levelEditor.model.vo.IsoEditItem;

import mx.collections.ArrayCollection;

import org.osflash.signals.Signal;

public class ItemAddedSignal extends Signal
{
	public function ItemAddedSignal() {
		super(IsoEditItem,Boolean);
	}
}
}
