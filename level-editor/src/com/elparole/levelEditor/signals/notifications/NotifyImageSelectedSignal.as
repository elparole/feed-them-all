package com.elparole.levelEditor.signals.notifications
{

	import org.osflash.signals.Signal;

	public class NotifyImageSelectedSignal extends Signal
	{
		public function NotifyImageSelectedSignal() {
			super();
		}
	}
}
