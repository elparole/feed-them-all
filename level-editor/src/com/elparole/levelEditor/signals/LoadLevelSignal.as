package com.elparole.levelEditor.signals
{

import com.elparole.levelEditor.model.vo.LevelVO;

import org.osflash.signals.Signal;

	public class LoadLevelSignal extends Signal
	{
		public function LoadLevelSignal() {

			super(LevelVO);

		}
	}
}
