/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 16:00
 */
package com.elparole.levelEditor.signals
{
import org.osflash.signals.Signal;

public class SetPageSignal extends Signal
{
	public function SetPageSignal() {
		super(Class);
	}
}
}
