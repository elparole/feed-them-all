/**
 * User: Elparole
 * Date: 25.02.13
 * Time: 16:00
 */
package com.elparole.levelEditor.signals
{
import org.osflash.signals.Signal;

public class EnsureProjFileSignal extends Signal
{
	public function EnsureProjFileSignal() {
		super( );
	}
}
}
